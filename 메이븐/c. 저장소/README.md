#

## 중앙 저장소(Central Repository)

* 공식 저장소
  - https://repo1.maven.org/maven2/
  - https://repo2.maven.org/maven2/

* 검색 엔진
  - https://search.maven.org/
  - https://mvnrepository.com/

## 로컬 저장소(Local Repository)

%USER_HOME%/.m2 디렉토리라고 보면됨.

## 원격 저장소(Remote Repository)

* 저장소 추가

pom.xml에 아래와 같은 형식으로 추가

```
<repositories>
  <repository>
    <id>저장소 ID</id>
    <name>이름</name>
    <url>주소</url>
  </repository>
</repositories>
```

* 예제

pom.xml 에 추가
```
...
    <dependency>
      <groupId>org.aspectj</groupId>
      <artifactId>aspectjrt</artifactId>
      <version>1.6.10.RELEASE</version>
    </dependency>
...
``` 
