#

## 프로젝트 생성
```
$ mvn archetype:generate \
-DarchetypeGroupId=org.codehaus.mojo.archetypes \
-DarchetypeArtifactId=javafx
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] >>> maven-archetype-plugin:3.2.1:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO] 
[INFO] <<< maven-archetype-plugin:3.2.1:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO] 
[INFO] 
[INFO] --- maven-archetype-plugin:3.2.1:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] Archetype [org.codehaus.mojo.archetypes:javafx:0.6] found in catalog remote
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/mojo/archetypes/javafx/0.6/javafx-0.6.pom
.
Define value for property 'groupId': com.tuyano <엔터>
Define value for property 'artifactId': mvn-fx-app <엔터>
Define value for property 'version' 1.0-SNAPSHOT: : <엔터>
Define value for property 'package' com.tuyano: : com.tuyano.fx <엔터>
Confirm properties configuration:
groupId: com.tuyano
artifactId: mvn-fx-app
version: 1.0-SNAPSHOT
package: com.tuyano.fx
 Y: : <엔터>
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: javafx:0.6
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-fx-app
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: package, Value: com.tuyano.fx
[INFO] Parameter: packageInPathFormat, Value: com/tuyano/fx
[INFO] Parameter: package, Value: com.tuyano.fx
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-fx-app
[INFO] Project created from Archetype in dir: /home/hadoop/work/maven_test/mvn-fx-app
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  42.506 s
[INFO] Finished at: 2022-01-07T06:20:50Z
[INFO] ------------------------------------------------------------------------
```