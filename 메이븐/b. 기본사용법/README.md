# 메이븐 사용법(in command line)

## 프로젝트 생성
```
$ cd ~/work
$ mkdir maven_test
$ cd maven_test
$ mvn archetype:generate
```
```
...
1850: remote -> org.apache.maven.archetypes:maven-archetype-quickstart (An archetype which contains a sample Maven project.)
...
Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): 1850: <엔터>
Choose org.apache.maven.archetypes:maven-archetype-quickstart version: 
1: 1.0-alpha-1
2: 1.0-alpha-2
3: 1.0-alpha-3
4: 1.0-alpha-4
5: 1.0
6: 1.1
7: 1.3
8: 1.4
Choose a number: 8: <엔터>
...
Define value for property 'groupId': com.tuyano <엔터>
Define value for property 'artifactId': mvn-app <엔터>
Define value for property 'version' 1.0-SNAPSHOT: : <엔터>
Define value for property 'package' com.tuyano: : <엔터>
Confirm properties configuration:
groupId: com.tuyano
artifactId: mvn-app
version: 1.0-SNAPSHOT
package: com.tuyano
 Y: : <엔터>
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: maven-archetype-quickstart:1.4
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-app
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: package, Value: com.tuyano
[INFO] Parameter: packageInPathFormat, Value: com/tuyano
[INFO] Parameter: package, Value: com.tuyano
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-app
[INFO] Project created from Archetype in dir: /home/hadoop/work/maven_test/mvn-app
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  06:23 min
[INFO] Finished at: 2022-01-06T00:52:11Z
[INFO] ------------------------------------------------------------------------
```


## 컴파일
* compile
```
$ mvn compile
```
```
loading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-resources-plugin/3.0.2/maven-resources-plugin-3.0.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-resources-plugin/3.0.2/maven-resources-plugin-3.0.2.pom (7.1 kB at 4.1 kB/s)
...
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/2.0.4/plexus-utils-2.0.4.pom
...
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-component-annotations/1.7.1/plexus-component-annotations-1.7.1.pom
...
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/maven_test/mvn-app/target/classes
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  35.105 s
[INFO] Finished at: 2022-01-06T04:05:58Z
[INFO] ------------------------------------------------------------------------
```

* compile 결과 확인
```
$ ls -l target/
```
```
total 12
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:05 classes
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:05 generated-sources
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:05 maven-status
```


## test
* test compile
```
$ mvn test-compile
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-app >-------------------------
[INFO] Building mvn-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/junit/junit/4.11/junit-4.11.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar (45 kB at 27 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/junit/junit/4.11/junit-4.11.jar (245 kB at 124 kB/s)
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-app ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-app ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/maven_test/mvn-app/target/test-classes
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  4.092 s
[INFO] Finished at: 2022-01-06T04:09:16Z
[INFO] ------------------------------------------------------------------------
```

```
$ ls -al target/
```
```
total 20
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:05 classes
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:05 generated-sources
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:09 generated-test-sources
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:05 maven-status
drwxrwxr-x 3 hadoop hadoop 4096 Jan  6 04:09 test-classes
```
> NOTE. generated-test-sources/ test-classes/ 가 생성.

* test
```
$ mvn test
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-app >-------------------------
[INFO] Building mvn-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-surefire-plugin/2.22.1/maven-surefire-plugin-2.22.1.pom
...
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-app ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-app ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ mvn-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/maven-surefire-common/2.22.1/maven-surefire-common-2.22.1.pom
...
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running com.tuyano.AppTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.061 s - in com.tuyano.AppTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  15.994 s
[INFO] Finished at: 2022-01-06T04:12:10Z
[INFO] ------------------------------------------------------------------------
```


## 패키지
```
$ mvn package
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-app >-------------------------
[INFO] Building mvn-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-jar-plugin/3.0.2/maven-jar-plugin-3.0.2.pom
...
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-app ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-app ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ mvn-app ---
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running com.tuyano.AppTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.013 s - in com.tuyano.AppTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] 
[INFO] --- maven-jar-plugin:3.0.2:jar (default-jar) @ mvn-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-archiver/3.1.1/maven-archiver-3.1.1.pom
...
[INFO] Building jar: /home/hadoop/work/maven_test/mvn-app/target/mvn-app-1.0-SNAPSHOT.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  9.849 s
[INFO] Finished at: 2022-01-06T04:18:46Z
[INFO] ------------------------------------------------------------------------
```


## 클린
```
$ mvn clean
```


## 실행

* pom.xml 편집
```
$ vi pom.xml
```
```
...
        <!-- exec:java -->
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>exec-maven-plugin</artifactId>
          <version>3.0.0</version>
          <configuration>
            <mainClass>com.tuyano.App</mainClass>
          </configuration>
        </plugin>
...
```
> NOTE. exec-maven-plugin 플러그인 추가

> ref) https://www.mojohaus.org/exec-maven-plugin/

* mvn exec:java
```
$ mvn exec:java
```
```
[INFO] Scanning for projects...
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-clean-plugin/3.1.0/maven-clean-plugin-3.1.0.pom
...
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-app >-------------------------
[INFO] Building mvn-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- exec-maven-plugin:3.0.0:java (default-cli) @ mvn-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-component-annotations/1.5.5/plexus-component-annotations-1.5.5.pom
...
Hello World!
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  14.683 s
[INFO] Finished at: 2022-01-06T04:29:45Z
[INFO] ------------------------------------------------------------------------
```


## 메이븐의 goal

1. 기본
- maven-compiler-plugin : compile
- maven-jar-plugin      : package
- maven-surefire-plugin : test

2. 추가 (feat. exec-maven-plugin)
- exec:java => 플러그인:골
> NOTE. 플러그인 하나에 여러개의 골을 가질수 있기 때문에 위와 같은 형식을 사용.


## 실행 가능한 jar 만들기
* 실행 실패
```
$ java -jar target/mvn-app-1.0-SNAPSHOT.jar 
```
```
no main manifest attribute, in target/mvn-app-1.0-SNAPSHOT.jar
```

* pom.xml 편집
```
$ vi pom.xml
```
```
...
        <!-- make runnable jar -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-jar-plugin</artifactId>
          <version>3.2.0</version>
          <configuration>
            <archive>
              <manifest>
                <addClasspath>true</addClasspath>
                <mainClass>com.tuyano.App</mainClass>
              </manifest>
            </archive>
          </configuration>
        </plugin>
...
```

* 패키지
```
$ mvn package
```
```
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for com.tuyano:mvn-app:jar:1.0-SNAPSHOT
[WARNING] 'build.pluginManagement.plugins.plugin.(groupId:artifactId)' must be unique but found duplicate declaration of plugin org.apache.maven.plugins:maven-jar-plugin @ line 82, column 17
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-app >-------------------------
[INFO] Building mvn-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-jar-plugin/3.2.0/maven-jar-plugin-3.2.0.pom
...
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-app ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-app ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ mvn-app ---
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running com.tuyano.AppTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.024 s - in com.tuyano.AppTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] 
[INFO] --- maven-jar-plugin:3.2.0:jar (default-jar) @ mvn-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/3.0.0/file-management-3.0.0.pom
...
[INFO] Building jar: /home/hadoop/work/maven_test/mvn-app/target/mvn-app-1.0-SNAPSHOT.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.050 s
[INFO] Finished at: 2022-01-06T05:57:51Z
[INFO] ------------------------------------------------------------------------
```

* 실행
```
$ java -jar target/mvn-app-1.0-SNAPSHOT.jar 
```
```
Hello World!
```