# 

## 설치

```
# chown hadoop:hadoop /opt/
$ cd ~/work/download/
$ wget https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.tar.gz
$ cd /opt/
$ tar zxvf ~/work/download/apache-maven-3.8.4-bin.tar.gz
$ ln -s apache-maven-3.8.4 maven
```

* 환경변수 추가
```
vi ~/.bash_profile
```
```
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

export MAVEN_HOME=/opt/maven
export M3_HOME=$MAVEN_HOME
export PATH=$PATH:/opt/maven/bin
```
> NOTE. .bashrc에 hadoop설정에 관련된 것을 설정하였기 때문에 .bash_profile을 생성하는 경우 주의가 필요

> NOTE. .bash_profile이 있는 경우에는 .profile을 login시 읽지 않음.

* 버전 확인
```
$ mvn -v
```
```
Apache Maven 3.8.4 (9b656c72d54e5bacbed989b64718c159fe39b537)
Maven home: /opt/maven
Java version: 1.8.0_312, vendor: Temurin, runtime: /home/hadoop/platform/sw/jdk8u312-b07/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "5.4.0-91-generic", arch: "amd64", family: "unix"
hadoop@hadoop:~$ 
```