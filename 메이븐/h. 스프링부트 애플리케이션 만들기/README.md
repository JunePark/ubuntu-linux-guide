#

##
* 프로젝트 생성
```
$ cd ~/work/maven_test
$ mvn archetype:generate \
-DarchetypeGroupId=org.springframework.boot \
-DarchetypeArtifactId=spring-boot-sample-jetty-archetype
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] >>> maven-archetype-plugin:3.2.1:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO] 
[INFO] <<< maven-archetype-plugin:3.2.1:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO] 
[INFO] 
[INFO] --- maven-archetype-plugin:3.2.1:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] Archetype [org.springframework.boot:spring-boot-sample-jetty-archetype:1.0.2.RELEASE] found in catalog remote
Downloading from central: https://repo.maven.apache.org/maven2/org/springframework/boot/spring-boot-sample-jetty-archetype/1.0.2.RELEASE/spring-boot-sample-jetty-archetype-1.0.2.RELEASE.pom
...
Define value for property 'groupId': com.tuyano <엔터>
Define value for property 'artifactId': mvn-spring-app <엔터>
Define value for property 'version' 1.0-SNAPSHOT: : <엔터>
Define value for property 'package' com.tuyano: : com.tuyano.spring <엔터>
Confirm properties configuration:
groupId: com.tuyano
artifactId: mvn-spring-app
version: 1.0-SNAPSHOT
package: com.tuyano.spring
 Y: : <엔터>
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: spring-boot-sample-jetty-archetype:1.0.2.RELEASE
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-spring-app
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: package, Value: com.tuyano.spring
[INFO] Parameter: packageInPathFormat, Value: com/tuyano/spring
[INFO] Parameter: package, Value: com.tuyano.spring
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-spring-app
[INFO] Project created from Archetype in dir: /home/hadoop/work/maven_test/mvn-spring-app
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  38.900 s
[INFO] Finished at: 2022-01-07T11:03:35Z
[INFO] ------------------------------------------------------------------------
```

* 패키지
```
$ cd mvn-spring-app/
$ mvn package
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] ---------------------< com.tuyano:mvn-spring-app >----------------------
[INFO] Building Spring Boot Jetty Sample 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/springframework/boot/spring-boot-maven-plugin/1.0.2.RELEASE/spring-boot-maven-plugin-1.0.2.RELEASE.pom
...
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ mvn-spring-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-spring-app/src/main/resources
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-spring-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ mvn-spring-app ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /home/hadoop/work/maven_test/mvn-spring-app/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ mvn-spring-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-spring-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ mvn-spring-app ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/maven_test/mvn-spring-app/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.15:test (default-test) @ mvn-spring-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/maven-surefire-common/2.15/maven-surefire-common-2.15.pom
...
[INFO] Surefire report directory: /home/hadoop/work/maven_test/mvn-spring-app/target/surefire-reports
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit4/2.15/surefire-junit4-2.15.pom
...
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.tuyano.spring.jetty.SampleJettyApplicationTests
11:05:30.979 [main] DEBUG o.s.t.c.j.SpringJUnit4ClassRunner - SpringJUnit4ClassRunner constructor called with [class com.tuyano.spring.jetty.SampleJettyApplicationTests].
11:05:31.059 [main] DEBUG o.s.test.context.ContextLoaderUtils - Found explicit ContextLoader class [org.springframework.boot.test.SpringApplicationContextLoader] for context configuration attributes [ContextConfigurationAttributes@5f375618 declaringClass = 'com.tuyano.spring.jetty.SampleJettyApplicationTests', locations = '{}', classes = '{class com.tuyano.spring.jetty.SampleJettyApplication}', inheritLocations = true, initializers = '{}', inheritInitializers = true, name = [null], contextLoaderClass = 'org.springframework.boot.test.SpringApplicationContextLoader']
11:05:31.072 [main] DEBUG o.s.test.context.ContextLoaderUtils - Could not find an 'annotation declaring class' for annotation type [org.springframework.test.context.ActiveProfiles] and class [com.tuyano.spring.jetty.SampleJettyApplicationTests]
11:05:31.082 [main] INFO  o.s.test.context.TestContextManager - Could not instantiate TestExecutionListener class [org.springframework.test.context.transaction.TransactionalTestExecutionListener]. Specify custom listener classes or make the default listener classes (and their dependencies) available.
11:05:31.088 [main] DEBUG o.s.t.annotation.ProfileValueUtils - Retrieved @ProfileValueSourceConfiguration [null] for test class [com.tuyano.spring.jetty.SampleJettyApplicationTests]
11:05:31.090 [main] DEBUG o.s.t.annotation.ProfileValueUtils - Retrieved ProfileValueSource type [class org.springframework.test.annotation.SystemProfileValueSource] for class [com.tuyano.spring.jetty.SampleJettyApplicationTests]
11:05:31.093 [main] DEBUG o.s.t.annotation.ProfileValueUtils - Retrieved @ProfileValueSourceConfiguration [null] for test class [com.tuyano.spring.jetty.SampleJettyApplicationTests]
11:05:31.094 [main] DEBUG o.s.t.annotation.ProfileValueUtils - Retrieved ProfileValueSource type [class org.springframework.test.annotation.SystemProfileValueSource] for class [com.tuyano.spring.jetty.SampleJettyApplicationTests]
11:05:31.102 [main] DEBUG o.s.t.annotation.ProfileValueUtils - Retrieved @ProfileValueSourceConfiguration [null] for test class [com.tuyano.spring.jetty.SampleJettyApplicationTests]
11:05:31.104 [main] DEBUG o.s.t.annotation.ProfileValueUtils - Retrieved ProfileValueSource type [class org.springframework.test.annotation.SystemProfileValueSource] for class [com.tuyano.spring.jetty.SampleJettyApplicationTests]
11:05:31.117 [main] DEBUG o.s.t.c.s.DependencyInjectionTestExecutionListener - Performing dependency injection for test context [[DefaultTestContext@3532ec19 testClass = SampleJettyApplicationTests, testInstance = com.tuyano.spring.jetty.SampleJettyApplicationTests@68c4039c, testMethod = [null], testException = [null], mergedContextConfiguration = [WebMergedContextConfiguration@ae45eb6 testClass = SampleJettyApplicationTests, locations = '{}', classes = '{class com.tuyano.spring.jetty.SampleJettyApplication}', contextInitializerClasses = '[]', activeProfiles = '{}', resourceBasePath = 'src/main/webapp', contextLoader = 'org.springframework.boot.test.SpringApplicationContextLoader', parent = [null]]]].

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.0.2.RELEASE)

2022-01-07 11:05:32.142  INFO 470022 --- [           main] o.a.maven.surefire.booter.ForkedBooter   : Starting ForkedBooter v2.15 on hadoop with PID 470022 (/home/hadoop/.m2/repository/org/apache/maven/surefire/surefire-booter/2.15/surefire-booter-2.15.jar started by hadoop in /home/hadoop/work/maven_test/mvn-spring-app)
2022-01-07 11:05:32.190  INFO 470022 --- [           main] ationConfigEmbeddedWebApplicationContext : Refreshing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@71b1176b: startup date [Fri Jan 07 11:05:32 UTC 2022]; root of context hierarchy
2022-01-07 11:05:34.559  INFO 470022 --- [           main] e.j.JettyEmbeddedServletContainerFactory : Server initialized with port: 0
2022-01-07 11:05:34.593  INFO 470022 --- [           main] org.eclipse.jetty.server.Server          : jetty-8.1.14.v20131031
2022-01-07 11:05:34.813  INFO 470022 --- [           main] /                                        : Initializing Spring embedded WebApplicationContext
2022-01-07 11:05:34.814  INFO 470022 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 2628 ms
2022-01-07 11:05:35.353  INFO 470022 --- [           main] o.s.b.c.e.ServletRegistrationBean        : Mapping servlet: 'dispatcherServlet' to [/]
2022-01-07 11:05:35.358  INFO 470022 --- [           main] o.s.b.c.embedded.FilterRegistrationBean  : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
2022-01-07 11:05:35.569  INFO 470022 --- [           main] o.e.jetty.server.AbstractConnector       : Started SelectChannelConnector@0.0.0.0:36095
2022-01-07 11:05:35.705  INFO 470022 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2022-01-07 11:05:36.281  INFO 470022 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/],methods=[],params=[],headers=[],consumes=[],produces=[],custom=[]}" onto public java.lang.String com.tuyano.spring.jetty.web.SampleController.helloWorld()
2022-01-07 11:05:36.317  INFO 470022 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2022-01-07 11:05:36.321  INFO 470022 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2022-01-07 11:05:36.708  INFO 470022 --- [           main] /                                        : Initializing Spring FrameworkServlet 'dispatcherServlet'
2022-01-07 11:05:36.708  INFO 470022 --- [           main] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization started
2022-01-07 11:05:36.726  INFO 470022 --- [           main] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization completed in 17 ms
2022-01-07 11:05:36.781  INFO 470022 --- [           main] o.e.jetty.server.AbstractConnector       : Started SelectChannelConnector@0.0.0.0:41013
2022-01-07 11:05:36.788  INFO 470022 --- [           main] .s.b.c.e.j.JettyEmbeddedServletContainer : Jetty started on port: 41013
2022-01-07 11:05:36.794  INFO 470022 --- [           main] o.a.maven.surefire.booter.ForkedBooter   : Started ForkedBooter in 5.301 seconds (JVM running for 7.697)
2022-01-07 11:05:37.065  INFO 470022 --- [           main] ationConfigEmbeddedWebApplicationContext : Closing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@71b1176b: startup date [Fri Jan 07 11:05:32 UTC 2022]; root of context hierarchy
2022-01-07 11:05:37.084  INFO 470022 --- [           main] /                                        : Destroying Spring FrameworkServlet 'dispatcherServlet'
2022-01-07 11:05:37.101  INFO 470022 --- [           main] o.e.jetty.server.handler.ContextHandler  : stopped o.s.b.c.e.j.JettyEmbeddedWebAppContext{/,null}
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 6.615 sec - in com.tuyano.spring.jetty.SampleJettyApplicationTests

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ mvn-spring-app ---
Downloading from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.1/commons-lang-2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.1/commons-lang-2.1.pom (9.9 kB at 30 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.1/commons-lang-2.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.1/commons-lang-2.1.jar (208 kB at 602 kB/s)
[INFO] Building jar: /home/hadoop/work/maven_test/mvn-spring-app/target/mvn-spring-app-1.0-SNAPSHOT.jar
[INFO] 
[INFO] --- spring-boot-maven-plugin:1.0.2.RELEASE:repackage (default) @ mvn-spring-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/springframework/boot/spring-boot-loader-tools/1.0.2.RELEASE/spring-boot-loader-tools-1.0.2.RELEASE.pom
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  01:10 min
[INFO] Finished at: 2022-01-07T11:06:05Z
[INFO] ------------------------------------------------------------------------
```

* 실행
```
$ java -jar target/mvn-spring-app-1.0-SNAPSHOT.jar
```
```

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.0.2.RELEASE)

2022-01-07 11:08:33.648  INFO 470154 --- [           main] c.t.spring.jetty.SampleJettyApplication  : Starting SampleJettyApplication on hadoop with PID 470154 (/home/hadoop/work/maven_test/mvn-spring-app/target/mvn-spring-app-1.0-SNAPSHOT.jar started by hadoop in /home/hadoop/work/maven_test/mvn-spring-app)
2022-01-07 11:08:33.713  INFO 470154 --- [           main] ationConfigEmbeddedWebApplicationContext : Refreshing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@65cd4f83: startup date [Fri Jan 07 11:08:33 UTC 2022]; root of context hierarchy
2022-01-07 11:08:34.922  INFO 470154 --- [           main] e.j.JettyEmbeddedServletContainerFactory : Server initialized with port: 8080
2022-01-07 11:08:34.927  INFO 470154 --- [           main] org.eclipse.jetty.server.Server          : jetty-8.1.14.v20131031
2022-01-07 11:08:35.021  INFO 470154 --- [           main] /                                        : Initializing Spring embedded WebApplicationContext
2022-01-07 11:08:35.021  INFO 470154 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1311 ms
2022-01-07 11:08:35.509  INFO 470154 --- [           main] o.s.b.c.e.ServletRegistrationBean        : Mapping servlet: 'dispatcherServlet' to [/]
2022-01-07 11:08:35.516  INFO 470154 --- [           main] o.s.b.c.embedded.FilterRegistrationBean  : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
2022-01-07 11:08:35.672  INFO 470154 --- [           main] o.e.jetty.server.AbstractConnector       : Started SelectChannelConnector@0.0.0.0:8080
2022-01-07 11:08:35.807  INFO 470154 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2022-01-07 11:08:36.027  INFO 470154 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/],methods=[],params=[],headers=[],consumes=[],produces=[],custom=[]}" onto public java.lang.String com.tuyano.spring.jetty.web.SampleController.helloWorld()
2022-01-07 11:08:36.061  INFO 470154 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2022-01-07 11:08:36.061  INFO 470154 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2022-01-07 11:08:36.588  INFO 470154 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2022-01-07 11:08:36.619  INFO 470154 --- [           main] /                                        : Initializing Spring FrameworkServlet 'dispatcherServlet'
2022-01-07 11:08:36.619  INFO 470154 --- [           main] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization started
2022-01-07 11:08:36.657  INFO 470154 --- [           main] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization completed in 35 ms
2022-01-07 11:08:36.700  INFO 470154 --- [           main] o.e.jetty.server.AbstractConnector       : Started SelectChannelConnector@0.0.0.0:8080
2022-01-07 11:08:36.705  INFO 470154 --- [           main] .s.b.c.e.j.JettyEmbeddedServletContainer : Jetty started on port: 8080
2022-01-07 11:08:36.709  INFO 470154 --- [           main] c.t.spring.jetty.SampleJettyApplication  : Started SampleJettyApplication in 3.672 seconds (JVM running for 4.384)
^C2022-01-07 11:13:19.084  INFO 470154 --- [       Thread-2] ationConfigEmbeddedWebApplicationContext : Closing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@65cd4f83: startup date [Fri Jan 07 11:08:33 UTC 2022]; root of context hierarchy
2022-01-07 11:13:19.086  INFO 470154 --- [       Thread-2] o.s.j.e.a.AnnotationMBeanExporter        : Unregistering JMX-exposed beans on shutdown
2022-01-07 11:13:19.101  INFO 470154 --- [       Thread-2] /                                        : Destroying Spring FrameworkServlet 'dispatcherServlet'
2022-01-07 11:13:19.105  INFO 470154 --- [       Thread-2] o.e.jetty.server.handler.ContextHandler  : stopped o.s.b.c.e.j.JettyEmbeddedWebAppContext{/,null}
```

* 웹접속
```
http://localhost:8080/

or

http://192.168.126.81:8080/
```
