#

##

* 프로젝트 생성
```
$ cd ~/work/mavan_test/
$ mvn archetype:generate \
-DarchetypeGroupId=org.glassfish.jersey.archetypes \
-DarchetypeArtifactId=jersey-quickstart-webapp
```
```

[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] >>> maven-archetype-plugin:3.2.1:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO] 
[INFO] <<< maven-archetype-plugin:3.2.1:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO] 
[INFO] 
[INFO] --- maven-archetype-plugin:3.2.1:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] Archetype [org.glassfish.jersey.archetypes:jersey-quickstart-webapp:3.0.3] found in catalog remote
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/jersey/archetypes/jersey-quickstart-webapp/3.0.3/jersey-quickstart-webapp-3.0.3.pom
...
Define value for property 'groupId': com.tuyano <엔터>
Define value for property 'artifactId': mvn-rest-app <엔터>
Define value for property 'version' 1.0-SNAPSHOT: : <엔터>
Define value for property 'package' com.tuyano: : com.tuyano.rest <엔터>
Confirm properties configuration:
groupId: com.tuyano
artifactId: mvn-rest-app
version: 1.0-SNAPSHOT
package: com.tuyano.rest
 Y: : <엔터>
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Old (1.x) Archetype: jersey-quickstart-webapp:3.0.3
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: basedir, Value: /home/hadoop/work/maven_test
[INFO] Parameter: package, Value: com.tuyano.rest
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-rest-app
[INFO] Parameter: packageName, Value: com.tuyano.rest
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] project created from Old (1.x) Archetype in dir: /home/hadoop/work/maven_test/mvn-rest-app
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  38.131 s
[INFO] Finished at: 2022-01-07T10:36:45Z
[INFO] ------------------------------------------------------------------------
```

* 패키지
```
$ cd mvn-rest-app/
$ mvn package
```
```
[INFO] Scanning for projects...
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/jersey/jersey-bom/3.0.3/jersey-bom-3.0.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/jersey/jersey-bom/3.0.3/jersey-bom-3.0.3.pom (19 kB at 13 kB/s)
[INFO] 
[INFO] ----------------------< com.tuyano:mvn-rest-app >-----------------------
[INFO] Building mvn-rest-app 1.0-SNAPSHOT
[INFO] --------------------------------[ war ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-compiler-plugin/3.8.1/maven-compiler-plugin-3.8.1.pom
...
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ mvn-rest-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 0 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ mvn-rest-app ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/maven_test/mvn-rest-app/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ mvn-rest-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-rest-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ mvn-rest-app ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ mvn-rest-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-booter/2.12.4/surefire-booter-2.12.4.pom
...
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-war-plugin:2.2:war (default-war) @ mvn-rest-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-archiver/2.5/maven-archiver-2.5.pom
...
[INFO] Packaging webapp
[INFO] Assembling webapp [mvn-rest-app] in [/home/hadoop/work/maven_test/mvn-rest-app/target/mvn-rest-app]
[INFO] Processing war project
[INFO] Copying webapp resources [/home/hadoop/work/maven_test/mvn-rest-app/src/main/webapp]
[INFO] Webapp assembled in [165 msecs]
[INFO] Building war: /home/hadoop/work/maven_test/mvn-rest-app/target/mvn-rest-app.war
[INFO] WEB-INF/web.xml already added, skipping
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  26.618 s
[INFO] Finished at: 2022-01-07T10:40:11Z
[INFO] ------------------------------------------------------------------------
```

* pom.xml 수정
```
            <plugin>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>maven-jetty-plugin</artifactId>
                <version>6.1.10</version>
                <configuration>
                    <scanIntervalSeconds>10</scanIntervalSeconds>
                    <connectors>
                        <connector implementation="org.mortbay.jetty.nio.SelectChannelConnector">
                            <port>8080</port>
                            <maxIdleTime>60000</maxIdleTime>
                        </connector>
                    </connectors>
                </configuration>
            </plugin>

<jersey.version>3.0.0-M1</jersey.version>
```
> NOTE. jersey.version 은 3.0.0-M1 까지만 가능한듯!

* 실행
```
$ mvn jetty:run
```