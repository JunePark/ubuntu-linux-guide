#

## 라이브러리 프로젝트 생성
```
$ cd ~/work/maven_test/
$ mvn archetype:generate
```
```
...
Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): 1850: <엔터>
Choose org.apache.maven.archetypes:maven-archetype-quickstart version: 
1: 1.0-alpha-1
2: 1.0-alpha-2
3: 1.0-alpha-3
4: 1.0-alpha-4
5: 1.0
6: 1.1
7: 1.3
8: 1.4
Choose a number: 8: 
Define value for property 'groupId': com.tuyano   
Define value for property 'artifactId': mvn-lib
Define value for property 'version' 1.0-SNAPSHOT: : 
Define value for property 'package' com.tuyano: : com.tuyano.lib
Confirm properties configuration:
groupId: com.tuyano
artifactId: mvn-lib
version: 1.0-SNAPSHOT
package: com.tuyano.lib
 Y: : 
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: maven-archetype-quickstart:1.4
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-lib
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: package, Value: com.tuyano.lib
[INFO] Parameter: packageInPathFormat, Value: com/tuyano/lib
[INFO] Parameter: package, Value: com.tuyano.lib
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-lib
[INFO] Project created from Archetype in dir: /home/hadoop/work/maven_test/mvn-lib
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  31.075 s
[INFO] Finished at: 2022-01-06T09:39:36Z
[INFO] ------------------------------------------------------------------------
```

* 패키지
```
$ mvn package
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-lib >-------------------------
[INFO] Building mvn-lib 1.0
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-lib ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-lib/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-lib ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 2 source files to /home/hadoop/work/maven_test/mvn-lib/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-lib ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-lib/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-lib ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/maven_test/mvn-lib/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ mvn-lib ---
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running com.tuyano.lib.AppTest
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.061 s - in com.tuyano.lib.AppTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] 
[INFO] --- maven-jar-plugin:3.0.2:jar (default-jar) @ mvn-lib ---
[INFO] Building jar: /home/hadoop/work/maven_test/mvn-lib/target/mvn-lib-1.0.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.937 s
[INFO] Finished at: 2022-01-06T09:46:24Z
[INFO] ------------------------------------------------------------------------
```

* 실행
```
$ mvn exec:java
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-lib >-------------------------
[INFO] Building mvn-lib 1.0
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- exec-maven-plugin:3.0.0:java (default-cli) @ mvn-lib ---
 NAME: Hanako
 MSG: I am flower...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.166 s
[INFO] Finished at: 2022-01-06T09:47:37Z
[INFO] ------------------------------------------------------------------------
```

* 로컬 저장소에 저장
```
$ mvn install
```
```

[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-lib >-------------------------
[INFO] Building mvn-lib 1.0
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-lib ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-lib/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-lib ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-lib ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-lib/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-lib ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ mvn-lib ---
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running com.tuyano.lib.AppTest
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.052 s - in com.tuyano.lib.AppTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] 
[INFO] --- maven-jar-plugin:3.0.2:jar (default-jar) @ mvn-lib ---
[INFO] 
[INFO] --- maven-install-plugin:2.5.2:install (default-install) @ mvn-lib ---
Downloading from central: https://repo.maven.apache.org/maven2/junit/junit/3.8.1/junit-3.8.1.pom
...
[INFO] Installing /home/hadoop/work/maven_test/mvn-lib/target/mvn-lib-1.0.jar to /home/hadoop/.m2/repository/com/tuyano/mvn-lib/1.0/mvn-lib-1.0.jar
[INFO] Installing /home/hadoop/work/maven_test/mvn-lib/pom.xml to /home/hadoop/.m2/repository/com/tuyano/mvn-lib/1.0/mvn-lib-1.0.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  6.954 s
[INFO] Finished at: 2022-01-06T09:50:52Z
[INFO] ------------------------------------------------------------------------
```

* 저장 확인
```
$ ls -l ~/.m2/repository/com/tuyano/mvn-lib/
```
```
total 8
drwxrwxr-x 2 hadoop hadoop 4096 Jan  6 09:50 1.0
-rw-rw-r-- 1 hadoop hadoop  293 Jan  6 09:50 maven-metadata-local.xml
```

```
$ ls -l ~/.m2/repository/com/tuyano/mvn-lib/1.0/
```
```
total 12
-rw-rw-r-- 1 hadoop hadoop 3415 Jan  6 09:46 mvn-lib-1.0.jar
-rw-rw-r-- 1 hadoop hadoop 2972 Jan  6 09:46 mvn-lib-1.0.pom
-rw-rw-r-- 1 hadoop hadoop  176 Jan  6 09:50 _remote.repositories
```

* 패키지
```
$ mvn package
```
```
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for com.tuyano:mvn-app:jar:1.0-SNAPSHOT
[WARNING] 'build.pluginManagement.plugins.plugin.(groupId:artifactId)' must be unique but found duplicate declaration of plugin org.apache.maven.plugins:maven-jar-plugin @ line 105, column 17
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-app >-------------------------
[INFO] Building mvn-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-app ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/maven_test/mvn-app/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-app ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/maven_test/mvn-app/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ mvn-app ---
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running com.tuyano.AppTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.251 s - in com.tuyano.AppTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] 
[INFO] --- maven-jar-plugin:3.2.0:jar (default-jar) @ mvn-app ---
[INFO] Building jar: /home/hadoop/work/maven_test/mvn-app/target/mvn-app-1.0-SNAPSHOT.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.772 s
[INFO] Finished at: 2022-01-06T10:03:17Z
[INFO] ------------------------------------------------------------------------
```

* 실행
```
$ mvn exec:java
```
```
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for com.tuyano:mvn-app:jar:1.0-SNAPSHOT
[WARNING] 'build.pluginManagement.plugins.plugin.(groupId:artifactId)' must be unique but found duplicate declaration of plugin org.apache.maven.plugins:maven-jar-plugin @ line 105, column 17
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] -------------------------< com.tuyano:mvn-app >-------------------------
[INFO] Building mvn-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- exec-maven-plugin:3.0.0:java (default-cli) @ mvn-app ---

=========================
Hello,Sachiko.
I am Happy
=========================

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.107 s
[INFO] Finished at: 2022-01-06T10:04:07Z
[INFO] ------------------------------------------------------------------------
hadoop@hadoop:~/work/maven_test/mvn-app$ 
```