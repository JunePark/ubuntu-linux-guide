#

##
```
$ mvn archetype:generate \
-DarchetypeGroupId=org.apache.maven.archetypes \
-DarchetypeArtifactId=maven-archetype-webapp
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] >>> maven-archetype-plugin:3.2.1:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO] 
[INFO] <<< maven-archetype-plugin:3.2.1:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO] 
[INFO] 
[INFO] --- maven-archetype-plugin:3.2.1:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] Archetype [org.apache.maven.archetypes:maven-archetype-webapp:1.4] found in catalog remote
Define value for property 'groupId': com.tuyano <엔터>
Define value for property 'artifactId': mvn-web-app <엔터>
Define value for property 'version' 1.0-SNAPSHOT: : <엔터>
Define value for property 'package' com.tuyano: : com.tuyano.web <엔터>
Confirm properties configuration:
groupId: com.tuyano
artifactId: mvn-web-app
version: 1.0-SNAPSHOT
package: com.tuyano.web
 Y: : <엔터>
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: maven-archetype-webapp:1.4
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-web-app
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: package, Value: com.tuyano.web
[INFO] Parameter: packageInPathFormat, Value: com/tuyano/web
[INFO] Parameter: package, Value: com.tuyano.web
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: groupId, Value: com.tuyano
[INFO] Parameter: artifactId, Value: mvn-web-app
[INFO] Project created from Archetype in dir: /home/hadoop/work/maven_test/mvn-web-app
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  34.668 s
[INFO] Finished at: 2022-01-07T07:01:41Z
[INFO] ------------------------------------------------------------------------
```

* 패키지
```
$ mvn package
```
```
[INFO] Scanning for projects...
[INFO] 
[INFO] -----------------------< com.tuyano:mvn-web-app >-----------------------
[INFO] Building mvn-web-app Maven Webapp 1.0-SNAPSHOT
[INFO] --------------------------------[ war ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-war-plugin/3.2.2/maven-war-plugin-3.2.2.pom
...
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-web-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-web-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-web-app ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-web-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-web-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-web-app ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ mvn-web-app ---
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-war-plugin:3.2.2:war (default-war) @ mvn-web-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-archiver/3.2.0/maven-archiver-3.2.0.pom
...
[INFO] Packaging webapp
[INFO] Assembling webapp [mvn-web-app] in [/home/hadoop/work/maven_test/mvn-web-app/target/mvn-web-app]
[INFO] Processing war project
[INFO] Copying webapp resources [/home/hadoop/work/maven_test/mvn-web-app/src/main/webapp]
[INFO] Webapp assembled in [54 msecs]
[INFO] Building war: /home/hadoop/work/maven_test/mvn-web-app/target/mvn-web-app.war
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  15.340 s
[INFO] Finished at: 2022-01-07T09:49:25Z
[INFO] ------------------------------------------------------------------------
```

* 실행
```
$ mvn jetty:run
```
```
[INFO] Scanning for projects...
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/maven-jetty-plugin/6.1.26/maven-jetty-plugin-6.1.26.pom
...
[INFO] 
[INFO] -----------------------< com.tuyano:mvn-web-app >-----------------------
[INFO] Building mvn-web-app Maven Webapp 1.0-SNAPSHOT
[INFO] --------------------------------[ war ]---------------------------------
[INFO] 
[INFO] >>> maven-jetty-plugin:6.1.26:run (default-cli) > test-compile @ mvn-web-app >>>
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ mvn-web-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-web-app/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ mvn-web-app ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ mvn-web-app ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/maven_test/mvn-web-app/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ mvn-web-app ---
[INFO] No sources to compile
[INFO] 
[INFO] <<< maven-jetty-plugin:6.1.26:run (default-cli) < test-compile @ mvn-web-app <<<
[INFO] 
[INFO] 
[INFO] --- maven-jetty-plugin:6.1.26:run (default-cli) @ mvn-web-app ---
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jetty/6.1.26/jetty-6.1.26.pom
...
[INFO] Configuring Jetty for project: mvn-web-app Maven Webapp
[INFO] Webapp source directory = /home/hadoop/work/maven_test/mvn-web-app/src/main/webapp
[INFO] Reload Mechanic: automatic
[INFO] Classes directory /home/hadoop/work/maven_test/mvn-web-app/target/classes does not exist
[INFO] Logging to org.slf4j.impl.MavenSimpleLogger(org.mortbay.log) via org.mortbay.log.Slf4jLog
[INFO] Context path = /mvn-web-app
[INFO] Tmp directory =  determined at runtime
[INFO] Web defaults = org/mortbay/jetty/webapp/webdefault.xml
[INFO] Web overrides =  none
[INFO] web.xml file = /home/hadoop/work/maven_test/mvn-web-app/src/main/webapp/WEB-INF/web.xml
[INFO] Webapp directory = /home/hadoop/work/maven_test/mvn-web-app/src/main/webapp
[INFO] Starting jetty 6.1.26 ...
[INFO] jetty-6.1.26
[INFO] No Transaction manager found - if your webapp requires one, please configure one.
[INFO] Started SelectChannelConnector@0.0.0.0:8080
[INFO] Started Jetty Server
[INFO] Starting scanner at interval of 10 seconds.

^C[INFO] Shutdown hook executing
[INFO] Stopped SelectChannelConnector@0.0.0.0:8080
[INFO] Shutdown hook complete
[INFO] Jetty server exiting.
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  01:50 min
[INFO] Finished at: 2022-01-07T10:05:20Z
[INFO] ------------------------------------------------------------------------
```