#

1. 여기서는 hadoop 계정으로 설정

2. 최소한의 보안만 설정


## 설치

* 설치
```
# apt install samba
```
```
...
The following additional packages will be installed:
  attr ibverbs-providers libavahi-client3 libavahi-common-data libavahi-common3 libboost-iostreams1.71.0 libboost-thread1.71.0 libcephfs2 libcups2 libibverbs1 libjansson4 libldb2
  libnl-route-3-200 librados2 librdmacm1 libtalloc2 libtevent0 libwbclient0 python3-crypto python3-dnspython python3-gpg python3-ldb python3-markdown python3-packaging
  python3-pygments python3-pyparsing python3-samba python3-talloc python3-tdb samba-common samba-common-bin samba-dsdb-modules samba-libs samba-vfs-modules tdb-tools
Suggested packages:
  cups-common python-markdown-doc python-pygments-doc ttf-bitstream-vera python-pyparsing-doc bind9 bind9utils ctdb ldb-tools ntp | chrony smbldap-tools winbind heimdal-clients
The following NEW packages will be installed:
  attr ibverbs-providers libavahi-client3 libavahi-common-data libavahi-common3 libboost-iostreams1.71.0 libboost-thread1.71.0 libcephfs2 libcups2 libibverbs1 libjansson4 libldb2
  libnl-route-3-200 librados2 librdmacm1 libtalloc2 libtevent0 libwbclient0 python3-crypto python3-dnspython python3-gpg python3-ldb python3-markdown python3-packaging
  python3-pygments python3-pyparsing python3-samba python3-talloc python3-tdb samba samba-common samba-common-bin samba-dsdb-modules samba-libs samba-vfs-modules tdb-tools
0 upgraded, 36 newly installed, 0 to remove and 49 not upgraded.
Need to get 17.3 MB of archives.
After this operation, 101 MB of additional disk space will be used.
Do you want to continue? [Y/n] 
...
```
> NOTE. sambashare 그룹이 OS에 추가됨.

* 디렉토리 및 계정 설정
```
# mkdir /share
# chgrp sambashare /share
# chmod 770 /share
# id hadoop
# usermod -G sambashare -a hadoop
# id hadoop
# smbpasswd -a hadoop
1234
```
> NOTE. smbpasswd로 설정한 계정의 암호는 OS 암호와는 다른것임.

* 설정
```
# cd /etc/samba/
# cp -p smb.conf smb.conf.orig
# vi smb.conf
```
```
...
   unix charset = UTF-8
...
[homes]
   comment = Home Directories
   browseable = no

   read only = no

   create mask = 0600
...
[Share]
   path = /share
   writable = yes
   guest ok = no
   create mode = 0777
   directory mode = 0777
   valid users = @sambashare
```

* 체크
```
# testparm
```
```
Load smb config files from /etc/samba/smb.conf
Loaded services file OK.
Weak crypto is allowed
Server role: ROLE_STANDALONE

Press enter to see a dump of your service definitions

# Global parameters
[global]
        log file = /var/log/samba/log.%m
        logging = file
        map to guest = Bad User
        max log size = 1000
        obey pam restrictions = Yes
        pam password change = Yes
        panic action = /usr/share/samba/panic-action %d
        passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
        passwd program = /usr/bin/passwd %u
        server role = standalone server
        server string = %h server (Samba, Ubuntu)
        unix password sync = Yes
        usershare allow guests = Yes
        idmap config * : backend = tdb


[homes]
        browseable = No
        comment = Home Directories
        create mask = 0600
        read only = No


[printers]
        browseable = No
        comment = All Printers
        create mask = 0700
        path = /var/spool/samba
        printable = Yes


[print$]
        comment = Printer Drivers
        path = /var/lib/samba/printers


[Share]
        create mask = 0777
        directory mask = 0777
        path = /share
        read only = No
        valid users = @sambashare
```

* 등록
```
# systemctl status smbd
# systemctl enable smbd
# systemctl status smbd
```

* 방화벽 중지
```
# ufw disable
```
> NOTE. 만약 사용중이라면 disable 처리


## Windows에서 접근

탐색기에서 \\192.168.126.81\ 선택하면 Share 폴더가 나오고 그 폴더로 진입하려고 하면 인증창이 나오는데
hadoop / 1234 로 로그인하면 된다.


## 사용 체크
```
# smbstatus
```
```

Samba version 4.13.14-Ubuntu
PID     Username     Group        Machine                                   Protocol Version  Encryption           Signing              
----------------------------------------------------------------------------------------------------------------------------------------
466065  hadoop       hadoop       192.168.126.1 (ipv4:192.168.126.1:54473)  SMB3_11           -                    partial(AES-128-CMAC)

Service      pid     Machine       Connected at                     Encryption   Signing     
---------------------------------------------------------------------------------------------
Share        466065  192.168.126.1 Fri Jan  7 09:23:44 AM 2022 UTC  -            -           

Locked files:
Pid          User(ID)   DenyMode   Access      R/W        Oplock           SharePath   Name   Time
--------------------------------------------------------------------------------------------------
466065       1000       DENY_NONE  0x100081    RDONLY     NONE             /share   .   Fri Jan  7 09:25:14 2022
466065       1000       DENY_NONE  0x100081    RDONLY     NONE             /share   .   Fri Jan  7 09:25:14 2022
466065       1000       DENY_NONE  0x100081    RDONLY     NONE             /share   .   Fri Jan  7 09:25:08 2022

```
