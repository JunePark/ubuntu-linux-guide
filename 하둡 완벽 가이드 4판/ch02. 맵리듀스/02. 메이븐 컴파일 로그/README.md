```
$ ll
total 132
drwxrwxr-x 30 hadoop hadoop 4096 Dec 29 09:41 ./
drwxrwxr-x  9 hadoop hadoop 4096 Jan 14 03:46 ../
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 appc/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 book/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch02-mr-intro/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch03-hdfs/
drwxrwxr-x  2 hadoop hadoop 4096 Dec 29 09:41 ch04-yarn/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch05-io/
drwxrwxr-x  5 hadoop hadoop 4096 Dec 29 09:41 ch06-mr-dev/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch08-mr-types/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch09-mr-features/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch10-setup/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch12-avro/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch13-parquet/
drwxrwxr-x  2 hadoop hadoop 4096 Dec 29 09:41 ch14-flume/
drwxrwxr-x  4 hadoop hadoop 4096 Dec 29 09:41 ch15-sqoop/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch16-pig/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch17-hive/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch18-crunch/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch19-spark/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch20-hbase/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch21-zk/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 ch22-case-studies/
drwxrwxr-x  3 hadoop hadoop 4096 Dec 29 09:41 common/
drwxrwxr-x  5 hadoop hadoop 4096 Dec 29 09:41 conf/
drwxrwxr-x  8 hadoop hadoop 4096 Dec 29 09:41 .git/
-rw-rw-r--  1 hadoop hadoop  140 Dec 29 09:41 .gitignore
drwxrwxr-x  2 hadoop hadoop 4096 Dec 29 09:41 hadoop-examples/
drwxrwxr-x  2 hadoop hadoop 4096 Dec 29 09:41 hadoop-meta/
drwxrwxr-x 13 hadoop hadoop 4096 Dec 29 09:41 input/
-rw-rw-r--  1 hadoop hadoop 1139 Dec 29 09:41 pom.xml
-rw-rw-r--  1 hadoop hadoop 1683 Dec 29 09:41 README.md
drwxrwxr-x  6 hadoop hadoop 4096 Dec 29 09:41 snippet/
$ mvn -version
Apache Maven 3.8.4 (9b656c72d54e5bacbed989b64718c159fe39b537)
Maven home: /opt/maven
Java version: 1.8.0_312, vendor: Temurin, runtime: /home/hadoop/platform/sw/jdk8u312-b07/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "5.4.0-92-generic", arch: "amd64", family: "unix"
$ mvn package -DskipTests
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO] 
[INFO] Hadoop: The Definitive Guide, Project                              [pom]
[INFO] Common Code                                                        [jar]
[INFO] Chapter 2: MapReduce                                               [jar]
[INFO] Chapter 3: The Hadoop Distributed Filesystem                       [jar]
[INFO] Chapter 5: Hadoop I/O                                              [jar]
[INFO] Chapter 6: Developing a MapReduce Application                      [jar]
[INFO] Chapter 8: MapReduce Types and Formats                             [jar]
[INFO] Chapter 9: MapReduce Features                                      [jar]
[INFO] Chapter 12: Avro                                                   [jar]
[INFO] Chapter 13: Parquet                                                [jar]
[INFO] Chapter 15: Sqoop                                                  [jar]
[INFO] Chapter 16: Pig                                                    [jar]
[INFO] Chapter 17: Hive                                                   [jar]
[INFO] Chapter 18: Crunch                                                 [jar]
[INFO] Chapter 19: Spark                                                  [jar]
[INFO] Chapter 20: HBase                                                  [jar]
[INFO] Chapter 21: ZooKeeper                                              [jar]
[INFO] Chapter 22: Case Studies                                           [jar]
[INFO] Hadoop Examples JAR                                                [pom]
[INFO] Snippet testing                                                    [jar]
[INFO] Hadoop: The Definitive Guide, Example Code                         [pom]
[INFO] 
[INFO] ------------------------< com.hadoopbook:book >-------------------------
[INFO] Building Hadoop: The Definitive Guide, Project 4.0                [1/21]
[INFO] --------------------------------[ pom ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-enforcer-plugin/1.3.1/maven-enforcer-plugin-1.3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-enforcer-plugin/1.3.1/maven-enforcer-plugin-1.3.1.pom (6.5 kB at 4.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer/1.3.1/enforcer-1.3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer/1.3.1/enforcer-1.3.1.pom (12 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-enforcer-plugin/1.3.1/maven-enforcer-plugin-1.3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-enforcer-plugin/1.3.1/maven-enforcer-plugin-1.3.1.jar (26 kB at 66 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-failsafe-plugin/2.17/maven-failsafe-plugin-2.17.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-failsafe-plugin/2.17/maven-failsafe-plugin-2.17.pom (8.4 kB at 25 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire/2.17/surefire-2.17.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire/2.17/surefire-2.17.pom (17 kB at 50 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-failsafe-plugin/2.17/maven-failsafe-plugin-2.17.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-failsafe-plugin/2.17/maven-failsafe-plugin-2.17.jar (79 kB at 156 kB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ book ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0-alpha-10/doxia-sink-api-1.0-alpha-10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0-alpha-10/doxia-sink-api-1.0-alpha-10.pom (1.3 kB at 4.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.0-alpha-10/doxia-1.0-alpha-10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.0-alpha-10/doxia-1.0-alpha-10.pom (9.2 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/6/maven-parent-6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/6/maven-parent-6.pom (20 kB at 60 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.3/commons-lang-2.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.3/commons-lang-2.3.pom (11 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-api/1.3.1/enforcer-api-1.3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-api/1.3.1/enforcer-api-1.3.1.pom (2.7 kB at 8.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-rules/1.3.1/enforcer-rules-1.3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-rules/1.3.1/enforcer-rules-1.3.1.pom (3.8 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-common-artifact-filters/1.4/maven-common-artifact-filters-1.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-common-artifact-filters/1.4/maven-common-artifact-filters-1.4.pom (3.8 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.8/maven-model-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.8/maven-model-2.0.8.pom (3.1 kB at 9.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.8/maven-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.8/maven-2.0.8.pom (12 kB at 37 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/aether/aether-util/0.9.0.M2/aether-util-0.9.0.M2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/aether/aether-util/0.9.0.M2/aether-util-0.9.0.M2.pom (2.0 kB at 6.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/aether/aether/0.9.0.M2/aether-0.9.0.M2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/aether/aether/0.9.0.M2/aether-0.9.0.M2.pom (28 kB at 81 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-i18n/1.0-beta-6/plexus-i18n-1.0-beta-6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-i18n/1.0-beta-6/plexus-i18n-1.0-beta-6.pom (771 B at 2.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-components/1.1.4/plexus-components-1.1.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-components/1.1.4/plexus-components-1.1.4.pom (2.1 kB at 6.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0-alpha-10/doxia-sink-api-1.0-alpha-10.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.5.8/plexus-utils-1.5.8.jar
Downloading from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.3/commons-lang-2.3.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-api/1.3.1/enforcer-api-1.3.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-rules/1.3.1/enforcer-rules-1.3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.5.8/plexus-utils-1.5.8.jar (268 kB at 491 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-common-artifact-filters/1.4/maven-common-artifact-filters-1.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-api/1.3.1/enforcer-api-1.3.1.jar (9.8 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/aether/aether-util/0.9.0.M2/aether-util-0.9.0.M2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0-alpha-10/doxia-sink-api-1.0-alpha-10.jar (9.9 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-i18n/1.0-beta-6/plexus-i18n-1.0-beta-6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-common-artifact-filters/1.4/maven-common-artifact-filters-1.4.jar (32 kB at 36 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-i18n/1.0-beta-6/plexus-i18n-1.0-beta-6.jar (12 kB at 10 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.3/commons-lang-2.3.jar (245 kB at 202 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/enforcer/enforcer-rules/1.3.1/enforcer-rules-1.3.1.jar (87 kB at 70 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/aether/aether-util/0.9.0.M2/aether-util-0.9.0.M2.jar (134 kB at 99 kB/s)
[INFO] 
[INFO] -----------------------< com.hadoopbook:common >------------------------
[INFO] Building Common Code 4.0                                          [2/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-surefire-plugin/2.17/maven-surefire-plugin-2.17.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-surefire-plugin/2.17/maven-surefire-plugin-2.17.pom (5.0 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-surefire-plugin/2.17/maven-surefire-plugin-2.17.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-surefire-plugin/2.17/maven-surefire-plugin-2.17.jar (34 kB at 115 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/2.5.1/hadoop-client-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/2.5.1/hadoop-client-2.5.1.pom (11 kB at 39 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project-dist/2.5.1/hadoop-project-dist-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project-dist/2.5.1/hadoop-project-dist-2.5.1.pom (18 kB at 59 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project/2.5.1/hadoop-project-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project/2.5.1/hadoop-project-2.5.1.pom (39 kB at 128 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-main/2.5.1/hadoop-main-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-main/2.5.1/hadoop-main-2.5.1.pom (18 kB at 62 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.5.1/hadoop-common-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.5.1/hadoop-common-2.5.1.pom (28 kB at 89 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-annotations/2.5.1/hadoop-annotations-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-annotations/2.5.1/hadoop-annotations-2.5.1.pom (2.4 kB at 8.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-cli/commons-cli/1.2/commons-cli-1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-cli/commons-cli/1.2/commons-cli-1.2.pom (8.0 kB at 27 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/11/commons-parent-11.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/11/commons-parent-11.pom (25 kB at 85 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math3/3.1.1/commons-math3-3.1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math3/3.1.1/commons-math3-3.1.1.pom (14 kB at 49 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/xmlenc/xmlenc/0.52/xmlenc-0.52.pom
Downloaded from central: https://repo.maven.apache.org/maven2/xmlenc/xmlenc/0.52/xmlenc-0.52.pom (623 B at 2.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.1/commons-httpclient-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.1/commons-httpclient-3.1.pom (7.8 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.0.4/commons-logging-1.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.0.4/commons-logging-1.0.4.pom (5.3 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.2/commons-codec-1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.2/commons-codec-1.2.pom (3.8 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.4/commons-codec-1.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.4/commons-codec-1.4.pom (10 kB at 37 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/3.1/commons-net-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/3.1/commons-net-3.1.pom (18 kB at 65 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/23/commons-parent-23.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/23/commons-parent-23.pom (44 kB at 151 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.pom (13 kB at 44 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/log4j/log4j/1.2.15/log4j-1.2.15.pom
Downloaded from central: https://repo.maven.apache.org/maven2/log4j/log4j/1.2.15/log4j-1.2.15.pom (18 kB at 62 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.6/commons-lang-2.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.6/commons-lang-2.6.pom (17 kB at 61 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/17/commons-parent-17.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/17/commons-parent-17.pom (31 kB at 106 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-configuration/commons-configuration/1.6/commons-configuration-1.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-configuration/commons-configuration/1.6/commons-configuration-1.6.pom (13 kB at 47 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.1.1/commons-logging-1.1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.1.1/commons-logging-1.1.1.pom (18 kB at 64 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/5/commons-parent-5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/5/commons-parent-5.pom (16 kB at 56 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-digester/commons-digester/1.8/commons-digester-1.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-digester/commons-digester/1.8/commons-digester-1.8.pom (7.0 kB at 25 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils/1.7.0/commons-beanutils-1.7.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils/1.7.0/commons-beanutils-1.7.0.pom (357 B at 1.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.0.3/commons-logging-1.0.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.0.3/commons-logging-1.0.3.pom (866 B at 3.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.1/commons-logging-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.1/commons-logging-1.1.pom (6.2 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/logkit/logkit/1.0.1/logkit-1.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/logkit/logkit/1.0.1/logkit-1.0.1.pom (147 B at 515 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/avalon-framework/avalon-framework/4.1.3/avalon-framework-4.1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/avalon-framework/avalon-framework/4.1.3/avalon-framework-4.1.3.pom (167 B at 592 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils-core/1.8.0/commons-beanutils-core-1.8.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils-core/1.8.0/commons-beanutils-core-1.8.0.pom (1.6 kB at 5.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-log4j12/1.7.5/slf4j-log4j12-1.7.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-log4j12/1.7.5/slf4j-log4j12-1.7.5.pom (1.6 kB at 5.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.13/jackson-core-asl-1.9.13.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.13/jackson-core-asl-1.9.13.pom (1.3 kB at 4.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.13/jackson-mapper-asl-1.9.13.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.13/jackson-mapper-asl-1.9.13.pom (1.5 kB at 5.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.7/avro-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.7/avro-1.7.7.pom (5.6 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-parent/1.7.7/avro-parent-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-parent/1.7.7/avro-parent-1.7.7.pom (19 kB at 64 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-toplevel/1.7.7/avro-toplevel-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-toplevel/1.7.7/avro-toplevel-1.7.7.pom (9.6 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.3/paranamer-2.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.3/paranamer-2.3.pom (1.6 kB at 5.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer-parent/2.3/paranamer-parent-2.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer-parent/2.3/paranamer-parent-2.3.pom (11 kB at 38 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/codehaus-parent/1/codehaus-parent-1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/codehaus-parent/1/codehaus-parent-1.pom (3.4 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5/snappy-java-1.0.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5/snappy-java-1.0.5.pom (13 kB at 45 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-compress/1.4.1/commons-compress-1.4.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-compress/1.4.1/commons-compress-1.4.1.pom (11 kB at 38 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/tukaani/xz/1.0/xz-1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/tukaani/xz/1.0/xz-1.0.pom (1.9 kB at 6.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/protobuf/protobuf-java/2.5.0/protobuf-java-2.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/protobuf/protobuf-java/2.5.0/protobuf-java-2.5.0.pom (8.4 kB at 29 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.5.1/hadoop-auth-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.5.1/hadoop-auth-2.5.1.pom (6.5 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.2.5/httpclient-4.2.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.2.5/httpclient-4.2.5.pom (5.9 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-client/4.2.5/httpcomponents-client-4.2.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-client/4.2.5/httpcomponents-client-4.2.5.pom (15 kB at 52 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/project/6/project-6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/project/6/project-6.pom (24 kB at 84 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.2.4/httpcore-4.2.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.2.4/httpcore-4.2.4.pom (5.7 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.2.4/httpcomponents-core-4.2.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.2.4/httpcomponents-core-4.2.4.pom (12 kB at 42 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-kerberos-codec/2.0.0-M15/apacheds-kerberos-codec-2.0.0-M15.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-kerberos-codec/2.0.0-M15/apacheds-kerberos-codec-2.0.0-M15.pom (3.9 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-parent/2.0.0-M15/apacheds-parent-2.0.0-M15.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-parent/2.0.0-M15/apacheds-parent-2.0.0-M15.pom (43 kB at 145 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/project/project/31/project-31.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/project/project/31/project-31.pom (27 kB at 92 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-i18n/2.0.0-M15/apacheds-i18n-2.0.0-M15.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-i18n/2.0.0-M15/apacheds-i18n-2.0.0-M15.pom (2.4 kB at 8.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-asn1-api/1.0.0-M20/api-asn1-api-1.0.0-M20.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-asn1-api/1.0.0-M20/api-asn1-api-1.0.0-M20.pom (2.7 kB at 9.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-asn1-parent/1.0.0-M20/api-asn1-parent-1.0.0-M20.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-asn1-parent/1.0.0-M20/api-asn1-parent-1.0.0-M20.pom (1.5 kB at 4.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-parent/1.0.0-M20/api-parent-1.0.0-M20.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-parent/1.0.0-M20/api-parent-1.0.0-M20.pom (27 kB at 94 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-util/1.0.0-M20/api-util-1.0.0-M20.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-util/1.0.0-M20/api-util-1.0.0-M20.pom (2.4 kB at 8.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/zookeeper/zookeeper/3.4.6/zookeeper-3.4.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/zookeeper/zookeeper/3.4.6/zookeeper-3.4.6.pom (3.4 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.7.0.Final/netty-3.7.0.Final.pom
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.7.0.Final/netty-3.7.0.Final.pom (26 kB at 91 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.5.1/hadoop-hdfs-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.5.1/hadoop-hdfs-2.5.1.pom (24 kB at 78 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.2.Final/netty-3.6.2.Final.pom
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.2.Final/netty-3.6.2.Final.pom (26 kB at 81 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-app/2.5.1/hadoop-mapreduce-client-app-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-app/2.5.1/hadoop-mapreduce-client-app-2.5.1.pom (4.3 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client/2.5.1/hadoop-mapreduce-client-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client/2.5.1/hadoop-mapreduce-client-2.5.1.pom (6.7 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-common/2.5.1/hadoop-mapreduce-client-common-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-common/2.5.1/hadoop-mapreduce-client-common-2.5.1.pom (3.3 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.5.1/hadoop-yarn-common-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.5.1/hadoop-yarn-common-2.5.1.pom (9.3 kB at 33 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn/2.5.1/hadoop-yarn-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn/2.5.1/hadoop-yarn-2.5.1.pom (3.5 kB at 13 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-api/2.5.1/hadoop-yarn-api-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-api/2.5.1/hadoop-yarn-api-2.5.1.pom (4.6 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/xml/bind/jaxb-api/2.2.2/jaxb-api-2.2.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/xml/bind/jaxb-api/2.2.2/jaxb-api-2.2.2.pom (5.6 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/xml/stream/stax-api/1.0-2/stax-api-1.0-2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/xml/stream/stax-api/1.0-2/stax-api-1.0-2.pom (962 B at 3.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.9/jersey-core-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.9/jersey-core-1.9.pom (10 kB at 36 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-project/1.9/jersey-project-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-project/1.9/jersey-project-1.9.pom (18 kB at 62 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/java/jvnet-parent/1/jvnet-parent-1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/java/jvnet-parent/1/jvnet-parent-1.pom (4.7 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.9.13/jackson-jaxrs-1.9.13.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.9.13/jackson-jaxrs-1.9.13.pom (1.9 kB at 6.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.9.13/jackson-xc-1.9.13.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.9.13/jackson-xc-1.9.13.pom (1.9 kB at 6.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/inject/guice/3.0/guice-3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/inject/guice/3.0/guice-3.0.pom (7.3 kB at 26 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/inject/guice-parent/3.0/guice-parent-3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/inject/guice-parent/3.0/guice-parent-3.0.pom (13 kB at 46 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/google/5/google-5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/google/5/google-5.pom (2.5 kB at 8.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.9/jersey-server-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.9/jersey-server-1.9.pom (12 kB at 41 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/asm/asm/3.1/asm-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/asm/asm/3.1/asm-3.1.pom (278 B at 996 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/asm/asm-parent/3.1/asm-parent-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/asm/asm-parent/3.1/asm-parent-3.1.pom (4.2 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.9/jersey-json-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.9/jersey-json-1.9.pom (11 kB at 38 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jettison/jettison/1.1/jettison-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jettison/jettison/1.1/jettison-1.1.pom (3.4 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/xml/bind/jaxb-impl/2.2.3-1/jaxb-impl-2.2.3-1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/xml/bind/jaxb-impl/2.2.3-1/jaxb-impl-2.2.3-1.pom (5.3 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.8.3/jackson-core-asl-1.8.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.8.3/jackson-core-asl-1.8.3.pom (1.0 kB at 3.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.8.3/jackson-mapper-asl-1.8.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.8.3/jackson-mapper-asl-1.8.3.pom (1.2 kB at 4.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.8.3/jackson-jaxrs-1.8.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.8.3/jackson-jaxrs-1.8.3.pom (1.6 kB at 5.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.8.3/jackson-xc-1.8.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.8.3/jackson-xc-1.8.3.pom (1.6 kB at 5.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/contribs/jersey-guice/1.9/jersey-guice-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/contribs/jersey-guice/1.9/jersey-guice-1.9.pom (7.5 kB at 27 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/contribs/jersey-contribs/1.9/jersey-contribs-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/contribs/jersey-contribs/1.9/jersey-contribs-1.9.pom (4.2 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-client/2.5.1/hadoop-yarn-client-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-client/2.5.1/hadoop-yarn-client-2.5.1.pom (5.2 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-client/1.9/jersey-client-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-client/1.9/jersey-client-1.9.pom (6.4 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.5.1/hadoop-mapreduce-client-core-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.5.1/hadoop-mapreduce-client-core-2.5.1.pom (3.4 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-common/2.5.1/hadoop-yarn-server-common-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-common/2.5.1/hadoop-yarn-server-common-2.5.1.pom (5.7 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server/2.5.1/hadoop-yarn-server-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server/2.5.1/hadoop-yarn-server-2.5.1.pom (1.8 kB at 6.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/fusesource/leveldbjni/leveldbjni-all/1.8/leveldbjni-all-1.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/fusesource/leveldbjni/leveldbjni-all/1.8/leveldbjni-all-1.8.pom (7.2 kB at 26 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/fusesource/leveldbjni/leveldbjni-project/1.8/leveldbjni-project-1.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/fusesource/leveldbjni/leveldbjni-project/1.8/leveldbjni-project-1.8.pom (9.6 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/fusesource/fusesource-pom/1.9/fusesource-pom-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/fusesource/fusesource-pom/1.9/fusesource-pom-1.9.pom (15 kB at 52 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-shuffle/2.5.1/hadoop-mapreduce-client-shuffle-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-shuffle/2.5.1/hadoop-mapreduce-client-shuffle-2.5.1.pom (3.1 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.5/servlet-api-2.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.5/servlet-api-2.5.pom (157 B at 560 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.5.1/hadoop-mapreduce-client-jobclient-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.5.1/hadoop-mapreduce-client-jobclient-2.5.1.pom (5.5 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/jline/jline/0.9.94/jline-0.9.94.pom
Downloaded from central: https://repo.maven.apache.org/maven2/jline/jline/0.9.94/jline-0.9.94.pom (6.4 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-nodemanager/2.5.1/hadoop-yarn-server-nodemanager-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-nodemanager/2.5.1/hadoop-yarn-server-nodemanager-2.5.1.pom (12 kB at 41 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-minicluster/2.5.1/hadoop-minicluster-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-minicluster/2.5.1/hadoop-minicluster-2.5.1.pom (3.2 kB at 9.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.23/jasper-compiler-5.5.23.pom
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.23/jasper-compiler-5.5.23.pom (919 B at 3.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/tomcat-parent/5.5.23/tomcat-parent-5.5.23.pom
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/tomcat-parent/5.5.23/tomcat-parent-5.5.23.pom (898 B at 3.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.23/jasper-runtime-5.5.23.pom
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.23/jasper-runtime-5.5.23.pom (934 B at 3.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.4/servlet-api-2.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.4/servlet-api-2.4.pom (156 B at 536 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-el/commons-el/1.0/commons-el-1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-el/commons-el/1.0/commons-el-1.0.pom (4.1 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/servlet/jsp/jsp-api/2.1/jsp-api-2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/servlet/jsp/jsp-api/2.1/jsp-api-2.1.pom (157 B at 566 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.9.0/jets3t-0.9.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.9.0/jets3t-0.9.0.pom (2.4 kB at 8.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.1.2/httpclient-4.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.1.2/httpclient-4.1.2.pom (6.0 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-client/4.1.2/httpcomponents-client-4.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-client/4.1.2/httpcomponents-client-4.1.2.pom (11 kB at 39 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/project/4.1.1/project-4.1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/project/4.1.1/project-4.1.1.pom (17 kB at 58 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.2/httpcore-4.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.2/httpcore-4.1.2.pom (7.4 kB at 26 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.1.2/httpcomponents-core-4.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.1.2/httpcomponents-core-4.1.2.pom (9.8 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/jamesmurty/utils/java-xmlbuilder/0.4/java-xmlbuilder-0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/jamesmurty/utils/java-xmlbuilder/0.4/java-xmlbuilder-0.4.pom (3.0 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/oss/oss-parent/3/oss-parent-3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/oss/oss-parent/3/oss-parent-3.pom (3.4 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.3/servlet-api-2.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.3/servlet-api-2.3.pom (156 B at 551 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/jcraft/jsch/0.1.42/jsch-0.1.42.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/jcraft/jsch/0.1.42/jsch-0.1.42.pom (967 B at 3.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-daemon/commons-daemon/1.0.13/commons-daemon-1.0.13.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-daemon/commons-daemon/1.0.13/commons-daemon-1.0.13.pom (4.3 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-tests/2.5.1/hadoop-yarn-server-tests-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-tests/2.5.1/hadoop-yarn-server-tests-2.5.1.pom (5.3 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/inject/extensions/guice-servlet/3.0/guice-servlet-3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/inject/extensions/guice-servlet/3.0/guice-servlet-3.0.pom (915 B at 3.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/inject/extensions/extensions-parent/3.0/extensions-parent-3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/inject/extensions/extensions-parent/3.0/extensions-parent-3.0.pom (3.6 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-resourcemanager/2.5.1/hadoop-yarn-server-resourcemanager-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-resourcemanager/2.5.1/hadoop-yarn-server-resourcemanager-2.5.1.pom (10.0 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-applicationhistoryservice/2.5.1/hadoop-yarn-server-applicationhistoryservice-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-applicationhistoryservice/2.5.1/hadoop-yarn-server-applicationhistoryservice-2.5.1.pom (6.3 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-web-proxy/2.5.1/hadoop-yarn-server-web-proxy-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-web-proxy/2.5.1/hadoop-yarn-server-web-proxy-2.5.1.pom (4.7 kB at 13 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-hs/2.5.1/hadoop-mapreduce-client-hs-2.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-hs/2.5.1/hadoop-mapreduce-client-hs-2.5.1.pom (3.6 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/mrunit/mrunit/1.1.0/mrunit-1.1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/mrunit/mrunit/1.1.0/mrunit-1.1.0.pom (13 kB at 46 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-core/1.5.1/powermock-core-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-core/1.5.1/powermock-core-1.5.1.pom (1.9 kB at 4.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock/1.5.1/powermock-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock/1.5.1/powermock-1.5.1.pom (10.0 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/oss/oss-parent/5/oss-parent-5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/oss/oss-parent/5/oss-parent-5.pom (4.1 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-reflect/1.5.1/powermock-reflect-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-reflect/1.5.1/powermock-reflect-1.5.1.pom (1.0 kB at 3.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/objenesis/objenesis/1.2/objenesis-1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/objenesis/objenesis/1.2/objenesis-1.2.pom (3.0 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/objenesis/objenesis-parent/1.2/objenesis-parent-1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/objenesis/objenesis-parent/1.2/objenesis-parent-1.2.pom (10 kB at 37 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.18.0-GA/javassist-3.18.0-GA.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.18.0-GA/javassist-3.18.0-GA.pom (9.2 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-mockito/1.5.1/powermock-api-mockito-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-mockito/1.5.1/powermock-api-mockito-1.5.1.pom (1.2 kB at 4.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api/1.5.1/powermock-api-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api/1.5.1/powermock-api-1.5.1.pom (805 B at 2.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-support/1.5.1/powermock-api-support-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-support/1.5.1/powermock-api-support-1.5.1.pom (1.3 kB at 4.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4/1.5.1/powermock-module-junit4-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4/1.5.1/powermock-module-junit4-1.5.1.pom (1.3 kB at 4.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-modules/1.5.1/powermock-modules-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-modules/1.5.1/powermock-modules-1.5.1.pom (798 B at 2.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4-common/1.5.1/powermock-module-junit4-common-1.5.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4-common/1.5.1/powermock-module-junit4-common-1.5.1.pom (1.1 kB at 4.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/2.5.1/hadoop-client-2.5.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.5.1/hadoop-common-2.5.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/commons-cli/commons-cli/1.2/commons-cli-1.2.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math3/3.1.1/commons-math3-3.1.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/xmlenc/xmlenc/0.52/xmlenc-0.52.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/2.5.1/hadoop-client-2.5.1.jar (2.6 kB at 9.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.1/commons-httpclient-3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/xmlenc/xmlenc/0.52/xmlenc-0.52.jar (15 kB at 44 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.4/commons-codec-1.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-cli/commons-cli/1.2/commons-cli-1.2.jar (41 kB at 119 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-io/commons-io/2.4/commons-io-2.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.4/commons-codec-1.4.jar (58 kB at 87 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/3.1/commons-net-3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-io/commons-io/2.4/commons-io-2.4.jar (185 kB at 262 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.1/commons-httpclient-3.1.jar (305 kB at 422 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/log4j/log4j/1.2.15/log4j-1.2.15.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/3.1/commons-net-3.1.jar (273 kB at 238 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.6/commons-lang-2.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/log4j/log4j/1.2.15/log4j-1.2.15.jar (392 kB at 337 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-configuration/commons-configuration/1.6/commons-configuration-1.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math3/3.1.1/commons-math3-3.1.1.jar (1.6 MB at 1.2 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-digester/commons-digester/1.8/commons-digester-1.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.jar (575 kB at 428 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils/1.7.0/commons-beanutils-1.7.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.5.1/hadoop-common-2.5.1.jar (3.0 MB at 2.1 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils-core/1.8.0/commons-beanutils-core-1.8.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-configuration/commons-configuration/1.6/commons-configuration-1.6.jar (299 kB at 188 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-log4j12/1.7.5/slf4j-log4j12-1.7.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.6/commons-lang-2.6.jar (284 kB at 175 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.13/jackson-core-asl-1.9.13.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-digester/commons-digester/1.8/commons-digester-1.8.jar (144 kB at 88 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.13/jackson-mapper-asl-1.9.13.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils-core/1.8.0/commons-beanutils-core-1.8.0.jar (206 kB at 121 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/commons-beanutils/commons-beanutils/1.7.0/commons-beanutils-1.7.0.jar (189 kB at 111 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.3/paranamer-2.3.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.7/avro-1.7.7.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-log4j12/1.7.5/slf4j-log4j12-1.7.5.jar (8.9 kB at 4.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5/snappy-java-1.0.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.13/jackson-core-asl-1.9.13.jar (232 kB at 117 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/protobuf/protobuf-java/2.5.0/protobuf-java-2.5.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.13/jackson-mapper-asl-1.9.13.jar (781 kB at 392 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.5.1/hadoop-auth-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.3/paranamer-2.3.jar (30 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.2.5/httpclient-4.2.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.7/avro-1.7.7.jar (436 kB at 208 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-kerberos-codec/2.0.0-M15/apacheds-kerberos-codec-2.0.0-M15.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5/snappy-java-1.0.5.jar (1.3 MB at 552 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-i18n/2.0.0-M15/apacheds-i18n-2.0.0-M15.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.5.1/hadoop-auth-2.5.1.jar (52 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-asn1-api/1.0.0-M20/api-asn1-api-1.0.0-M20.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.2.5/httpclient-4.2.5.jar (433 kB at 184 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-util/1.0.0-M20/api-util-1.0.0-M20.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/protobuf/protobuf-java/2.5.0/protobuf-java-2.5.0.jar (533 kB at 216 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/zookeeper/zookeeper/3.4.6/zookeeper-3.4.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-kerberos-codec/2.0.0-M15/apacheds-kerberos-codec-2.0.0-M15.jar (691 kB at 278 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-compress/1.4.1/commons-compress-1.4.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/server/apacheds-i18n/2.0.0-M15/apacheds-i18n-2.0.0-M15.jar (45 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/tukaani/xz/1.0/xz-1.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-asn1-api/1.0.0-M20/api-asn1-api-1.0.0-M20.jar (17 kB at 6.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.5.1/hadoop-hdfs-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/directory/api/api-util/1.0.0-M20/api-util-1.0.0-M20.jar (80 kB at 30 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.2.Final/netty-3.6.2.Final.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-compress/1.4.1/commons-compress-1.4.1.jar (241 kB at 86 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-app/2.5.1/hadoop-mapreduce-client-app-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/zookeeper/zookeeper/3.4.6/zookeeper-3.4.6.jar (793 kB at 278 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-common/2.5.1/hadoop-mapreduce-client-common-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/tukaani/xz/1.0/xz-1.0.jar (95 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-client/2.5.1/hadoop-yarn-client-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.2.Final/netty-3.6.2.Final.jar (1.2 MB at 389 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-shuffle/2.5.1/hadoop-mapreduce-client-shuffle-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-app/2.5.1/hadoop-mapreduce-client-app-2.5.1.jar (491 kB at 155 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/fusesource/leveldbjni/leveldbjni-all/1.8/leveldbjni-all-1.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-common/2.5.1/hadoop-mapreduce-client-common-2.5.1.jar (663 kB at 207 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-api/2.5.1/hadoop-yarn-api-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-client/2.5.1/hadoop-yarn-client-2.5.1.jar (118 kB at 36 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.5.1/hadoop-mapreduce-client-core-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.5.1/hadoop-hdfs-2.5.1.jar (7.1 MB at 2.1 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.5.1/hadoop-yarn-common-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-shuffle/2.5.1/hadoop-mapreduce-client-shuffle-2.5.1.jar (44 kB at 13 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/xml/bind/jaxb-api/2.2.2/jaxb-api-2.2.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/fusesource/leveldbjni/leveldbjni-all/1.8/leveldbjni-all-1.8.jar (1.0 MB at 294 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/xml/stream/stax-api/1.0-2/stax-api-1.0-2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.5.1/hadoop-mapreduce-client-core-2.5.1.jar (1.5 MB at 413 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.9.13/jackson-jaxrs-1.9.13.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-api/2.5.1/hadoop-yarn-api-2.5.1.jar (1.6 MB at 447 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.9.13/jackson-xc-1.9.13.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.5.1/hadoop-yarn-common-2.5.1.jar (1.4 MB at 383 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.5.1/hadoop-mapreduce-client-jobclient-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/javax/xml/bind/jaxb-api/2.2.2/jaxb-api-2.2.2.jar (105 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-annotations/2.5.1/hadoop-annotations-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/javax/xml/stream/stax-api/1.0-2/stax-api-1.0-2.jar (23 kB at 6.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-minicluster/2.5.1/hadoop-minicluster-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.9.13/jackson-jaxrs-1.9.13.jar (18 kB at 4.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.5.1/hadoop-common-2.5.1-tests.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-annotations/2.5.1/hadoop-annotations-2.5.1.jar (17 kB at 4.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.5/servlet-api-2.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.9.13/jackson-xc-1.9.13.jar (27 kB at 6.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.9/jersey-core-1.9.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.5.1/hadoop-mapreduce-client-jobclient-2.5.1.jar (36 kB at 8.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.9/jersey-json-1.9.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-minicluster/2.5.1/hadoop-minicluster-2.5.1.jar (2.1 kB at 494 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jettison/jettison/1.1/jettison-1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/javax/servlet/servlet-api/2.5/servlet-api-2.5.jar (105 kB at 25 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/xml/bind/jaxb-impl/2.2.3-1/jaxb-impl-2.2.3-1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.9/jersey-json-1.9.jar (148 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.9/jersey-server-1.9.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.9/jersey-core-1.9.jar (459 kB at 105 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/asm/asm/3.1/asm-3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.5.1/hadoop-common-2.5.1-tests.jar (1.6 MB at 368 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.23/jasper-compiler-5.5.23.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jettison/jettison/1.1/jettison-1.1.jar (68 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.23/jasper-runtime-5.5.23.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/xml/bind/jaxb-impl/2.2.3-1/jaxb-impl-2.2.3-1.jar (890 kB at 193 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/servlet/jsp/jsp-api/2.1/jsp-api-2.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/asm/asm/3.1/asm-3.1.jar (43 kB at 9.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-el/commons-el/1.0/commons-el-1.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.9/jersey-server-1.9.jar (713 kB at 151 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.9.0/jets3t-0.9.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.23/jasper-compiler-5.5.23.jar (408 kB at 86 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.2/httpcore-4.1.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.23/jasper-runtime-5.5.23.jar (77 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/jamesmurty/utils/java-xmlbuilder/0.4/java-xmlbuilder-0.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/javax/servlet/jsp/jsp-api/2.1/jsp-api-2.1.jar (101 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/jcraft/jsch/0.1.42/jsch-0.1.42.jar
Downloaded from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.9.0/jets3t-0.9.0.jar (540 kB at 107 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.5.1/hadoop-hdfs-2.5.1-tests.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.2/httpcore-4.1.2.jar (181 kB at 36 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-daemon/commons-daemon/1.0.13/commons-daemon-1.0.13.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/jamesmurty/utils/java-xmlbuilder/0.4/java-xmlbuilder-0.4.jar (18 kB at 3.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-tests/2.5.1/hadoop-yarn-server-tests-2.5.1-tests.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/jcraft/jsch/0.1.42/jsch-0.1.42.jar (186 kB at 36 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-common/2.5.1/hadoop-yarn-server-common-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-daemon/commons-daemon/1.0.13/commons-daemon-1.0.13.jar (24 kB at 4.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-nodemanager/2.5.1/hadoop-yarn-server-nodemanager-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-el/commons-el/1.0/commons-el-1.0.jar (112 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-client/1.9/jersey-client-1.9.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.5.1/hadoop-hdfs-2.5.1-tests.jar (2.7 MB at 496 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/inject/guice/3.0/guice-3.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-common/2.5.1/hadoop-yarn-server-common-2.5.1.jar (242 kB at 44 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/contribs/jersey-guice/1.9/jersey-guice-1.9.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-tests/2.5.1/hadoop-yarn-server-tests-2.5.1-tests.jar (46 kB at 8.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-resourcemanager/2.5.1/hadoop-yarn-server-resourcemanager-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-nodemanager/2.5.1/hadoop-yarn-server-nodemanager-2.5.1.jar (535 kB at 93 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-applicationhistoryservice/2.5.1/hadoop-yarn-server-applicationhistoryservice-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/inject/guice/3.0/guice-3.0.jar (710 kB at 122 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-web-proxy/2.5.1/hadoop-yarn-server-web-proxy-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-client/1.9/jersey-client-1.9.jar (130 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.5.1/hadoop-mapreduce-client-jobclient-2.5.1-tests.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/contribs/jersey-guice/1.9/jersey-guice-1.9.jar (15 kB at 2.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/inject/extensions/guice-servlet/3.0/guice-servlet-3.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-resourcemanager/2.5.1/hadoop-yarn-server-resourcemanager-2.5.1.jar (855 kB at 142 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-hs/2.5.1/hadoop-mapreduce-client-hs-2.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-applicationhistoryservice/2.5.1/hadoop-yarn-server-applicationhistoryservice-2.5.1.jar (143 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/mrunit/mrunit/1.1.0/mrunit-1.1.0-hadoop2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-web-proxy/2.5.1/hadoop-yarn-server-web-proxy-2.5.1.jar (29 kB at 4.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.1.1/commons-logging-1.1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/inject/extensions/guice-servlet/3.0/guice-servlet-3.0.jar (65 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-core/1.5.1/powermock-core-1.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-hs/2.5.1/hadoop-mapreduce-client-hs-2.5.1.jar (233 kB at 37 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-reflect/1.5.1/powermock-reflect-1.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-logging/commons-logging/1.1.1/commons-logging-1.1.1.jar (61 kB at 9.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.18.0-GA/javassist-3.18.0-GA.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/mrunit/mrunit/1.1.0/mrunit-1.1.0-hadoop2.jar (138 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-mockito/1.5.1/powermock-api-mockito-1.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.5.1/hadoop-mapreduce-client-jobclient-2.5.1-tests.jar (1.5 MB at 231 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-support/1.5.1/powermock-api-support-1.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-reflect/1.5.1/powermock-reflect-1.5.1.jar (43 kB at 6.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4/1.5.1/powermock-module-junit4-1.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.18.0-GA/javassist-3.18.0-GA.jar (714 kB at 107 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4-common/1.5.1/powermock-module-junit4-common-1.5.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-core/1.5.1/powermock-core-1.5.1.jar (84 kB at 13 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-mockito/1.5.1/powermock-api-mockito-1.5.1.jar (68 kB at 10 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-api-support/1.5.1/powermock-api-support-1.5.1.jar (19 kB at 2.7 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4-common/1.5.1/powermock-module-junit4-common-1.5.1.jar (13 kB at 1.9 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/powermock/powermock-module-junit4/1.5.1/powermock-module-junit4-1.5.1.jar (24 kB at 3.4 kB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ common ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ common ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/common/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ common ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 10 source files to /home/hadoop/work/hadoop-book/common/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/common/src/main/java/JobBuilder.java:[28,16] Job() in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/common/src/main/java/JobBuilder.java:[41,15] Job(org.apache.hadoop.conf.Configuration) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ common ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/common/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ common ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /home/hadoop/work/hadoop-book/common/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ common ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/maven-surefire-common/2.17/maven-surefire-common-2.17.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/maven-surefire-common/2.17/maven-surefire-common-2.17.pom (6.1 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-api/2.17/surefire-api-2.17.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-api/2.17/surefire-api-2.17.pom (2.3 kB at 7.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-booter/2.17/surefire-booter-2.17.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-booter/2.17/surefire-booter-2.17.pom (2.8 kB at 9.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/maven-surefire-common/2.17/maven-surefire-common-2.17.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-booter/2.17/surefire-booter-2.17.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-api/2.17/surefire-api-2.17.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/maven-surefire-common/2.17/maven-surefire-common-2.17.jar (265 kB at 862 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-booter/2.17/surefire-booter-2.17.jar (40 kB at 119 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-api/2.17/surefire-api-2.17.jar (147 kB at 445 kB/s)
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ common ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/common/target/common-4.0.jar
[INFO] 
[INFO] --------------------< com.hadoopbook:ch02-mr-intro >--------------------
[INFO] Building Chapter 2: MapReduce 4.0                                 [3/21]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch02-mr-intro ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch02-mr-intro ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch02-mr-intro/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch02-mr-intro ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 9 source files to /home/hadoop/work/hadoop-book/ch02-mr-intro/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch02-mr-intro/src/main/java/MaxTemperatureWithCombiner.java:[19,15] Job() in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch02-mr-intro/src/main/java/MaxTemperature.java:[18,15] Job() in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch02-mr-intro ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch02-mr-intro/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch02-mr-intro ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch02-mr-intro ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch02-mr-intro ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch02-mr-intro/target/ch02-mr-intro-4.0.jar
[INFO] 
[INFO] ----------------------< com.hadoopbook:ch03-hdfs >----------------------
[INFO] Building Chapter 3: The Hadoop Distributed Filesystem 4.0         [4/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/hamcrest/hamcrest-all/1.3/hamcrest-all-1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/hamcrest/hamcrest-all/1.3/hamcrest-all-1.3.pom (650 B at 2.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/hamcrest/hamcrest-all/1.3/hamcrest-all-1.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/hamcrest/hamcrest-all/1.3/hamcrest-all-1.3.jar (307 kB at 932 kB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch03-hdfs ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch03-hdfs ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch03-hdfs/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch03-hdfs ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 8 source files to /home/hadoop/work/hadoop-book/ch03-hdfs/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch03-hdfs ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch03-hdfs/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch03-hdfs ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 4 source files to /home/hadoop/work/hadoop-book/ch03-hdfs/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch03-hdfs ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch03-hdfs ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch03-hdfs/target/ch03-hdfs-4.0.jar
[INFO] 
[INFO] -----------------------< com.hadoopbook:ch05-io >-----------------------
[INFO] Building Chapter 5: Hadoop I/O 4.0                                [5/21]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch05-io ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch05-io ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch05-io/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch05-io ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 17 source files to /home/hadoop/work/hadoop-book/ch05-io/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/main/java/MapFileWriteDemo.java:[30,16] Writer(org.apache.hadoop.conf.Configuration,org.apache.hadoop.fs.FileSystem,java.lang.String,java.lang.Class<? extends org.apache.hadoop.io.WritableComparable>,java.lang.Class) in org.apache.hadoop.io.MapFile.Writer has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/main/java/MapFileFixer.java:[22,34] Reader(org.apache.hadoop.fs.FileSystem,org.apache.hadoop.fs.Path,org.apache.hadoop.conf.Configuration) in org.apache.hadoop.io.SequenceFile.Reader has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/main/java/SequenceFileWriteDemo.java:[34,28] createWriter(org.apache.hadoop.fs.FileSystem,org.apache.hadoop.conf.Configuration,org.apache.hadoop.fs.Path,java.lang.Class,java.lang.Class) in org.apache.hadoop.io.SequenceFile has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/main/java/MaxTemperatureWithMapOutputCompression.java:[26,15] Job(org.apache.hadoop.conf.Configuration) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/main/java/MaxTemperatureWithCompression.java:[20,15] Job() in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/main/java/SequenceFileReadDemo.java:[24,16] Reader(org.apache.hadoop.fs.FileSystem,org.apache.hadoop.fs.Path,org.apache.hadoop.conf.Configuration) in org.apache.hadoop.io.SequenceFile.Reader has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch05-io ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch05-io ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 19 source files to /home/hadoop/work/hadoop-book/ch05-io/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/GenericWritableTest.java:[15,18] cloneInto(org.apache.hadoop.io.Writable,org.apache.hadoop.io.Writable) in org.apache.hadoop.io.WritableUtils has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/GenericWritableTest.java:[19,18] cloneInto(org.apache.hadoop.io.Writable,org.apache.hadoop.io.Writable) in org.apache.hadoop.io.WritableUtils has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/ArrayWritableTest.java:[19,18] cloneInto(org.apache.hadoop.io.Writable,org.apache.hadoop.io.Writable) in org.apache.hadoop.io.WritableUtils has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/SequenceFileSeekAndSyncTest.java:[31,14] Reader(org.apache.hadoop.fs.FileSystem,org.apache.hadoop.fs.Path,org.apache.hadoop.conf.Configuration) in org.apache.hadoop.io.SequenceFile.Reader has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/IntPairTest.java:[39,45] unchecked call to compare(T,T) as a member of the raw type java.util.Comparator
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/TextPairTest.java:[61,45] unchecked call to compare(T,T) as a member of the raw type java.util.Comparator
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/MapWritableTest.java:[19,18] cloneInto(org.apache.hadoop.io.Writable,org.apache.hadoop.io.Writable) in org.apache.hadoop.io.WritableUtils has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/MapWritableTest.java:[33,18] cloneInto(org.apache.hadoop.io.Writable,org.apache.hadoop.io.Writable) in org.apache.hadoop.io.WritableUtils has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/MapFileSeekTest.java:[27,14] Reader(org.apache.hadoop.fs.FileSystem,java.lang.String,org.apache.hadoop.conf.Configuration) in org.apache.hadoop.io.MapFile.Reader has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch05-io/src/test/java/ObjectWritableTest.java:[14,18] cloneInto(org.apache.hadoop.io.Writable,org.apache.hadoop.io.Writable) in org.apache.hadoop.io.WritableUtils has been deprecated
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch05-io ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch05-io ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch05-io/target/ch05-io-4.0.jar
[INFO] 
[INFO] ---------------------< com.hadoopbook:ch06-mr-dev >---------------------
[INFO] Building Chapter 6: Developing a MapReduce Application 4.0        [6/21]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch06-mr-dev ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch06-mr-dev ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 4 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch06-mr-dev ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 13 source files to /home/hadoop/work/hadoop-book/ch06-mr-dev/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch06-mr-dev/src/main/java/v3/MaxTemperatureDriver.java:[27,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch06-mr-dev/src/main/java/LoggingDriver.java:[20,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch06-mr-dev/src/main/java/v2/MaxTemperatureDriver.java:[27,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch06-mr-dev/src/main/java/v4/MaxTemperatureDriver.java:[27,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch06-mr-dev ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch06-mr-dev ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 8 source files to /home/hadoop/work/hadoop-book/ch06-mr-dev/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch06-mr-dev ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch06-mr-dev ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch06-mr-dev/target/ch06-mr-dev-4.0.jar
[INFO] 
[INFO] --------------------< com.hadoopbook:ch08-mr-types >--------------------
[INFO] Building Chapter 8: MapReduce Types and Formats 4.0               [7/21]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch08-mr-types ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch08-mr-types ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch08-mr-types/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch08-mr-types ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 21 source files to /home/hadoop/work/hadoop-book/ch08-mr-types/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch08-mr-types/src/main/java/MaxTemperatureWithMultipleInputs.java:[40,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch08-mr-types/src/main/java/MinimalMapReduce.java:[22,15] Job(org.apache.hadoop.conf.Configuration) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch08-mr-types ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch08-mr-types/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch08-mr-types ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/hadoop-book/ch08-mr-types/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch08-mr-types ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch08-mr-types ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch08-mr-types/target/ch08-mr-types-4.0.jar
[INFO] 
[INFO] ------------------< com.hadoopbook:ch09-mr-features >-------------------
[INFO] Building Chapter 9: MapReduce Features 4.0                        [8/21]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch09-mr-features ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch09-mr-features ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch09-mr-features ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 31 source files to /home/hadoop/work/hadoop-book/ch09-mr-features/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/SortByTemperatureUsingTotalOrderPartitioner.java:[6,35] org.apache.hadoop.filecache.DistributedCache in org.apache.hadoop.filecache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/MaxTemperatureByStationNameUsingDistributedCacheFileApi.java:[7,35] org.apache.hadoop.filecache.DistributedCache in org.apache.hadoop.filecache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/SortByTemperatureUsingTotalOrderPartitioner.java:[39,18] makeQualified(org.apache.hadoop.fs.FileSystem) in org.apache.hadoop.fs.Path has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/SortByTemperatureUsingTotalOrderPartitioner.java:[42,26] setPartitionFile(org.apache.hadoop.mapred.JobConf,org.apache.hadoop.fs.Path) in org.apache.hadoop.mapred.lib.TotalOrderPartitioner has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/SortByTemperatureUsingTotalOrderPartitioner.java:[47,5] org.apache.hadoop.filecache.DistributedCache in org.apache.hadoop.filecache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/SortByTemperatureUsingTotalOrderPartitioner.java:[47,21] addCacheFile(java.net.URI,org.apache.hadoop.conf.Configuration) in org.apache.hadoop.mapreduce.filecache.DistributedCache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/SortByTemperatureUsingTotalOrderPartitioner.java:[48,5] org.apache.hadoop.filecache.DistributedCache in org.apache.hadoop.filecache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/SortByTemperatureUsingTotalOrderPartitioner.java:[48,21] createSymlink(org.apache.hadoop.conf.Configuration) in org.apache.hadoop.mapreduce.filecache.DistributedCache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/MaxTemperatureByStationNameUsingDistributedCacheFileApi.java:[42,29] org.apache.hadoop.filecache.DistributedCache in org.apache.hadoop.filecache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/MaxTemperatureByStationNameUsingDistributedCacheFileApi.java:[42,45] getLocalCacheFiles(org.apache.hadoop.conf.Configuration) in org.apache.hadoop.mapreduce.filecache.DistributedCache has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/oldapi/MissingTemperatureFields.java:[31,42] org.apache.hadoop.mapred.Task.Counter in org.apache.hadoop.mapred.Task has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch09-mr-features/src/main/java/JoinRecordWithStationName.java:[29,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch09-mr-features ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch09-mr-features/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch09-mr-features ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/hadoop-book/ch09-mr-features/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch09-mr-features ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch09-mr-features ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch09-mr-features/target/ch09-mr-features-4.0.jar
[INFO] 
[INFO] ----------------------< com.hadoopbook:ch12-avro >----------------------
[INFO] Building Chapter 12: Avro 4.0                                     [9/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.7/avro-maven-plugin-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.7/avro-maven-plugin-1.7.7.pom (3.5 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.7/avro-maven-plugin-1.7.7.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.7/avro-maven-plugin-1.7.7.jar (24 kB at 75 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-assembly-plugin/2.4/maven-assembly-plugin-2.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-assembly-plugin/2.4/maven-assembly-plugin-2.4.pom (17 kB at 53 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-assembly-plugin/2.4/maven-assembly-plugin-2.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-assembly-plugin/2.4/maven-assembly-plugin-2.4.jar (226 kB at 691 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.7/avro-mapred-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.7/avro-mapred-1.7.7.pom (6.7 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.7/avro-ipc-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.7/avro-ipc-1.7.7.pom (5.2 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.4.0.Final/netty-3.4.0.Final.pom
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.4.0.Final/netty-3.4.0.Final.pom (25 kB at 51 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.7/avro-mapred-1.7.7-hadoop2.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.7/avro-ipc-1.7.7.jar
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.4.0.Final/netty-3.4.0.Final.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.7/avro-ipc-1.7.7-tests.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.7/avro-mapred-1.7.7-hadoop2.jar (181 kB at 521 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.7/avro-ipc-1.7.7.jar (193 kB at 513 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.7/avro-ipc-1.7.7-tests.jar (347 kB at 880 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.4.0.Final/netty-3.4.0.Final.jar (957 kB at 2.4 MB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch12-avro ---
[INFO] 
[INFO] --- avro-maven-plugin:1.7.7:schema (schemas) @ ch12-avro ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/2.0.10/maven-plugin-api-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/2.0.10/maven-plugin-api-2.0.10.pom (1.5 kB at 4.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.10/maven-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.10/maven-2.0.10.pom (24 kB at 86 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.10/maven-project-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.10/maven-project-2.0.10.pom (2.8 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.10/maven-settings-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.10/maven-settings-2.0.10.pom (2.2 kB at 8.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.10/maven-model-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.10/maven-model-2.0.10.pom (3.3 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-interpolation/1.1/plexus-interpolation-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-interpolation/1.1/plexus-interpolation-1.1.pom (1.4 kB at 5.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.10/maven-profile-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.10/maven-profile-2.0.10.pom (2.1 kB at 7.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.10/maven-artifact-manager-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.10/maven-artifact-manager-2.0.10.pom (2.7 kB at 9.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.10/maven-repository-metadata-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.10/maven-repository-metadata-2.0.10.pom (2.3 kB at 8.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.10/maven-artifact-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.10/maven-artifact-2.0.10.pom (1.6 kB at 5.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.10/maven-plugin-registry-2.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.10/maven-plugin-registry-2.0.10.pom (2.0 kB at 7.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.2.1/file-management-1.2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.2.1/file-management-1.2.1.pom (3.9 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-io/1.1/maven-shared-io-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-io/1.1/maven-shared-io-1.1.pom (4.1 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/8/maven-shared-components-8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/8/maven-shared-components-8.pom (2.7 kB at 9.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/7/maven-parent-7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/7/maven-parent-7.pom (21 kB at 77 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.2/maven-artifact-2.0.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.2/maven-artifact-2.0.2.pom (765 B at 2.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.2/maven-2.0.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.2/maven-2.0.2.pom (13 kB at 47 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.2/maven-artifact-manager-2.0.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.2/maven-artifact-manager-2.0.2.pom (1.4 kB at 5.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.2/maven-repository-metadata-2.0.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.2/maven-repository-metadata-2.0.2.pom (1.3 kB at 4.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon-provider-api/1.0-alpha-6/wagon-provider-api-1.0-alpha-6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon-provider-api/1.0-alpha-6/wagon-provider-api-1.0-alpha-6.pom (588 B at 2.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon/1.0-alpha-6/wagon-1.0-alpha-6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon/1.0-alpha-6/wagon-1.0-alpha-6.pom (6.4 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.4.6/plexus-utils-1.4.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.4.6/plexus-utils-1.4.6.pom (2.3 kB at 8.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.7/avro-compiler-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.7/avro-compiler-1.7.7.pom (4.4 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-api/1.6.4/slf4j-api-1.6.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-api/1.6.4/slf4j-api-1.6.4.pom (2.7 kB at 9.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-parent/1.6.4/slf4j-parent-1.6.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-parent/1.6.4/slf4j-parent-1.6.4.pom (12 kB at 42 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-simple/1.6.4/slf4j-simple-1.6.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-simple/1.6.4/slf4j-simple-1.6.4.pom (1.5 kB at 5.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/2.0.10/maven-plugin-api-2.0.10.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.10/maven-project-2.0.10.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.10/maven-settings-2.0.10.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.10/maven-profile-2.0.10.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.10/maven-model-2.0.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/2.0.10/maven-plugin-api-2.0.10.jar (13 kB at 44 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.10/maven-artifact-manager-2.0.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.10/maven-settings-2.0.10.jar (51 kB at 152 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.10/maven-repository-metadata-2.0.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.10/maven-profile-2.0.10.jar (37 kB at 109 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.10/maven-plugin-registry-2.0.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.10/maven-project-2.0.10.jar (123 kB at 358 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-interpolation/1.1/plexus-interpolation-1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.10/maven-model-2.0.10.jar (96 kB at 287 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.5.5/plexus-utils-1.5.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.10/maven-artifact-manager-2.0.10.jar (58 kB at 105 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.10/maven-artifact-2.0.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.10/maven-repository-metadata-2.0.10.jar (26 kB at 40 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.2.1/file-management-1.2.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-interpolation/1.1/plexus-interpolation-1.1.jar (35 kB at 55 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-io/1.1/maven-shared-io-1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.10/maven-plugin-registry-2.0.10.jar (30 kB at 46 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon-provider-api/1.0-alpha-6/wagon-provider-api-1.0-alpha-6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.5.5/plexus-utils-1.5.5.jar (251 kB at 376 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.7/avro-compiler-1.7.7.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.10/maven-artifact-2.0.10.jar (89 kB at 107 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-api/1.6.4/slf4j-api-1.6.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.2.1/file-management-1.2.1.jar (38 kB at 39 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-simple/1.6.4/slf4j-simple-1.6.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-io/1.1/maven-shared-io-1.1.jar (39 kB at 41 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon-provider-api/1.0-alpha-6/wagon-provider-api-1.0-alpha-6.jar (43 kB at 44 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.7/avro-compiler-1.7.7.jar (77 kB at 78 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-api/1.6.4/slf4j-api-1.6.4.jar (26 kB at 23 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/slf4j-simple/1.6.4/slf4j-simple-1.6.4.jar (7.7 kB at 5.9 kB/s)
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch12-avro ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch12-avro ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 10 source files to /home/hadoop/work/hadoop-book/ch12-avro/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroGenericMaxTemperature.java:[77,21] unchecked call to AvroKey(T) as a member of the raw type org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroGenericMaxTemperature.java:[77,20] unchecked method invocation: method write in interface org.apache.hadoop.mapreduce.TaskInputOutputContext is applied to given types
  required: KEYOUT,VALUEOUT
  found: org.apache.avro.mapred.AvroKey,org.apache.hadoop.io.NullWritable
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroGenericMaxTemperature.java:[77,21] unchecked conversion
  required: KEYOUT
  found:    org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroGenericMaxTemperature.java:[97,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroSort.java:[41,23] unchecked call to AvroKey(T) as a member of the raw type org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroSort.java:[41,22] unchecked method invocation: method write in interface org.apache.hadoop.mapreduce.TaskInputOutputContext is applied to given types
  required: KEYOUT,VALUEOUT
  found: org.apache.avro.mapred.AvroKey,org.apache.hadoop.io.NullWritable
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroSort.java:[41,23] unchecked conversion
  required: KEYOUT
  found:    org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch12-avro/src/main/java/AvroSort.java:[61,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch12-avro ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch12-avro/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch12-avro ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/hadoop-book/ch12-avro/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch12-avro ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch12-avro ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch12-avro/target/ch12-avro-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (job) @ ch12-avro ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/reporting/maven-reporting-api/2.2.1/maven-reporting-api-2.2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/reporting/maven-reporting-api/2.2.1/maven-reporting-api-2.2.1.pom (1.9 kB at 5.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/reporting/maven-reporting/2.2.1/maven-reporting-2.2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/reporting/maven-reporting/2.2.1/maven-reporting-2.2.1.pom (1.4 kB at 4.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1/doxia-sink-api-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1/doxia-sink-api-1.1.pom (2.0 kB at 6.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.1/doxia-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.1/doxia-1.1.pom (15 kB at 48 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1/doxia-logging-api-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1/doxia-logging-api-1.1.pom (1.6 kB at 5.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-container-default/1.0-alpha-30/plexus-container-default-1.0-alpha-30.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-container-default/1.0-alpha-30/plexus-container-default-1.0-alpha-30.pom (3.5 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-containers/1.0-alpha-30/plexus-containers-1.0-alpha-30.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-containers/1.0-alpha-30/plexus-containers-1.0-alpha-30.pom (1.9 kB at 5.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-classworlds/1.2-alpha-9/plexus-classworlds-1.2-alpha-9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-classworlds/1.2-alpha-9/plexus-classworlds-1.2-alpha-9.pom (3.2 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/1.0.10/plexus-1.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/1.0.10/plexus-1.0.10.pom (8.2 kB at 26 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.8/maven-artifact-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.8/maven-artifact-2.0.8.pom (1.6 kB at 5.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.8/maven-project-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.8/maven-project-2.0.8.pom (2.7 kB at 8.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.8/maven-settings-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.8/maven-settings-2.0.8.pom (2.1 kB at 6.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.8/maven-profile-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.8/maven-profile-2.0.8.pom (2.0 kB at 6.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.8/maven-artifact-manager-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.8/maven-artifact-manager-2.0.8.pom (2.7 kB at 8.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.8/maven-repository-metadata-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.8/maven-repository-metadata-2.0.8.pom (1.9 kB at 6.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.8/maven-plugin-registry-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.8/maven-plugin-registry-2.0.8.pom (2.0 kB at 6.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/2.0.8/maven-plugin-api-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/2.0.8/maven-plugin-api-2.0.8.pom (1.5 kB at 4.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/2.1/plexus-utils-2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/2.1/plexus-utils-2.1.pom (4.0 kB at 13 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-archiver/2.2/plexus-archiver-2.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-archiver/2.2/plexus-archiver-2.2.pom (3.2 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-components/1.1.20/plexus-components-1.1.20.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-components/1.1.20/plexus-components-1.1.20.pom (3.0 kB at 9.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/3.1/plexus-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/3.1/plexus-3.1.pom (19 kB at 58 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/3.0.7/plexus-utils-3.0.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/3.0.7/plexus-utils-3.0.7.pom (2.5 kB at 7.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-io/2.0.4/plexus-io-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-io/2.0.4/plexus-io-2.0.4.pom (1.7 kB at 5.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.1/file-management-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.1/file-management-1.1.pom (2.7 kB at 8.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/4/maven-shared-components-4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/4/maven-shared-components-4.pom (2.2 kB at 7.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/4/maven-parent-4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/4/maven-parent-4.pom (10.0 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-io/1.0/maven-shared-io-1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-io/1.0/maven-shared-io-1.0.pom (3.0 kB at 9.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.2/plexus-utils-1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/1.2/plexus-utils-1.2.pom (767 B at 2.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/1.0.5/plexus-1.0.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/1.0.5/plexus-1.0.5.pom (5.9 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-io/2.0.6/plexus-io-2.0.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-io/2.0.6/plexus-io-2.0.6.pom (2.2 kB at 7.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-repository-builder/1.0-alpha-2/maven-repository-builder-1.0-alpha-2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-repository-builder/1.0-alpha-2/maven-repository-builder-1.0-alpha-2.pom (3.2 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/7/maven-shared-components-7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/7/maven-shared-components-7.pom (2.6 kB at 8.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.4/maven-artifact-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/2.0.4/maven-artifact-2.0.4.pom (765 B at 2.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.4/maven-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/2.0.4/maven-2.0.4.pom (12 kB at 37 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.4/maven-artifact-manager-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.4/maven-artifact-manager-2.0.4.pom (1.4 kB at 4.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.4/maven-repository-metadata-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/2.0.4/maven-repository-metadata-2.0.4.pom (1.5 kB at 4.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.4/maven-project-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.4/maven-project-2.0.4.pom (1.8 kB at 5.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.4/maven-settings-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/2.0.4/maven-settings-2.0.4.pom (1.6 kB at 5.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.4/maven-model-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/2.0.4/maven-model-2.0.4.pom (2.7 kB at 8.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.4/maven-profile-2.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.4/maven-profile-2.0.4.pom (1.6 kB at 5.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-common-artifact-filters/1.0-alpha-1/maven-common-artifact-filters-1.0-alpha-1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-common-artifact-filters/1.0-alpha-1/maven-common-artifact-filters-1.0-alpha-1.pom (1.8 kB at 5.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/6/maven-shared-components-6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/6/maven-shared-components-6.pom (3.1 kB at 9.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/reporting/maven-reporting-api/2.2.1/maven-reporting-api-2.2.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1/doxia-logging-api-1.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-archiver/2.2/plexus-archiver-2.2.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.1/file-management-1.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1/doxia-sink-api-1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1/doxia-logging-api-1.1.jar (11 kB at 40 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-io/2.0.6/plexus-io-2.0.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/reporting/maven-reporting-api/2.2.1/maven-reporting-api-2.2.1.jar (9.8 kB at 30 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-repository-builder/1.0-alpha-2/maven-repository-builder-1.0-alpha-2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1/doxia-sink-api-1.1.jar (13 kB at 39 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/1.1/file-management-1.1.jar (31 kB at 93 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-archiver/2.2/plexus-archiver-2.2.jar (185 kB at 544 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-io/2.0.6/plexus-io-2.0.6.jar (58 kB at 103 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-repository-builder/1.0-alpha-2/maven-repository-builder-1.0-alpha-2.jar (23 kB at 36 kB/s)
[INFO] Reading assembly descriptor: src/main/assembly/job.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch12-avro/target/../../avro-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch12-avro/target/../../avro-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch12-avro/target/ch12-avro-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch12-avro/target/../../avro-examples.jar
[INFO] 
[INFO] --------------------< com.hadoopbook:ch13-parquet >---------------------
[INFO] Building Chapter 13: Parquet 4.0                                 [10/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-avro/1.5.0/parquet-avro-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-avro/1.5.0/parquet-avro-1.5.0.pom (3.6 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet/1.5.0/parquet-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet/1.5.0/parquet-1.5.0.pom (15 kB at 46 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-column/1.5.0/parquet-column-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-column/1.5.0/parquet-column-1.5.0.pom (3.5 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-common/1.5.0/parquet-common-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-common/1.5.0/parquet-common-1.5.0.pom (914 B at 2.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-encoding/1.5.0/parquet-encoding-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-encoding/1.5.0/parquet-encoding-1.5.0.pom (2.1 kB at 6.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-generator/1.5.0/parquet-generator-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-generator/1.5.0/parquet-generator-1.5.0.pom (949 B at 3.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.5/commons-codec-1.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.5/commons-codec-1.5.pom (10 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/20/commons-parent-20.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/20/commons-parent-20.pom (33 kB at 102 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-hadoop/1.5.0/parquet-hadoop-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-hadoop/1.5.0/parquet-hadoop-1.5.0.pom (2.5 kB at 7.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-format/2.1.0/parquet-format-2.1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-format/2.1.0/parquet-format-2.1.0.pom (6.4 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-jackson/1.5.0/parquet-jackson-1.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-jackson/1.5.0/parquet-jackson-1.5.0.pom (3.1 kB at 8.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.11/jackson-mapper-asl-1.9.11.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.11/jackson-mapper-asl-1.9.11.pom (1.2 kB at 3.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.11/jackson-core-asl-1.9.11.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.11/jackson-core-asl-1.9.11.pom (1.0 kB at 3.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-avro/1.5.0/parquet-avro-1.5.0.jar
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-common/1.5.0/parquet-common-1.5.0.jar
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-encoding/1.5.0/parquet-encoding-1.5.0.jar
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-column/1.5.0/parquet-column-1.5.0.jar
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-generator/1.5.0/parquet-generator-1.5.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-common/1.5.0/parquet-common-1.5.0.jar (12 kB at 39 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.5/commons-codec-1.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-avro/1.5.0/parquet-avro-1.5.0.jar (42 kB at 127 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-hadoop/1.5.0/parquet-hadoop-1.5.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-encoding/1.5.0/parquet-encoding-1.5.0.jar (273 kB at 802 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-jackson/1.5.0/parquet-jackson-1.5.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-generator/1.5.0/parquet-generator-1.5.0.jar (10 kB at 29 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.11/jackson-mapper-asl-1.9.11.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-column/1.5.0/parquet-column-1.5.0.jar (748 kB at 1.9 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.11/jackson-core-asl-1.9.11.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.5/commons-codec-1.5.jar (73 kB at 126 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-format/2.1.0/parquet-format-2.1.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.9.11/jackson-mapper-asl-1.9.11.jar (780 kB at 1.1 MB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.9.11/jackson-core-asl-1.9.11.jar (232 kB at 307 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-jackson/1.5.0/parquet-jackson-1.5.0.jar (1.0 MB at 1.2 MB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-hadoop/1.5.0/parquet-hadoop-1.5.0.jar (160 kB at 171 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/parquet-format/2.1.0/parquet-format-2.1.0.jar (340 kB at 343 kB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch13-parquet ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch13-parquet ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch13-parquet/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch13-parquet ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 4 source files to /home/hadoop/work/hadoop-book/ch13-parquet/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch13-parquet/src/main/java/TextToParquetWithAvro.java:[56,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch13-parquet/src/main/java/ParquetToTextWithAvro.java:[39,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch13-parquet/src/main/java/TextToParquetWithExample.java:[54,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch13-parquet/src/main/java/ParquetToTextWithExample.java:[39,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch13-parquet ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 4 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch13-parquet ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /home/hadoop/work/hadoop-book/ch13-parquet/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch13-parquet ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch13-parquet ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch13-parquet/target/ch13-parquet-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (job) @ ch13-parquet ---
[INFO] Reading assembly descriptor: src/main/assembly/job.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch13-parquet/target/../../parquet-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch13-parquet/target/../../parquet-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch13-parquet/target/ch13-parquet-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch13-parquet/target/../../parquet-examples.jar
[INFO] 
[INFO] ---------------------< com.hadoopbook:ch15-sqoop >----------------------
[INFO] Building Chapter 15: Sqoop 4.0                                   [11/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/sqoop/sqoop/1.4.5/sqoop-1.4.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/sqoop/sqoop/1.4.5/sqoop-1.4.5.pom (1.6 kB at 4.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/sqoop/sqoop/1.4.5/sqoop-1.4.5-hadoop200.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/sqoop/sqoop/1.4.5/sqoop-1.4.5-hadoop200.jar (967 kB at 3.2 MB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch15-sqoop ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch15-sqoop ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch15-sqoop ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /home/hadoop/work/hadoop-book/ch15-sqoop/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[11,30] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[12,30] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[13,30] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[14,30] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[15,30] com.cloudera.sqoop.lib.BooleanParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[16,30] com.cloudera.sqoop.lib.BlobRef in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[17,30] com.cloudera.sqoop.lib.ClobRef in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[18,30] com.cloudera.sqoop.lib.LargeObjectLoader in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[19,30] com.cloudera.sqoop.lib.SqoopRecord in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetId.java:[4,30] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetId.java:[4,30] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetId.java:[4,43] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetIdGenericAvro.java:[48,21] unchecked call to AvroKey(T) as a member of the raw type org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetIdGenericAvro.java:[48,20] unchecked method invocation: method write in interface org.apache.hadoop.mapreduce.TaskInputOutputContext is applied to given types
  required: KEYOUT,VALUEOUT
  found: org.apache.avro.mapred.AvroKey,org.apache.avro.mapred.AvroValue<org.apache.avro.generic.GenericRecord>
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetIdGenericAvro.java:[48,21] unchecked conversion
  required: KEYOUT
  found:    org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetIdGenericAvro.java:[67,21] unchecked call to AvroKey(T) as a member of the raw type org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetIdGenericAvro.java:[67,20] unchecked method invocation: method write in interface org.apache.hadoop.mapreduce.TaskInputOutputContext is applied to given types
  required: KEYOUT,VALUEOUT
  found: org.apache.avro.mapred.AvroKey,org.apache.hadoop.io.NullWritable
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetIdGenericAvro.java:[67,21] unchecked conversion
  required: KEYOUT
  found:    org.apache.avro.mapred.AvroKey
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetIdGenericAvro.java:[82,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[37,29] com.cloudera.sqoop.lib.SqoopRecord in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[143,15] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[144,24] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[145,18] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[146,24] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[147,20] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[148,27] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[151,15] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[152,24] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[153,18] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[154,24] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[155,20] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[156,27] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[158,32] com.cloudera.sqoop.lib.LargeObjectLoader in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[161,33] com.cloudera.sqoop.lib.LargeObjectLoader in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[169,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[170,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[171,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[172,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[173,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[174,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[178,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[179,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[180,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[181,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[182,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[183,5] com.cloudera.sqoop.lib.JdbcWritableBridge in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[201,40] com.cloudera.sqoop.lib.BigDecimalSerializer in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[236,27] com.cloudera.sqoop.lib.BigDecimalSerializer in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[274,27] com.cloudera.sqoop.lib.BigDecimalSerializer in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[295,24] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[295,62] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[299,26] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[305,26] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[308,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[310,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[312,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[314,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[316,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[318,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[324,25] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[325,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[327,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[329,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[331,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[333,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[335,17] com.cloudera.sqoop.lib.FieldFormatter in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[337,24] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[337,61] com.cloudera.sqoop.lib.DelimiterSet in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[338,11] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[339,43] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[339,55] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[341,27] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[347,51] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[347,63] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[349,27] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[355,46] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[355,58] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[357,27] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[363,46] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[363,58] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[365,27] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[371,49] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[371,61] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[373,27] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[379,49] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[379,61] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/Widget.java:[381,27] com.cloudera.sqoop.lib.RecordParser in com.cloudera.sqoop.lib has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetId.java:[25,16] com.cloudera.sqoop.lib.RecordParser.ParseError in com.cloudera.sqoop.lib.RecordParser has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch15-sqoop/src/main/java/MaxWidgetId.java:[78,15] Job(org.apache.hadoop.conf.Configuration) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch15-sqoop ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch15-sqoop/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch15-sqoop ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch15-sqoop ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch15-sqoop ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch15-sqoop/target/ch15-sqoop-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ ch15-sqoop ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/jar.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch15-sqoop/target/../../sqoop-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch15-sqoop/target/../../sqoop-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch15-sqoop/target/ch15-sqoop-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch15-sqoop/target/../../sqoop-examples.jar
[INFO] 
[INFO] ----------------------< com.hadoopbook:ch16-pig >-----------------------
[INFO] Building Chapter 16: Pig 4.0                                     [12/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/pig/pig/0.13.0/pig-0.13.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/pig/pig/0.13.0/pig-0.13.0.pom (5.0 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/1.4.1/commons-net-1.4.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/1.4.1/commons-net-1.4.1.pom (4.8 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/oro/oro/2.0.8/oro-2.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/oro/oro/2.0.8/oro-2.0.8.pom (140 B at 505 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.12/jasper-runtime-5.5.12.pom
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.12/jasper-runtime-5.5.12.pom (155 B at 559 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.12/jasper-compiler-5.5.12.pom
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.12/jasper-compiler-5.5.12.pom (156 B at 557 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-api-2.1/6.1.14/jsp-api-2.1-6.1.14.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-api-2.1/6.1.14/jsp-api-2.1-6.1.14.pom (4.3 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/project/6.1.14/project-6.1.14.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/project/6.1.14/project-6.1.14.pom (8.8 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/servlet-api-2.5/6.1.14/servlet-api-2.5-6.1.14.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/servlet-api-2.5/6.1.14/servlet-api-2.5-6.1.14.pom (3.4 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-2.1/6.1.14/jsp-2.1-6.1.14.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-2.1/6.1.14/jsp-2.1-6.1.14.pom (6.2 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.7.1/jets3t-0.7.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.7.1/jets3t-0.7.1.pom (1.5 kB at 5.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.3/commons-codec-1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.3/commons-codec-1.3.pom (6.1 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/sf/kosmosfs/kfs/0.3/kfs-0.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/sf/kosmosfs/kfs/0.3/kfs-0.3.pom (685 B at 2.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/hsqldb/hsqldb/1.8.0.10/hsqldb-1.8.0.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/hsqldb/hsqldb/1.8.0.10/hsqldb-1.8.0.10.pom (600 B at 2.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-runtime/3.4/antlr-runtime-3.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-runtime/3.4/antlr-runtime-3.4.pom (3.1 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-master/3.4/antlr-master-3.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-master/3.4/antlr-master-3.4.pom (9.4 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/stringtemplate/3.2.1/stringtemplate-3.2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/stringtemplate/3.2.1/stringtemplate-3.2.1.pom (7.5 kB at 27 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/ST4/4.0.4/ST4-4.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/ST4/4.0.4/ST4-4.0.4.pom (9.6 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-runtime/3.3/antlr-runtime-3.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-runtime/3.3/antlr-runtime-3.3.pom (4.1 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-master/3.3/antlr-master-3.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-master/3.3/antlr-master-3.3.pom (9.4 kB at 33 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/joda-time/joda-time/2.1/joda-time-2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/joda-time/joda-time/2.1/joda-time-2.1.pom (15 kB at 55 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/pig/pig/0.13.0/pig-0.13.0.jar
Downloading from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/1.4.1/commons-net-1.4.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.12/jasper-runtime-5.5.12.jar
Downloading from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.12/jasper-compiler-5.5.12.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-api-2.1/6.1.14/jsp-api-2.1-6.1.14.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-api-2.1/6.1.14/jsp-api-2.1-6.1.14.jar (135 kB at 390 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-2.1/6.1.14/jsp-2.1-6.1.14.jar
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-runtime/5.5.12/jasper-runtime-5.5.12.jar (77 kB at 218 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.7.1/jets3t-0.7.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/tomcat/jasper-compiler/5.5.12/jasper-compiler-5.5.12.jar (405 kB at 1.1 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/servlet-api-2.5/6.1.14/servlet-api-2.5-6.1.14.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/1.4.1/commons-net-1.4.1.jar (181 kB at 461 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/sf/kosmosfs/kfs/0.3/kfs-0.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/pig/pig/0.13.0/pig-0.13.0.jar (3.8 MB at 6.9 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/hsqldb/hsqldb/1.8.0.10/hsqldb-1.8.0.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/servlet-api-2.5/6.1.14/servlet-api-2.5-6.1.14.jar (132 kB at 190 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/oro/oro/2.0.8/oro-2.0.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jsp-2.1/6.1.14/jsp-2.1-6.1.14.jar (1.0 MB at 1.5 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-runtime/3.4/antlr-runtime-3.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.7.1/jets3t-0.7.1.jar (378 kB at 532 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/stringtemplate/3.2.1/stringtemplate-3.2.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/net/sf/kosmosfs/kfs/0.3/kfs-0.3.jar (12 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/antlr/ST4/4.0.4/ST4-4.0.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/hsqldb/hsqldb/1.8.0.10/hsqldb-1.8.0.10.jar (707 kB at 802 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/joda-time/joda-time/2.1/joda-time-2.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/antlr-runtime/3.4/antlr-runtime-3.4.jar (164 kB at 164 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.2.4/httpcore-4.2.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/oro/oro/2.0.8/oro-2.0.8.jar (65 kB at 64 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/stringtemplate/3.2.1/stringtemplate-3.2.1.jar (149 kB at 143 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/antlr/ST4/4.0.4/ST4-4.0.4.jar (237 kB at 225 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/joda-time/joda-time/2.1/joda-time-2.1.jar (570 kB at 465 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.2.4/httpcore-4.2.4.jar (227 kB at 176 kB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch16-pig ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch16-pig ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch16-pig/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch16-pig ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 4 source files to /home/hadoop/work/hadoop-book/ch16-pig/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch16-pig ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch16-pig/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch16-pig ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 2 source files to /home/hadoop/work/hadoop-book/ch16-pig/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch16-pig ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch16-pig ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch16-pig/target/ch16-pig-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ ch16-pig ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/jar.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch16-pig/target/../../pig-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch16-pig/target/../../pig-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch16-pig/target/ch16-pig-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch16-pig/target/../../pig-examples.jar
[INFO] 
[INFO] ----------------------< com.hadoopbook:ch17-hive >----------------------
[INFO] Building Chapter 17: Hive 4.0                                    [13/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-common/0.13.1/hive-common-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-common/0.13.1/hive-common-0.13.1.pom (5.2 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive/0.13.1/hive-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive/0.13.1/hive-0.13.1.pom (37 kB at 132 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-shims/0.13.1/hive-shims-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-shims/0.13.1/hive-shims-0.13.1.pom (2.4 kB at 5.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common/0.13.1/hive-shims-common-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common/0.13.1/hive-shims-common-0.13.1.pom (2.3 kB at 8.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libthrift/0.9.0/libthrift-0.9.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libthrift/0.9.0/libthrift-0.9.0.pom (3.3 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.5/commons-lang-2.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-lang/commons-lang/2.5/commons-lang-2.5.pom (17 kB at 62 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/12/commons-parent-12.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/12/commons-parent-12.pom (27 kB at 96 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.1.3/httpclient-4.1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.1.3/httpclient-4.1.3.pom (6.0 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-client/4.1.3/httpcomponents-client-4.1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-client/4.1.3/httpcomponents-client-4.1.3.pom (12 kB at 43 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/project/5/project-5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/project/5/project-5.pom (23 kB at 81 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.4/httpcore-4.1.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.4/httpcore-4.1.4.pom (7.4 kB at 26 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.1.4/httpcomponents-core-4.1.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.1.4/httpcomponents-core-4.1.4.pom (9.8 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.3/httpcore-4.1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.3/httpcore-4.1.3.pom (7.4 kB at 26 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.1.3/httpcomponents-core-4.1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcomponents-core/4.1.3/httpcomponents-core-4.1.3.pom (9.8 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20/0.13.1/hive-shims-0.20-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20/0.13.1/hive-shims-0.20-0.13.1.pom (2.4 kB at 8.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common-secure/0.13.1/hive-shims-common-secure-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common-secure/0.13.1/hive-shims-common-secure-0.13.1.pom (3.0 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20S/0.13.1/hive-shims-0.20S-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20S/0.13.1/hive-shims-0.20S-0.13.1.pom (2.2 kB at 7.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.23/0.13.1/hive-shims-0.23-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.23/0.13.1/hive-shims-0.23-0.13.1.pom (5.1 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-exec/0.13.1/hive-exec-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-exec/0.13.1/hive-exec-0.13.1.pom (18 kB at 60 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-ant/0.13.1/hive-ant-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-ant/0.13.1/hive-ant-0.13.1.pom (1.9 kB at 6.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant/1.9.1/ant-1.9.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant/1.9.1/ant-1.9.1.pom (9.5 kB at 30 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant-parent/1.9.1/ant-parent-1.9.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant-parent/1.9.1/ant-parent-1.9.1.pom (5.6 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant-launcher/1.9.1/ant-launcher-1.9.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant-launcher/1.9.1/ant-launcher-1.9.1.pom (2.3 kB at 8.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/velocity/velocity/1.5/velocity-1.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/velocity/velocity/1.5/velocity-1.5.pom (7.8 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-collections/commons-collections/3.1/commons-collections-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-collections/commons-collections/3.1/commons-collections-3.1.pom (6.1 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-metastore/0.13.1/hive-metastore-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-metastore/0.13.1/hive-metastore-0.13.1.pom (7.5 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-serde/0.13.1/hive-serde-0.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-serde/0.13.1/hive-serde-0.13.1.pom (5.6 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/jolbox/bonecp/0.8.0.RELEASE/bonecp-0.8.0.RELEASE.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/jolbox/bonecp/0.8.0.RELEASE/bonecp-0.8.0.RELEASE.pom (4.7 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/jolbox/bonecp-parent/0.8.0.RELEASE/bonecp-parent-0.8.0.RELEASE.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/jolbox/bonecp-parent/0.8.0.RELEASE/bonecp-parent-0.8.0.RELEASE.pom (15 kB at 55 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/derby/derby/10.10.1.1/derby-10.10.1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/derby/derby/10.10.1.1/derby-10.10.1.1.pom (2.2 kB at 8.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/derby/derby-project/10.10.1.1/derby-project-10.10.1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/derby/derby-project/10.10.1.1/derby-project-10.10.1.1.pom (5.9 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/datanucleus/datanucleus-api-jdo/3.2.6/datanucleus-api-jdo-3.2.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/datanucleus/datanucleus-api-jdo/3.2.6/datanucleus-api-jdo-3.2.6.pom (9.4 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/jdo/jdo-api/3.0.1/jdo-api-3.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/jdo/jdo-api/3.0.1/jdo-api-3.0.1.pom (7.9 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/transaction/jta/1.1/jta-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/transaction/jta/1.1/jta-1.1.pom (598 B at 2.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libfb303/0.9.0/libfb303-0.9.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libfb303/0.9.0/libfb303-0.9.0.pom (2.7 kB at 9.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.0.1/commons-httpclient-3.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.0.1/commons-httpclient-3.0.1.pom (8.1 kB at 29 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/groovy/groovy-all/2.1.6/groovy-all-2.1.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/groovy/groovy-all/2.1.6/groovy-all-2.1.6.pom (18 kB at 64 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/stax/stax-api/1.0.1/stax-api-1.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/stax/stax-api/1.0.1/stax-api-1.0.1.pom (1.5 kB at 5.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-common/0.13.1/hive-common-0.13.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-shims/0.13.1/hive-shims-0.13.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common/0.13.1/hive-shims-common-0.13.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20/0.13.1/hive-shims-0.20-0.13.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common-secure/0.13.1/hive-shims-common-secure-0.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-common/0.13.1/hive-common-0.13.1.jar (170 kB at 553 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20S/0.13.1/hive-shims-0.20S-0.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common/0.13.1/hive-shims-common-0.13.1.jar (39 kB at 118 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.23/0.13.1/hive-shims-0.23-0.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-shims/0.13.1/hive-shims-0.13.1.jar (7.6 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-exec/0.13.1/hive-exec-0.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-common-secure/0.13.1/hive-shims-common-secure-0.13.1.jar (75 kB at 225 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20/0.13.1/hive-shims-0.20-0.13.1.jar (30 kB at 89 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/velocity/velocity/1.5/velocity-1.5.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-ant/0.13.1/hive-ant-0.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.23/0.13.1/hive-shims-0.23-0.13.1.jar (38 kB at 60 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-metastore/0.13.1/hive-metastore-0.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-ant/0.13.1/hive-ant-0.13.1.jar (37 kB at 57 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-serde/0.13.1/hive-serde-0.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/velocity/velocity/1.5/velocity-1.5.jar (392 kB at 594 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/jolbox/bonecp/0.8.0.RELEASE/bonecp-0.8.0.RELEASE.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/shims/hive-shims-0.20S/0.13.1/hive-shims-0.20S-0.13.1.jar (25 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/derby/derby/10.10.1.1/derby-10.10.1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/jolbox/bonecp/0.8.0.RELEASE/bonecp-0.8.0.RELEASE.jar (111 kB at 97 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/datanucleus/datanucleus-api-jdo/3.2.6/datanucleus-api-jdo-3.2.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-serde/0.13.1/hive-serde-0.13.1.jar (731 kB at 428 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/jdo/jdo-api/3.0.1/jdo-api-3.0.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/datanucleus/datanucleus-api-jdo/3.2.6/datanucleus-api-jdo-3.2.6.jar (340 kB at 192 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/transaction/jta/1.1/jta-1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/derby/derby/10.10.1.1/derby-10.10.1.1.jar (2.8 MB at 1.5 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libthrift/0.9.0/libthrift-0.9.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-exec/0.13.1/hive-exec-0.13.1.jar (15 MB at 7.9 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.1.3/httpclient-4.1.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hive/hive-metastore/0.13.1/hive-metastore-0.13.1.jar (4.7 MB at 2.4 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.3/httpcore-4.1.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/javax/jdo/jdo-api/3.0.1/jdo-api-3.0.1.jar (201 kB at 99 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.0.1/commons-httpclient-3.0.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/javax/transaction/jta/1.1/jta-1.1.jar (15 kB at 7.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant/1.9.1/ant-1.9.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libthrift/0.9.0/libthrift-0.9.0.jar (348 kB at 161 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant-launcher/1.9.1/ant-launcher-1.9.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpclient/4.1.3/httpclient-4.1.3.jar (353 kB at 156 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libfb303/0.9.0/libfb303-0.9.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore/4.1.3/httpcore-4.1.3.jar (181 kB at 78 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/jline/jline/0.9.94/jline-0.9.94.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-httpclient/commons-httpclient/3.0.1/commons-httpclient-3.0.1.jar (280 kB at 119 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.7.0.Final/netty-3.7.0.Final.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant-launcher/1.9.1/ant-launcher-1.9.1.jar (18 kB at 7.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/groovy/groovy-all/2.1.6/groovy-all-2.1.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/ant/ant/1.9.1/ant-1.9.1.jar (2.0 MB at 806 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/stax/stax-api/1.0.1/stax-api-1.0.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/thrift/libfb303/0.9.0/libfb303-0.9.0.jar (275 kB at 106 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/jline/jline/0.9.94/jline-0.9.94.jar (87 kB at 33 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.7.0.Final/netty-3.7.0.Final.jar (1.2 MB at 444 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/stax/stax-api/1.0.1/stax-api-1.0.1.jar (27 kB at 9.5 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/groovy/groovy-all/2.1.6/groovy-all-2.1.6.jar (6.4 MB at 2.2 MB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch17-hive ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch17-hive ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch17-hive/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch17-hive ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /home/hadoop/work/hadoop-book/ch17-hive/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch17-hive/src/main/java/com/hadoopbook/hive/Maximum.java:[3,38] org.apache.hadoop.hive.ql.exec.UDAF in org.apache.hadoop.hive.ql.exec has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch17-hive/src/main/java/com/hadoopbook/hive/Mean.java:[3,38] org.apache.hadoop.hive.ql.exec.UDAF in org.apache.hadoop.hive.ql.exec has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch17-hive/src/main/java/com/hadoopbook/hive/Maximum.java:[7,30] org.apache.hadoop.hive.ql.exec.UDAF in org.apache.hadoop.hive.ql.exec has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch17-hive/src/main/java/com/hadoopbook/hive/Mean.java:[7,27] org.apache.hadoop.hive.ql.exec.UDAF in org.apache.hadoop.hive.ql.exec has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch17-hive ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch17-hive/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch17-hive ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch17-hive ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch17-hive ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch17-hive/target/ch17-hive-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ ch17-hive ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/jar.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch17-hive/target/../../hive-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch17-hive/target/../../hive-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch17-hive/target/ch17-hive-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch17-hive/target/../../hive-examples.jar
[INFO] 
[INFO] ---------------------< com.hadoopbook:ch18-crunch >---------------------
[INFO] Building Chapter 18: Crunch 4.0                                  [14/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-core/0.11.0-hadoop2/crunch-core-0.11.0-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-core/0.11.0-hadoop2/crunch-core-0.11.0-hadoop2.pom (6.0 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-parent/0.11.0-hadoop2/crunch-parent-0.11.0-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-parent/0.11.0-hadoop2/crunch-parent-0.11.0-hadoop2.pom (31 kB at 78 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-avro/1.7.7/trevni-avro-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-avro/1.7.7/trevni-avro-1.7.7.pom (5.5 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-java/1.7.7/trevni-java-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-java/1.7.7/trevni-java-1.7.7.pom (3.2 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-core/1.7.7/trevni-core-1.7.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-core/1.7.7/trevni-core-1.7.7.pom (2.2 kB at 5.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.16.1-GA/javassist-3.16.1-GA.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.16.1-GA/javassist-3.16.1-GA.pom (9.0 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-test/0.11.0-hadoop2/crunch-test-0.11.0-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-test/0.11.0-hadoop2/crunch-test-0.11.0-hadoop2.pom (2.8 kB at 8.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mockito/mockito-all/1.9.0/mockito-all-1.9.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/mockito/mockito-all/1.9.0/mockito-all-1.9.0.pom (1.0 kB at 3.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-core/0.11.0-hadoop2/crunch-core-0.11.0-hadoop2.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.7/avro-mapred-1.7.7-hadoop1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-avro/1.7.7/trevni-avro-1.7.7.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-core/1.7.7/trevni-core-1.7.7.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.16.1-GA/javassist-3.16.1-GA.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-core/1.7.7/trevni-core-1.7.7.jar (47 kB at 161 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-test/0.11.0-hadoop2/crunch-test-0.11.0-hadoop2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/trevni-avro/1.7.7/trevni-avro-1.7.7.jar (43 kB at 129 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mockito/mockito-all/1.9.0/mockito-all-1.9.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-core/0.11.0-hadoop2/crunch-core-0.11.0-hadoop2.jar (751 kB at 2.0 MB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/javassist/javassist/3.16.1-GA/javassist-3.16.1-GA.jar (659 kB at 1.7 MB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.7/avro-mapred-1.7.7-hadoop1.jar (181 kB at 401 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/crunch/crunch-test/0.11.0-hadoop2/crunch-test-0.11.0-hadoop2.jar (607 kB at 1.0 MB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/mockito/mockito-all/1.9.0/mockito-all-1.9.0.jar (1.5 MB at 2.0 MB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch18-crunch ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch18-crunch ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch18-crunch/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch18-crunch ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 14 source files to /home/hadoop/work/hadoop-book/ch18-crunch/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch18-crunch ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 10 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch18-crunch ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 19 source files to /home/hadoop/work/hadoop-book/ch18-crunch/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch18-crunch ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch18-crunch ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch18-crunch/target/ch18-crunch-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ ch18-crunch ---
[INFO] Reading assembly descriptor: src/main/assembly/hadoop-job.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch18-crunch/target/ch18-crunch-4.0-job.jar
[INFO] 
[INFO] ---------------------< com.hadoopbook:ch19-spark >----------------------
[INFO] Building Chapter 19: Spark 4.0                                   [15/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/net/alchim31/maven/scala-maven-plugin/3.1.6/scala-maven-plugin-3.1.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/alchim31/maven/scala-maven-plugin/3.1.6/scala-maven-plugin-3.1.6.pom (16 kB at 40 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/alchim31/maven/scala-maven-plugin/3.1.6/scala-maven-plugin-3.1.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/net/alchim31/maven/scala-maven-plugin/3.1.6/scala-maven-plugin-3.1.6.jar (108 kB at 331 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest-maven-plugin/1.0/scalatest-maven-plugin-1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest-maven-plugin/1.0/scalatest-maven-plugin-1.0.pom (7.6 kB at 24 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest-maven-plugin/1.0/scalatest-maven-plugin-1.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest-maven-plugin/1.0/scalatest-maven-plugin-1.0.jar (24 kB at 72 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.6/avro-maven-plugin-1.7.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.6/avro-maven-plugin-1.7.6.pom (3.5 kB at 8.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-parent/1.7.6/avro-parent-1.7.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-parent/1.7.6/avro-parent-1.7.6.pom (19 kB at 60 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-toplevel/1.7.6/avro-toplevel-1.7.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-toplevel/1.7.6/avro-toplevel-1.7.6.pom (9.6 kB at 27 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.6/avro-maven-plugin-1.7.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-maven-plugin/1.7.6/avro-maven-plugin-1.7.6.jar (24 kB at 74 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/spark/spark-core_2.10/1.1.0/spark-core_2.10-1.1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/spark/spark-core_2.10/1.1.0/spark-core_2.10-1.1.0.pom (11 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/spark/spark-parent/1.1.0/spark-parent-1.1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/spark/spark-parent/1.1.0/spark-parent-1.1.0.pom (41 kB at 126 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/1.0.4/hadoop-client-1.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/1.0.4/hadoop-client-1.0.4.pom (3.5 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-core/1.0.4/hadoop-core-1.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-core/1.0.4/hadoop-core-1.0.4.pom (4.4 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math/2.1/commons-math-2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math/2.1/commons-math-2.1.pom (10 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/14/commons-parent-14.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/14/commons-parent-14.pom (31 kB at 99 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.0.1/jackson-mapper-asl-1.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.0.1/jackson-mapper-asl-1.0.1.pom (1.2 kB at 3.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.0.1/jackson-core-asl-1.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.0.1/jackson-core-asl-1.0.1.pom (1.0 kB at 3.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-recipes/2.4.0/curator-recipes-2.4.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-recipes/2.4.0/curator-recipes-2.4.0.pom (2.4 kB at 7.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/curator/apache-curator/2.4.0/apache-curator-2.4.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/curator/apache-curator/2.4.0/apache-curator-2.4.0.pom (21 kB at 65 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-framework/2.4.0/curator-framework-2.4.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-framework/2.4.0/curator-framework-2.4.0.pom (2.2 kB at 7.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-client/2.4.0/curator-client-2.4.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-client/2.4.0/curator-client-2.4.0.pom (2.3 kB at 7.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-plus/8.1.14.v20131031/jetty-plus-8.1.14.v20131031.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-plus/8.1.14.v20131031/jetty-plus-8.1.14.v20131031.pom (3.6 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.transaction/1.1.1.v201105210645/javax.transaction-1.1.1.v201105210645.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.transaction/1.1.1.v201105210645/javax.transaction-1.1.1.v201105210645.pom (740 B at 2.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-jndi/8.1.14.v20131031/jetty-jndi-8.1.14.v20131031.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-jndi/8.1.14.v20131031/jetty-jndi-8.1.14.v20131031.pom (2.7 kB at 7.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.mail.glassfish/1.4.1.v201005082020/javax.mail.glassfish-1.4.1.v201005082020.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.mail.glassfish/1.4.1.v201005082020/javax.mail.glassfish-1.4.1.v201005082020.pom (954 B at 2.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.activation/1.1.0.v201105071233/javax.activation-1.1.0.v201105071233.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.activation/1.1.0.v201105071233/javax.activation-1.1.0.v201105071233.pom (734 B at 2.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-lang3/3.3.2/commons-lang3-3.3.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-lang3/3.3.2/commons-lang3-3.3.2.pom (20 kB at 64 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/33/commons-parent-33.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/33/commons-parent-33.pom (53 kB at 164 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/jul-to-slf4j/1.7.5/jul-to-slf4j-1.7.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/jul-to-slf4j/1.7.5/jul-to-slf4j-1.7.5.pom (2.0 kB at 6.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/jcl-over-slf4j/1.7.5/jcl-over-slf4j-1.7.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/jcl-over-slf4j/1.7.5/jcl-over-slf4j-1.7.5.pom (1.9 kB at 5.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/ning/compress-lzf/1.0.0/compress-lzf-1.0.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/ning/compress-lzf/1.0.0/compress-lzf-1.0.0.pom (7.9 kB at 25 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5.3/snappy-java-1.0.5.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5.3/snappy-java-1.0.5.3.pom (13 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/jpountz/lz4/lz4/1.2.0/lz4-1.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/jpountz/lz4/lz4/1.2.0/lz4-1.2.0.pom (1.9 kB at 5.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/chill_2.10/0.3.6/chill_2.10-0.3.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/chill_2.10/0.3.6/chill_2.10-0.3.6.pom (2.5 kB at 8.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.3/scala-library-2.10.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.3/scala-library-2.10.3.pom (2.0 kB at 6.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/chill-java/0.3.6/chill-java-0.3.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/chill-java/0.3.6/chill-java-0.3.6.pom (2.0 kB at 6.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/kryo/kryo/2.21/kryo-2.21.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/kryo/kryo/2.21/kryo-2.21.pom (5.7 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/reflectasm/reflectasm/1.07/reflectasm-1.07.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/reflectasm/reflectasm/1.07/reflectasm-1.07.pom (3.1 kB at 9.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/minlog/minlog/1.2/minlog-1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/minlog/minlog/1.2/minlog-1.2.pom (1.6 kB at 5.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/2.2/commons-net-2.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/2.2/commons-net-2.2.pom (13 kB at 40 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-remote_2.10/2.2.3-shaded-protobuf/akka-remote_2.10-2.2.3-shaded-protobuf.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-remote_2.10/2.2.3-shaded-protobuf/akka-remote_2.10-2.2.3-shaded-protobuf.pom (3.8 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-actor_2.10/2.2.3-shaded-protobuf/akka-actor_2.10-2.2.3-shaded-protobuf.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-actor_2.10/2.2.3-shaded-protobuf/akka-actor_2.10-2.2.3-shaded-protobuf.pom (1.9 kB at 6.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.2/scala-library-2.10.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.2/scala-library-2.10.2.pom (2.0 kB at 6.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/config/1.0.2/config-1.0.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/config/1.0.2/config-1.0.2.pom (1.9 kB at 5.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.6.Final/netty-3.6.6.Final.pom
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.6.Final/netty-3.6.6.Final.pom (26 kB at 81 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/protobuf/protobuf-java/2.4.1-shaded/protobuf-java-2.4.1-shaded.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/protobuf/protobuf-java/2.4.1-shaded/protobuf-java-2.4.1-shaded.pom (7.5 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/uncommons/maths/uncommons-maths/1.2.2a/uncommons-maths-1.2.2a.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/uncommons/maths/uncommons-maths/1.2.2a/uncommons-maths-1.2.2a.pom (1.6 kB at 5.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-slf4j_2.10/2.2.3-shaded-protobuf/akka-slf4j_2.10-2.2.3-shaded-protobuf.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-slf4j_2.10/2.2.3-shaded-protobuf/akka-slf4j_2.10-2.2.3-shaded-protobuf.pom (2.6 kB at 8.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.4/scala-library-2.10.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.4/scala-library-2.10.4.pom (2.0 kB at 6.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-jackson_2.10/3.2.10/json4s-jackson_2.10-3.2.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-jackson_2.10/3.2.10/json4s-jackson_2.10-3.2.10.pom (2.4 kB at 5.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.0/scala-library-2.10.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.0/scala-library-2.10.0.pom (1.9 kB at 5.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-core_2.10/3.2.10/json4s-core_2.10-3.2.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-core_2.10/3.2.10/json4s-core_2.10-3.2.10.pom (2.5 kB at 8.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-ast_2.10/3.2.10/json4s-ast_2.10-3.2.10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-ast_2.10/3.2.10/json4s-ast_2.10-3.2.10.pom (2.0 kB at 4.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.6/paranamer-2.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.6/paranamer-2.6.pom (4.8 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer-parent/2.6/paranamer-parent-2.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer-parent/2.6/paranamer-parent-2.6.pom (11 kB at 36 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/codehaus-parent/4/codehaus-parent-4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/codehaus-parent/4/codehaus-parent-4.pom (4.8 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scalap/2.10.0/scalap-2.10.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scalap/2.10.0/scalap-2.10.0.pom (1.8 kB at 5.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.0/scala-compiler-2.10.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.0/scala-compiler-2.10.0.pom (2.2 kB at 6.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-reflect/2.10.0/scala-reflect-2.10.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-reflect/2.10.0/scala-reflect-2.10.0.pom (1.8 kB at 5.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.3.1/jackson-databind-2.3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.3.1/jackson-databind-2.3.1.pom (5.9 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/oss-parent/12/oss-parent-12.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/oss-parent/12/oss-parent-12.pom (23 kB at 73 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.3.0/jackson-annotations-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.3.0/jackson-annotations-2.3.0.pom (1.3 kB at 4.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/oss-parent/11/oss-parent-11.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/oss-parent/11/oss-parent-11.pom (23 kB at 73 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.3.1/jackson-core-2.3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.3.1/jackson-core-2.3.1.pom (6.0 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/colt/colt/1.2.0/colt-1.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/colt/colt/1.2.0/colt-1.2.0.pom (322 B at 712 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/concurrent/concurrent/1.3.4/concurrent-1.3.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/concurrent/concurrent/1.3.4/concurrent-1.3.4.pom (1.1 kB at 3.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/mesos/mesos/0.18.1/mesos-0.18.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/mesos/mesos/0.18.1/mesos-0.18.1.pom (7.8 kB at 25 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty-all/4.0.23.Final/netty-all-4.0.23.Final.pom
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty-all/4.0.23.Final/netty-all-4.0.23.Final.pom (17 kB at 54 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty-parent/4.0.23.Final/netty-parent-4.0.23.Final.pom
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty-parent/4.0.23.Final/netty-parent-4.0.23.Final.pom (40 kB at 126 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/clearspring/analytics/stream/2.7.0/stream-2.7.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/clearspring/analytics/stream/2.7.0/stream-2.7.0.pom (6.6 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-core/3.0.0/metrics-core-3.0.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-core/3.0.0/metrics-core-3.0.0.pom (845 B at 2.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-parent/3.0.0/metrics-parent-3.0.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-parent/3.0.0/metrics-parent-3.0.0.pom (10 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-jvm/3.0.0/metrics-jvm-3.0.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-jvm/3.0.0/metrics-jvm-3.0.0.pom (965 B at 2.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-json/3.0.0/metrics-json-3.0.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-json/3.0.0/metrics-json-3.0.0.pom (1.4 kB at 4.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.2.2/jackson-databind-2.2.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.2.2/jackson-databind-2.2.2.pom (5.4 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/oss-parent/10/oss-parent-10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/oss-parent/10/oss-parent-10.pom (23 kB at 73 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.2.2/jackson-annotations-2.2.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.2.2/jackson-annotations-2.2.2.pom (1.3 kB at 4.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.2.2/jackson-core-2.2.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.2.2/jackson-core-2.2.2.pom (6.0 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-graphite/3.0.0/metrics-graphite-3.0.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-graphite/3.0.0/metrics-graphite-3.0.0.pom (936 B at 3.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon-client/0.5.0/tachyon-client-0.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon-client/0.5.0/tachyon-client-0.5.0.pom (3.4 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon-parent/0.5.0/tachyon-parent-0.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon-parent/0.5.0/tachyon-parent-0.5.0.pom (5.0 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon/0.5.0/tachyon-0.5.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon/0.5.0/tachyon-0.5.0.pom (14 kB at 42 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-lang3/3.0/commons-lang3-3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-lang3/3.0/commons-lang3-3.0.pom (18 kB at 56 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/21/commons-parent-21.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/21/commons-parent-21.pom (34 kB at 97 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.3.0/jackson-databind-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.3.0/jackson-databind-2.3.0.pom (5.4 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.3.0/jackson-core-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.3.0/jackson-core-2.3.0.pom (6.0 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/pyrolite/2.0.1/pyrolite-2.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/pyrolite/2.0.1/pyrolite-2.0.1.pom (1.2 kB at 3.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/sf/py4j/py4j/0.8.2.1/py4j-0.8.2.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/sf/py4j/py4j/0.8.2.1/py4j-0.8.2.1.pom (1.9 kB at 5.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.6/avro-mapred-1.7.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.6/avro-mapred-1.7.6.pom (6.7 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.6/avro-ipc-1.7.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.6/avro-ipc-1.7.6.pom (5.2 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/guava/guava/14.0.1/guava-14.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/guava/guava/14.0.1/guava-14.0.1.pom (5.4 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/guava/guava-parent/14.0.1/guava-parent-14.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/guava/guava-parent/14.0.1/guava-parent-14.0.1.pom (2.6 kB at 7.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest_2.10/2.2.0/scalatest_2.10-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest_2.10/2.2.0/scalatest_2.10-2.2.0.pom (5.4 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-reflect/2.10.4/scala-reflect-2.10.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-reflect/2.10.4/scala-reflect-2.10.4.pom (1.9 kB at 6.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/spark/spark-core_2.10/1.1.0/spark-core_2.10-1.1.0.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/1.0.4/hadoop-client-1.0.4.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math/2.1/commons-math-2.1.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-core/1.0.4/hadoop-core-1.0.4.jar
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.3/commons-codec-1.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.3/commons-codec-1.3.jar (47 kB at 139 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-recipes/2.4.0/curator-recipes-2.4.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/1.0.4/hadoop-client-1.0.4.jar (410 B at 1.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-framework/2.4.0/curator-framework-2.4.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-math/2.1/commons-math-2.1.jar (832 kB at 2.4 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-client/2.4.0/curator-client-2.4.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-core/1.0.4/hadoop-core-1.0.4.jar (3.9 MB at 6.5 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-plus/8.1.14.v20131031/jetty-plus-8.1.14.v20131031.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-recipes/2.4.0/curator-recipes-2.4.0.jar (241 kB at 382 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.transaction/1.1.1.v201105210645/javax.transaction-1.1.1.v201105210645.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-framework/2.4.0/curator-framework-2.4.0.jar (180 kB at 266 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-jndi/8.1.14.v20131031/jetty-jndi-8.1.14.v20131031.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/curator/curator-client/2.4.0/curator-client-2.4.0.jar (67 kB at 100 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.mail.glassfish/1.4.1.v201005082020/javax.mail.glassfish-1.4.1.v201005082020.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/spark/spark-core_2.10/1.1.0/spark-core_2.10-1.1.0.jar (6.4 MB at 7.0 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.activation/1.1.0.v201105071233/javax.activation-1.1.0.v201105071233.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.transaction/1.1.1.v201105210645/javax.transaction-1.1.1.v201105210645.jar (29 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-lang3/3.3.2/commons-lang3-3.3.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-plus/8.1.14.v20131031/jetty-plus-8.1.14.v20131031.jar (85 kB at 93 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/jul-to-slf4j/1.7.5/jul-to-slf4j-1.7.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-jndi/8.1.14.v20131031/jetty-jndi-8.1.14.v20131031.jar (40 kB at 40 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/slf4j/jcl-over-slf4j/1.7.5/jcl-over-slf4j-1.7.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.mail.glassfish/1.4.1.v201005082020/javax.mail.glassfish-1.4.1.v201005082020.jar (491 kB at 477 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/ning/compress-lzf/1.0.0/compress-lzf-1.0.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-lang3/3.3.2/commons-lang3-3.3.2.jar (413 kB at 342 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5.3/snappy-java-1.0.5.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/jul-to-slf4j/1.7.5/jul-to-slf4j-1.7.5.jar (5.0 kB at 4.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/jpountz/lz4/lz4/1.2.0/lz4-1.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/orbit/javax.activation/1.1.0.v201105071233/javax.activation-1.1.0.v201105071233.jar (54 kB at 43 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/chill_2.10/0.3.6/chill_2.10-0.3.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/slf4j/jcl-over-slf4j/1.7.5/jcl-over-slf4j-1.7.5.jar (17 kB at 13 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/kryo/kryo/2.21/kryo-2.21.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/ning/compress-lzf/1.0.0/compress-lzf-1.0.0.jar (77 kB at 57 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/reflectasm/reflectasm/1.07/reflectasm-1.07-shaded.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/xerial/snappy/snappy-java/1.0.5.3/snappy-java-1.0.5.3.jar (1.3 MB at 855 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/minlog/minlog/1.2/minlog-1.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/net/jpountz/lz4/lz4/1.2.0/lz4-1.2.0.jar (166 kB at 106 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/objenesis/objenesis/1.2/objenesis-1.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/chill_2.10/0.3.6/chill_2.10-0.3.6.jar (217 kB at 133 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/twitter/chill-java/0.3.6/chill-java-0.3.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/kryo/kryo/2.21/kryo-2.21.jar (363 kB at 220 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/2.2/commons-net-2.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/minlog/minlog/1.2/minlog-1.2.jar (5.0 kB at 2.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-remote_2.10/2.2.3-shaded-protobuf/akka-remote_2.10-2.2.3-shaded-protobuf.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/esotericsoftware/reflectasm/reflectasm/1.07/reflectasm-1.07-shaded.jar (66 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-actor_2.10/2.2.3-shaded-protobuf/akka-actor_2.10-2.2.3-shaded-protobuf.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/objenesis/objenesis/1.2/objenesis-1.2.jar (36 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/config/1.0.2/config-1.0.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/twitter/chill-java/0.3.6/chill-java-0.3.6.jar (47 kB at 24 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.6.Final/netty-3.6.6.Final.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-net/commons-net/2.2/commons-net-2.2.jar (212 kB at 108 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/protobuf/protobuf-java/2.4.1-shaded/protobuf-java-2.4.1-shaded.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/config/1.0.2/config-1.0.2.jar (187 kB at 85 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/uncommons/maths/uncommons-maths/1.2.2a/uncommons-maths-1.2.2a.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-remote_2.10/2.2.3-shaded-protobuf/akka-remote_2.10-2.2.3-shaded-protobuf.jar (1.3 MB at 563 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-slf4j_2.10/2.2.3-shaded-protobuf/akka-slf4j_2.10-2.2.3-shaded-protobuf.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-actor_2.10/2.2.3-shaded-protobuf/akka-actor_2.10-2.2.3-shaded-protobuf.jar (2.7 MB at 1.2 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.4/scala-library-2.10.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty/3.6.6.Final/netty-3.6.6.Final.jar (1.2 MB at 497 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-jackson_2.10/3.2.10/json4s-jackson_2.10-3.2.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/protobuf/protobuf-java/2.4.1-shaded/protobuf-java-2.4.1-shaded.jar (455 kB at 185 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-core_2.10/3.2.10/json4s-core_2.10-3.2.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/uncommons/maths/uncommons-maths/1.2.2a/uncommons-maths-1.2.2a.jar (49 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-ast_2.10/3.2.10/json4s-ast_2.10-3.2.10.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/akka/akka-slf4j_2.10/2.2.3-shaded-protobuf/akka-slf4j_2.10-2.2.3-shaded-protobuf.jar (14 kB at 5.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.6/paranamer-2.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-jackson_2.10/3.2.10/json4s-jackson_2.10-3.2.10.jar (40 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scalap/2.10.0/scalap-2.10.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/thoughtworks/paranamer/paranamer/2.6/paranamer-2.6.jar (33 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.0/scala-compiler-2.10.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-ast_2.10/3.2.10/json4s-ast_2.10-3.2.10.jar (84 kB at 29 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.3.1/jackson-databind-2.3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/json4s/json4s-core_2.10/3.2.10/json4s-core_2.10-3.2.10.jar (585 kB at 200 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.3.0/jackson-annotations-2.3.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.10.4/scala-library-2.10.4.jar (7.1 MB at 2.4 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.3.1/jackson-core-2.3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scalap/2.10.0/scalap-2.10.0.jar (855 kB at 272 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/colt/colt/1.2.0/colt-1.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.3.0/jackson-annotations-2.3.0.jar (35 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/concurrent/concurrent/1.3.4/concurrent-1.3.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-core/2.3.1/jackson-core-2.3.1.jar (198 kB at 60 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/mesos/mesos/0.18.1/mesos-0.18.1-shaded-protobuf.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.3.1/jackson-databind-2.3.1.jar (914 kB at 278 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/io/netty/netty-all/4.0.23.Final/netty-all-4.0.23.Final.jar
Downloaded from central: https://repo.maven.apache.org/maven2/colt/colt/1.2.0/colt-1.2.0.jar (582 kB at 166 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/clearspring/analytics/stream/2.7.0/stream-2.7.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/concurrent/concurrent/1.3.4/concurrent-1.3.4.jar (189 kB at 53 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-core/3.0.0/metrics-core-3.0.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/mesos/mesos/0.18.1/mesos-0.18.1-shaded-protobuf.jar (955 kB at 266 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-jvm/3.0.0/metrics-jvm-3.0.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/io/netty/netty-all/4.0.23.Final/netty-all-4.0.23.Final.jar (1.8 MB at 480 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-json/3.0.0/metrics-json-3.0.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.0/scala-compiler-2.10.0.jar (14 MB at 3.7 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-graphite/3.0.0/metrics-graphite-3.0.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/clearspring/analytics/stream/2.7.0/stream-2.7.0.jar (174 kB at 45 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon-client/0.5.0/tachyon-client-0.5.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-core/3.0.0/metrics-core-3.0.0.jar (85 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon/0.5.0/tachyon-0.5.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-jvm/3.0.0/metrics-jvm-3.0.0.jar (31 kB at 7.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/spark-project/pyrolite/2.0.1/pyrolite-2.0.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-json/3.0.0/metrics-json-3.0.0.jar (16 kB at 3.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/sf/py4j/py4j/0.8.2.1/py4j-0.8.2.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/codahale/metrics/metrics-graphite/3.0.0/metrics-graphite-3.0.0.jar (9.8 kB at 2.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.6/avro-mapred-1.7.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon-client/0.5.0/tachyon-client-0.5.0.jar (2.3 kB at 553 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.6/avro-ipc-1.7.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/spark-project/pyrolite/2.0.1/pyrolite-2.0.1.jar (68 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.6/avro-ipc-1.7.6-tests.jar
Downloaded from central: https://repo.maven.apache.org/maven2/net/sf/py4j/py4j/0.8.2.1/py4j-0.8.2.1.jar (81 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/google/guava/guava/14.0.1/guava-14.0.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-mapred/1.7.6/avro-mapred-1.7.6.jar (172 kB at 39 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest_2.10/2.2.0/scalatest_2.10-2.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/tachyonproject/tachyon/0.5.0/tachyon-0.5.0.jar (2.1 MB at 469 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-reflect/2.10.4/scala-reflect-2.10.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.6/avro-ipc-1.7.6.jar (193 kB at 42 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-ipc/1.7.6/avro-ipc-1.7.6-tests.jar (346 kB at 73 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/google/guava/guava/14.0.1/guava-14.0.1.jar (2.2 MB at 445 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-reflect/2.10.4/scala-reflect-2.10.4.jar (3.2 MB at 631 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/scalatest/scalatest_2.10/2.2.0/scalatest_2.10-2.2.0.jar (7.5 MB at 1.5 MB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch19-spark ---
[INFO] 
[INFO] --- avro-maven-plugin:1.7.6:schema (schemas) @ ch19-spark ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.6/avro-compiler-1.7.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.6/avro-compiler-1.7.6.pom (4.4 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.6/avro-1.7.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.6/avro-1.7.6.pom (5.6 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.6/avro-compiler-1.7.6.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.6/avro-1.7.6.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro/1.7.6/avro-1.7.6.jar (426 kB at 1.5 MB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/avro/avro-compiler/1.7.6/avro-compiler-1.7.6.jar (77 kB at 235 kB/s)
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch19-spark ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch19-spark/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch19-spark ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/hadoop-book/ch19-spark/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- scala-maven-plugin:3.1.6:compile (default) @ ch19-spark ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0/doxia-sink-api-1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0/doxia-sink-api-1.0.pom (1.4 kB at 4.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.0/doxia-1.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.0/doxia-1.0.pom (9.6 kB at 30 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/10/maven-parent-10.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/10/maven-parent-10.pom (32 kB at 97 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-core/3.0.4/maven-core-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-core/3.0.4/maven-core-3.0.4.pom (6.4 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/3.0.4/maven-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven/3.0.4/maven-3.0.4.pom (22 kB at 68 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/3.0.4/maven-model-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/3.0.4/maven-model-3.0.4.pom (3.8 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/2.0.6/plexus-utils-2.0.6.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/2.0.6/plexus-utils-2.0.6.pom (2.9 kB at 8.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/3.0.4/maven-settings-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/3.0.4/maven-settings-3.0.4.pom (1.8 kB at 5.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings-builder/3.0.4/maven-settings-builder-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings-builder/3.0.4/maven-settings-builder-3.0.4.pom (2.3 kB at 7.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/3.0.4/maven-repository-metadata-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/3.0.4/maven-repository-metadata-3.0.4.pom (1.9 kB at 5.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/3.0.4/maven-artifact-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact/3.0.4/maven-artifact-3.0.4.pom (1.6 kB at 5.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/3.0.4/maven-plugin-api-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/3.0.4/maven-plugin-api-3.0.4.pom (2.7 kB at 8.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-plexus/2.3.0/sisu-inject-plexus-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-plexus/2.3.0/sisu-inject-plexus-2.3.0.pom (6.1 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/guice-plexus/2.3.0/guice-plexus-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/guice-plexus/2.3.0/guice-plexus-2.3.0.pom (3.8 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/guice-bean/2.3.0/guice-bean-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/guice-bean/2.3.0/guice-bean-2.3.0.pom (3.0 kB at 9.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/containers/2.3.0/containers-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/containers/2.3.0/containers-2.3.0.pom (1.2 kB at 3.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject/2.3.0/sisu-inject-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject/2.3.0/sisu-inject-2.3.0.pom (3.2 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-parent/2.3.0/sisu-parent-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-parent/2.3.0/sisu-parent-2.3.0.pom (11 kB at 33 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-bean/2.3.0/sisu-inject-bean-2.3.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-bean/2.3.0/sisu-inject-bean-2.3.0.pom (7.1 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-guava/0.9.9/sisu-guava-0.9.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-guava/0.9.9/sisu-guava-0.9.9.pom (1.1 kB at 3.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/guava-parent/0.9.9/guava-parent-0.9.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/inject/guava-parent/0.9.9/guava-parent-0.9.9.pom (11 kB at 34 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model-builder/3.0.4/maven-model-builder-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model-builder/3.0.4/maven-model-builder-3.0.4.pom (2.5 kB at 7.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-aether-provider/3.0.4/maven-aether-provider-3.0.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-aether-provider/3.0.4/maven-aether-provider-3.0.4.pom (2.8 kB at 8.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-api/1.13.1/aether-api-1.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-api/1.13.1/aether-api-1.13.1.pom (1.4 kB at 4.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether/1.13.1/aether-1.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether/1.13.1/aether-1.13.1.pom (10 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-spi/1.13.1/aether-spi-1.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-spi/1.13.1/aether-spi-1.13.1.pom (1.4 kB at 4.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-util/1.13.1/aether-util-1.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-util/1.13.1/aether-util-1.13.1.pom (1.7 kB at 5.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-impl/1.13.1/aether-impl-1.13.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-impl/1.13.1/aether-impl-1.13.1.pom (2.5 kB at 7.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-dependency-tree/1.2/maven-dependency-tree-1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-dependency-tree/1.2/maven-dependency-tree-1.2.pom (3.6 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/9/maven-shared-components-9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-shared-components/9/maven-shared-components-9.pom (3.5 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-exec/1.1/commons-exec-1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-exec/1.1/commons-exec-1.1.pom (11 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1.2/doxia-sink-api-1.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1.2/doxia-sink-api-1.1.2.pom (1.6 kB at 4.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.1.2/doxia-1.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia/1.1.2/doxia-1.1.2.pom (18 kB at 57 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1.2/doxia-logging-api-1.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1.2/doxia-logging-api-1.1.2.pom (1.6 kB at 4.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-invoker/2.0.11/maven-invoker-2.0.11.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-invoker/2.0.11/maven-invoker-2.0.11.pom (5.1 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/zinc/zinc/0.2.5/zinc-0.2.5.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/zinc/zinc/0.2.5/zinc-0.2.5.pom (2.4 kB at 7.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.9.2/scala-library-2.9.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.9.2/scala-library-2.9.2.pom (1.7 kB at 5.4 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/incremental-compiler/0.12.3/incremental-compiler-0.12.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/incremental-compiler/0.12.3/incremental-compiler-0.12.3.pom (2.1 kB at 6.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/sbt-interface/0.12.3/sbt-interface-0.12.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/sbt-interface/0.12.3/sbt-interface-0.12.3.pom (1.7 kB at 5.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.9.2/scala-compiler-2.9.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.9.2/scala-compiler-2.9.2.pom (2.0 kB at 6.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/compiler-interface/0.12.3/compiler-interface-0.12.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/compiler-interface/0.12.3/compiler-interface-0.12.3.pom (1.7 kB at 5.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-core/3.0.4/maven-core-3.0.4.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/3.0.4/maven-settings-3.0.4.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings-builder/3.0.4/maven-settings-builder-3.0.4.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/3.0.4/maven-repository-metadata-3.0.4.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/3.0.4/maven-plugin-api-3.0.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings/3.0.4/maven-settings-3.0.4.jar (47 kB at 147 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model-builder/3.0.4/maven-model-builder-3.0.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-settings-builder/3.0.4/maven-settings-builder-3.0.4.jar (41 kB at 125 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-aether-provider/3.0.4/maven-aether-provider-3.0.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/3.0.4/maven-plugin-api-3.0.4.jar (49 kB at 148 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-spi/1.13.1/aether-spi-1.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-repository-metadata/3.0.4/maven-repository-metadata-3.0.4.jar (30 kB at 88 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-impl/1.13.1/aether-impl-1.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-core/3.0.4/maven-core-3.0.4.jar (559 kB at 1.6 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-api/1.13.1/aether-api-1.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model-builder/3.0.4/maven-model-builder-3.0.4.jar (151 kB at 255 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-util/1.13.1/aether-util-1.13.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-aether-provider/3.0.4/maven-aether-provider-3.0.4.jar (56 kB at 88 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-plexus/2.3.0/sisu-inject-plexus-2.3.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-spi/1.13.1/aether-spi-1.13.1.jar (15 kB at 24 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-bean/2.3.0/sisu-inject-bean-2.3.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-impl/1.13.1/aether-impl-1.13.1.jar (130 kB at 199 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-guava/0.9.9/sisu-guava-0.9.9.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-api/1.13.1/aether-api-1.13.1.jar (90 kB at 136 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-dependency-tree/1.2/maven-dependency-tree-1.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/aether/aether-util/1.13.1/aether-util-1.13.1.jar (130 kB at 150 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-exec/1.1/commons-exec-1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-plexus/2.3.0/sisu-inject-plexus-2.3.0.jar (204 kB at 213 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-classworlds/2.4/plexus-classworlds-2.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-inject-bean/2.3.0/sisu-inject-bean-2.3.0.jar (289 kB at 294 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.8/maven-project-2.0.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-dependency-tree/1.2/maven-dependency-tree-1.2.jar (35 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.8/maven-profile-2.0.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/sonatype/sisu/sisu-guava/0.9.9/sisu-guava-0.9.9.jar (1.5 MB at 1.4 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.8/maven-artifact-manager-2.0.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-exec/1.1/commons-exec-1.1.jar (53 kB at 46 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.8/maven-plugin-registry-2.0.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-classworlds/2.4/plexus-classworlds-2.4.jar (47 kB at 37 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1.2/doxia-sink-api-1.1.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-project/2.0.8/maven-project-2.0.8.jar (117 kB at 89 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1.2/doxia-logging-api-1.1.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-profile/2.0.8/maven-profile-2.0.8.jar (35 kB at 27 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/3.0.4/maven-model-3.0.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-artifact-manager/2.0.8/maven-artifact-manager-2.0.8.jar (57 kB at 41 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-invoker/2.0.11/maven-invoker-2.0.11.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-registry/2.0.8/maven-plugin-registry-2.0.8.jar (29 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/zinc/zinc/0.2.5/zinc-0.2.5.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.1.2/doxia-sink-api-1.1.2.jar (13 kB at 7.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.9.2/scala-library-2.9.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-logging-api/1.1.2/doxia-logging-api-1.1.2.jar (11 kB at 7.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/incremental-compiler/0.12.3/incremental-compiler-0.12.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-model/3.0.4/maven-model-3.0.4.jar (164 kB at 99 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/sbt-interface/0.12.3/sbt-interface-0.12.3.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-invoker/2.0.11/maven-invoker-2.0.11.jar (29 kB at 17 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.9.2/scala-compiler-2.9.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/zinc/zinc/0.2.5/zinc-0.2.5.jar (320 kB at 186 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/compiler-interface/0.12.3/compiler-interface-0.12.3-sources.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/sbt-interface/0.12.3/sbt-interface-0.12.3.jar (42 kB at 21 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/compiler-interface/0.12.3/compiler-interface-0.12.3-sources.jar (22 kB at 11 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/typesafe/sbt/incremental-compiler/0.12.3/incremental-compiler-0.12.3.jar (2.0 MB at 946 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-library/2.9.2/scala-library-2.9.2.jar (8.9 MB at 3.9 MB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.9.2/scala-compiler-2.9.2.jar (11 MB at 4.0 MB/s)
[WARNING]  Expected all dependencies to require Scala version: 2.10.4
[WARNING]  com.twitter:chill_2.10:0.3.6 requires scala version: 2.10.3
[WARNING] Multiple versions of scala libraries detected!
[INFO] /home/hadoop/work/hadoop-book/ch19-spark/src/main/java:-1: info: compiling
[INFO] /home/hadoop/work/hadoop-book/ch19-spark/src/main/scala:-1: info: compiling
[INFO] Compiling 3 source files to /home/hadoop/work/hadoop-book/ch19-spark/target/classes at 1642143496510
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.4/scala-compiler-2.10.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.4/scala-compiler-2.10.4.jar (14 MB at 11 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.4/scala-compiler-2.10.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/scala-lang/scala-compiler/2.10.4/scala-compiler-2.10.4.pom (2.2 kB at 6.8 kB/s)
[INFO] prepare-compile in 0 s
[INFO] compile in 6 s
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch19-spark ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 5 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch19-spark ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /home/hadoop/work/hadoop-book/ch19-spark/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- scala-maven-plugin:3.1.6:testCompile (default) @ ch19-spark ---
[WARNING]  Expected all dependencies to require Scala version: 2.10.4
[WARNING]  com.twitter:chill_2.10:0.3.6 requires scala version: 2.10.3
[WARNING] Multiple versions of scala libraries detected!
[INFO] /home/hadoop/work/hadoop-book/ch19-spark/src/test/java:-1: info: compiling
[INFO] /home/hadoop/work/hadoop-book/ch19-spark/target/generated-test-sources/avro:-1: info: compiling
[INFO] /home/hadoop/work/hadoop-book/ch19-spark/src/test/scala:-1: info: compiling
[INFO] Compiling 11 source files to /home/hadoop/work/hadoop-book/ch19-spark/target/test-classes at 1642143503339
[INFO] prepare-compile in 0 s
[INFO] compile in 9 s
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch19-spark ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- scalatest-maven-plugin:1.0:test (test) @ ch19-spark ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0/doxia-sink-api-1.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/doxia/doxia-sink-api/1.0/doxia-sink-api-1.0.jar (10 kB at 31 kB/s)
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch19-spark ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch19-spark/target/ch19-spark-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ ch19-spark ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/jar.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch19-spark/target/../../spark-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch19-spark/target/../../spark-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch19-spark/target/ch19-spark-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch19-spark/target/../../spark-examples.jar
[INFO] 
[INFO] ---------------------< com.hadoopbook:ch20-hbase >----------------------
[INFO] Building Chapter 20: HBase 4.0                                   [16/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-client/0.98.7-hadoop2/hbase-client-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-client/0.98.7-hadoop2/hbase-client-0.98.7-hadoop2.pom (8.2 kB at 26 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase/0.98.7-hadoop2/hbase-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase/0.98.7-hadoop2/hbase-0.98.7-hadoop2.pom (80 kB at 250 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/apache/12/apache-12.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/apache/12/apache-12.pom (16 kB at 49 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-annotations/0.98.7-hadoop2/hbase-annotations-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-annotations/0.98.7-hadoop2/hbase-annotations-0.98.7-hadoop2.pom (1.7 kB at 5.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/github/stephenc/findbugs/findbugs-annotations/1.3.9-1/findbugs-annotations-1.3.9-1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/github/stephenc/findbugs/findbugs-annotations/1.3.9-1/findbugs-annotations-1.3.9-1.pom (6.2 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-common/0.98.7-hadoop2/hbase-common-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-common/0.98.7-hadoop2/hbase-common-0.98.7-hadoop2.pom (13 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.7/commons-codec-1.7.pom
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.7/commons-codec-1.7.pom (10 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/26/commons-parent-26.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/commons/commons-parent/26/commons-parent-26.pom (48 kB at 150 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.2.0/hadoop-common-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.2.0/hadoop-common-2.2.0.pom (27 kB at 85 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project-dist/2.2.0/hadoop-project-dist-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project-dist/2.2.0/hadoop-project-dist-2.2.0.pom (18 kB at 56 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project/2.2.0/hadoop-project-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-project/2.2.0/hadoop-project-2.2.0.pom (37 kB at 115 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-main/2.2.0/hadoop-main-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-main/2.2.0/hadoop-main-2.2.0.pom (17 kB at 50 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-annotations/2.2.0/hadoop-annotations-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-annotations/2.2.0/hadoop-annotations-2.2.0.pom (2.4 kB at 7.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.6.1/jets3t-0.6.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.6.1/jets3t-0.6.1.pom (1.5 kB at 4.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.8.8/jackson-core-asl-1.8.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.8.8/jackson-core-asl-1.8.8.pom (1.0 kB at 3.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.8.8/jackson-mapper-asl-1.8.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.8.8/jackson-mapper-asl-1.8.8.pom (1.2 kB at 3.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.2.0/hadoop-auth-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.2.0/hadoop-auth-2.2.0.pom (7.0 kB at 22 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.2.0/hadoop-mapreduce-client-core-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.2.0/hadoop-mapreduce-client-core-2.2.0.pom (3.1 kB at 9.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client/2.2.0/hadoop-mapreduce-client-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client/2.2.0/hadoop-mapreduce-client-2.2.0.pom (6.4 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.2.0/hadoop-yarn-common-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.2.0/hadoop-yarn-common-2.2.0.pom (5.0 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn/2.2.0/hadoop-yarn-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn/2.2.0/hadoop-yarn-2.2.0.pom (6.5 kB at 20 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-api/2.2.0/hadoop-yarn-api-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-api/2.2.0/hadoop-yarn-api-2.2.0.pom (3.0 kB at 9.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-test-framework/jersey-test-framework-grizzly2/1.9/jersey-test-framework-grizzly2-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-test-framework/jersey-test-framework-grizzly2/1.9/jersey-test-framework-grizzly2-1.9.pom (6.2 kB at 19 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-test-framework/1.9/jersey-test-framework-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-test-framework/1.9/jersey-test-framework-1.9.pom (3.1 kB at 9.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-test-framework/jersey-test-framework-core/1.9/jersey-test-framework-core-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-test-framework/jersey-test-framework-core/1.9/jersey-test-framework-core-1.9.pom (6.7 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/javax/servlet/javax.servlet-api/3.0.1/javax.servlet-api-3.0.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/javax/servlet/javax.servlet-api/3.0.1/javax.servlet-api-3.0.1.pom (13 kB at 40 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-grizzly2/1.9/jersey-grizzly2-1.9.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-grizzly2/1.9/jersey-grizzly2-1.9.pom (5.6 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-http/2.1.2/grizzly-http-2.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-http/2.1.2/grizzly-http-2.1.2.pom (4.1 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-project/2.1.2/grizzly-project-2.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-project/2.1.2/grizzly-project-2.1.2.pom (17 kB at 38 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-framework/2.1.2/grizzly-framework-2.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-framework/2.1.2/grizzly-framework-2.1.2.pom (6.5 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/gmbal/gmbal-api-only/3.0.0-b023/gmbal-api-only-3.0.0-b023.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/gmbal/gmbal-api-only/3.0.0-b023/gmbal-api-only-3.0.0-b023.pom (5.2 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/external/management-api/3.0.0-b012/management-api-3.0.0-b012.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/external/management-api/3.0.0-b012/management-api-3.0.0-b012.pom (2.7 kB at 8.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-http-server/2.1.2/grizzly-http-server-2.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-http-server/2.1.2/grizzly-http-server-2.1.2.pom (4.6 kB at 15 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-rcm/2.1.2/grizzly-rcm-2.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-rcm/2.1.2/grizzly-rcm-2.1.2.pom (3.6 kB at 9.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-http-servlet/2.1.2/grizzly-http-servlet-2.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/grizzly/grizzly-http-servlet/2.1.2/grizzly-http-servlet-2.1.2.pom (6.6 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/javax.servlet/3.1/javax.servlet-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/javax.servlet/3.1/javax.servlet-3.1.pom (5.1 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/api-pom/3.1/api-pom-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/api-pom/3.1/api-pom-3.1.pom (4.9 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/glassfish-parent/3.1/glassfish-parent-3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/glassfish-parent/3.1/glassfish-parent-3.1.pom (60 kB at 189 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/glassfish/pom/8/pom-8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/glassfish/pom/8/pom-8.pom (11 kB at 33 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-protocol/0.98.7-hadoop2/hbase-protocol-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-protocol/0.98.7-hadoop2/hbase-protocol-0.98.7-hadoop2.pom (7.9 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/cloudera/htrace/htrace-core/2.04/htrace-core-2.04.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/cloudera/htrace/htrace-core/2.04/htrace-core-2.04.pom (2.8 kB at 8.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/cloudera/htrace/htrace/2.04/htrace-2.04.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/cloudera/htrace/htrace/2.04/htrace-2.04.pom (6.6 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/jruby/joni/joni/2.1.2/joni-2.1.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/jruby/joni/joni/2.1.2/joni-2.1.2.pom (4.4 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/jruby/jcodings/jcodings/1.0.8/jcodings-1.0.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/jruby/jcodings/jcodings/1.0.8/jcodings-1.0.8.pom (4.3 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-server/0.98.7-hadoop2/hbase-server-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-server/0.98.7-hadoop2/hbase-server-0.98.7-hadoop2.pom (27 kB at 72 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-prefix-tree/0.98.7-hadoop2/hbase-prefix-tree-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-prefix-tree/0.98.7-hadoop2/hbase-prefix-tree-0.98.7-hadoop2.pom (5.7 kB at 18 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop-compat/0.98.7-hadoop2/hbase-hadoop-compat-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop-compat/0.98.7-hadoop2/hbase-hadoop-compat-0.98.7-hadoop2.pom (3.9 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop2-compat/0.98.7-hadoop2/hbase-hadoop2-compat-0.98.7-hadoop2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop2-compat/0.98.7-hadoop2/hbase-hadoop2-compat-0.98.7-hadoop2.pom (6.6 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/yammer/metrics/metrics-core/2.2.0/metrics-core-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/yammer/metrics/metrics-core/2.2.0/metrics-core-2.2.0.pom (1.3 kB at 3.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/yammer/metrics/metrics-parent/2.2.0/metrics-parent-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/yammer/metrics/metrics-parent/2.2.0/metrics-parent-2.2.0.pom (10 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/github/stephenc/high-scale-lib/high-scale-lib/1.1.1/high-scale-lib-1.1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/github/stephenc/high-scale-lib/high-scale-lib/1.1.1/high-scale-lib-1.1.1.pom (933 B at 2.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/github/stephenc/high-scale-lib/high-scale-lib-parent/1.1.1/high-scale-lib-parent-1.1.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/github/stephenc/high-scale-lib/high-scale-lib-parent/1.1.1/high-scale-lib-parent-1.1.1.pom (4.4 kB at 14 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jetty-sslengine/6.1.26/jetty-sslengine-6.1.26.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jetty-sslengine/6.1.26/jetty-sslengine-6.1.26.pom (3.8 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.8.8/jackson-jaxrs-1.8.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.8.8/jackson-jaxrs-1.8.8.pom (1.6 kB at 5.1 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/jamon/jamon-runtime/2.3.1/jamon-runtime-2.3.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/jamon/jamon-runtime/2.3.1/jamon-runtime-2.3.1.pom (1.8 kB at 5.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/jamon/jamon-project/1.0.2/jamon-project-1.0.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/jamon/jamon-project/1.0.2/jamon-project-1.0.2.pom (9.2 kB at 29 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.8/jersey-core-1.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.8/jersey-core-1.8.pom (10 kB at 31 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-project/1.8/jersey-project-1.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-project/1.8/jersey-project-1.8.pom (17 kB at 54 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.8/jersey-json-1.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.8/jersey-json-1.8.pom (11 kB at 33 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.7.1/jackson-core-asl-1.7.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.7.1/jackson-core-asl-1.7.1.pom (1.0 kB at 3.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.7.1/jackson-mapper-asl-1.7.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.7.1/jackson-mapper-asl-1.7.1.pom (1.2 kB at 3.5 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.7.1/jackson-jaxrs-1.7.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.7.1/jackson-jaxrs-1.7.1.pom (1.6 kB at 5.2 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.7.1/jackson-xc-1.7.1.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.7.1/jackson-xc-1.7.1.pom (1.6 kB at 5.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.8/jersey-server-1.8.pom
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.8/jersey-server-1.8.pom (11 kB at 35 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/2.2.0/hadoop-client-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-client/2.2.0/hadoop-client-2.2.0.pom (11 kB at 32 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.2.0/hadoop-hdfs-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.2.0/hadoop-hdfs-2.2.0.pom (24 kB at 74 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-app/2.2.0/hadoop-mapreduce-client-app-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-app/2.2.0/hadoop-mapreduce-client-app-2.2.0.pom (4.3 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-common/2.2.0/hadoop-mapreduce-client-common-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-common/2.2.0/hadoop-mapreduce-client-common-2.2.0.pom (3.3 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-client/2.2.0/hadoop-yarn-client-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-client/2.2.0/hadoop-yarn-client-2.2.0.pom (2.0 kB at 4.6 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-common/2.2.0/hadoop-yarn-server-common-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-common/2.2.0/hadoop-yarn-server-common-2.2.0.pom (3.5 kB at 11 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server/2.2.0/hadoop-yarn-server-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server/2.2.0/hadoop-yarn-server-2.2.0.pom (1.8 kB at 5.8 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-shuffle/2.2.0/hadoop-mapreduce-client-shuffle-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-shuffle/2.2.0/hadoop-mapreduce-client-shuffle-2.2.0.pom (1.7 kB at 4.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.2.0/hadoop-mapreduce-client-jobclient-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-jobclient/2.2.0/hadoop-mapreduce-client-jobclient-2.2.0.pom (5.5 kB at 16 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-nodemanager/2.2.0/hadoop-yarn-server-nodemanager-2.2.0.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-server-nodemanager/2.2.0/hadoop-yarn-server-nodemanager-2.2.0.pom (7.3 kB at 23 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-client/0.98.7-hadoop2/hbase-client-0.98.7-hadoop2.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-annotations/0.98.7-hadoop2/hbase-annotations-0.98.7-hadoop2.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-protocol/0.98.7-hadoop2/hbase-protocol-0.98.7-hadoop2.jar
Downloading from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.7/commons-codec-1.7.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-common/0.98.7-hadoop2/hbase-common-0.98.7-hadoop2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-annotations/0.98.7-hadoop2/hbase-annotations-0.98.7-hadoop2.jar (21 kB at 64 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/cloudera/htrace/htrace-core/2.04/htrace-core-2.04.jar
Downloaded from central: https://repo.maven.apache.org/maven2/commons-codec/commons-codec/1.7/commons-codec-1.7.jar (260 kB at 757 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.8.8/jackson-mapper-asl-1.8.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-common/0.98.7-hadoop2/hbase-common-0.98.7-hadoop2.jar (444 kB at 1.3 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/jruby/joni/joni/2.1.2/joni-2.1.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-client/0.98.7-hadoop2/hbase-client-0.98.7-hadoop2.jar (941 kB at 2.0 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/jruby/jcodings/jcodings/1.0.8/jcodings-1.0.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/cloudera/htrace/htrace-core/2.04/htrace-core-2.04.jar (32 kB at 50 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.2.0/hadoop-common-2.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/jruby/joni/joni/2.1.2/joni-2.1.2.jar (187 kB at 290 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.6.1/jets3t-0.6.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-mapper-asl/1.8.8/jackson-mapper-asl-1.8.8.jar (669 kB at 969 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.2.0/hadoop-auth-2.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-protocol/0.98.7-hadoop2/hbase-protocol-0.98.7-hadoop2.jar (3.5 MB at 4.5 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.2.0/hadoop-mapreduce-client-core-2.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/jruby/jcodings/jcodings/1.0.8/jcodings-1.0.8.jar (1.3 MB at 1.5 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.2.0/hadoop-yarn-common-2.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-auth/2.2.0/hadoop-auth-2.2.0.jar (50 kB at 50 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/github/stephenc/findbugs/findbugs-annotations/1.3.9-1/findbugs-annotations-1.3.9-1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-common/2.2.0/hadoop-common-2.2.0.jar (2.7 MB at 2.4 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-server/0.98.7-hadoop2/hbase-server-0.98.7-hadoop2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-mapreduce-client-core/2.2.0/hadoop-mapreduce-client-core-2.2.0.jar (1.5 MB at 1.3 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-prefix-tree/0.98.7-hadoop2/hbase-prefix-tree-0.98.7-hadoop2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/net/java/dev/jets3t/jets3t/0.6.1/jets3t-0.6.1.jar (322 kB at 276 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-common/0.98.7-hadoop2/hbase-common-0.98.7-hadoop2-tests.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-yarn-common/2.2.0/hadoop-yarn-common-2.2.0.jar (1.3 MB at 1.0 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop-compat/0.98.7-hadoop2/hbase-hadoop-compat-0.98.7-hadoop2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/github/stephenc/findbugs/findbugs-annotations/1.3.9-1/findbugs-annotations-1.3.9-1.jar (15 kB at 12 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop2-compat/0.98.7-hadoop2/hbase-hadoop2-compat-0.98.7-hadoop2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-prefix-tree/0.98.7-hadoop2/hbase-prefix-tree-0.98.7-hadoop2.jar (98 kB at 67 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/yammer/metrics/metrics-core/2.2.0/metrics-core-2.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-common/0.98.7-hadoop2/hbase-common-0.98.7-hadoop2-tests.jar (172 kB at 117 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/github/stephenc/high-scale-lib/high-scale-lib/1.1.1/high-scale-lib-1.1.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop-compat/0.98.7-hadoop2/hbase-hadoop-compat-0.98.7-hadoop2.jar (33 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jetty-sslengine/6.1.26/jetty-sslengine-6.1.26.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-hadoop2-compat/0.98.7-hadoop2/hbase-hadoop2-compat-0.98.7-hadoop2.jar (74 kB at 45 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.8.8/jackson-core-asl-1.8.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hbase/hbase-server/0.98.7-hadoop2/hbase-server-0.98.7-hadoop2.jar (3.6 MB at 2.2 MB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.8.8/jackson-jaxrs-1.8.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/github/stephenc/high-scale-lib/high-scale-lib/1.1.1/high-scale-lib-1.1.1.jar (96 kB at 55 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/jamon/jamon-runtime/2.3.1/jamon-runtime-2.3.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/yammer/metrics/metrics-core/2.2.0/metrics-core-2.2.0.jar (82 kB at 46 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.8/jersey-core-1.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/mortbay/jetty/jetty-sslengine/6.1.26/jetty-sslengine-6.1.26.jar (19 kB at 9.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.8/jersey-json-1.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-core-asl/1.8.8/jackson-core-asl-1.8.8.jar (228 kB at 115 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.7.1/jackson-xc-1.7.1.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-jaxrs/1.8.8/jackson-jaxrs-1.8.8.jar (18 kB at 8.9 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.8/jersey-server-1.8.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/jamon/jamon-runtime/2.3.1/jamon-runtime-2.3.1.jar (21 kB at 10 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.2.0/hadoop-hdfs-2.2.0.jar
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-core/1.8/jersey-core-1.8.jar (458 kB at 206 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-json/1.8/jersey-json-1.8.jar (148 kB at 66 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/jackson/jackson-xc/1.7.1/jackson-xc-1.7.1.jar (32 kB at 14 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/sun/jersey/jersey-server/1.8/jersey-server-1.8.jar (694 kB at 280 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/hadoop/hadoop-hdfs/2.2.0/hadoop-hdfs-2.2.0.jar (5.2 MB at 2.1 MB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch20-hbase ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch20-hbase ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch20-hbase/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch20-hbase ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 9 source files to /home/hadoop/work/hadoop-book/ch20-hbase/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch20-hbase/src/main/java/SimpleRowCounter.java:[35,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch20-hbase/src/main/java/HBaseTemperatureBulkImporter.java:[55,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch20-hbase/src/main/java/HBaseTemperatureDirectImporter.java:[64,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch20-hbase/src/main/java/HBaseTemperatureImporter.java:[46,15] Job(org.apache.hadoop.conf.Configuration,java.lang.String) in org.apache.hadoop.mapreduce.Job has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch20-hbase ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch20-hbase/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch20-hbase ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch20-hbase ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch20-hbase ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch20-hbase/target/ch20-hbase-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ ch20-hbase ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/jar.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch20-hbase/target/../../hbase-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch20-hbase/target/../../hbase-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch20-hbase/target/ch20-hbase-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch20-hbase/target/../../hbase-examples.jar
[INFO] 
[INFO] -----------------------< com.hadoopbook:ch21-zk >-----------------------
[INFO] Building Chapter 21: ZooKeeper 4.0                               [17/21]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch21-zk ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch21-zk ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch21-zk/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch21-zk ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 10 source files to /home/hadoop/work/hadoop-book/ch21-zk/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch21-zk ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch21-zk/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch21-zk ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch21-zk ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch21-zk ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch21-zk/target/ch21-zk-4.0.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ ch21-zk ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/jar.xml
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch21-zk/target/../../zookeeper-examples.jar
[WARNING] Configuration options: 'appendAssemblyId' is set to false, and 'classifier' is missing.
Instead of attaching the assembly file: /home/hadoop/work/hadoop-book/ch21-zk/target/../../zookeeper-examples.jar, it will become the file for main project artifact.
NOTE: If multiple descriptors or descriptor-formats are provided for this project, the value of this file will be non-deterministic!
[WARNING] Replacing pre-existing project main-artifact file: /home/hadoop/work/hadoop-book/ch21-zk/target/ch21-zk-4.0.jar
with assembly file: /home/hadoop/work/hadoop-book/ch21-zk/target/../../zookeeper-examples.jar
[INFO] 
[INFO] ------------------< com.hadoopbook:ch22-case-studies >------------------
[INFO] Building Chapter 22: Case Studies 4.0                            [18/21]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ ch22-case-studies ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ ch22-case-studies ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ ch22-case-studies ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 2 source files to /home/hadoop/work/hadoop-book/ch22-case-studies/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[4,57] org.apache.hadoop.record.Record in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[5,53] org.apache.hadoop.record.meta.RecordTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[6,47] org.apache.hadoop.record.meta.RecordTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[9,57] org.apache.hadoop.record.meta.RecordTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[10,73] org.apache.hadoop.record.meta.TypeID in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[11,69] org.apache.hadoop.record.meta.TypeID in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[12,73] org.apache.hadoop.record.meta.TypeID in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[13,74] org.apache.hadoop.record.meta.TypeID in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[14,69] org.apache.hadoop.record.meta.TypeID in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[35,46] org.apache.hadoop.record.meta.RecordTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[38,65] org.apache.hadoop.record.meta.RecordTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[53,53] org.apache.hadoop.record.meta.FieldTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[56,36] org.apache.hadoop.record.meta.FieldTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[57,55] org.apache.hadoop.record.meta.FieldTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[60,38] org.apache.hadoop.record.meta.FieldTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[100,55] org.apache.hadoop.record.RecordOutput in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[110,71] org.apache.hadoop.record.RecordInput in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[120,57] org.apache.hadoop.record.RecordInput in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[146,58] org.apache.hadoop.record.meta.FieldTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[146,136] org.apache.hadoop.record.meta.FieldTypeInfo in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[147,38] org.apache.hadoop.record.meta.Utils in org.apache.hadoop.record.meta has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[218,66] org.apache.hadoop.record.RecordComparator in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[226,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[227,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[231,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[232,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[236,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[237,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[241,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[242,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[246,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[247,43] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[260,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[261,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[265,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[266,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[270,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[271,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[275,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[276,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[280,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[281,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[285,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[286,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[290,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[291,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[295,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[296,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[300,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[301,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[305,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[306,44] org.apache.hadoop.record.Utils in org.apache.hadoop.record has been deprecated
[WARNING] /home/hadoop/work/hadoop-book/ch22-case-studies/src/main/java/fm/last/hadoop/io/records/TrackStats.java:[321,29] org.apache.hadoop.record.RecordComparator in org.apache.hadoop.record has been deprecated
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ ch22-case-studies ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/ch22-case-studies/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ ch22-case-studies ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ ch22-case-studies ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ ch22-case-studies ---
[INFO] Building jar: /home/hadoop/work/hadoop-book/ch22-case-studies/target/ch22-case-studies-4.0.jar
[INFO] 
[INFO] -------------------< com.hadoopbook:hadoop-examples >-------------------
[INFO] Building Hadoop Examples JAR 4.0                                 [19/21]
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ hadoop-examples ---
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (make-assembly) @ hadoop-examples ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/jar.xml
[WARNING] Cannot include project artifact: com.hadoopbook:hadoop-examples:pom:4.0; it doesn't have an associated file or directory.
[INFO] Building jar: /home/hadoop/work/hadoop-book/hadoop-examples/target/../../hadoop-examples.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.4:single (oozie-workflow-application) @ hadoop-examples ---
[INFO] Reading assembly descriptor: ../book/src/main/assembly/oozie-workflow-application.xml
[INFO] Copying files to /home/hadoop/work/hadoop-book/hadoop-examples/target/.
[WARNING] Assembly file: /home/hadoop/work/hadoop-book/hadoop-examples/target/. is not a regular file (it may be a directory). It cannot be attached to the project build for installation or deployment.
[INFO] 
[INFO] -----------------------< com.hadoopbook:snippet >-----------------------
[INFO] Building Snippet testing 4.0                                     [20/21]
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/junit-addons/junit-addons/1.4/junit-addons-1.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/junit-addons/junit-addons/1.4/junit-addons-1.4.pom (626 B at 2.3 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/xerces/xercesImpl/2.6.2/xercesImpl-2.6.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/xerces/xercesImpl/2.6.2/xercesImpl-2.6.2.pom (150 B at 543 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/xerces/xmlParserAPIs/2.6.2/xmlParserAPIs-2.6.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/xerces/xmlParserAPIs/2.6.2/xmlParserAPIs-2.6.2.pom (153 B at 552 B/s)
Downloading from central: https://repo.maven.apache.org/maven2/junit-addons/junit-addons/1.4/junit-addons-1.4.jar
Downloading from central: https://repo.maven.apache.org/maven2/xerces/xercesImpl/2.6.2/xercesImpl-2.6.2.jar
Downloading from central: https://repo.maven.apache.org/maven2/xerces/xmlParserAPIs/2.6.2/xmlParserAPIs-2.6.2.jar
Downloaded from central: https://repo.maven.apache.org/maven2/junit-addons/junit-addons/1.4/junit-addons-1.4.jar (54 kB at 190 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/xerces/xmlParserAPIs/2.6.2/xmlParserAPIs-2.6.2.jar (125 kB at 378 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/xerces/xercesImpl/2.6.2/xercesImpl-2.6.2.jar (1.0 MB at 2.7 MB/s)
[INFO] 
[INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-versions) @ snippet ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ snippet ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/hadoop/work/hadoop-book/snippet/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ snippet ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ snippet ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ snippet ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /home/hadoop/work/hadoop-book/snippet/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 1.6
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ snippet ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ snippet ---
[WARNING] JAR will be empty - no content was marked for inclusion!
[INFO] Building jar: /home/hadoop/work/hadoop-book/snippet/target/snippet-4.0.jar
[INFO] 
[INFO] ------------------------< com.hadoopbook:root >-------------------------
[INFO] Building Hadoop: The Definitive Guide, Example Code 4.0          [21/21]
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary for Hadoop: The Definitive Guide, Example Code 4.0:
[INFO] 
[INFO] Hadoop: The Definitive Guide, Project .............. SUCCESS [ 10.006 s]
[INFO] Common Code ........................................ SUCCESS [ 53.475 s]
[INFO] Chapter 2: MapReduce ............................... SUCCESS [  0.235 s]
[INFO] Chapter 3: The Hadoop Distributed Filesystem ....... SUCCESS [  1.405 s]
[INFO] Chapter 5: Hadoop I/O .............................. SUCCESS [  0.671 s]
[INFO] Chapter 6: Developing a MapReduce Application ...... SUCCESS [  0.352 s]
[INFO] Chapter 8: MapReduce Types and Formats ............. SUCCESS [  0.461 s]
[INFO] Chapter 9: MapReduce Features ...................... SUCCESS [  0.429 s]
[INFO] Chapter 12: Avro ................................... SUCCESS [ 29.975 s]
[INFO] Chapter 13: Parquet ................................ SUCCESS [  7.075 s]
[INFO] Chapter 15: Sqoop .................................. SUCCESS [  0.929 s]
[INFO] Chapter 16: Pig .................................... SUCCESS [  7.414 s]
[INFO] Chapter 17: Hive ................................... SUCCESS [ 14.486 s]
[INFO] Chapter 18: Crunch ................................. SUCCESS [  5.645 s]
[INFO] Chapter 19: Spark .................................. SUCCESS [01:11 min]
[INFO] Chapter 20: HBase .................................. SUCCESS [ 27.607 s]
[INFO] Chapter 21: ZooKeeper .............................. SUCCESS [  0.120 s]
[INFO] Chapter 22: Case Studies ........................... SUCCESS [  0.205 s]
[INFO] Hadoop Examples JAR ................................ SUCCESS [  0.113 s]
[INFO] Snippet testing .................................... SUCCESS [  1.306 s]
[INFO] Hadoop: The Definitive Guide, Example Code ......... SUCCESS [  0.001 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  03:54 min
[INFO] Finished at: 2022-01-14T06:59:02Z
[INFO] ------------------------------------------------------------------------
$ ll
total 13532
drwxrwxr-x 30 hadoop hadoop    4096 Jan 14 06:59 ./
drwxrwxr-x  9 hadoop hadoop    4096 Jan 14 03:46 ../
drwxrwxr-x  3 hadoop hadoop    4096 Dec 29 09:41 appc/
-rw-rw-r--  1 hadoop hadoop 6030072 Jan 14 06:56 avro-examples.jar
drwxrwxr-x  3 hadoop hadoop    4096 Dec 29 09:41 book/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 ch02-mr-intro/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 ch03-hdfs/
drwxrwxr-x  2 hadoop hadoop    4096 Dec 29 09:41 ch04-yarn/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 ch05-io/
drwxrwxr-x  6 hadoop hadoop    4096 Jan 14 06:56 ch06-mr-dev/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 ch08-mr-types/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 ch09-mr-features/
drwxrwxr-x  3 hadoop hadoop    4096 Dec 29 09:41 ch10-setup/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 ch12-avro/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 ch13-parquet/
drwxrwxr-x  2 hadoop hadoop    4096 Dec 29 09:41 ch14-flume/
drwxrwxr-x  5 hadoop hadoop    4096 Jan 14 06:56 ch15-sqoop/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:57 ch16-pig/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:57 ch17-hive/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:57 ch18-crunch/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:57 ch19-spark/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:59 ch20-hbase/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:59 ch21-zk/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:59 ch22-case-studies/
drwxrwxr-x  4 hadoop hadoop    4096 Jan 14 06:56 common/
drwxrwxr-x  5 hadoop hadoop    4096 Dec 29 09:41 conf/
drwxrwxr-x  8 hadoop hadoop    4096 Dec 29 09:41 .git/
-rw-rw-r--  1 hadoop hadoop     140 Dec 29 09:41 .gitignore
drwxrwxr-x  3 hadoop hadoop    4096 Jan 14 06:59 hadoop-examples/
-rw-rw-r--  1 hadoop hadoop  186426 Jan 14 06:59 hadoop-examples.jar
drwxrwxr-x  2 hadoop hadoop    4096 Dec 29 09:41 hadoop-meta/
-rw-rw-r--  1 hadoop hadoop   36953 Jan 14 06:59 hbase-examples.jar
-rw-rw-r--  1 hadoop hadoop    6037 Jan 14 06:57 hive-examples.jar
drwxrwxr-x 13 hadoop hadoop    4096 Dec 29 09:41 input/
-rw-rw-r--  1 hadoop hadoop 7387005 Jan 14 06:56 parquet-examples.jar
-rw-rw-r--  1 hadoop hadoop    7004 Jan 14 06:57 pig-examples.jar
-rw-rw-r--  1 hadoop hadoop    1139 Dec 29 09:41 pom.xml
-rw-rw-r--  1 hadoop hadoop    1683 Dec 29 09:41 README.md
drwxrwxr-x  7 hadoop hadoop    4096 Jan 14 06:59 snippet/
-rw-rw-r--  1 hadoop hadoop   16575 Jan 14 06:58 spark-examples.jar
-rw-rw-r--  1 hadoop hadoop   15851 Jan 14 06:56 sqoop-examples.jar
-rw-rw-r--  1 hadoop hadoop   13501 Jan 14 06:59 zookeeper-examples.jar
$ 
```