#

## 실제 예제에 사용할 데이터 다운로드

ref) http://hadoopsupport.blogspot.com/2017/05/download-national-climatic-data-center.html

> NOTE. 1901년부터 현재까지 데이터가 모두 있음. 속도 문제로 전체 다운로드 시간은 2일 이상 소요.

```
$ mkdir -p ~/work/ncdc
$ cd ~/work/ncdc
$ wget -r -np -A *.gz ftp://ftp.ncdc.noaa.gov/pub/data/noaa/
```

## 데이터 개요

![ncdc](./images/NCDC-0000.png)

![ncdc](./images/NCDC-0001.png)

## 실행 스크립트

* ch02-mr-intro/src/main/awk/max_temperature.sh
```
#!/usr/bin/env bash
for year in all/*
do
  echo -ne `basename $year .gz`"\t"
  gunzip -c $year | \
    awk '{ temp = substr($0, 88, 5) + 0;
           q = substr($0, 93, 1);
           if (temp !=9999 && q ~ /[01459]/ && temp > max) max = temp }
         END { print max }'
done
```

* awk 문 해석
```
for year in all/*
do
  ...
done

all 디렉토리 하위에 있는 모든 파일에 대해서 for loop 수행(loop 수행시마다 처리할 파일명은 year 변수에 할당됨)
``` 

```
echo -ne `basename $year .gz`"\t"

echo -ne "..." 는 no new line + escape chararacter를 처리하라는 의미이고 ``안의 명령문은 shell의 내장 함수가 아닌 외부 프로그램을 실행하라는 의미. basename 명령어는 첫번째 인자로 파일명을 두번째 인자로 제거할 suffix를 받아서 디렉토리와 확장자를 제외한 순수 파일명만을 추출하는 프로그램.
여기서는 처리할 파일들이 상당히 많기 때문에 처리할 파일명을 화면에 출력하여 어떤 파일을 처리중인지 확인하는 용도로만 사용됨.
```

```
gunzip -c $year | awk '...'

주의할것은 앞서 `basename $year .gz`로 $year 변수의 값이 변경되지 않는다는 점. 따라서 위의 명령어는 처리할 파일명(여기서는 확장자가 *.gz 파일이어야함) 을 gunzip을 이용하여 압축을 해제하고 압축 해제한 내용은 파일로 저장하지 않고 표준 출력(-c 옵션)으로 내보내겠다는 의미. | 를 이용했으므로 표준 출력 결과를 awk 의 입력으로 하게됨.
```

```
awk '{ temp = substr($0, 88, 5) + 0;
           q = substr($0, 93, 1);
           if (temp !=9999 && q ~ /[01459]/ && temp > max) max = temp }
         END { print max }'

awk 'pattern { action }' 문법이 기본이지만 여기서는,
awk '{ pattern } BEGIN { action }' 문법이고 pattern이 길어서 각 문장을 하나로 합치기 위해 ; 로 연결. { action } 앞에 END 가 붙은 것은 모든 레코드를 다 처리한 다음에 action을 실행하라는 의미.
```

```
awk '{ temp = substr($0, 88, 5) + 0;
           q = substr($0, 93, 1);
           if (temp !=9999 && q ~ /[01459]/ && temp > max) max = temp }
         END { print max }'

pattern: 압축해제한 입력 값이 들어오면(여러개의 레코드) parsing을 수행

$0 : awk는 입력되는 라인(레코드) 자체를 $0으로 표시
substr($0, 88, 5): 라인(레코드)별로 88번째부터 5개 컬럼값 → 기온(temp)
substr($0, 93, 1): 라인(레코드)별로 93번째부터 1개 컬럼값 → 특성코드(q)
```

```
awk '{ temp = substr($0, 88, 5) + 0;
           q = substr($0, 93, 1);
           if (temp !=9999 && q ~ /[01459]/ && temp > max) max = temp }
         END { print max }'

ncdc에서 기온값이 누락된 경우에는 9999로 설정함.
특성코드값이 유효한지 체크: 0 1 4 5 9 에 해당하면 유효.
```

```
awk '{ temp = substr($0, 88, 5) + 0;
           q = substr($0, 93, 1);
           if (temp !=9999 && q ~ /[01459]/ && temp > max) max = temp }
         END { print max }'

결론을 말하자면, 압축해제된 기상관측소별 측정 파일의 내용(하나)을 입력으로 주면 최고 온도를 추출.(기상관측소별 해당 년도의 최고온도)
```
> ref) https://recipes4dev.tistory.com/171

* 결론
```
for loop gunzip -c | awk {} done
형식으로 처리하는 방식으로 개별 .gz 파일을 압축해제 한다음 awk의 입력으로 들어와서 처리하게 되는데 gunzip이 map함수 awk가 reduce함수에 비교할수 있는데 문제는 gunzip을 수행이 끝난 결과가 awk로 입력이 되므로 .gz 파일이 크든 작든 상관없이 하나의 파일이 awk의 입력으로 들어가게 되는 문제점이 있음. 그리고 이것을 병렬처리가 아닌 순차적으로 처리하기 때문에 단일 머신에서 수행시간의 한계점은 분명히 존재함.
그리고 년도별 최고온도를 산출하기 위해서는 처리작업이 더 필요함.
```