#

## sample.txt 를 HDFS에 저장
```
$ cd ~/work/hadoop-book/
$ hadoop fs -mkdir -p /user/hadoop/input/ncdc
$ hadoop fs -put input/ncdc/sample.txt /user/hadoop/input/ncdc/.
$ hadoop fs -ls /user/hadoop/input/ncdc/
```

## 실행
* 입력 파일 확인
```
$ hadoop fs -cat /user/hadoop/input/ncdc/sample.txt
```
```
0067011990999991950051507004+68750+023550FM-12+038299999V0203301N00671220001CN9999999N9+00001+99999999999
0043011990999991950051512004+68750+023550FM-12+038299999V0203201N00671220001CN9999999N9+00221+99999999999
0043011990999991950051518004+68750+023550FM-12+038299999V0203201N00261220001CN9999999N9-00111+99999999999
0043012650999991949032412004+62300+010750FM-12+048599999V0202701N00461220001CN0500001N9+01111+99999999999
0043012650999991949032418004+62300+010750FM-12+048599999V0202701N00461220001CN0500001N9+00781+99999999999hadoop@hadoop:~/work/hadoop-book$ 
```

* 실행
```
$ cd ~/work/hadoop-book/
$ hadoop fs -rmdir /user/hadoop/output/
```
> NOTE. 이미 output 디렉토리가 있다면 옮기거나 삭제할것!

```
$ hadoop jar hadoop-examples.jar MaxTemperature input/ncdc/sample.txt output
```
```
2022-01-14 07:31:35,261 INFO client.DefaultNoHARMFailoverProxyProvider: Connecting to ResourceManager at localhost/127.0.0.1:8032
2022-01-14 07:31:36,075 WARN mapreduce.JobResourceUploader: Hadoop command-line option parsing not performed. Implement the Tool interface and execute your application with ToolRunner to remedy this.
2022-01-14 07:31:36,102 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1641559844955_0010
2022-01-14 07:31:36,398 INFO input.FileInputFormat: Total input files to process : 1
2022-01-14 07:31:36,487 INFO mapreduce.JobSubmitter: number of splits:1
2022-01-14 07:31:36,748 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1641559844955_0010
2022-01-14 07:31:36,749 INFO mapreduce.JobSubmitter: Executing with tokens: []
2022-01-14 07:31:37,000 INFO conf.Configuration: resource-types.xml not found
2022-01-14 07:31:37,001 INFO resource.ResourceUtils: Unable to find 'resource-types.xml'.
2022-01-14 07:31:37,713 INFO impl.YarnClientImpl: Submitted application application_1641559844955_0010
2022-01-14 07:31:37,776 INFO mapreduce.Job: The url to track the job: http://hadoop:8088/proxy/application_1641559844955_0010/
2022-01-14 07:31:37,778 INFO mapreduce.Job: Running job: job_1641559844955_0010
2022-01-14 07:31:49,817 INFO mapreduce.Job: Job job_1641559844955_0010 running in uber mode : false
2022-01-14 07:31:49,818 INFO mapreduce.Job:  map 0% reduce 0%
2022-01-14 07:31:59,274 INFO mapreduce.Job:  map 100% reduce 0%
2022-01-14 07:32:05,322 INFO mapreduce.Job:  map 100% reduce 100%
2022-01-14 07:32:06,343 INFO mapreduce.Job: Job job_1641559844955_0010 completed successfully
2022-01-14 07:32:06,467 INFO mapreduce.Job: Counters: 54
        File System Counters
                FILE: Number of bytes read=61
                FILE: Number of bytes written=545481
                FILE: Number of read operations=0
                FILE: Number of large read operations=0
                FILE: Number of write operations=0
                HDFS: Number of bytes read=644
                HDFS: Number of bytes written=17
                HDFS: Number of read operations=8
                HDFS: Number of large read operations=0
                HDFS: Number of write operations=2
                HDFS: Number of bytes read erasure-coded=0
        Job Counters 
                Launched map tasks=1
                Launched reduce tasks=1
                Data-local map tasks=1
                Total time spent by all maps in occupied slots (ms)=7568
                Total time spent by all reduces in occupied slots (ms)=3002
                Total time spent by all map tasks (ms)=7568
                Total time spent by all reduce tasks (ms)=3002
                Total vcore-milliseconds taken by all map tasks=7568
                Total vcore-milliseconds taken by all reduce tasks=3002
                Total megabyte-milliseconds taken by all map tasks=7749632
                Total megabyte-milliseconds taken by all reduce tasks=3074048
        Map-Reduce Framework
                Map input records=5
                Map output records=5
                Map output bytes=45
                Map output materialized bytes=61
                Input split bytes=115
                Combine input records=0
                Combine output records=0
                Reduce input groups=2
                Reduce shuffle bytes=61
                Reduce input records=5
                Reduce output records=2
                Spilled Records=10
                Shuffled Maps =1
                Failed Shuffles=0
                Merged Map outputs=1
                GC time elapsed (ms)=155
                CPU time spent (ms)=2230
                Physical memory (bytes) snapshot=491683840
                Virtual memory (bytes) snapshot=5116289024
                Total committed heap usage (bytes)=391118848
                Peak Map Physical memory (bytes)=294182912
                Peak Map Virtual memory (bytes)=2548903936
                Peak Reduce Physical memory (bytes)=197500928
                Peak Reduce Virtual memory (bytes)=2567385088
        Shuffle Errors
                BAD_ID=0
                CONNECTION=0
                IO_ERROR=0
                WRONG_LENGTH=0
                WRONG_MAP=0
                WRONG_REDUCE=0
        File Input Format Counters 
                Bytes Read=529
        File Output Format Counters 
                Bytes Written=17
```

* 확인
```
$ hadoop fs -ls output
```
```
Found 2 items
-rw-r--r--   1 hadoop supergroup          0 2022-01-14 07:32 output/_SUCCESS
-rw-r--r--   1 hadoop supergroup         17 2022-01-14 07:32 output/part-r-00000
```


```
$ hadoop fs -cat output/part-r-00000
```
```
1949    111
1950    22
```


* 웹 확인
 
![MapReduce](./images/MapReduce-0000.png)