# 개요


## Hive 아키텍쳐 

* 하이브 아키텍쳐와 데이터 모델

https://youtu.be/BTwLa3HrT-k

https://trello.com/c/d0BYROd5


* 하이브 쿼리 인터페이스와 데이터 처리과정

https://youtu.be/5jbHpJjhiVY

https://trello.com/c/NzEAz5ID


* 하이브 테이블 관리

https://youtu.be/06_WdTHXnN0

https://trello.com/c/ToUunVPo

