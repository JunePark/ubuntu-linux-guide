# 개요

ref) https://kontext.tech/column/hadoop/561/apache-hive-312-installation-on-linux-guide

NOTE. hive는 최신버전인 3.1.2에서도 java11에서 실행시 에러가 발생함으로 java8을 설치

## 설치

```
$ cd ~/work/download
$ wget https://dlcdn.apache.org/hive/hive-3.1.2/apache-hive-3.1.2-bin.tar.gz
$ cd /opt/paltform/sw
$ tar zxvf ~/work/download/apache-hive-3.1.2-bin.tar.gz
$ ln -s apache-hive-3.1.2-bin hive
```

NOTE. 2022-05-16 현재 최신 버전
```
https://dlcdn.apache.org/hive/hive-3.1.3/apache-hive-3.1.3-bin.tar.gz
```

## 환경 변수 설정

* 환경 변수 설정
```
$ vi /opt/platform/platform-env.sh
```
```
...
if [[ -d ${PLATFORM_HOME}/sw/hive/ ]]; then
        export HIVE_HOME=${PLATFORM_HOME}/sw/hive
fi

...
if [[ -d ${HIVE_HOME} ]]; then
        TEMP_PATH=${TEMP_PATH}:${HIVE_HOME}/bin
fi
```

## derby 모드로 설정(not recommended)

### 설정
```
$ cd ${HIVE_HOME}/conf
$ vi hive-site.xml
```

```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
  <property>
    <name>hive.metastore.warehouse.dir</name>
    <value>/user/hive/warehouse</value>
  </property>
  <property>
    <name>javax.jdo.option.ConnectionURL</name>
    <value>jdbc:derby:;databaseName=/home/hadoop/metastore_db;create=true</value>
  </property>
</configuration>
```
> NOTE. metastore는 derby모드로 먼저 설정한 다음 제대로 동작하면 이후에 설정 변경

### metastore 생성
```
$ cd ~
$ rm -rf metastore_db/
$ schematool -initSchema -dbType derby
```
```
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/apache-hive-3.1.2-bin/lib/log4j-slf4j-impl-2.10.0.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/hadoop-3.3.1/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
Metastore connection URL:        jdbc:derby:;databaseName=/home/hadoop/metastore_db;create=true
Metastore Connection Driver :    org.apache.derby.jdbc.EmbeddedDriver
Metastore connection User:       APP
Starting metastore schema initialization to 3.1.0
Initialization script hive-schema-3.1.0.derby.sql

...

Initialization script completed
schemaTool completed
```
> NOTE. ~/metastore_db/ 디렉토리에 생성.

### 실행

* 실행 체크
```
$ hive
```
```
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/apache-hive-3.1.2-bin/lib/log4j-slf4j-impl-2.10.0.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/hadoop-3.3.1/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
Hive Session ID = b5972756-c172-4b49-8aa2-2d7994cae07f

Logging initialized using configuration in jar:file:/home/hadoop/platform/sw/apache-hive-3.1.2-bin/lib/hive-common-3.1.2.jar!/hive-log4j2.properties Async: true
Hive-on-MR is deprecated in Hive 2 and may not be available in the future versions. Consider using a different execution engine (i.e. spark, tez) or using Hive 1.X releases.
hive> 
```
> NOTE. 처음 실행시 hdfs에 /tmp 디렉토리 생성됨.

```
hive> show tables;
```
```
OK
Time taken: 0.556 seconds
```

* 테이블 생성 체크
```
$ echo 'X' > ~/work/dummy.txt
$ hive -e "CREATE TABLE dummy (value STRING); \
LOAD DATA LOCAL INPATH '/home/hadoop/work/dummy.txt' \
OVERWRITE INTO TABLE dummy"
```
> NOTE. 최초 create table 실행시 hdfs에 /user/hive/warehouse 디렉토리 생성됨.

```
$ hive
> select * from dummy;
```

## 외부 metastore 이용

### mariadb 설치

* 이미 설치가 되어 있는 경우에는 삭제하고 설치
```
# dpkg -l|grep mariadb
# dpkg --purge --force-remove-reinstreq mariadb-server 
# dpkg --purge --force-remove-reinstreq mariadb-server-10.3 
# dpkg --purge --force-remove-reinstreq mariadb-server-core-10.3
# dpkg -l|grep mariadb
```

* 설치
```
# apt install mariadb-server
# systemctl status mariadb
```
> NOTE. 설치 후 기동중인지 확인.

* 보안 설정
```
# mysql_secure_installation
```
> NOTE. 가능하면 보안 설정을 하자!

```


NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] 
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] 
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] n
 ... skipping.

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] 
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] 
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
> NOTE. 편의상 원격 root 로그인을 허용하고 나머지는 기본값 상태 그대로 엔터.

* 로그인
```
# mysql -u root -p
```
```
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 59
Server version: 10.3.32-MariaDB-0ubuntu0.20.04.1 Ubuntu 20.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
```

```
> use mysql;
> select user, host from user;
```
```
+------+-----------+
| user | host      |
+------+-----------+
| root | localhost |
+------+-----------+
1 row in set (0.000 sec)

MariaDB [mysql]> 
```
NOTE. 현재는 root 유저만 있음.

* 데이터베이스(schema) 생성 및 사용자 생성
```
> CREATE DATABASE hive_metastore;
> CREATE USER 'hive'@'localhost' IDENTIFIED BY 'hive';
> GRANT ALL ON hive_metastore.* TO 'hive'@'localhost';

> use mysql;
> select user, host from user;
> show databases;
```
NOTE. hive 계정이 hive_metastore 데이터베이스를 사용할수 있게 설정.

* mysql-connector 설치
```
$ cd ~/work
$ wget https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.27/mysql-connector-java-8.0.27.jar
$ cp -p mysql-connector-java-8.0.27.jar $HIVE_HOME/lib/.
```

* 설정 파일 임시 생성
```
$ vi $HIVE_HOME/conf/hive-site.xml
```
```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
  <property>
    <name>hive.metastore.warehouse.dir</name>
    <value>/user/hive/warehouse</value>
  </property>
  <property>
    <name>javax.jdo.option.ConnectionURL</name>
    <value>jdbc:mysql://localhost/hive_metastore</value>
  </property>
  <property>
    <name>javax.jdo.option.ConnectionDriverName</name>
    <!--<value>org.mariadb.jdbc.Driver</value>-->
    <value>com.mysql.cj.jdbc.Driver</value>
  </property>
  <property>
    <name>javax.jdo.option.ConnectionUserName</name>
    <value>hive</value>
  </property>
  <property>
    <name>javax.jdo.option.ConnectionPassword</name>
    <value>hive</value>
  </property>
  <!--
  <property>
     <name>hive.metastore.uris</name>
     <value>thrift://localhost:9083</value>
  </property>
  <property>
   <name>hive.metastore.db.type</name>
   <value>mysql</value>
  </property>
  -->
</configuration>
```
> NOTE. hive.metastore.* 설정은 별도의 metastore 프로세스를 띄워야 함으로 여기서는 일단 사용 안함

> $HIVE_HOME/bin/hive --service metastore & 로 서비스 시작

* init schema
```
$ cd $HIVE_HOME/bin
$ ./schematool -dbType mysql -initSchema
```
```
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/apache-hive-3.1.2-bin/lib/log4j-slf4j-impl-2.10.0.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/hadoop-3.3.1/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
Metastore connection URL:        jdbc:mysql://localhost/hive_metastore
Metastore Connection Driver :    com.mysql.cj.jdbc.Driver
Metastore connection User:       hive
Starting metastore schema initialization to 3.1.0
Initialization script hive-schema-3.1.0.mysql.sql

...

Initialization script completed
schemaTool completed
```


```
$ hive --service schemaTool -dbType mysql -info
```
```
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/apache-hive-3.1.2-bin/lib/log4j-slf4j-impl-2.10.0.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/hadoop-3.3.1/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
Metastore connection URL:        jdbc:mysql://localhost/hive_metastore
Metastore Connection Driver :    com.mysql.cj.jdbc.Driver
Metastore connection User:       hive
Hive distribution version:       3.1.0
Metastore schema version:        3.1.0
schemaTool completed
```

* 스키마 생성 확인
```
$ mysql -u hive -p
> use hive_metastore;
> show tables;
```
```
+-------------------------------+
| Tables_in_hive_metastore      |
+-------------------------------+
| AUX_TABLE                     |
| BUCKETING_COLS                |
| CDS                           |
| COLUMNS_V2                    |
| COMPACTION_QUEUE              |
| COMPLETED_COMPACTIONS         |
| COMPLETED_TXN_COMPONENTS      |
| CTLGS                         |
| DATABASE_PARAMS               |
| DBS                           |
| DB_PRIVS                      |
| DELEGATION_TOKENS             |
| FUNCS                         |
| FUNC_RU                       |
| GLOBAL_PRIVS                  |
| HIVE_LOCKS                    |
| IDXS                          |
| INDEX_PARAMS                  |
| I_SCHEMA                      |
| KEY_CONSTRAINTS               |
| MASTER_KEYS                   |
| MATERIALIZATION_REBUILD_LOCKS |
| METASTORE_DB_PROPERTIES       |
| MIN_HISTORY_LEVEL             |
| MV_CREATION_METADATA          |
| MV_TABLES_USED                |
| NEXT_COMPACTION_QUEUE_ID      |
| NEXT_LOCK_ID                  |
| NEXT_TXN_ID                   |
| NEXT_WRITE_ID                 |
| NOTIFICATION_LOG              |
| NOTIFICATION_SEQUENCE         |
| NUCLEUS_TABLES                |
| PARTITIONS                    |
| PARTITION_EVENTS              |
| PARTITION_KEYS                |
| PARTITION_KEY_VALS            |
| PARTITION_PARAMS              |
| PART_COL_PRIVS                |
| PART_COL_STATS                |
| PART_PRIVS                    |
| REPL_TXN_MAP                  |
| ROLES                         |
| ROLE_MAP                      |
| RUNTIME_STATS                 |
| SCHEMA_VERSION                |
| SDS                           |
| SD_PARAMS                     |
| SEQUENCE_TABLE                |
| SERDES                        |
| SERDE_PARAMS                  |
| SKEWED_COL_NAMES              |
| SKEWED_COL_VALUE_LOC_MAP      |
| SKEWED_STRING_LIST            |
| SKEWED_STRING_LIST_VALUES     |
| SKEWED_VALUES                 |
| SORT_COLS                     |
| TABLE_PARAMS                  |
| TAB_COL_STATS                 |
| TBLS                          |
| TBL_COL_PRIVS                 |
| TBL_PRIVS                     |
| TXNS                          |
| TXN_COMPONENTS                |
| TXN_TO_WRITE_ID               |
| TYPES                         |
| TYPE_FIELDS                   |
| VERSION                       |
| WM_MAPPING                    |
| WM_POOL                       |
| WM_POOL_TO_TRIGGER            |
| WM_RESOURCEPLAN               |
| WM_TRIGGER                    |
| WRITE_SET                     |
+-------------------------------+
74 rows in set (0.001 sec)

MariaDB [hive_metastore]> 
```

* 한글 깨짐 해결
```
# mysql -u root -p

use_hive_metastore;
alter table COLUMNS_V2 modify COMMENT varchar(256) character set utf8 collate utf8_general_ci;
alter table TABLE_PARAMS modify PARAM_VALUE mediumtext character set utf8 collate utf8_general_ci;
alter table SERDE_PARAMS modify PARAM_VALUE mediumtext character set utf8 collate utf8_general_ci;
alter table SD_PARAMS modify PARAM_VALUE mediumtext character set utf8 collate utf8_general_ci;
alter table PARTITION_PARAMS modify PARAM_VALUE varchar(4000) character set utf8 collate utf8_general_ci;
alter table PARTITION_KEYS modify PKEY_COMMENT varchar(4000) character set utf8 collate utf8_general_ci;
alter table INDEX_PARAMS modify PARAM_VALUE varchar(4000) character set utf8 collate utf8_general_ci;
alter table DATABASE_PARAMS modify PARAM_VALUE varchar(4000) character set utf8 collate utf8_general_ci;
alter table DBS modify `DESC` varchar(4000) character set utf8 collate utf8_general_ci;
```

##  연결 테스트

* metastore 서비스를 통한 접속 체크
```
$ hive
> show tables;
> CREATE TABLE dummy (value STRING);
> LOAD DATA LOCAL INPATH '/home/hadoop/work/dummy.txt' OVERWRITE INTO TABLE dummy;
```
> metastore 서비스롤 통하여 접속.

* mariadb에 접속이 가능한지 beeline으로 접속 체크
```
$ beeline
> !connect jdbc:mysql://localhost/hive_metastore hive hive
> show tables;
```
```
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/apache-hive-3.1.2-bin/lib/log4j-slf4j-impl-2.10.0.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/hadoop-3.3.1/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
Beeline version 3.1.2 by Apache Hive
beeline> !connect jdbc:mysql://localhost/hive_metastore hive hive
Connecting to jdbc:mysql://localhost/hive_metastore
Connected to: MySQL (version 5.5.5-10.3.32-MariaDB-0ubuntu0.20.04.1)
Driver: MySQL Connector/J (version mysql-connector-java-8.0.27 (Revision: e920b979015ae7117d60d72bcc8f077a839cd791))
Transaction isolation: TRANSACTION_REPEATABLE_READ
0: jdbc:mysql://localhost/hive_metastore>
```

* hiveserver2 서비스 실행
```
$ $HIVE_HOME/bin/hive --service hiveserver2 &
```
> default port는 10000.
> web은 http://localhost:10002/ 로 확인 가능. (http://IPaddr:10002/ 로도 확인 가능)

* hiveserver2 를 통한 thrift 서비스 접속 체크
```
$ beeline -u jdbc:hive2://localhost:10000 -n hive 
```
```
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/apache-hive-3.1.2-bin/lib/log4j-slf4j-impl-2.10.0.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/home/hadoop/platform/sw/hadoop-3.3.1/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
Connecting to jdbc:hive2://localhost:10000
21/12/29 05:09:52 [main]: WARN jdbc.HiveConnection: Failed to connect to localhost:10000
Error: Could not open client transport with JDBC Uri: jdbc:hive2://localhost:10000: Failed to open new session: java.lang.RuntimeException: org.apache.hadoop.ipc.RemoteException(org.apache.hadoop.security.authorize.AuthorizationException): User: hadoop is not allowed to impersonate hive (state=08S01,code=0)
Beeline version 3.1.2 by Apache Hive
```
> NOTE. 권한 관련하여 에러 발생

> NOTE. 로그는 /tmp/hadoop/hive.log

* hadoop의 core-site.xml 설정 변경
```
$ cd $HADOOP_HOME/etc/hadoop
$ vi core-site.xml
```
```
  <property>
      <name>hadoop.proxyuser.hadoop.groups</name>
      <value>*</value>
  </property>
  <property>
      <name>hadoop.proxyuser.hadoop.hosts</name>
      <value>*</value>
  </property>
```
> NOTE. ref) https://blog.naver.com/hanajava/221121197997 

* 재기동
```
$ stop-all.sh
$ start-all.sh
```

* 접속 체크
```
$ beeline -u jdbc:hive2://localhost:10000 -n hadoop -p
```
```
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/opt/platform/sw/apache-hive-3.1.3-bin/lib/log4j-slf4j-impl-2.17.1.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/opt/platform/sw/hadoop-3.3.2/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
Connecting to jdbc:hive2://localhost:10000/;user=hadoop
Enter password for jdbc:hive2://localhost:10000/: 
Connected to: Apache Hive (version 3.1.3)
Driver: Hive JDBC (version 3.1.3)
Transaction isolation: TRANSACTION_REPEATABLE_READ
Beeline version 3.1.3 by Apache Hive
0: jdbc:hive2://localhost:10000/> show tables;
INFO  : Compiling command(queryId=hadoop_20220530003912_aec517df-4517-4176-876c-b4cde72f61e5): show tables
INFO  : Concurrency mode is disabled, not creating a lock manager
INFO  : Semantic Analysis Completed (retrial = false)
INFO  : Returning Hive schema: Schema(fieldSchemas:[FieldSchema(name:tab_name, type:string, comment:from deserializer)], properties:null)
INFO  : Completed compiling command(queryId=hadoop_20220530003912_aec517df-4517-4176-876c-b4cde72f61e5); Time taken: 0.027 seconds
INFO  : Concurrency mode is disabled, not creating a lock manager
INFO  : Executing command(queryId=hadoop_20220530003912_aec517df-4517-4176-876c-b4cde72f61e5): show tables
INFO  : Starting task [Stage-0:DDL] in serial mode
INFO  : Completed executing command(queryId=hadoop_20220530003912_aec517df-4517-4176-876c-b4cde72f61e5); Time taken: 0.017 seconds
INFO  : OK
INFO  : Concurrency mode is disabled, not creating a lock manager
+-----------+
| tab_name  |
+-----------+
| dummy     |
+-----------+
1 row selected (0.126 seconds)
0: jdbc:hive2://localhost:10000/> CREATE TABLE dummy2 (value STRING);
0: jdbc:hive2://localhost:10000/> LOAD DATA LOCAL INPATH '/home/hadoop/work/dummy.txt' OVERWRITE INTO TABLE dummy2;
```
> NOTE. 결론) beeline 의 -n 계정명은 HDFS상에 사용할 계정명 입력

## 완성

### hive 서비스 기동 및 접속

* metastore 실행
```
$ nohup hive --service metastore &
```
> NOTE. 사용하고자 하는 경우에는 hive-site.xml 수정 필요.

* hiveserver2 실행 
```
$ nohup hiveserver2 &
```

* beeline으로 접속
```
beeline
```
```
!connect jdbc:hive2://localhost:10000 hadoop
```
