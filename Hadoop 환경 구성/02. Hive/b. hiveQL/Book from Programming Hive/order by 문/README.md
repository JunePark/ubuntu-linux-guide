# ORDER BY vs SORT BY

ORDER BY는 전체 정령 SORT BY는 각 리듀서별 정렬

* ORDER BY
```
SELET s.ymd, s.symbol, s.price_close
 FROM stocks s
 ORDER BY s.ymd ASC, s.symbol DESC
```
NOTE. hive.mapred.mode=strict로 설정(기본값은 nonstrict)된 경우 LIMIT을 요구한다.

* SORT BY
```
SELET s.ymd, s.symbol, s.price_close
 FROM stocks s
 SORT BY s.ymd ASC, s.symbol DESC
```

# DISTRIBUTE BY with SORT BY

```
SELECT s.ymd, s.symbol, s.price_close
  FROM stocks s
  DISTRIBUTE BY s.symbol
  SORT BY s.symbol ASC, s.ymd ASC
;
```
> NOTE. DISTRIBUTE BY 절에 명시한 컬럼값을 포함하는 레코드가 동일한 리듀서로 보내는 것을 보장한다.

> DISTRIBUTE BY 는 GROUP BY와 비슷해 보인다.

# CLUSTER BY

* DISTRIBUTE BY 와 SORT BY에 동일한 컬럼 사용
```
SELECT s.ymd, s.symbol, s.price_close
  FROM stocks s
  DISTRIBUTE BY s.symbol
  SORT BY s.symbol ASC
;
```

* 위와 같은 조건일때 CLUSTER BY 절 사용
```
SELECT s.ymd, s.symbol, s.price_close
  FROM stocks s
  CLUSTER BY s.symbol
;
```