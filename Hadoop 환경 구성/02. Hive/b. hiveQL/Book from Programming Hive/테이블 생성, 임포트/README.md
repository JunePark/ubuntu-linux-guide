#

## 테이블 생성, 데이터 임포트


### stock 테이블

* 생성
```
USE default;

DROP TABLE IF EXISTS stocks;

CREATE EXTERNAL TABLE IF NOT EXISTS stocks (
 `exchange` STRING,
 symbol STRING,
 ymd STRING,
 price_open FLOAT,
 price_high FLOAT,
 price_low FLOAT,
 price_close FLOAT,
 volume INT,
 price_adj_close FLOAT)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
LOCATION '/data/stocks'
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/programming_hive/data/stocks/stocks.csv'
OVERWRITE INTO TABLE stocks
;
```

* 체크
```
select * from stocks limit 10;
```


### employees 테이블

#### 일반 테이블

* 생성
```
CREATE TABLE IF NOT EXISTS mydb.employees
(
 name STRING COMMENT 'Employee name',
 salary FLOAT COMMENT 'Employee salary',
 subordinates ARRAY<STRING> COMMENT 'Names of subordinates',
 deductions MAP<STRING, FLOAT>  COMMENT 'Keys are deductions names, values are percentages',
 address STRUCT<street:STRING, city:STRING, state:STRING, zip:INT>  COMMENT 'Home address'
)
COMMENT 'Description of the table'
LOCATION '/user/hive/warehouse/mydb.db/employees'
TBLPROPERTIES ('creator'='me', 'created_at'='2012-01-02 10:00:00')
;
```

* 데이터 로드
```
$ hadoop fs -put employees.txt /user/hive/warehouse/mydb.db/employees/.
```

```
select * from mydb.employees;
```

#### 파티션 테이블

* 생성
```
CREATE TABLE IF NOT EXISTS mydb.employees2
(
 name STRING COMMENT 'Employee name',
 salary FLOAT COMMENT 'Employee salary',
 subordinates ARRAY<STRING> COMMENT 'Names of subordinates',
 deductions MAP<STRING, FLOAT>  COMMENT 'Keys are deductions names, values are percentages',
 address STRUCT<street:STRING, city:STRING, state:STRING, zip:INT>  COMMENT 'Home address'
)
PARTITIONED BY(country STRING, state STRING)
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/programming_hive/data/employees/employees.txt'
INTO TABLE mydb.employees2
PARTITION (country = 'US', state = 'CA')
;
```

```
LOAD DATA LOCAL INPATH '/home/hadoop/work/programming_hive/data/employees/employees.txt'
INTO TABLE mydb.employees2
PARTITION (country = 'CA', state = 'AB')
;
```

* 체크
```
select name, salary from mydb.employees2;
```

```
select name, salary from mydb.employees2
 where country = 'CA'
;
```

* 파티션 체크
```
show partitions mydb.employees2;
```
```
+----------------------+
|      partition       |
+----------------------+
| country=CA/state=AB  |
| country=US/state=CA  |
+----------------------+
```


### 테이블간 데이터 임포트

#### 샘플 테이블

* 테이블 생성
```
DROP TABLE IF EXISTS mydb.staged_employees;

CREATE TABLE IF NOT EXISTS mydb.staged_employees
(
 name STRING COMMENT 'Employee name',
 salary FLOAT COMMENT 'Employee salary',
 subordinates ARRAY<STRING> COMMENT 'Names of subordinates',
 deductions MAP<STRING, FLOAT>  COMMENT 'Keys are deductions names, values are percentages',
 address STRUCT<street:STRING, city:STRING, state:STRING, zip:INT>  COMMENT 'Home address'
)
PARTITIONED BY(cnty STRING, st STRING)
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/programming_hive/data/employees/employees.txt'
INTO TABLE mydb.staged_employees
PARTITION (cnty = 'KR', st = 'SL')
;
```

#### 파티션별 데이터 임포트

* 파티션별 데이터 임포트
```
INSERT OVERWRITE TABLE mydb.employees2
PARTITION (country = 'KR', state = 'GG')
select * from mydb.staged_employees
 where country = 'KR' and state = 'SL'
;
```
```
Error: Error while compiling statement: FAILED: SemanticException [Error 10004]: Line 4:6 Invalid table alias or column reference 'country': (possible column names are: name, salary, subordinates, deductions, address, cnty, st) (state=42000,code=10004)
```
> NOTE. select * from 이 문제.

* 파티션별 데이터 임포트(수정)
```
INSERT OVERWRITE TABLE mydb.employees2
PARTITION (country = 'KR', state = 'GG')
select name, salary, subordinates, deductions, address
  from mydb.staged_employees
 where cnty = 'KR' and st = 'SL'
; 
```
> target과 source의 컬럼 개수가 맞아야 에러가 안남.

```
select * from mydb.employees2
 where country = 'KR' and state = 'GG'
;
```

#### dynamic partition insert

* 테스트 데이터 입력
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/programming_hive/data/employees/employees.txt'
INTO TABLE mydb.staged_employees
PARTITION (cnty = 'KR', st = 'UL')
;
```

* 테이블간 데이터 임포트
```
set hive.exec.dynamic.partition.mode=nonstrict;

INSERT OVERWRITE TABLE mydb.employees2
PARTITION (country, state)
select * from mydb.staged_employees
 where cnty = 'CA' and st = 'BC'
; 
```
> PARITION에 값을 명시적으로 입력하지 않으면 위와 같이 select *로 해서 컬럼 위치로 파티션을 인식함.

> PARITION (country = 'US', state) 처럼 정적파티션키는 동적파티션키 앞에 위치해야함.

```
select * from mydb.employees2
 where country = 'CA' and state = 'BC'
;