# 조인문

> NOTE. Hive는 조인을 수행한 후에 WHERE절을 평가한다. 따라서 WHERE절은 NULL이 되지 않는 컬럼값에 대해서만 필터를 적용하는 표현식을 써야한다. 이러한 필터는 내부조인의 ON 에서는 가능하지만 외부조인은 무시한다.


## 내부 조인(Inner Join)

동등 조인(EQUI-JOIN) 만 지원
> NOTE. 동등 조인, 비동등 조인은 ON절에 오는 조인 조건의 표현식(predicate)을 의미하는 것이지 조인문을 분류하는 것을 의미 하지는 않는다.


* 셀프 조인(self-join)을 이용한 내부 조인
```
SELECT a.ymd, a.price_close, b.price_close
  FROM stocks a JOIN stocks b
    ON a.ymd = b.ymd
 WHERE a.symbol = 'AAPL' AND b.symbol = 'IBM'
;
```
```
+-------------+----------------+----------------+
|    a.ymd    | a.price_close  | b.price_close  |
+-------------+----------------+----------------+
| 1984-09-07  | 26.5           | 121.62         |
| 1984-09-10  | 26.37          | 122.75         |
| 1984-09-11  | 26.87          | 122.25         |
| 1984-09-12  | 26.12          | 122.5          |
| 1984-09-13  | 27.5           | 126.12         |
...
| 2010-02-02  | 195.86         | 125.53         |
| 2010-02-03  | 199.23         | 125.66         |
| 2010-02-04  | 192.05         | 123.0          |
| 2010-02-05  | 195.46         | 123.52         |
| 2010-02-08  | 194.12         | 121.88         |
+-------------+----------------+----------------+
```
> NOTE. 1984-09-07보다 오래된 IBM의 주가는 표시되지 않음.


* 비동등 조인(NON-EQUI-JOIN) 으로 실행
```
SELECT a.ymd, a.price_close, b.price_close
  FROM stocks a JOIN stocks b
    ON a.ymd <= b.ymd
 WHERE a.symbol = 'AAPL' AND b.symbol = 'IBM'
;
```
> NOTE.  결과는 EQUI-JOIN 결과와 동일

> NOTE. Hive는 ON절에 OR 사용을 지원 안함.


## 테스트 테이블 생성
```
USE default;

CREATE EXTERNAL TABLE IF NOT EXISTS dividends (
    ymd       STRING,
    dividend  FLOAT
)
PARTITIONED BY (`exchange` STRING, symbol STRING)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ','
LOCATION '/data/dividends'
;

CREATE EXTERNAL TABLE IF NOT EXISTS staged_dividends (
    `exchange` STRING,
    symbol     STRING,
    ymd        STRING,
    dividend   FLOAT
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ','
LOCATION '/data/staged_dividends'
;

LOAD DATA LOCAL INPATH '/home/hadoop/work/programming_hive/data/dividends/dividends.csv'
OVERWRITE INTO TABLE staged_dividends
;

SELECT `exchange`, count(*)
  FROM staged_dividends
 GROUP BY `exchange`
```
```
+-----------+--------+
| exchange  |  _c1   |
+-----------+--------+
| NASDAQ    | 4353   |
| NYSE      | 10855  |
+-----------+--------+
```

```
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions=1000;
set hive.exec.max.dynamic.partitions.pernode=300;

INSERT OVERWRITE TABLE dividends
PARTITION (`exchange` = 'NASDAQ', symbol)
select ymd, dividend, symbol from staged_dividends
 where `exchange` = 'NASDAQ'
;

INSERT OVERWRITE TABLE dividends
PARTITION (`exchange` = 'NYSE', symbol)
select ymd, dividend, symbol from staged_dividends
 where `exchange` = 'NYSE'
;

SELECT `exchange`, count(*)
  FROM dividends
 GROUP BY `exchange`
; 
```
```
+-----------+--------+
| exchange  |  _c1   |
+-----------+--------+
| NASDAQ    | 4353   |
| NYSE      | 10855  |
+-----------+--------+
```


## 내부 조인(Inner Join) (feat. dividends)

* 애플의 배당금 지급일의 종가 조회
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM stocks s JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
 WHERE s.symbol = 'AAPL'
 ORDER BY s.ymd
; 
```
```
+-------------+-----------+----------------+-------------+
|    s.ymd    | s.symbol  | s.price_close  | d.dividend  |
+-------------+-----------+----------------+-------------+
| 1987-05-11  | AAPL      | 77.0           | 0.015       |
| 1987-08-10  | AAPL      | 48.25          | 0.015       |
| 1987-11-17  | AAPL      | 35.0           | 0.02        |
| 1988-02-12  | AAPL      | 41.0           | 0.02        |
| 1988-05-16  | AAPL      | 41.25          | 0.02        |
| 1988-08-15  | AAPL      | 41.25          | 0.02        |
| 1988-11-21  | AAPL      | 36.63          | 0.025       |
| 1989-02-17  | AAPL      | 36.75          | 0.025       |
| 1989-05-22  | AAPL      | 46.0           | 0.025       |
| 1989-08-21  | AAPL      | 42.25          | 0.025       |
| 1989-11-17  | AAPL      | 44.75          | 0.0275      |
| 1990-02-16  | AAPL      | 33.75          | 0.0275      |
| 1990-05-21  | AAPL      | 39.5           | 0.0275      |
| 1990-08-20  | AAPL      | 36.75          | 0.0275      |
| 1990-11-16  | AAPL      | 35.13          | 0.03        |
| 1991-02-15  | AAPL      | 57.63          | 0.03        |
| 1991-05-20  | AAPL      | 44.25          | 0.03        |
| 1991-08-19  | AAPL      | 50.5           | 0.03        |
| 1991-11-18  | AAPL      | 52.13          | 0.03        |
| 1992-02-14  | AAPL      | 64.12          | 0.03        |
| 1992-06-01  | AAPL      | 57.5           | 0.03        |
| 1992-08-17  | AAPL      | 44.75          | 0.03        |
| 1992-11-30  | AAPL      | 57.5           | 0.03        |
| 1993-02-12  | AAPL      | 53.88          | 0.03        |
| 1993-05-28  | AAPL      | 56.63          | 0.03        |
| 1993-08-16  | AAPL      | 27.5           | 0.03        |
| 1993-11-19  | AAPL      | 33.0           | 0.03        |
| 1994-02-07  | AAPL      | 36.5           | 0.03        |
| 1994-05-27  | AAPL      | 29.94          | 0.03        |
| 1994-08-15  | AAPL      | 34.63          | 0.03        |
| 1994-11-18  | AAPL      | 40.0           | 0.03        |
| 1995-02-13  | AAPL      | 43.75          | 0.03        |
| 1995-05-26  | AAPL      | 42.69          | 0.03        |
| 1995-08-16  | AAPL      | 44.5           | 0.03        |
| 1995-11-21  | AAPL      | 38.63          | 0.03        |
+-------------+-----------+----------------+-------------+
```
> 년간 약 4번의 배당금(4개월마다, 분기별)을 지급함을 알수 있다.


* 애플, IBM, GE의 종가 조회
```
SELECT a.ymd, a.price_close, b.price_close, c.price_close
  FROM stocks a JOIN stocks b ON a.ymd = b.ymd
                JOIN stocks c ON a.ymd = c.ymd
 WHERE a.symbol = 'AAPL' AND b.symbol = 'IBM' and c.symbol = 'GE'
 ORDER BY a.ymd
;
```
```
+-------------+----------------+----------------+----------------+
|    a.ymd    | a.price_close  | b.price_close  | c.price_close  |
+-------------+----------------+----------------+----------------+
| 1984-09-07  | 26.5           | 121.62         | 55.88          |
| 1984-09-10  | 26.37          | 122.75         | 56.0           |
| 1984-09-11  | 26.87          | 122.25         | 57.0           |
| 1984-09-12  | 26.12          | 122.5          | 57.13          |
| 1984-09-13  | 27.5           | 126.12         | 58.88          |
...
| 2010-02-01  | 194.73         | 124.67         | 16.25          |
| 2010-02-02  | 195.86         | 125.53         | 16.85          |
| 2010-02-03  | 199.23         | 125.66         | 16.68          |
| 2010-02-04  | 192.05         | 123.0          | 16.04          |
| 2010-02-05  | 195.46         | 123.52         | 15.79          |
| 2010-02-08  | 194.12         | 121.88         | 15.6           |
+-------------+----------------+----------------+----------------+
```
> NOTE. 동등 조인의 결과임을 명심하자.


## 조인 최적화

* stocks과 dividends 테이블 건수 비교
```
select count(*) from stocks;
```
```
+----------+
|   _c0    |
+----------+
| 2075394  |
+----------+
```

```
select count(*) from dividends;
```
```
+--------+
|  _c0   |
+--------+
| 15208  |
+--------+
```

> stocks 테이블이 dividends 테이블 보다 크다.

> NOTE. Hive는 쿼리의 마지막 테이블이 가장 크다고 가정한다. 따라서 조인 쿼리를 구성할 때 가장 큰 테이블이 가장 마지막으로 오도록 해야한다.


* 튜닝전
```
select s.ymd, s.symbol, s.price_close, d.dividend
  FROM stocks s JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
 WHERE s.symbol = 'AAPL'
;
```

* 튜닝후
```
select s.ymd, s.symbol, s.price_close, d.dividend
  FROM dividends d JOIN stocks s 
    ON s.ymd = d.ymd AND s.symbol = d.symbol
 WHERE s.symbol = 'AAPL'
;
```

* 힌트 사용
```
select /*+ STREAMTABLE(s) */ s.ymd, s.symbol, s.price_close, d.dividend
  FROM stocks s JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
 WHERE s.symbol = 'AAPL'
;
```


## 왼쪽 외부 조인(LEFT OUTER JOIN)

* 애플의 종가와 배당금 조회
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM stocks s LEFT OUTER JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
 WHERE s.symbol = 'AAPL'
;   
```
```
...
| 1987-05-15  | AAPL      | 78.25          | NULL        |
| 1987-05-14  | AAPL      | 79.25          | NULL        |
| 1987-05-13  | AAPL      | 78.5           | NULL        |
| 1987-05-12  | AAPL      | 75.5           | NULL        |
| 1987-05-11  | AAPL      | 77.0           | 0.015       |
| 1987-05-08  | AAPL      | 79.0           | NULL        |
| 1987-05-07  | AAPL      | 80.25          | NULL        |
| 1987-05-06  | AAPL      | 80.0           | NULL        |
| 1987-05-05  | AAPL      | 80.25          | NULL        |
...
```
> NOTE. 배당금이 지금되지 않은 날은 null로 채워짐.


* 쿼리 속도 향상을 위해서 WHERE절에 dividends컬럼에 대한 조건을 추가하면?
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM stocks s LEFT OUTER JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
 WHERE s.symbol = 'AAPL'
   AND s.`exchange` = 'NASDAQ' AND d.`exchange` = 'NASDAQ'
;
```
```
+-------------+-----------+----------------+-------------+
|    s.ymd    | s.symbol  | s.price_close  | d.dividend  |
+-------------+-----------+----------------+-------------+
| 1995-11-21  | AAPL      | 38.63          | 0.03        |
| 1995-08-16  | AAPL      | 44.5           | 0.03        |
| 1995-05-26  | AAPL      | 42.69          | 0.03        |
| 1995-02-13  | AAPL      | 43.75          | 0.03        |
| 1994-11-18  | AAPL      | 40.0           | 0.03        |
| 1994-08-15  | AAPL      | 34.63          | 0.03        |
| 1994-05-27  | AAPL      | 29.94          | 0.03        |
| 1994-02-07  | AAPL      | 36.5           | 0.03        |
| 1993-11-19  | AAPL      | 33.0           | 0.03        |
| 1993-08-16  | AAPL      | 27.5           | 0.03        |
| 1993-05-28  | AAPL      | 56.63          | 0.03        |
| 1993-02-12  | AAPL      | 53.88          | 0.03        |
| 1992-11-30  | AAPL      | 57.5           | 0.03        |
| 1992-08-17  | AAPL      | 44.75          | 0.03        |
| 1992-06-01  | AAPL      | 57.5           | 0.03        |
| 1992-02-14  | AAPL      | 64.12          | 0.03        |
| 1991-11-18  | AAPL      | 52.13          | 0.03        |
| 1991-08-19  | AAPL      | 50.5           | 0.03        |
| 1991-05-20  | AAPL      | 44.25          | 0.03        |
| 1991-02-15  | AAPL      | 57.63          | 0.03        |
| 1990-11-16  | AAPL      | 35.13          | 0.03        |
| 1990-08-20  | AAPL      | 36.75          | 0.0275      |
| 1990-05-21  | AAPL      | 39.5           | 0.0275      |
| 1990-02-16  | AAPL      | 33.75          | 0.0275      |
| 1989-11-17  | AAPL      | 44.75          | 0.0275      |
| 1989-08-21  | AAPL      | 42.25          | 0.025       |
| 1989-05-22  | AAPL      | 46.0           | 0.025       |
| 1989-02-17  | AAPL      | 36.75          | 0.025       |
| 1988-11-21  | AAPL      | 36.63          | 0.025       |
| 1988-08-15  | AAPL      | 41.25          | 0.02        |
| 1988-05-16  | AAPL      | 41.25          | 0.02        |
| 1988-02-12  | AAPL      | 41.0           | 0.02        |
| 1987-11-17  | AAPL      | 35.0           | 0.02        |
| 1987-08-10  | AAPL      | 48.25          | 0.015       |
| 1987-05-11  | AAPL      | 77.0           | 0.015       |
+-------------+-----------+----------------+-------------+
```
> NOTE. 결과가 내부 조인 결과로 돌아왔다!

> NOTE. JOIN 이후에 WHERE절을 판단하기 때문에 이런 결과가 발생함.(JOIN 결과 대부분의 d.exchange값은 NULL임.)

* 해결법1: WHERE절에서 dividends 테이블 참조 제거
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM stocks s LEFT OUTER JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
 WHERE s.symbol = 'AAPL'
   AND s.`exchange` = 'NASDAQ' --AND d.`exchange` = 'NASDAQ'
;
```

* 해결법2?: WHERE절의 조건을 ON 절로 이동
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM stocks s LEFT OUTER JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
   AND s.symbol = 'AAPL'
   AND s.`exchange` = 'NASDAQ' AND d.`exchange` = 'NASDAQ'
;
```
```
...
| 2009-02-10  | AMEN      | 3.56           | NULL        |
| 2009-02-09  | AMEN      | 3.87           | NULL        |
| 2009-02-06  | AMEN      | 3.5            | NULL        |
...
```
> NOTE. 결과가 이상하게 나옴.

> NOTE. 외부 조인은 ON절에 있는 파티션 필터를 무시함.(내부 조인은 이러한 필터가 정상 작동함.)


* 해결법3: 중첩 SELECT 문 사용
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM
    (
      SELECT *
        FROM stocks
       WHERE symbol = 'AAPL' AND  `exchange` = 'NASDAQ'
    ) s
  LEFT OUTER JOIN
    (
      SELECT *
        FROM dividends
       WHERE symbol = 'AAPL' AND  `exchange` = 'NASDAQ'
    ) d
  ON s.ymd = d.ymd
;
```
> JOIN하기 전에 중첩 SELECT문에서 WHERE절의 파티션 필터가 적용됨.



## 오른쪽 외부 조인(RIGHT OUTER JOIN)
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM dividends d RIGHT OUTER JOIN stocks s
    ON d.ymd = s.ymd AND d.symbol = s.symbol
 WHERE s.symbol = 'AAPL'
;
```
```
...
| 1987-05-14  | AAPL      | 79.25          | NULL        |
| 1987-05-13  | AAPL      | 78.5           | NULL        |
| 1987-05-12  | AAPL      | 75.5           | NULL        |
| 1987-05-11  | AAPL      | 77.0           | 0.015       |
| 1987-05-08  | AAPL      | 79.0           | NULL        |
| 1987-05-07  | AAPL      | 80.25          | NULL        |
| 1987-05-06  | AAPL      | 80.0           | NULL        |
...
```


## 완전 외부 조인(FULL OUTER JOIN)
```
SELECT s.ymd, s.symbol, s.price_close, d.dividend
  FROM dividends d FULL OUTER JOIN stocks s
    ON d.ymd = s.ymd AND d.symbol = s.symbol
 WHERE s.symbol = 'AAPL'
;
```
> NOTE. 앞선 오른쪽 외부 조인과 결과가 동일하다. 이는 stock 레코드와 일치하지 않는 dividend 레코드가 존재하지 않기 때문이다.(즉, stock 테이블에는 존재하지 않는 종목에 대해서 배당금을 지급 내역은 존재하지 않는다.)


## 왼쪽 세미 조인(LEFT SEMI JOIN)

> NOTE. Hive는 오른쪽 세미 조인은 지원하지 않는다.

* IN 절은 Hive에서 허용하지 않음.
```
SELECT s.ymd, s.symbol, s.price_close
  FROM stocks s
 WHERE s.ymd, s.symbol
    IN (SELECT d.ymd, d.symbol FROM dividends d)
;
```

* 왼쪽 세미 조인
```
SELECT s.ymd, s.symbol, s.price_close
  FROM stocks s LEFT SEMI JOIN dividends d
    ON s.ymd = d.ymd AND s.symbol = d.symbol
;
```
```
...

| 2009-09-22  | IYM       | 57.32          |
| 2009-06-24  | IYM       | 42.37          |
| 2009-03-25  | IYM       | 36.37          |
| 2008-12-23  | IYM       | 35.19          |
| 2008-09-24  | IYM       | 63.71          |
| 2008-06-25  | IYM       | 85.93          |
| 2007-09-25  | IYM       | 72.95          |
| 2007-03-23  | IYM       | 64.82          |
...
```
> NOTE. 세미 조인시 SELECT와 WHERE절에서 오른쪽 테이블의 컬럼을 참조할 수 없다.


## 카타시안 프로젝트 조인(Cartesian Product JOIN)
```
select count(*)
  from stocks JOIN dividends
;
```
```
+--------------+
|     _c0      |
+--------------+
| 31562591952  |
+--------------+
1 row selected (674.256 seconds)
```
> NOTE. 상당히 오래걸림. 

> stocks: 2075394, dividends: 15208 => 2075394 * 15208 결과