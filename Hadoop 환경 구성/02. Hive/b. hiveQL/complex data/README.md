#

## nested 컬럼에 따른 구분자

* MAP of ARRAY     : 3 of 2 => 4level

* ARRAY of MAP     : 2 of 3 => 4level

* ARRAY of STRUCT  : 2 of 2 => 3level

* ARRAY of ARRAY   : 2 of 2 => 3level

> NOTE. X of Y 일때 구분자는 X+1의 level 구분자를 사용하는게 기본인데 Y 가 MAP인 경우는 Y+1의 level 구분자를 사용하게 됨.


## Collection Data Type(feat. ARRAY OF STRUCT)
* 테스트 데이터
```
Michael|Montreal,Toronto|Female:27,Male:30|DB:80|Product:DeveloperLead
Will|Montreal|Male:35|Perl:85|Product:Lead,Test:Lead
Shelley|New York|Male:22,Female:27|Python:80|Test:Lead,COE:Architect
Lucy|Vancouver|Female,57|Sales:89,HR:94|Sales:Lead
```
> NOTE> 세번째 컬럼의 구분자를 유심히 볼것!

* 테이블 생성
```
CREATE TABLE employee_array (
  name string,
  work_place ARRAY<string>,
  gender_age ARRAY<STRUCT<gender:string,age:int>>,
  skills_score MAP<string,int>,
  depart_title MAP<STRING,ARRAY<STRING>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'           -- 1레벨 구분자 : 기본값은 \u001
  COLLECTION ITEMS TERMINATED BY ',' -- 2레벨 구분자 : 기본값은 \u002
  MAP KEYS TERMINATED BY ':'         -- 3레벨 구분자 : 기본값은 \u003
  LINES TERMINATED BY '\n'           -- line(record) 구분자 : 기본값이 '\n'이면 생략 가능
STORED AS TEXTFILE
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/Apache-Hive-Essentials-Second-Edition/data/employee_array.txt' OVERWRITE INTO TABLE employee_array;
```

* 데이터 체크
```
select * from employee_array;

+----------------------+----------------------------+----------------------------------------------------+------------------------------+----------------------------------------+
| employee_array.name  | employee_array.work_place  |             employee_array.gender_age              | employee_array.skills_score  |      employee_array.depart_title       |
+----------------------+----------------------------+----------------------------------------------------+------------------------------+----------------------------------------+
| Michael              | ["Montreal","Toronto"]     | [{"gender":"Female","age":27},{"gender":"Male","age":30}] | {"DB":80}                    | {"Product":["Developer","Lead"]}       |
| Will                 | ["Montreal"]               | [{"gender":"Male","age":35}]                       | {"Perl":85}                  | {"Product":["Lead"],"Test":["Lead"]}   |
| Shelley              | ["New York"]               | [{"gender":"Male","age":22},{"gender":"Female","age":27}] | {"Python":80}                | {"Test":["Lead"],"COE":["Architect"]}  |
| Lucy                 | ["Vancouver"]              | [{"gender":"Female","age":null},{"gender":"57","age":null}] | {"Sales":89,"HR":94}         | {"Sales":["Lead"]}                     |
+----------------------+----------------------------+----------------------------------------------------+------------------------------+----------------------------------------+
4 rows selected (0.172 seconds)

select gender_age[0].gender, gender_age[0].age, gender_age[1].gender, gender_age[1].age from employee_array;

+---------+-------+---------+-------+
| gender  |  age  | gender  |  age  |
+---------+-------+---------+-------+
| Female  | 27    | Male    | 30    |
| Male    | 35    | NULL    | NULL  |
| Male    | 22    | Female  | 27    |
| Female  | NULL  | 57      | NULL  |
+---------+-------+---------+-------+
4 rows selected (0.193 seconds)
```