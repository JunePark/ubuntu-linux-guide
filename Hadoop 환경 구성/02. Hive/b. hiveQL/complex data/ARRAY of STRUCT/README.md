# ARRAY OF STRUCT


## 테스트 테이블 생성
```
create external table tb_test3
(
name string,
score_list array<struct<subject:string,score:int>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'              -- 1level 구분자 (컬럼 구분자)
  COLLECTION ITEMS TERMINATED BY ';'    -- 2level 구분자 (컬렉션 타입중 ARRAY, STRUCT 구분자)
  MAP KEYS TERMINATED BY ':'            -- 3level 구분자 (컬렉션 타입중 MAP 구분자)
  LINES TERMINATED BY '\n'
STORED AS TEXTFILE
location '/data/tb_test3'
;
```

### 테스트 CASE

* case1)
```
insert into tb_test3 select "A", array(named_struct("subject","math","score",100)) from (select 1) x;

$ hadoop fs -cat /data/tb_test3/000000_0 | od -c
0000000   A   |   m   a   t   h   :   1   0   0  \n
0000013
```

* case2)
```
insert into tb_test3 select "A", array(named_struct("subject","math","score",100), named_struct("subject","english","score",90)) from (select 1) x;

$ hadoop fs -cat /data/tb_test3/000000_0 | od -c
0000000   A   |   m   a   t   h   :   1   0   0   ;   e   n   g   l   i
0000020   s   h   :   9   0  \n
0000026
```

```
select * from tb_test3;
+----------------+----------------------------------------------------+
| tb_test3.name  |                tb_test3.score_list                 |
+----------------+----------------------------------------------------+
| A              | [{"subject":"math","score":100},{"subject":"english","score":90}] |
+----------------+----------------------------------------------------+
1 row selected (0.168 seconds)
```

```
select score_list.subject, score_list.score from tb_test3;
+---------------------+-----------+
|       subject       |   score   |
+---------------------+-----------+
| ["math","english"]  | [100,90]  |
+---------------------+-----------+
1 row selected (0.109 seconds)
0: jdbc:hive2://localhost:10000> 
```

```
select score_list.subject[0], score_list.score[0] from tb_test3;
+-------+------+
|  _c0  | _c1  |
+-------+------+
| math  | 100  |
+-------+------+
1 row selected (0.111 seconds)
```