# ARRAY OF MAP

결론: ARRAY OF MAP은 사용하지 말자!

적재 데이터 생성시 \u004와 같은 구분자를 사용해야 Hive에서 정상적으로 SELECT가 가능해짐.


## 테스트 테이블 생성
```
create external table tb_test2
(
name string,
score_list array<map<string,int>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'              -- 1level 구분자 (컬럼 구분자)
  COLLECTION ITEMS TERMINATED BY ';'    -- 2level 구분자 (컬렉션 타입중 ARRAY, STRUCT 구분자)
  MAP KEYS TERMINATED BY ':'            -- 3level 구분자 (컬렉션 타입중 MAP 구분자)
  LINES TERMINATED BY '\n'
STORED AS TEXTFILE
location '/data/tb_test2'
;
```

> NOTE. level 구분자는 1, 2, 3level 구분자만 사용자가 변경 가능.

> NOTE. level별 기본 구분자는 1level(\u001), 2level(\u002), 3level(\u003) 임.

> NOTE. MAP은 기본적으로 3level 구분자에 의해 구분이 되는데 nested가 됨으로 인해서 한단계가 낮아져 4level 구분자에 의해 구분이 됨.


## 테스트 CASE

* case1)
```
insert into tb_test2 select "A", array(map("math",100,"english",90,"history",85)) from (select 1) x;

$ hadoop fs -cat /data/tb_test2/000000_0 | od -c
0000000   A   |   m   a   t   h 004   1   0   0   :   e   n   g   l   i
0000020   s   h 004   9   0   :   h   i   s   t   o   r   y 004   8   5
0000040  \n
0000041
```
> NOTE. 0번째 ARRAY에 math, english, history 3개의 MAP 데이터를 입력
 
>       ARRAY<MAP>에서 MAP에 대한 key/value 구분자는 MAP이 nested 컬럼으로 사용되어서 4level의 구분자가 적용됨.(MAP은 3level인데 nested가 되어서 다음단계인 4level이 적용.) 4level은 3level 기본구분자 값에 +1을 한 \u004값이됨.

>       MAP의 구분자가 : 가 된것은 3level의 구분자가 적용된 것임.

* case2)
```
insert into tb_test2 select "A", array(map("math",100), map("english",90), map("history",85)) from (select 1) x;

$ hadoop fs -cat /data/tb_test2/000000_0 | od -c
0000000   A   |   m   a   t   h 004   1   0   0   ;   e   n   g   l   i
0000020   s   h 004   9   0   ;   h   i   s   t   o   r   y 004   8   5
0000040  \n
0000041
```

> NOTE. 0번째 ARRAY에 math, 1번째 ARRAY에 english, 2번째 ARRAY에 history MAP 데이터를 각각 입력

>       case1과 마찬가지로 nested MAP의 key/value구분자는 4level이 적용되어서 \u004값이 사용됨.

>       MAP의 구분자가 ; 가 적용됨.(의도한 COLLECTION ITEMS 의 구분자가 적용됨.)

```
select * from tb_test2;
+----------------+-----------------------------------------------+
| tb_test2.name  |              tb_test2.score_list              |
+----------------+-----------------------------------------------+
| A              | [{"math":100},{"english":90},{"history":85}]  |
+----------------+-----------------------------------------------+
1 row selected (0.137 seconds)
```

```
select score_list[0]['math'] from tb_test2;       -- 결과: 100
select score_list[1]['english'] from tb_test2;    -- 결과: 90
select score_list[2]['history'] from tb_test2;    -- 결과: 85
```

* case3)
```
insert into tb_test2 select "A", array(map("math",100,"english",90,"history",85), map("korean",70,"science",65)) from (select 1) x;

$ hadoop fs -cat /data/tb_test2/000000_0 | od -c
0000000   A   |   m   a   t   h 004   1   0   0   :   e   n   g   l   i
0000020   s   h 004   9   0   :   h   i   s   t   o   r   y 004   8   5
0000040   ;   k   o   r   e   a   n 004   7   0   :   s   c   i   e   n
0000060   c   e 004   6   5  \n
0000066
```
> NOTE. 0번째 ARRAY에 math, english, history 3개의 MAP 데이터를 입력하고 1번째 ARRAY에 korean, science 2개의 MAP 데이터를 입력

```
select * from tb_test2;
+----------------+----------------------------------------------------+
| tb_test2.name  |                tb_test2.score_list                 |
+----------------+----------------------------------------------------+
| A              | [{"math":100,"english":90,"history":85},{"korean":70,"science":65}] |
+----------------+----------------------------------------------------+
1 row selected (0.19 seconds)
```

```
select score_list[0]['math'] from tb_test2;       -- 결과: 100
select score_list[0]['english'] from tb_test2;    -- 결과: 90
select score_list[0]['history'] from tb_test2;    -- 결과: 85

select score_list[1]['korean'] from tb_test2;     -- 결과: 70
select score_list[1]['science'] from tb_test2;    -- 결과: 65
```
