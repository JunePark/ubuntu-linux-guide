# ARRAY OF STRUCT


## ARRAY WITHIN STRUCT

ref) https://bigdatansql.com/2021/05/20/array-within-struct/

* 테스트 데이터
```
101	David Foster	B.J. Cook:Amy Skylark,Rebecca Dyer:Sara|Erin|Jordan,Katharine McPhee:Boy
102	Billy Bob Thornton	Melissa Lee Gatlin:Amanda,Pietra Dawn Cherniak:Harry James|William,Connie Angland:Bella
103	Tony Curtis	Janet Leigh:Kelly|Jamie Lee,Christine Kaufmann:Alexandra|Allexgra,Lesille Allen:Nicholas Bernard|Benjamin Curtis
```
>> NOTE. | 구분자는 ^D 문자임(\u004).

* 테이블 생성
```
CREATE TABLE CelebMarriages
(
  id BIGINT,
  name STRING,
  marriages ARRAY < STRUCT <
                         spouse: STRING,
                         children: ARRAY <STRING>
                  > >
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
  COLLECTION ITEMS TERMINATED BY ','
  MAP KEYS TERMINATED BY ':'
  LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION '/data/celebmarriages'
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/celebmarriages.csv' INTO TABLE CelebMarriages;
```

* 데이터 체크
```
SELECT * FROM CelebMarriages;


SELECT name, marriages.spouse[0], marriages.children[0] FROM CelebMarriages;
SELECT name, marriages.spouse[1], marriages.children[1] FROM CelebMarriages;
SELECT name, marriages.spouse[2], marriages.children[2] FROM CelebMarriages;
```