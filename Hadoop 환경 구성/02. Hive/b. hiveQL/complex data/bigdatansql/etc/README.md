#

## named struct 로드
* 테스트 데이터
```
1|hello|apple:gala,weightkg:1
2|world|apple:bingo,weightkg:2
```

* 테이블 생성
```
CREATE EXTERNAL TABLE NAMED_STRUCT(
  col1 BIGINT,
  col2 STRING,
  col3 MAP<STRING,String>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'
  COLLECTION ITEMS TERMINATED BY ','
  MAP KEYS TERMINATED BY ':'
  LINES TERMINATED BY '\n'
STORED AS TEXTFILE
location '/data/named_struct'
;
```
> NOTE. MAP 타입으로 데이터 타입을 지정.

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/named_struct.csv' INTO TABLE NAMED_STRUCT;
```

* 데이터 확인
```
select * from NAMED_STRUCT;
```


## ARRAY OF STRUCT
* 테스트 데이터
```
java.nio.HeapByteBuffer[pos=0 lim=0 cap=0]|[bearerID -2147483648: bearerERABID 5;bearerID -2147483648: bearerERABID 6]|1
```
> NOTE. nested 된 MAP의 구분자는 \u0004임.

* 테이블 생성
```
CREATE EXTERNAL TABLE COLLECTION_MAP(
  featureGroupIndicatorBitmapTDD3 STRING,
  bearerRecords ARRAY<MAP<STRING,String>>,
  catM1BrCeLevel STRING
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'
  COLLECTION ITEMS TERMINATED BY ';'
  MAP KEYS TERMINATED BY ':'
  LINES TERMINATED BY '\n'
STORED AS TEXTFILE
location '/data/collection_map'
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/collection_map.csv' INTO TABLE COLLECTION_MAP;
```

* 데이터 체크
```
select * from COLLECTION_MAP;
```