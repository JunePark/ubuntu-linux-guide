# ARRAY OF STRUCT


## STRUCT WITHIN ARRAY - 1

ref) https://bigdatansql.com/2021/05/17/struct-within-array-1/

* 테스트 데이터
```
1001112|Steve|180-A+D Zinc Oxide Cream-2-20,188-Aveeno Baby Daily Lotion-1-21,189-Aveeno Baby Wash & Shampoo-2-23.5,199-Avon MUSK FOR BOYS-1-18.5
1001113|George|285-Gillette Satin Care-1-10.5,288-Skin Therapy Shave Gel-1-11.5
1001114|Ivan|291-Suede After Shave-2-22.5
```

* 테이블 생성
```
CREATE external TABLE CustOrders(
  CustID BIGINT,
  CustName STRING,
  Orders Array<Struct<OrderID: Int,OrderItem: string,Qty: Int,PricePerProduct: Decimal(9,2)>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'
  COLLECTION ITEMS TERMINATED BY ','
  MAP KEYS TERMINATED BY '-'
  LINES TERMINATED BY '\n'
STORED AS TEXTFILE
location '/data/custorders'
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/StructInArrayCustOrders.csv' INTO TABLE CustOrders;
```

* 데이터 체크
```
SELECT orders[0] FROM CustOrders;

SELECT orders[0].orderid FROM CustOrders;

SELECT custid,orders[0].orderid,
       SUM(orders[0].qty*orders[0].PricePerProduct)
  FROM CustOrders
 WHERE orders[0].orderid=180
 GROUP BY custid, orders[0].orderid
;

SELECT custid,custname,
       orders[0].orderid,
       orders[0].qty,
       orders[0].PricePerProduct
  FROM CustOrders
 WHERE custid=1001112
;
```


## STRUCT WITHIN ARRAY - 2

ref) https://bigdatansql.com/2021/05/17/struct-within-array-2/

* 테스트 데이터
```
1345653	Zafa Iqbal	110909316904:1541394100:Completed,221065796761:1541854500:On Hold
1345654	Tahir Farooq	110909316918:1541594241:Completed,221065796762:1541876502:Cancelled
1345655	Danial Hussain	110909316928:1541894546:Completed,221065796763:1541897534:Completed
```

* 테이블 생성
```
CREATE EXTERNAL TABLE CustOrders2(
  CustID BIGINT,
  CustName STRING,
  SalesInfo Array<Struct<ProductID: BigInt,StatusModifiedDate:BigInt,Status:String>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
  COLLECTION ITEMS TERMINATED BY ','
  MAP KEYS TERMINATED BY ':'
  LINES TERMINATED BY '\n'
STORED AS TEXTFILE
location '/data/custorders2'
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/StructWithinArrayPart-2.csv' INTO TABLE CustOrders2;
```

* 데이터 체크
```
SELECT Custid
      ,SalesInfo[0].ProductID
      ,FROM_UNIXTIME(SalesInfo[0].StatusModifiedDate)
  FROM CustOrders2
;

SELECT Custid
      ,SalesInfo[0].ProductID
      ,FROM_UNIXTIME(SalesInfo[0].StatusModifiedDate)
  FROM CustOrders2
 WHERE SalesInfo[0].ProductID=110909316918
;

SELECT Custid
      ,SalesInfo[1].ProductID
      ,FROM_UNIXTIME(SalesInfo[1].StatusModifiedDate)
  FROM CustOrders2
 WHERE SalesInfo[1].Status='Cancelled'
;
```