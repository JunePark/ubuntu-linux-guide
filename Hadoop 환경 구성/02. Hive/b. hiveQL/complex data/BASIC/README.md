#

## Collection Data Type
* 테이블 생성
```
CREATE TABLE employee (
  name string,
  work_place ARRAY<string>,
  gender_age STRUCT<gender:string,age:int>,
  skills_score MAP<string,int>,
  depart_title MAP<STRING,ARRAY<STRING>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'           -- 1레벨 구분자 : 기본값은 \u001
  COLLECTION ITEMS TERMINATED BY ',' -- 2레벨 구분자 : 기본값은 \u002
  MAP KEYS TERMINATED BY ':'         -- 3레벨 구분자 : 기본값은 \u003
  LINES TERMINATED BY '\n'           -- line(record) 구분자 : 기본값이 '\n'이면 생략 가능
STORED AS TEXTFILE
;
```
> NOTE. STRING, ARRAY, STRUCT, MAP, UNION(여기서는 MAP of ARRAY) 타입의 컬럼으로 구성

> NOTE. hive는 nested된 컬럼까지 감안하여 8레벨까지 가능. 하지만 4~8레벨의 구분자는 \u004 ~ \u008로 고정되어 있어서 변경 불가능.

> NOTE. 여기에서 MAP<STRING,<ARRAY<STRING>> 으로 nested된 ARRAY는 4level이 됨.

* load data
```
LOAD DATA LOCAL INPATH '/path/to/employee.txt' OVERWRITE INTO TABLE employee;
```

* 데이터 검색
```
select * from employee2;

+----------------+-------------------------+-------------------------------+------------------------+----------------------------------------+
| employee.name  |   employee.work_place   |      employee.gender_age      | employee.skills_score  |         employee.depart_title          |
+----------------+-------------------------+-------------------------------+------------------------+----------------------------------------+
| Michael        | ["Montreal","Toronto"]  | {"gender":"Male","age":30}    | {"DB":80}              | {"Product":["Developer","Lead"]}       |
| Will           | ["Montreal"]            | {"gender":"Male","age":35}    | {"Perl":85}            | {"Product":["Lead"],"Test":["Lead"]}   |
| Shelley        | ["New York"]            | {"gender":"Female","age":27}  | {"Python":80}          | {"Test":["Lead"],"COE":["Architect"]}  |
| Lucy           | ["Vancouver"]           | {"gender":"Female","age":57}  | {"Sales":89,"HR":94}   | {"Sales":["Lead"]}                     |
+----------------+-------------------------+-------------------------------+------------------------+----------------------------------------+
4 rows selected (0.164 seconds)
```
> NOTE. hive 버전에 따라 STRUCT 타입의 값이 MAP 데이터 값처럼 출력이 될수 있음.

* ARRAY 컬럼 검색
```
SELECT work_place FROM employee;

+-------------------------+
|       work_place        |
+-------------------------+
| ["Montreal","Toronto"]  |
| ["Montreal"]            |
| ["New York"]            |
| ["Vancouver"]           |
+-------------------------+
4 rows selected (0.15 seconds)

SELECT work_place[0] AS col_1, work_place[1] AS col_2, work_place[2] AS col_3 FROM employee;

+------------+----------+--------+
|   col_1    |  col_2   | col_3  |
+------------+----------+--------+
| Montreal   | Toronto  | NULL   |
| Montreal   | NULL     | NULL   |
| New York   | NULL     | NULL   |
| Vancouver  | NULL     | NULL   |
+------------+----------+--------+
4 rows selected (0.197 seconds)
```
> NOTE. ARRAY는 []에 index로 검색.

* STRUCT 컬럼 검색
```
SELECT gender_age FROM employee;

+-------------------------------+
|          gender_age           |
+-------------------------------+
| {"gender":"Male","age":30}    |
| {"gender":"Male","age":35}    |
| {"gender":"Female","age":27}  |
| {"gender":"Female","age":57}  |
+-------------------------------+
4 rows selected (0.134 seconds)

SELECT gender_age.gender, gender_age.age FROM employee;

+---------+------+
| gender  | age  |
+---------+------+
| Male    | 30   |
| Male    | 35   |
| Female  | 27   |
| Female  | 57   |
+---------+------+
4 rows selected (0.194 seconds)
```
> NOTE. 구조체처럼 . 을 이용해서 검색

* MAP 컬럼 검색
```
SELECT skills_score FROM employee;

+-----------------------+
|     skills_score      |
+-----------------------+
| {"DB":80}             |
| {"Perl":85}           |
| {"Python":80}         |
| {"Sales":89,"HR":94}  |
+-----------------------+
4 rows selected (0.13 seconds)

SELECT name, skills_score['DB'] AS DB,
       skills_score['Perl'] AS Perl, skills_score['Python'] AS Python,
       skills_score['Sales'] as Sales, skills_score['HR'] as HR
  FROM employee
;

+----------+-------+-------+---------+--------+-------+
|   name   |  db   | perl  | python  | sales  |  hr   |
+----------+-------+-------+---------+--------+-------+
| Michael  | 80    | NULL  | NULL    | NULL   | NULL  |
| Will     | NULL  | 85    | NULL    | NULL   | NULL  |
| Shelley  | NULL  | NULL  | 80      | NULL   | NULL  |
| Lucy     | NULL  | NULL  | NULL    | 89     | 94    |
+----------+-------+-------+---------+--------+-------+
4 rows selected (0.131 seconds)
```
> NOTE. MAP은 []에 key값을 넣어서 검색. 컬럼명은 mysql과 같이 기본적으로 소문자로 출력됨.

* UNION(여기서는 MAP of ARRAY) 컬럼 검색
```
SELECT depart_title FROM employee;

+----------------------------------------+
|              depart_title              |
+----------------------------------------+
| {"Product":["Developer","Lead"]}       |
| {"Product":["Lead"],"Test":["Lead"]}   |
| {"Test":["Lead"],"COE":["Architect"]}  |
| {"Sales":["Lead"]}                     |
+----------------------------------------+
4 rows selected (0.122 seconds)

SELECT name, depart_title['Product'] AS Product, depart_title['Test'] AS Test,
       depart_title['COE'] AS COE, depart_title['Sales'] AS Sales
  FROM employee
;

+----------+-----------------------+-----------+----------------+-----------+
|   name   |        product        |   test    |      coe       |   sales   |
+----------+-----------------------+-----------+----------------+-----------+
| Michael  | ["Developer","Lead"]  | NULL      | NULL           | NULL      |
| Will     | ["Lead"]              | ["Lead"]  | NULL           | NULL      |
| Shelley  | NULL                  | ["Lead"]  | ["Architect"]  | NULL      |
| Lucy     | NULL                  | NULL      | NULL           | ["Lead"]  |
+----------+-----------------------+-----------+----------------+-----------+
4 rows selected (0.17 seconds)

SELECT name,
       depart_title['Product'][0] AS product_col0,
       depart_title['Test'][0] AS test_col0 
  FROM employee
;

+----------+---------------+------------+
|   name   | product_col0  | test_col0  |
+----------+---------------+------------+
| Michael  | Developer     | NULL       |
| Will     | Lead          | Lead       |
| Shelley  | NULL          | Lead       |
| Lucy     | NULL          | NULL       |
+----------+---------------+------------+
4 rows selected (0.154 seconds)
```
> NOTE. MAP OF ARRAY 타입에 대한 검색은 ['key명'][index] 로 검색.

