#

## Collection Data Type
* 테이블 생성
```
CREATE TABLE employee (
  name string,
  work_place ARRAY<string>,
  gender_age STRUCT<gender:string,age:int>,
  skills_score MAP<string,int>,
  depart_title MAP<STRING,ARRAY<STRING>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'           -- 1레벨 구분자 : 기본값은 \u001
  COLLECTION ITEMS TERMINATED BY ',' -- 2레벨 구분자 : 기본값은 \u002
  MAP KEYS TERMINATED BY ':'         -- 3레벨 구분자 : 기본값은 \u003
  LINES TERMINATED BY '\n'           -- line(record) 구분자 : 기본값이 '\n'이면 생략 가능
STORED AS TEXTFILE
;
```
> NOTE. STRING, ARRAY, STRUCT, MAP, UNION(여기서는 MAP of ARRAY) 타입의 컬럼으로 구성

> NOTE. hive는 nested된 컬럼까지 감안하여 8레벨까지 가능. 하지만 4~8레벨의 구분자는 \u004 ~ \u008로 고정되어 있어서 변경 불가능.

> NOTE. 여기에서 MAP<STRING,<ARRAY<STRING>> 으로 nested된 ARRAY는 4level이 됨.

* load data
```
LOAD DATA LOCAL INPATH '/path/to/employee.txt' OVERWRITE INTO TABLE employee;
```

* 데이터 검색
```
select * from employee;

+----------------+-------------------------+-------------------------------+------------------------+----------------------------------------+
| employee.name  |   employee.work_place   |      employee.gender_age      | employee.skills_score  |         employee.depart_title          |
+----------------+-------------------------+-------------------------------+------------------------+----------------------------------------+
| Michael        | ["Montreal","Toronto"]  | {"gender":"Male","age":30}    | {"DB":80}              | {"Product":["Developer","Lead"]}       |
| Will           | ["Montreal"]            | {"gender":"Male","age":35}    | {"Perl":85}            | {"Product":["Lead"],"Test":["Lead"]}   |
| Shelley        | ["New York"]            | {"gender":"Female","age":27}  | {"Python":80}          | {"Test":["Lead"],"COE":["Architect"]}  |
| Lucy           | ["Vancouver"]           | {"gender":"Female","age":57}  | {"Sales":89,"HR":94}   | {"Sales":["Lead"]}                     |
+----------------+-------------------------+-------------------------------+------------------------+----------------------------------------+
4 rows selected (0.164 seconds)
```
> NOTE. hive 버전에 따라 STRUCT 타입의 값이 MAP 데이터 값처럼 출력이 될수 있음.

* ARRAY 컬럼 검색
```
SELECT work_place FROM employee;

+-------------------------+
|       work_place        |
+-------------------------+
| ["Montreal","Toronto"]  |
| ["Montreal"]            |
| ["New York"]            |
| ["Vancouver"]           |
+-------------------------+
4 rows selected (0.15 seconds)

SELECT work_place[0] AS col_1, work_place[1] AS col_2, work_place[2] AS col_3 FROM employee;

+------------+----------+--------+
|   col_1    |  col_2   | col_3  |
+------------+----------+--------+
| Montreal   | Toronto  | NULL   |
| Montreal   | NULL     | NULL   |
| New York   | NULL     | NULL   |
| Vancouver  | NULL     | NULL   |
+------------+----------+--------+
4 rows selected (0.197 seconds)
```
> NOTE. ARRAY는 []에 index로 검색.

* STRUCT 컬럼 검색
```
SELECT gender_age FROM employee;

+-------------------------------+
|          gender_age           |
+-------------------------------+
| {"gender":"Male","age":30}    |
| {"gender":"Male","age":35}    |
| {"gender":"Female","age":27}  |
| {"gender":"Female","age":57}  |
+-------------------------------+
4 rows selected (0.134 seconds)

SELECT gender_age.gender, gender_age.age FROM employee;

+---------+------+
| gender  | age  |
+---------+------+
| Male    | 30   |
| Male    | 35   |
| Female  | 27   |
| Female  | 57   |
+---------+------+
4 rows selected (0.194 seconds)
```
> NOTE. 구조체처럼 . 을 이용해서 검색

* MAP 컬럼 검색
```
SELECT skills_score FROM employee;

+-----------------------+
|     skills_score      |
+-----------------------+
| {"DB":80}             |
| {"Perl":85}           |
| {"Python":80}         |
| {"Sales":89,"HR":94}  |
+-----------------------+
4 rows selected (0.13 seconds)

SELECT name, skills_score['DB'] AS DB,
       skills_score['Perl'] AS Perl, skills_score['Python'] AS Python,
       skills_score['Sales'] as Sales, skills_score['HR'] as HR
  FROM employee
;

+----------+-------+-------+---------+--------+-------+
|   name   |  db   | perl  | python  | sales  |  hr   |
+----------+-------+-------+---------+--------+-------+
| Michael  | 80    | NULL  | NULL    | NULL   | NULL  |
| Will     | NULL  | 85    | NULL    | NULL   | NULL  |
| Shelley  | NULL  | NULL  | 80      | NULL   | NULL  |
| Lucy     | NULL  | NULL  | NULL    | 89     | 94    |
+----------+-------+-------+---------+--------+-------+
4 rows selected (0.131 seconds)
```
> NOTE. MAP은 []에 key값을 넣어서 검색. 컬럼명은 mysql과 같이 기본적으로 소문자로 출력됨.

* UNION(여기서는 MAP of ARRAY) 컬럼 검색
```
SELECT depart_title FROM employee;

+----------------------------------------+
|              depart_title              |
+----------------------------------------+
| {"Product":["Developer","Lead"]}       |
| {"Product":["Lead"],"Test":["Lead"]}   |
| {"Test":["Lead"],"COE":["Architect"]}  |
| {"Sales":["Lead"]}                     |
+----------------------------------------+
4 rows selected (0.122 seconds)

SELECT name, depart_title['Product'] AS Product, depart_title['Test'] AS Test,
       depart_title['COE'] AS COE, depart_title['Sales'] AS Sales
  FROM employee
;

+----------+-----------------------+-----------+----------------+-----------+
|   name   |        product        |   test    |      coe       |   sales   |
+----------+-----------------------+-----------+----------------+-----------+
| Michael  | ["Developer","Lead"]  | NULL      | NULL           | NULL      |
| Will     | ["Lead"]              | ["Lead"]  | NULL           | NULL      |
| Shelley  | NULL                  | ["Lead"]  | ["Architect"]  | NULL      |
| Lucy     | NULL                  | NULL      | NULL           | ["Lead"]  |
+----------+-----------------------+-----------+----------------+-----------+
4 rows selected (0.17 seconds)

SELECT name,
       depart_title['Product'][0] AS product_col0,
       depart_title['Test'][0] AS test_col0 
  FROM employee
;

+----------+---------------+------------+
|   name   | product_col0  | test_col0  |
+----------+---------------+------------+
| Michael  | Developer     | NULL       |
| Will     | Lead          | Lead       |
| Shelley  | NULL          | Lead       |
| Lucy     | NULL          | NULL       |
+----------+---------------+------------+
4 rows selected (0.154 seconds)
```
> NOTE. MAP OF ARRAY 타입에 대한 검색은 ['key명'][index] 로 검색.


## Collection Data Type(ARRAY OF STRUCT)
* 테스트 데이터
```
Michael|Montreal,Toronto|Female:27,Male:30|DB:80|Product:DeveloperLead
Will|Montreal|Male:35|Perl:85|Product:Lead,Test:Lead
Shelley|New York|Male:22,Female:27|Python:80|Test:Lead,COE:Architect
Lucy|Vancouver|Female,57|Sales:89,HR:94|Sales:Lead
```
> NOTE> 세번째 컬럼의 구분자를 유심히 볼것!

* 테이블 생성
```
CREATE TABLE employee_array (
  name string,
  work_place ARRAY<string>,
  gender_age ARRAY<STRUCT<gender:string,age:int>>,
  skills_score MAP<string,int>,
  depart_title MAP<STRING,ARRAY<STRING>>
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'           -- 1레벨 구분자 : 기본값은 \u001
  COLLECTION ITEMS TERMINATED BY ',' -- 2레벨 구분자 : 기본값은 \u002
  MAP KEYS TERMINATED BY ':'         -- 3레벨 구분자 : 기본값은 \u003
  LINES TERMINATED BY '\n'           -- line(record) 구분자 : 기본값이 '\n'이면 생략 가능
STORED AS TEXTFILE
;
```

* 데이터 로드
```
LOAD DATA LOCAL INPATH '/home/hadoop/work/Apache-Hive-Essentials-Second-Edition/data/employee_array.txt' OVERWRITE INTO TABLE employee_array;
```

* 데이터 체크
```
select * from employee_array;

+----------------------+----------------------------+----------------------------------------------------+------------------------------+----------------------------------------+
| employee_array.name  | employee_array.work_place  |             employee_array.gender_age              | employee_array.skills_score  |      employee_array.depart_title       |
+----------------------+----------------------------+----------------------------------------------------+------------------------------+----------------------------------------+
| Michael              | ["Montreal","Toronto"]     | [{"gender":"Female","age":27},{"gender":"Male","age":30}] | {"DB":80}                    | {"Product":["Developer","Lead"]}       |
| Will                 | ["Montreal"]               | [{"gender":"Male","age":35}]                       | {"Perl":85}                  | {"Product":["Lead"],"Test":["Lead"]}   |
| Shelley              | ["New York"]               | [{"gender":"Male","age":22},{"gender":"Female","age":27}] | {"Python":80}                | {"Test":["Lead"],"COE":["Architect"]}  |
| Lucy                 | ["Vancouver"]              | [{"gender":"Female","age":null},{"gender":"57","age":null}] | {"Sales":89,"HR":94}         | {"Sales":["Lead"]}                     |
+----------------------+----------------------------+----------------------------------------------------+------------------------------+----------------------------------------+
4 rows selected (0.172 seconds)

select gender_age[0].gender, gender_age[0].age, gender_age[1].gender, gender_age[1].age from employee_array;

+---------+-------+---------+-------+
| gender  |  age  | gender  |  age  |
+---------+-------+---------+-------+
| Female  | 27    | Male    | 30    |
| Male    | 35    | NULL    | NULL  |
| Male    | 22    | Female  | 27    |
| Female  | NULL  | 57      | NULL  |
+---------+-------+---------+-------+
4 rows selected (0.193 seconds)
```

## LOAD

* from local
```
LOAD DATA LOCAL INPATH '/path/to/employee_hr.txt'
OVERWRITE INTO TABLE employee_hr
;
```
```
$ ls -al /path/to/
```
> NOTE. OS 파일시스템에 있는 원본파일은 그대로 있음.


* from hdfs
```
$ hadoop fs -put *.txt hivedemo/.
```
```
LOAD DATA INPATH '/user/hadoop/hivedemo/employee_hr.txt'
OVERWRITE INTO TABLE employee_hr
;

dfs -ls /user/hadoop/hivedemo/;
```
> NOTE. 원본 데이터가 삭제됨.


## INSERT

* dynamic partition
```
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nostrict;

INSERT INTO TABLE employee_partitioned PARTITION(year, month)
SELECT name, array('Toronto') as work_place, 
       named_struct("gender","Male","age",30) as gender_age,
       map("Python",90) as skills_score,
       map("R&D",array('Developer')) as depart_title, 
       cast(year(start_date) as int) as year,
       cast(month(start_date) as int) as month
  FROM employee_hr eh
 WHERE eh.employee_id = 102
;
```

* to files
```
INSERT OVERWRITE LOCAL DIRECTORY '/tmp/output1'
SELECT * FROM employee
;
```
```
$ ls -al /tmp/output1/
```
> NOTE. LOCAL을 빼면 HDFS에 결과가 만들어짐.

> NOTE. HDFS에는 디렉토리만 만들어짐.


* to files(feat. ROW FORMAT)
```
INSERT OVERWRITE LOCAL DIRECTORY '/tmp/output2'
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
SELECT * FROM employee
;
```
```
cat /tmp/output2/000000_0
```
```
Michael,MontrealToronto,Male30,DB80,ProductDeveloperLead
Will,Montreal,Male35,Perl85,ProductLeadTestLead
Shelley,New York,Female27,Python80,TestLeadCOEArchitect
Lucy,Vancouver,Female57,Sales89HR94,SalesLead
```
> NOTE. 필드 구분자가 , 로 되어있음.


* multi-insert
```
FROM employee
INSERT OVERWRITE DIRECTORY '/tmp/output3'
SELECT *
INSERT OVERWRITE DIRECTORY '/tmp/output4'
SELECT *
;
```

## sh을 이용

* append to local file
```
$ hive -e 'select * from employee' >> test1
```

* overwrite to local file
```
$ hive -e 'select * from employee' > test2
```

* append to HDFS file
```
$ hive -e 'select * from employee' | hadoop fs -appendToFile - /tmp/test1
```

* overwrite to HDFS file
```
$ hive -e 'select * from employee' | hadoop fs -put - /tmp/test2
```


## IMPORT, EXPORT

다른 시스템으로 데이터를 주거나 받을때 사용.

* export
```
EXPORT TABLE employee TO '/tmp/output5';

dfs -ls -R /tmp/output5/;
```
```
+----------------------------------------------------+
|                     DFS Output                     |
+----------------------------------------------------+
| -rw-r--r--   1 hadoop supergroup       1611 2022-01-01 08:49 /tmp/output5/_metadata |
| drwxr-xr-x   - hadoop supergroup          0 2022-01-01 08:49 /tmp/output5/data |
| -rw-r--r--   1 hadoop supergroup        227 2022-01-01 08:49 /tmp/output5/data/employee.txt |
+----------------------------------------------------+
```

* import
```
IMPORT FROM '/tmp/output5/';
```
```
Error: Error while compiling statement: FAILED: SemanticException [Error 10119]: Table exists and contains data files (state=42000,code=10119)
```
> NOTE. 만약 IMPORT할때의 테이블명과 동일한 테이블명이 존재한다면 import 실패

```
IMPORT TABLE employee_imported FROM '/tmp/output5/';
```
> NOTE. import시 테이블명을 명시하여 테이블을 생성.

* import to external table
```
IMPORT EXTERNAL TABLE employee_imported_external
FROM '/tmp/output5/'
LOCATION '/data/output6'
;
```
> NOTE. 에러 발생.

* import & export partition
```
EXPORT TABLE employee_partitioned partition 
(year=2018, month=12) TO '/tmp/output7';

IMPORT TABLE employee_partitioned_imported 
FROM '/tmp/output7';
```