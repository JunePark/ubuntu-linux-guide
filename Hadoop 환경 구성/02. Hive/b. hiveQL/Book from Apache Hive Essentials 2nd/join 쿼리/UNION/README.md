#

## SET OPERATION

    - INTERSECT

    - MINUS

    - UNION / UNION ALL

> NOTE. Hive 는 UNION, UNION ALL만 가능


## UNION , UNION ALL 차이점

UNION 은 중복된 row를 제거해 주지만 UNION ALL은 하지 않음.


## 예제

* UNION ALL
```
SELECT a.name as nm
  FROM employee a
UNION ALL
SELECT b.name as nm
  FROM employee_hr b
;
```
```
+----------+
|  _u1.nm  |
+----------+
| Michael  |
| Will     |
| Shelley  |
| Lucy     |
| Michael  |
| Will     |
| Steven   |
| Lucy     |
+----------+
8 rows selected (21.926 seconds)
```

* UNION
```
SELECT a.name as nm
  FROM employee a
UNION
SELECT b.name as nm
  FROM employee_hr b
;
```
```
+----------+
|  _u1.nm  |
+----------+
| Lucy     |
| Michael  |
| Shelley  |
| Steven   |
| Will     |
+----------+
5 rows selected (30.095 seconds)
```
> NOTE. 중복 제거를 해야함으로 UNION ALL에 비해서는 느림.

* Order with UNION
```
SELECT a.name as nm
  FROM employee a
UNION ALL
SELECT b.name as nm
  FROM employee_hr b
ORDER BY nm
;
```
```
+----------+
|  _u2.nm  |
+----------+
| Lucy     |
| Lucy     |
| Michael  |
| Michael  |
| Shelley  |
| Steven   |
| Will     |
| Will     |
+----------+
8 rows selected (29.208 seconds)
```
> NOTE> ORDER BY는 UNION ALL을 수행한 이후에 실행됨.


* INTERSECT
```
--Table employee implements INTERSECT employee_hr
SELECT a.name 
  FROM employee a
  JOIN employee_hr b
   ON a.name = b.name
;
```
```
+----------+
|  a.name  |
+----------+
| Michael  |
| Will     |
| Lucy     |
+----------+
3 rows selected (25.681 seconds)
```
> NOTE. INTERSECT는 INNER JOIN문으로 대체 가능.


* MINUS
```
SELECT a.name 
  FROM employee a
  LEFT JOIN employee_hr b ON a.name = b.name
WHERE b.name IS NULL
;
```
```
+----------+
|  a.name  |
+----------+
| Shelley  |
+----------+
1 row selected (24.803 seconds)
```
