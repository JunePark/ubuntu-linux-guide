# INNER JOIN

* INNER JOIN with EQUI JOIN CONDITION
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON emp.name = emph.name
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 547-968-091      |
| Will      | 527-948-090      |
| Lucy      | 577-928-094      |
+-----------+------------------+
3 rows selected (28.339 seconds)
```


* INNER JOIN with NON-EQUI JOIN CONDITION
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON emp.name != emph.name
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 527-948-090      |
| Michael   | 647-968-598      |
| Michael   | 577-928-094      |
| Will      | 547-968-091      |
| Will      | 647-968-598      |
| Will      | 577-928-094      |
| Shelley   | 547-968-091      |
| Shelley   | 527-948-090      |
| Shelley   | 647-968-598      |
| Shelley   | 577-928-094      |
| Lucy      | 547-968-091      |
| Lucy      | 527-948-090      |
| Lucy      | 647-968-598      |
+-----------+------------------+
13 rows selected (27.606 seconds)
```
> NOTE. employee 에는 [Michael,Will,Shelley,Lucy] employee_hr에는 [Michael,Will,Steven,Lucy] 가 존재하는데 employee를 기준으로 건수를 살펴보면 각각 3,3,4,3건이 나오고 합하면 13건


* ON절에 복잡한 평가식
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON
       IF(emp.name = 'Will', '1', emp.name) = CASE WHEN emph.name = 'Will' THEN '0' ELSE emph.name END
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 547-968-091      |
| Lucy      | 577-928-094      |
+-----------+------------------+
2 rows selected (26.829 seconds)
```
> NOTE. 결국 Will을 제외한 동등조인(EQUI JOIN)


* WHERE절에 필터 조건
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON emp.name = emph.name
 WHERE emp.name = 'Will'
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Will      | 527-948-090      |
+-----------+------------------+
1 row selected (26.47 seconds)
```
> NOTE. JOIN절을 먼저 수행한 다음 WHERE절을 수행


* 3개 이상 테이블 JOIN
```
SELECT emp.name, empi.employee_id, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON emp.name = emph.name
  JOIN employee_id empi ON emp.name = empi.name
;
```
```
+-----------+-------------------+------------------+
| emp.name  | empi.employee_id  | emph.sin_number  |
+-----------+-------------------+------------------+
| Michael   | 100               | 547-968-091      |
| Will      | 101               | 527-948-090      |
| Lucy      | 103               | 577-928-094      |
+-----------+-------------------+------------------+
3 rows selected (28.534 seconds)
```


* SELF JOIN
```
SELECT emp.name
  FROM employee emp
  JOIN employee emp_b ON emp.name = emp_b.name
;
```
```
+-----------+
| emp.name  |
+-----------+
| Michael   |
| Will      |
| Shelley   |
| Lucy      |
+-----------+
4 rows selected (27.148 seconds)
```
> NOTE. SELF JOIN 이라고 하는 조인 타입이 존재하는 것이 아니라 같은 테이블을 조인하는 방식을 일컫는 말이다.


* Implicit INNER JOIN (support since Hive 0.13.0)
```
SELECT emp.name, emph.sin_number
  FROM employee emp, employee_hr emph
 WHERE emp.name = emph.name
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 547-968-091      |
| Will      | 527-948-090      |
| Lucy      | 577-928-094      |
+-----------+------------------+
3 rows selected (26.386 seconds)
```
> NOTE. SQL-92 방식의 문법도 지원(INNER JOIN만 해당)


* 3개 이상 테이블 JOIN, JOIN별로 다른 컬럼 사용
```
SELECT emp.name, empi.employee_id, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON emp.name = emph.name
  JOIN employee_id empi ON emph.employee_id = empi.employee_id
;
```
```
+-----------+-------------------+------------------+
| emp.name  | empi.employee_id  | emph.sin_number  |
+-----------+-------------------+------------------+
| Michael   | 100               | 547-968-091      |
| Will      | 101               | 527-948-090      |
| Lucy      | 103               | 577-928-094      |
+-----------+-------------------+------------------+
3 rows selected (27.432 seconds)
```
> NOTE. JOIN별로 다른 컬럼을 사용하게 되면 별도의 job에서 수행하게 된다. 이것은 다른말로 하자면, JOIN별로 동일한 컬럼을 사용하게 되면 하나의 job에서 수행한다.


* 튜닝 팁 (Streaming tables)
```
SELECT /*+ STREAMTABLE(employee_hr) */
       emp.name, empi.employee_id, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON emp.name = emph.name
  JOIN employee_id empi ON emph.employee_id = empi.employee_id;
```

> NOTE. alias를 이용하여 STREAMTABLE(emph) 라고 적어도 된다.

> Usually, it is suggested to put the big table right at the end of the JOIN statement for better performance and to avoid Out Of Memory(OOM) exceptions. This is because the last table in the JOIN sequence is usaully streamed through reducers where as the others are buffered in the reducer by default.