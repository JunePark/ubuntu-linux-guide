# JOIN문


## JOIN문 종류

table_m(m개 row), table_n(n개 row) 에 대해서 조인시

    - INNER JOIN : m intersection n

    - LEFT OUTER JOIN : m

    - RIGHT OUTER JOIN : n

    - FULL JOIN : m + n - (m intersection n)

    - CROSS JOIN : m * n

    - SEMI JOIN

    - MAP JOIN


* JOIN CONDITION

    - EQUI JOIN

    - NON-EQUI JOIN

> NOTE. ON 절에 들어가는 predicate의 표현식을 의미.

> NOTE. Hive v2.2.0 이후부터 비동등조인(NON-EQUI JOIN)을 지원


## 샘플 테이블 생성
```
CREATE TABLE IF NOT EXISTS employee_id                         
(
  name string,
  employee_id int,
  work_place ARRAY<string>,
  gender_age STRUCT<gender:string,age:int>,
  skills_score MAP<string,int>,
  depart_title MAP<STRING,ARRAY<STRING>>
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'
COLLECTION ITEMS TERMINATED BY ','
MAP KEYS TERMINATED BY ':';

CREATE TABLE IF NOT EXISTS employee_hr
(
  name STRING
 ,employee_id INT
 ,sin_number STRING
 ,start_date DATE
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'
STORED AS TEXTFILE
;

LOAD DATA LOCAL INPATH '/path/to/data/employee_id.txt' OVERWRITE INTO TABLE employee_id;

LOAD DATA LOCAL INPATH '/path/to/data/employee_hr.txt' OVERWRITE INTO TABLE employee_hr;
```

