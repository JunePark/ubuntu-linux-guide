#

## MAP JOIN

* MAP JOIN
```
SELECT /*+ MAPJOIN(employee) */ emp.name, emph.sin_number
  FROM employee emp
 CROSS JOIN employee_hr emph WHERE emp.name <> emph.name
;
```
> NOTE. reducer job이 필요없이 mapper로만 job을 실행. 해당 테이블은 모든 mpper에게 broadcast로 전달.

* BUCKET Map Join settings
```
SET hive.optimize.bucketmapjoin = true; 
SET hive.optimize.bucketmapjoin.sortedmerge = true;
SET hive.input.format=org.apache.hadoop.hive.ql.io.BucketizedHiveInputFormat; 
```



## LEFT SEMI JOIN

* IN/EXIST절
```
SELECT a.name
  FROM employee a
 WHERE EXISTS
       (
        SELECT *
          FROM employee_id b
         WHERE a.name = b.name
       )
;
```

* 위와 동일한 결과를 얻기 위하여 LEFT SEMI JOIN 이용
```
SELECT a.name
  FROM employee a
  LEFT SEMI JOIN employee_id b
    ON a.name = b.name;
```
> NOTE. 표준 SQL문이 아님.