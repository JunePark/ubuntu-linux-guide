# OUTER JOIN


## LEFT JOIN
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  LEFT JOIN employee_hr emph ON emp.name = emph.name
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 547-968-091      |
| Will      | 527-948-090      |
| Shelley   | NULL             | -- NULL for mismatch
| Lucy      | 577-928-094      |
+-----------+------------------+
4 rows selected (25.18 seconds)
```
> NOTE. employee 테이블의 row갯수 만큼 결과가 나옴.


## RIGHT JOIN
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  RIGHT JOIN employee_hr emph ON emp.name = emph.name
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 547-968-091      |
| Will      | 527-948-090      |
| NULL      | 647-968-598      | -- NULL for mismatch
| Lucy      | 577-928-094      |
+-----------+------------------+
4 rows selected (25.96 seconds)
```
> NOTE. employee_hr 테이블의 row갯수 만큼 결과가 나옴.


## FULL OUTER JOIN
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  FULL JOIN employee_hr emph ON emp.name = emph.name
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Lucy      | 577-928-094      |
| Michael   | 547-968-091      |
| Shelley   | NULL             | -- NULL for mismatch
| NULL      | 647-968-598      | -- NULL for mismatch
| Will      | 527-948-090      |
+-----------+------------------+
5 rows selected (30.718 seconds)
```
> NOTE. m=4, n=4, m intersection n=3 => 4 + 4 -3 => 결과가 5건


## CROSS JOIN

* case1
```
SELECT emp.name, emph.sin_number
  FROM employee emp
 CROSS JOIN employee_hr emph
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 547-968-091      |
| Michael   | 527-948-090      |
| Michael   | 647-968-598      |
| Michael   | 577-928-094      |
| Will      | 547-968-091      |
| Will      | 527-948-090      |
| Will      | 647-968-598      |
| Will      | 577-928-094      |
| Shelley   | 547-968-091      |
| Shelley   | 527-948-090      |
| Shelley   | 647-968-598      |
| Shelley   | 577-928-094      |
| Lucy      | 547-968-091      |
| Lucy      | 527-948-090      |
| Lucy      | 647-968-598      |
| Lucy      | 577-928-094      |
+-----------+------------------+
16 rows selected (23.958 seconds)
```
> NOTE. m=4, n=4 => 4 * 4 => 결과가 16건


* case2
```
SELECT emp.name, emph.sin_number
  FROM employee emp
 JOIN employee_hr emph
;
```
> case1과 완전히 동일한 결과(출력 순서도 동일)


* case3
```
SELECT emp.name, emph.sin_number
  FROM employee emp
  JOIN employee_hr emph ON 1=1
;
```
> case1과 완전히 동일한 결과(출력 순서도 동일)


* unequal JOIN
```
SELECT emp.name, emph.sin_number
  FROM employee emp
 CROSS JOIN employee_hr emph
 WHERE emp.name <> emph.name
;
```
```
+-----------+------------------+
| emp.name  | emph.sin_number  |
+-----------+------------------+
| Michael   | 527-948-090      |
| Michael   | 647-968-598      |
| Michael   | 577-928-094      |
| Will      | 547-968-091      |
| Will      | 647-968-598      |
| Will      | 577-928-094      |
| Shelley   | 547-968-091      |
| Shelley   | 527-948-090      |
| Shelley   | 647-968-598      |
| Shelley   | 577-928-094      |
| Lucy      | 547-968-091      |
| Lucy      | 527-948-090      |
| Lucy      | 647-968-598      |
+-----------+------------------+
13 rows selected (24.132 seconds)
```
> NOTE. WHERE절을 이용한 필터링.
