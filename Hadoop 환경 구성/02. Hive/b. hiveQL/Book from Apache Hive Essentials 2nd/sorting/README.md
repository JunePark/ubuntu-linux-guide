#

## ORDER BY

*
```
SELECT name
  FROM employee
 ORDER BY name DESC
;
```
```
+----------+
|   name   |
+----------+
| Will     |
| Shelley  |
| Michael  |
| Lucy     |
+----------+
4 rows selected (29.184 seconds)
```
> NOTE. ASC이 기본임.


* ORDER BY 에 표현식
```
SELECT name
  FROM employee
 ORDER BY CASE WHEN name = 'Will' THEN 0 ELSE 1 END DESC
; 
```
```
+----------+
|   name   |
+----------+
| Lucy     |
| Shelley  |
| Michael  |
| Will     |
+----------+
4 rows selected (28.278 seconds)
```
> NOTE. Will이 마지막에 출력.


* NULL값 출력 순석
```
SELECT *
  FROM emp_simple
 ORDER BY work_place NULLS LAST
;
```
> NOTE. 기본적으로 ASC에서는 NULL이 제일 앞에, DESC에서는 NULL이 제일 마지막에 출력(NULL이 가장 작은값이라고 생각하면됨.)
```
+------------------+------------------------+
| emp_simple.name  | emp_simple.work_place  |
+------------------+------------------------+
| Lucy             | Montreal               |
| Michael          | Toronto                |
| Will             | NULL                   | -- NULL statys at the last
+------------------+------------------------+
3 rows selected (24.957 seconds)
```


## SORT BY

*
```
SET mapred.reduce.tasks = 2;

SELECT name
  FROM employee 
  SORT BY name DESC
;
```
```
+----------+
|   name   |
+----------+
| Shelley  | -- Once result is collected to client, it is order-less
| Michael  |
| Lucy     |
| Will     |
+----------+
4 rows selected (30.469 seconds)
```
> NOTE. reducer별로 SORT된 결과가 합해져서 나온거라 원하는 결과는 아님.

*
```
SET mapred.reduce.tasks = 1;

SELECT name
  FROM employee 
  SORT BY name DESC
;
```
```
+----------+
|   name   |
+----------+
| Will     |
| Shelley  |
| Michael  |
| Lucy     |
+----------+
4 rows selected (26.894 seconds)
```
> NOTE. ORDER BY 로 했을때와 결과가 동일.



## DISTRIBUTE BY

*
```
SELECT name
  FROM employee_hr
DISTRIBUTE BY employee_id
;
```
```
Error: Error while compiling statement: FAILED: SemanticException [Error 10004]: Line 3:14 Invalid table alias or column reference 'employee_id': (possible column names are: name) (state=42000,code=10004)
```

*
```
SELECT name, employee_id
  FROM employee_hr
DISTRIBUTE BY employee_id
;
```
```
+----------+--------------+
|   name   | employee_id  |
+----------+--------------+
| Lucy     | 103          |
| Steven   | 102          |
| Will     | 101          |
| Michael  | 100          |
+----------+--------------+
4 rows selected (26.053 seconds)
```

*
```
SELECT name, start_date
  FROM employee_hr
DISTRIBUTE BY start_date
 SORT BY name
;
```
```
+----------+-------------+
|   name   | start_date  |
+----------+-------------+
| Lucy     | 2010-01-03  |
| Michael  | 2014-01-29  |
| Steven   | 2012-11-03  |
| Will     | 2013-10-02  |
+----------+-------------+
4 rows selected (24.768 seconds)
```

*
```
SELECT name, employee_id
  FROM employee_hr
CLUSTER BY name
;
```
```
+----------+--------------+
|   name   | employee_id  |
+----------+--------------+
| Lucy     | 103          |
| Michael  | 100          |
| Steven   | 102          |
| Will     | 101          |
+----------+--------------+
4 rows selected (23.442 seconds)
```
> NOTE. SET mapred.reduce.tasks = 1; 와 SET mapred.reduce.tasks = 2; 의 결과가 다름.

> NOTE. CLUSTER BY => DISTRIBUTE BY + SORT BY 를 의미함으로 각 reducer별로 정렬을 보장하지만 전체 졍렬은 보장하지 않음.

* better performance
```
SELECT name, employee_id
  FROM (SELECT * FROM employee_hr CLUSTER BY name)
  base ORDER BY name
;
```
```
+----------+--------------+
|   name   | employee_id  |
+----------+--------------+
| Lucy     | 103          |
| Michael  | 100          |
| Steven   | 102          |
| Will     | 101          |
+----------+--------------+
4 rows selected (23.665 seconds)
```
> NOTE. 개별 reducer에서 name별로 SORT한 결과를 전체로 다시 정렬.

* better performance(feat. employee)
```
SELECT name, work_place
  FROM (SELECT * FROM employee CLUSTER BY name)
  base ORDER BY name
;
```
```
+----------+-------------------------+
|   name   |       work_place        |
+----------+-------------------------+
| Lucy     | ["Vancouver"]           |
| Michael  | ["Montreal","Toronto"]  |
| Shelley  | ["New York"]            |
| Will     | ["Montreal"]            |
+----------+-------------------------+
4 rows selected (23.382 seconds)
```