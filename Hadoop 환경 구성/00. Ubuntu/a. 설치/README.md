# Ubuntu OS 설정

## 1. IPv4 설정
* grub 수정(이 방법을 사용. 추후에 데몬으로 등록하는 프로그램에 적용시 이 방법을 사용해야 제대로 작동함)
```
# cp -p /etc/default/grub /etc/default/grub.orig
# vi /etc/default/grub

...
GRUB_CMDLINE_LINUX_DEFAULT="ipv6.disable=1"
GRUB_CMDLINE_LINUX="ipv6.disable=1"
...

# update-grub
```
> NOTE. reboot 필요 (모든 서버에서 실행할것)

* sysctl을 이용한 방법(추천하지 않음)
```
# vi /etc/sysctl.conf
...
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
net.ipv6.conf.all.disable_ipv6=1
net.ipv6.conf.default.disable_ipv6=1
net.ipv6.conf.lo.disable_ipv6 = 1
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# sysctl -p
```
> ref) https://www.thegeekdiary.com/how-to-disable-ipv6-on-ubuntu-18-04-bionic-beaver-linux/


## 2. scala 설치
```
$ curl -fL https://github.com/coursier/launchers/raw/master/cs-x86_64-pc-linux.gz | gzip -d > cs
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
100 18.6M  100 18.6M    0     0  6533k      0  0:00:02  0:00:02 --:--:-- 7895k
$ chmod +x cs
$ file cs
cs: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=ed7514057089065d80fe33f145130c1aae916d11, for GNU/Linux 3.2.0, with debug_info, not stripped
$ ./cs setup
Checking if a JVM is installed
Found a JVM installed under /opt/platform/sw/java.

Checking if ~/.local/share/coursier/bin is in PATH
  Should we add ~/.local/share/coursier/bin to your PATH via ~/.profile? [Y/n] 

Checking if the standard Scala applications are installed
  Installed ammonite
  Installed cs
  Installed coursier
  Installed scala
  Installed scalac
  Installed scala-cli
  Installed sbt
  Installed sbtn
  Installed scalafmt

$ scala
Welcome to Scala 3.1.2 (1.8.0_332, Java OpenJDK 64-Bit Server VM).
Type in expressions for evaluation. Or try :help.

scala> 
```
> ref) https://get-coursier.io/docs/cli-installation

___
.END OF UBUNTU