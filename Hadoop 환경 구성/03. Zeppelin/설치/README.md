# 개요

## 설치
```
$ cd ~/work
$ wget https://dlcdn.apache.org/zeppelin/zeppelin-0.10.0/zeppelin-0.10.0-bin-all.tgz
$ cd ~/platform/sw
$ tar zxvf 


참조
https://gitlab.com/SCcomz-BigData/sccomz-hadoop-install-guide/-/tree/main/09.Zeppelin


$ cd ~/download
$ wget https://dlcdn.apache.org/zeppelin/zeppelin-0.10.1/zeppelin-0.10.1-bin-all.tgz
$ tar zxvf zeppelin-0.10.1-bin-all.tgz -C /opt/platform/sw
$ cd /opt/platform/sw
$ ln -s zeppelin-0.10.1-bin-all zeppelin

http://192.168.0.55:9995
hadoop:hadoophadoop


spark 실행시 에러
================
$ sudo apt install python3-pip
$ python3 -m pip install pandas==1.5.3


pyspark 인터프리터
=================
@@@$ python3 -m pip install jupyter_client
@@@$ python3 -m pip install ipykernel

$ cd ~/download/
$ wget https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh
$ bash Anaconda3-2022.10-Linux-x86_64.sh

ref) https://meuse.tistory.com/entry/Linux%EC%97%90-Anaconda-%EC%84%A4%EC%B9%98%ED%95%98%EA%B8%B0

...
done
installation finished.
Do you wish the installer to initialize Anaconda3
by running conda init? [yes|no]
[no] >>> yes
modified      /home/hadoop/anaconda3/condabin/conda
modified      /home/hadoop/anaconda3/bin/conda
modified      /home/hadoop/anaconda3/bin/conda-env
no change     /home/hadoop/anaconda3/bin/activate
no change     /home/hadoop/anaconda3/bin/deactivate
no change     /home/hadoop/anaconda3/etc/profile.d/conda.sh
no change     /home/hadoop/anaconda3/etc/fish/conf.d/conda.fish
no change     /home/hadoop/anaconda3/shell/condabin/Conda.psm1
no change     /home/hadoop/anaconda3/shell/condabin/conda-hook.ps1
no change     /home/hadoop/anaconda3/lib/python3.9/site-packages/xontrib/conda.xsh
no change     /home/hadoop/anaconda3/etc/profile.d/conda.csh
modified      /home/hadoop/.bashrc

==> For changes to take effect, close and re-open your current shell. <==

If you'd prefer that conda's base environment not be activated on startup, 
   set the auto_activate_base parameter to false: 

conda config --set auto_activate_base false

Thank you for installing Anaconda3!

===========================================================================

Working with Python and Jupyter is a breeze in DataSpell. It is an IDE
designed for exploratory data analysis and ML. Get better data insights
with DataSpell.

DataSpell for Anaconda is available at: https://www.anaconda.com/dataspell

hadoop@sua:~/download$ 

$ sudo /home/hadoop/anaconda3/bin/conda create --prefix /home/hadoop/anaconda3/bin/python3 python=3.9

```