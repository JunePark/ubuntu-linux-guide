# Spark 설치

## 0. 계획
* hadoop 계정으로 spark 실행환경 구성

ref) https://dreamlog.tistory.com/607

## 1. 설치
```
$ cd ~/work/download/
$ wget https://dlcdn.apache.org/spark/spark-3.2.1/spark-3.2.1-bin-hadoop3.2-scala2.13.tgz
$ cd /opt/platform/sw/
$ tar zxvf ~/work/download/spark-3.2.1-bin-hadoop3.2-scala2.13.tgz
$ ln -s spark-3.2.1-bin-hadoop3.2-scala2.13 spark
```
> NOTE. 현재(2022-05-30) 기준으로는 scala 2.13 지원하는 spark 버전이 최신 버전임.

## 2. 설정


## 3. 불필요한 파일 삭제(in Linux)
```
$ cd /opt/platform/sw/spark/bin
$ rm -f *.cmd
```

## 4. 기타 설정
```
$ sudo vi /etc/profile.d/spark.sh

export SPARK_HOME=/opt/platform/sw/spark
export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin
export PYSPARK_PATH=/opt/python
```
> NOTE. 적용후 재로그인

## 5. 실행 계정 설정(필요시에만)
```
$ sudo chown -R hadoop:hadoop /opt/platform/sw/spark-3.2.1-bin-hadoop3.2-scala2.13/
$ sudo chown -R hadoop:hadoop /opt/platform/sw/spark
```

## 6. 실행
```
$ start-master.sh --port 7077 --webui-port 8083
```
> NOTE. 포트 기본값은 port 마스터는 7077, webui-port 마스터는 8080

## 7. 워커 스크립트 실행
```
$ start-worker.sh spark://HOSTNAME:7077

cf) 메모리/램 사용량을 변경해서 실행
$ start-worker.sh -m 212M spark://HOSTNAME:7077

> NOTE. HOSTNAME에는 설정한 호스트명 기입.
```

## 8. Spark-shell 실행
```
$ spark-shell
```

## 9. 웹 접속
웹URL:

http://HOSTNAME:8083

> NOTE. HOSTNAME에는 설정한 호스트명 기입.

___
.END OF SPARK