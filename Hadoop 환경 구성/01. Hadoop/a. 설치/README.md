# 개요

단일 머신 상에서 설치

* VMWare 환경
* IP: 192.168.126.81/24
* NAT로 네트워크 구성
* 2Core, 2GB 메모리, 60GB 이상 HDD 로 하드웨어 구성
* 호스트명: hadoop (optional)
* OS계정: hadoop (sudo를 사용할 수 있는 계정)

> NOTE. 모든 설치과정은 hadoop 계정으로 수행.

## Hadoop 설치

* 계정생성(optional)
```
# groupadd -g 1004 hadoop
# useradd hadoop -u 1004 -g hadoop -m -s /bin/bash
# usermod -a -G sudo hadoop
```

* 설치 파일 다운로드
```
$ mkdir -p ~/work/download/
$ cd ~/work/download/
$ wget https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u312-b07/OpenJDK8U-jdk_x64_linux_hotspot_8u312b07.tar.gz
$ wget https://archive.apache.org/dist/hadoop/common/hadoop-3.3.1/hadoop-3.3.1.tar.gz
```

NOTE. 최신버전 설치시(2022-05-16 기준)
```
$ wget https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u332-b09/OpenJDK8U-jdk_x64_linux_hotspot_8u332b09.tar.gz
$ wget https://dlcdn.apache.org/hadoop/common/hadoop-3.3.2/hadoop-3.3.2.tar.gz
```

```
# mkdir -p /opt/platform/
# chown hadoop:hadoop /opt/
# chown hadoop:hadoop /opt/platform/
$ mkdir /opt/platform/sw/
```

* java 설치(openjdk 8설치)
```
$ cd /opt/platform/sw/
$ tar zxvf ~/work/download/OpenJDK8U-jdk_x64_linux_hotspot_8u312b07.tar.gz
$ ln -s jdk8u312-b07 java
```
> NOTE. java11은 Hive 실행시 에러가 발생(현재 Hive v3에서도 에러가 발생)

* Hadoop 설치
```
$ cd /opt/platform/sw/
$ tar zxvf ~/work/download/hadoop-3.3.1.tar.gz
$ ln -s hadoop-3.3.1 hadoop
```

* 환경 변수 설정
```
$ touch /opt/platform/platform-env.sh
$ vi /opt/platform/platform-env.sh
```
```
#!/bin/bash

if [[ -d /opt/platform/ ]]; then
        export PLATFORM_HOME=/opt/platform
fi

if [[ -d ${PLATFORM_HOME}/sw/java/ ]]; then
        export JAVA_HOME=${PLATFORM_HOME}/sw/java
fi

if [[ -d ${PLATFORM_HOME}/sw/hadoop/ ]]; then
        export HADOOP_HOME=${PLATFORM_HOME}/sw/hadoop
fi

### Add Path
if [[ -d ${JAVA_HOME} ]]; then
        TEMP_PATH=${JAVA_HOME}/bin
fi

if [[ -d ${HADOOP_HOME} ]]; then
        TEMP_PATH=${TEMP_PATH}:${HADOOP_HOME}/bin
        TEMP_PATH=${TEMP_PATH}:${HADOOP_HOME}/sbin
fi

### Default PATH
if [[ "${DEFAULT_PATH}" == "" ]]; then
        DEFAULT_PATH=${PATH}
fi

export PATH=${TEMP_PATH}:${DEFAULT_PATH}
```

```
$ vi ~/.bashrc
```
```
...
#(맨 아래부분에)
# User specific aliases functions
if [ -f /opt/platform/platform-env.sh ]; then
    . /opt/platform/platform-env.sh
fi
```

* 설치 확인

> NOTE. 로그오프한 다음 다시 로그인 한다음에 아래 명령어 수행.

```
$ java -version
```
```
openjdk version "1.8.0_312"
OpenJDK Runtime Environment (Temurin)(build 1.8.0_312-b07)
OpenJDK 64-Bit Server VM (Temurin)(build 25.312-b07, mixed mode)
```

```
$ hadoop version
```
```
Hadoop 3.3.1
Source code repository https://github.com/apache/hadoop.git -r a3b9c37a397ad4188041dd80621bdeefc46885f2
Compiled by ubuntu on 2021-06-15T05:13Z
Compiled with protoc 3.7.1
From source with checksum 88a4ddb2299aca054416d6b7f81ca55
This command was run using /home/hadoop/platform/sw/hadoop-3.3.1/share/hadoop/common/hadoop-common-3.3.1.jar
```

## hadoop 설정
```
$ cd ${HADOOP_HOME}/etc/hadoop/
$ cp -p core-site.xml core-site.xml.orig
$ cp -p hdfs-site.xml hdfs-site.xml.orig
$ cp -p mapred-site.xml mapred-site.xml.orig
$ cp -p yarn-site.xml yarn-site.xml.orig
$ cp -p hadoop-env.sh hadoop-env.sh.orig
```

```
$ sudo mkdir /data
$ sudo chown hadoop:hadoop /data
```
or
```
# vgdisplay
  --- Volume group ---
  VG Name               ubuntu-vg
...
# lvcreate --size 512G --name hadoop-data-lv ubuntu-vg
# lvdisplay
  --- Logical volume ---
  LV Path                /dev/ubuntu-vg/ubuntu-lv
...
  --- Logical volume ---
  LV Path                /dev/ubuntu-vg/hadoop-data-lv
...
# ls -al /dev/ubuntu-vg/
lrwxrwxrwx  1 root root    7 May 25 06:11 hadoop-data-lv -> ../dm-1
lrwxrwxrwx  1 root root    7 May 25 05:44 ubuntu-lv -> ../dm-0
# mkfs.ext4 /dev/ubuntu-vg/hadoop-data-lv -E root_owner=1004:1004
mke2fs 1.46.5 (30-Dec-2021)
Discarding device blocks: done                            
Creating filesystem with 134217728 4k blocks and 33554432 inodes
Filesystem UUID: afb2b3f4-3e21-4766-9234-c0c6fcea0ca4
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
        4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968, 
        102400000

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (262144 blocks): done
Writing superblocks and filesystem accounting information: done
# mkdir /data
# chown hadoop:hadoop /data
# mount /dev/ubuntu-vg/hadoop-data-lv /data
# umount /data
# vi /etc/fstab
/dev/ubuntu-vg/hadoop-data-lv    /data    ext4    defaults    0    0
```
> NOTE. mkfs.ext4 의 -E root_owner=1004:1004 옵션에 대해서는 ref) https://xenostudy.tistory.com/632

```
#systemctl disable systemd-networkd-wait-online.service
Removed /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service.
```
> NOTE. 부팅시 a start job is running for wait for network to be configured... 로 2분 이상 멈추어지는 문제점을 없애기 위한 방법

* core-site.xml
```
<configuration>
  <property>
    <name>fs.defaultFS</name>
    <value>hdfs://localhost/</value>
  </property>
  <property>
    <name>hadoop.tmp.dir</name>
    <value>/data</value>
  </property>
</configuration>
```
> NOTE. hadoop.tmp.dir 은 tmp 디렉토리를 지정하는 것이 아니라 HDFS의 root를 지정한다고 보면 됨.

* hdfs-site.xml
```
<configuration>
  <property>
    <name>dfs.replication</name>
    <value>1</value>
  </property>
</configuration>
```

* mapred-site.xml
```
<configuration>
  <property>
    <name>mapreduce.framework.name</name>
    <value>yarn</value>
  </property>
  <property>
   <name>yarn.app.mapreduce.am.env</name>
   <value>HADOOP_MAPRED_HOME=/opt/platform/sw/hadoop</value>
  </property>
  <property>
   <name>mapreduce.map.env</name>
   <value>HADOOP_MAPRED_HOME=/opt/platform/sw/hadoop</value>
  </property>
  <property>
   <name>mapreduce.reduce.env</name>
   <value>HADOOP_MAPRED_HOME=/opt/platform/sw/hadoop</value>
  </property>
</configuration>
```
> HADOOP_MAPRED_HOME 설정값을 설정하지 않는 경우에는 insert select 문 등 정상적인 HiveQL이 실행되지 않음.(Hive에서 mapred 엔진 사용시)

* yarn-site.xml
```
<configuration>
  <property>
    <name>yarn.resourcemanager.hostname</name>
    <value>localhost</value>
  </property>
  <property>
    <name>yarn.nodemanager.aux-services</name>
    <value>mapreduce_shuffle</value>
  </property>
  <property>
    <name>yarn.resourcemanager.webapp.address</name>
    <value>0.0.0.0:8088</value>
  </property>
</configuration>
```
> NOTE. web ui가 127.0.0.1:8088로만 열리는 문제가 있어 webapp.address를 설정.

* hadoop-env.sh
```
...
### Platform Environment
. /opt/platform/platform-env.sh
```
> NOTE. 가능하면 위쪽에 추가

## HDFS 파일시스템 포맷
```
$ hdfs namenode -format
```
```
2021-12-29 06:52:05,045 INFO namenode.NameNode: STARTUP_MSG: 
/************************************************************
STARTUP_MSG: Starting NameNode
STARTUP_MSG:   host = hadoop/127.0.1.1
STARTUP_MSG:   args = [-format]
STARTUP_MSG:   version = 3.3.1
STARTUP_MSG:   classpath = /home/hadoop/platform/sw/hadoop/etc/hadoop:/home/hadoop/platform/sw/hadoop/share/hadoop/common/lib/kerby-pkix-1.0.1.jar:...
...
STARTUP_MSG:   build = https://github.com/apache/hadoop.git -r a3b9c37a397ad4188041dd80621bdeefc46885f2; compiled by 'ubuntu' on 2021-06-15T05:13Z
STARTUP_MSG:   java = 1.8.0_312
************************************************************/
2021-12-29 06:52:05,060 INFO namenode.NameNode: registered UNIX signal handlers for [TERM, HUP, INT]
2021-12-29 06:52:05,201 INFO namenode.NameNode: createNameNode [-format]
2021-12-29 06:52:05,614 INFO namenode.NameNode: Formatting using clusterid: CID-517edddb-33a5-4ab5-b595-b17e6652a763
2021-12-29 06:52:05,655 INFO namenode.FSEditLog: Edit logging is async:true
2021-12-29 06:52:05,682 INFO namenode.FSNamesystem: KeyProvider: null
2021-12-29 06:52:05,684 INFO namenode.FSNamesystem: fsLock is fair: true
2021-12-29 06:52:05,686 INFO namenode.FSNamesystem: Detailed lock hold time metrics enabled: false
2021-12-29 06:52:05,694 INFO namenode.FSNamesystem: fsOwner                = hadoop (auth:SIMPLE)
2021-12-29 06:52:05,694 INFO namenode.FSNamesystem: supergroup             = supergroup
2021-12-29 06:52:05,694 INFO namenode.FSNamesystem: isPermissionEnabled    = true
2021-12-29 06:52:05,694 INFO namenode.FSNamesystem: isStoragePolicyEnabled = true
2021-12-29 06:52:05,695 INFO namenode.FSNamesystem: HA Enabled: false
2021-12-29 06:52:05,742 INFO common.Util: dfs.datanode.fileio.profiling.sampling.percentage set to 0. Disabling file IO profiling
2021-12-29 06:52:05,754 INFO blockmanagement.DatanodeManager: dfs.block.invalidate.limit: configured=1000, counted=60, effected=1000
2021-12-29 06:52:05,754 INFO blockmanagement.DatanodeManager: dfs.namenode.datanode.registration.ip-hostname-check=true
2021-12-29 06:52:05,761 INFO blockmanagement.BlockManager: dfs.namenode.startup.delay.block.deletion.sec is set to 000:00:00:00.000
2021-12-29 06:52:05,762 INFO blockmanagement.BlockManager: The block deletion will start around 2021 Dec 29 06:52:05
2021-12-29 06:52:05,763 INFO util.GSet: Computing capacity for map BlocksMap
2021-12-29 06:52:05,764 INFO util.GSet: VM type       = 64-bit
2021-12-29 06:52:05,765 INFO util.GSet: 2.0% max memory 436 MB = 8.7 MB
2021-12-29 06:52:05,766 INFO util.GSet: capacity      = 2^20 = 1048576 entries
2021-12-29 06:52:05,778 INFO blockmanagement.BlockManager: Storage policy satisfier is disabled
2021-12-29 06:52:05,779 INFO blockmanagement.BlockManager: dfs.block.access.token.enable = false
2021-12-29 06:52:05,787 INFO blockmanagement.BlockManagerSafeMode: dfs.namenode.safemode.threshold-pct = 0.999
2021-12-29 06:52:05,787 INFO blockmanagement.BlockManagerSafeMode: dfs.namenode.safemode.min.datanodes = 0
2021-12-29 06:52:05,787 INFO blockmanagement.BlockManagerSafeMode: dfs.namenode.safemode.extension = 30000
2021-12-29 06:52:05,789 INFO blockmanagement.BlockManager: defaultReplication         = 1
2021-12-29 06:52:05,789 INFO blockmanagement.BlockManager: maxReplication             = 512
2021-12-29 06:52:05,789 INFO blockmanagement.BlockManager: minReplication             = 1
2021-12-29 06:52:05,790 INFO blockmanagement.BlockManager: maxReplicationStreams      = 2
2021-12-29 06:52:05,790 INFO blockmanagement.BlockManager: redundancyRecheckInterval  = 3000ms
2021-12-29 06:52:05,790 INFO blockmanagement.BlockManager: encryptDataTransfer        = false
2021-12-29 06:52:05,790 INFO blockmanagement.BlockManager: maxNumBlocksToLog          = 1000
2021-12-29 06:52:05,818 INFO namenode.FSDirectory: GLOBAL serial map: bits=29 maxEntries=536870911
2021-12-29 06:52:05,818 INFO namenode.FSDirectory: USER serial map: bits=24 maxEntries=16777215
2021-12-29 06:52:05,818 INFO namenode.FSDirectory: GROUP serial map: bits=24 maxEntries=16777215
2021-12-29 06:52:05,818 INFO namenode.FSDirectory: XATTR serial map: bits=24 maxEntries=16777215
2021-12-29 06:52:05,840 INFO util.GSet: Computing capacity for map INodeMap
2021-12-29 06:52:05,840 INFO util.GSet: VM type       = 64-bit
2021-12-29 06:52:05,841 INFO util.GSet: 1.0% max memory 436 MB = 4.4 MB
2021-12-29 06:52:05,841 INFO util.GSet: capacity      = 2^19 = 524288 entries
2021-12-29 06:52:05,841 INFO namenode.FSDirectory: ACLs enabled? true
2021-12-29 06:52:05,841 INFO namenode.FSDirectory: POSIX ACL inheritance enabled? true
2021-12-29 06:52:05,842 INFO namenode.FSDirectory: XAttrs enabled? true
2021-12-29 06:52:05,843 INFO namenode.NameNode: Caching file names occurring more than 10 times
2021-12-29 06:52:05,850 INFO snapshot.SnapshotManager: Loaded config captureOpenFiles: false, skipCaptureAccessTimeOnlyChange: false, snapshotDiffAllowSnapRootDescendant: true, maxSnapshotLimit: 65536
2021-12-29 06:52:05,852 INFO snapshot.SnapshotManager: SkipList is disabled
2021-12-29 06:52:05,860 INFO util.GSet: Computing capacity for map cachedBlocks
2021-12-29 06:52:05,860 INFO util.GSet: VM type       = 64-bit
2021-12-29 06:52:05,860 INFO util.GSet: 0.25% max memory 436 MB = 1.1 MB
2021-12-29 06:52:05,860 INFO util.GSet: capacity      = 2^17 = 131072 entries
2021-12-29 06:52:05,873 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.window.num.buckets = 10
2021-12-29 06:52:05,874 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.num.users = 10
2021-12-29 06:52:05,874 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.windows.minutes = 1,5,25
2021-12-29 06:52:05,878 INFO namenode.FSNamesystem: Retry cache on namenode is enabled
2021-12-29 06:52:05,878 INFO namenode.FSNamesystem: Retry cache will use 0.03 of total heap and retry cache entry expiry time is 600000 millis
2021-12-29 06:52:05,880 INFO util.GSet: Computing capacity for map NameNodeRetryCache
2021-12-29 06:52:05,880 INFO util.GSet: VM type       = 64-bit
2021-12-29 06:52:05,881 INFO util.GSet: 0.029999999329447746% max memory 436 MB = 133.9 KB
2021-12-29 06:52:05,881 INFO util.GSet: capacity      = 2^14 = 16384 entries
2021-12-29 06:52:05,919 INFO namenode.FSImage: Allocated new BlockPoolId: BP-17574746-127.0.1.1-1640760725905
2021-12-29 06:52:05,934 INFO common.Storage: Storage directory /data/dfs/name has been successfully formatted.
2021-12-29 06:52:05,973 INFO namenode.FSImageFormatProtobuf: Saving image file /data/dfs/name/current/fsimage.ckpt_0000000000000000000 using no compression
2021-12-29 06:52:06,094 INFO namenode.FSImageFormatProtobuf: Image file /data/dfs/name/current/fsimage.ckpt_0000000000000000000 of size 401 bytes saved in 0 seconds .
2021-12-29 06:52:06,112 INFO namenode.NNStorageRetentionManager: Going to retain 1 images with txid >= 0
2021-12-29 06:52:06,123 INFO namenode.FSNamesystem: Stopping services started for active state
2021-12-29 06:52:06,124 INFO namenode.FSNamesystem: Stopping services started for standby state
2021-12-29 06:52:06,129 INFO namenode.FSImage: FSImageSaver clean checkpoint: txid=0 when meet shutdown.
2021-12-29 06:52:06,130 INFO namenode.NameNode: SHUTDOWN_MSG: 
/************************************************************
SHUTDOWN_MSG: Shutting down NameNode at hadoop/127.0.1.1
************************************************************/
```

```
hadoop@sua:~$ hdfs namenode -format
WARNING: /opt/platform/sw/hadoop/logs does not exist. Creating.
2022-05-27 23:29:31,242 INFO namenode.NameNode: STARTUP_MSG: 
/************************************************************
STARTUP_MSG: Starting NameNode
STARTUP_MSG:   host = sua/127.0.1.1
STARTUP_MSG:   args = [-format]
STARTUP_MSG:   version = 3.3.2
STARTUP_MSG:   classpath = /opt/platform/sw/hadoop/etc/hadoop:/opt/platform/sw/hadoop/share/hadoop/common/lib/jaxb-api-2.2.11.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/hadoop-shaded-guava-1.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerby-config-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-lang3-3.12.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jersey-servlet-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/guava-27.0-jre.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/zookeeper-jute-3.5.6.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-util-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/hadoop-shaded-protobuf_3_7-1.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jsp-api-2.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jackson-xc-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jsr311-api-1.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerby-asn1-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-net-3.6.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/paranamer-2.3.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/json-smart-2.4.7.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-daemon-1.0.13.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-simplekdc-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-servlet-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jackson-core-asl-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jsr305-3.0.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-server-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/re2j-1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-core-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-crypto-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-beanutils-1.9.4.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-webapp-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jackson-mapper-asl-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/netty-3.10.6.Final.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/gson-2.8.9.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-io-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/curator-recipes-4.2.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-security-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/httpclient-4.5.13.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-xml-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/zookeeper-3.5.6.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-configuration2-2.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-collections-3.2.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/slf4j-api-1.7.30.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jul-to-slf4j-1.7.30.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-identity-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/asm-5.0.4.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jackson-annotations-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jersey-json-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/audience-annotations-0.5.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/httpcore-4.4.13.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-client-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/protobuf-java-2.5.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/stax2-api-4.2.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jcip-annotations-1.0-1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerby-pkix-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-io-2.8.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/log4j-1.2.17.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/avro-1.7.7.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-util-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/metrics-core-3.2.4.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/failureaccess-1.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-server-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/accessors-smart-2.4.7.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jettison-1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-cli-1.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/hadoop-annotations-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/nimbus-jose-jwt-9.8.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/curator-framework-4.2.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-codec-1.11.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/dnsjava-2.1.7.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jackson-jaxrs-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jersey-server-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jersey-core-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-admin-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/hadoop-auth-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-compress-1.21.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-math3-3.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/snappy-java-1.1.8.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-http-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/javax.servlet-api-3.1.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/j2objc-annotations-1.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/checker-qual-2.5.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/animal-sniffer-annotations-1.17.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jaxb-impl-2.2.3-1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerb-common-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jackson-databind-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jetty-util-ajax-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jackson-core-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-logging-1.1.3.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/token-provider-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/slf4j-log4j12-1.7.30.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/curator-client-4.2.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jakarta.activation-api-1.2.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/woodstox-core-5.3.0.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/jsch-0.1.55.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerby-util-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/commons-text-1.4.jar:/opt/platform/sw/hadoop/share/hadoop/common/lib/kerby-xdr-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/common/hadoop-common-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/hadoop-nfs-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/hadoop-common-3.3.2-tests.jar:/opt/platform/sw/hadoop/share/hadoop/common/hadoop-registry-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/common/hadoop-kms-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jaxb-api-2.2.11.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/hadoop-shaded-guava-1.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerby-config-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-lang3-3.12.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jersey-servlet-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/guava-27.0-jre.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/zookeeper-jute-3.5.6.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-util-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/hadoop-shaded-protobuf_3_7-1.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jackson-xc-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jsr311-api-1.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerby-asn1-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-net-3.6.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/paranamer-2.3.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/json-smart-2.4.7.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-daemon-1.0.13.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-simplekdc-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-servlet-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/netty-all-4.1.68.Final.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jackson-core-asl-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jsr305-3.0.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-server-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/re2j-1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-core-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-crypto-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-beanutils-1.9.4.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-webapp-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jackson-mapper-asl-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/netty-3.10.6.Final.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/gson-2.8.9.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-io-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/curator-recipes-4.2.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-security-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/httpclient-4.5.13.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-xml-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/zookeeper-3.5.6.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-configuration2-2.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-collections-3.2.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-identity-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/asm-5.0.4.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/okio-1.6.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jackson-annotations-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jersey-json-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/audience-annotations-0.5.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/httpcore-4.4.13.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-client-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/protobuf-java-2.5.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/stax2-api-4.2.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jcip-annotations-1.0-1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerby-pkix-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-io-2.8.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/json-simple-1.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/log4j-1.2.17.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/avro-1.7.7.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-util-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/leveldbjni-all-1.8.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/failureaccess-1.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-server-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/accessors-smart-2.4.7.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jettison-1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-cli-1.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/hadoop-annotations-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/nimbus-jose-jwt-9.8.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/curator-framework-4.2.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-codec-1.11.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/dnsjava-2.1.7.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jackson-jaxrs-1.9.13.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jersey-server-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jersey-core-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-admin-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/hadoop-auth-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-compress-1.21.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-math3-3.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/snappy-java-1.1.8.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-http-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/javax.servlet-api-3.1.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/j2objc-annotations-1.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/checker-qual-2.5.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/animal-sniffer-annotations-1.17.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jaxb-impl-2.2.3-1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerb-common-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jackson-databind-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jetty-util-ajax-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jackson-core-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-logging-1.1.3.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/token-provider-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/curator-client-4.2.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jakarta.activation-api-1.2.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/woodstox-core-5.3.0.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/jsch-0.1.55.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerby-util-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/okhttp-2.7.5.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/commons-text-1.4.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/lib/kerby-xdr-1.0.1.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-nfs-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-httpfs-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-rbf-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-client-3.3.2-tests.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-native-client-3.3.2-tests.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-3.3.2-tests.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-native-client-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-rbf-3.3.2-tests.jar:/opt/platform/sw/hadoop/share/hadoop/hdfs/hadoop-hdfs-client-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-hs-plugins-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-3.3.2-tests.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-nativetask-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-core-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-common-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-hs-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-app-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-shuffle-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-uploader-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/fst-2.50.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jline-3.9.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/asm-commons-9.1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/websocket-client-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/snakeyaml-1.26.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/swagger-annotations-1.5.4.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/bcprov-jdk15on-1.60.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/geronimo-jcache_1.0_spec-1.0-alpha-1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jna-5.2.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/asm-tree-9.1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/HikariCP-java7-2.4.12.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/javax-websocket-server-impl-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jackson-jaxrs-base-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/javax.websocket-api-1.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/javax.inject-1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jetty-client-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/json-io-2.5.1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/objenesis-2.6.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/websocket-common-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jersey-guice-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/java-util-1.9.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/guice-4.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/javax.websocket-client-api-1.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jersey-client-1.19.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/asm-analysis-9.1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/websocket-api-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jackson-jaxrs-json-provider-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/javax.ws.rs-api-2.1.1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/guice-servlet-4.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/javax-websocket-client-impl-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/mssql-jdbc-6.2.1.jre7.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/aopalliance-1.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jakarta.xml.bind-api-2.3.3.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jackson-module-jaxb-annotations-2.13.0.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/metrics-core-3.2.4.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jetty-jndi-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/bcpkix-jdk15on-1.60.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jetty-annotations-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/jetty-plus-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/websocket-servlet-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/ehcache-3.3.1.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/lib/websocket-server-9.4.43.v20210629.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-router-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-common-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-registry-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-applicationhistoryservice-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-web-proxy-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-resourcemanager-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-timeline-pluginstorage-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-applications-unmanaged-am-launcher-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-applications-distributedshell-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-sharedcachemanager-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-nodemanager-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-services-api-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-tests-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-client-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-services-core-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-server-common-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-applications-mawo-core-3.3.2.jar:/opt/platform/sw/hadoop/share/hadoop/yarn/hadoop-yarn-api-3.3.2.jar
STARTUP_MSG:   build = git@github.com:apache/hadoop.git -r 0bcb014209e219273cb6fd4152df7df713cbac61; compiled by 'chao' on 2022-02-21T18:39Z
STARTUP_MSG:   java = 1.8.0_332
************************************************************/
2022-05-27 23:29:31,253 INFO namenode.NameNode: registered UNIX signal handlers for [TERM, HUP, INT]
2022-05-27 23:29:31,333 INFO namenode.NameNode: createNameNode [-format]
2022-05-27 23:29:31,709 INFO namenode.NameNode: Formatting using clusterid: CID-e12f585b-9a52-4214-83a8-22dd4c1eaf79
2022-05-27 23:29:31,825 INFO namenode.FSEditLog: Edit logging is async:true
2022-05-27 23:29:31,860 INFO namenode.FSNamesystem: KeyProvider: null
2022-05-27 23:29:31,861 INFO namenode.FSNamesystem: fsLock is fair: true
2022-05-27 23:29:31,861 INFO namenode.FSNamesystem: Detailed lock hold time metrics enabled: false
2022-05-27 23:29:31,865 INFO namenode.FSNamesystem: fsOwner                = hadoop (auth:SIMPLE)
2022-05-27 23:29:31,865 INFO namenode.FSNamesystem: supergroup             = supergroup
2022-05-27 23:29:31,865 INFO namenode.FSNamesystem: isPermissionEnabled    = true
2022-05-27 23:29:31,865 INFO namenode.FSNamesystem: isStoragePolicyEnabled = true
2022-05-27 23:29:31,865 INFO namenode.FSNamesystem: HA Enabled: false
2022-05-27 23:29:31,899 INFO common.Util: dfs.datanode.fileio.profiling.sampling.percentage set to 0. Disabling file IO profiling
2022-05-27 23:29:31,908 INFO blockmanagement.DatanodeManager: dfs.block.invalidate.limit: configured=1000, counted=60, effected=1000
2022-05-27 23:29:31,908 INFO blockmanagement.DatanodeManager: dfs.namenode.datanode.registration.ip-hostname-check=true
2022-05-27 23:29:31,911 INFO blockmanagement.BlockManager: dfs.namenode.startup.delay.block.deletion.sec is set to 000:00:00:00.000
2022-05-27 23:29:31,911 INFO blockmanagement.BlockManager: The block deletion will start around 2022 May 27 23:29:31
2022-05-27 23:29:31,912 INFO util.GSet: Computing capacity for map BlocksMap
2022-05-27 23:29:31,912 INFO util.GSet: VM type       = 64-bit
2022-05-27 23:29:31,914 INFO util.GSet: 2.0% max memory 1.7 GB = 34.0 MB
2022-05-27 23:29:31,914 INFO util.GSet: capacity      = 2^22 = 4194304 entries
2022-05-27 23:29:31,925 INFO blockmanagement.BlockManager: Storage policy satisfier is disabled
2022-05-27 23:29:31,925 INFO blockmanagement.BlockManager: dfs.block.access.token.enable = false
2022-05-27 23:29:31,929 INFO blockmanagement.BlockManagerSafeMode: dfs.namenode.safemode.threshold-pct = 0.999
2022-05-27 23:29:31,929 INFO blockmanagement.BlockManagerSafeMode: dfs.namenode.safemode.min.datanodes = 0
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManagerSafeMode: dfs.namenode.safemode.extension = 30000
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManager: defaultReplication         = 1
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManager: maxReplication             = 512
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManager: minReplication             = 1
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManager: maxReplicationStreams      = 2
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManager: redundancyRecheckInterval  = 3000ms
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManager: encryptDataTransfer        = false
2022-05-27 23:29:31,930 INFO blockmanagement.BlockManager: maxNumBlocksToLog          = 1000
2022-05-27 23:29:31,950 INFO namenode.FSDirectory: GLOBAL serial map: bits=29 maxEntries=536870911
2022-05-27 23:29:31,950 INFO namenode.FSDirectory: USER serial map: bits=24 maxEntries=16777215
2022-05-27 23:29:31,950 INFO namenode.FSDirectory: GROUP serial map: bits=24 maxEntries=16777215
2022-05-27 23:29:31,950 INFO namenode.FSDirectory: XATTR serial map: bits=24 maxEntries=16777215
2022-05-27 23:29:31,961 INFO util.GSet: Computing capacity for map INodeMap
2022-05-27 23:29:31,961 INFO util.GSet: VM type       = 64-bit
2022-05-27 23:29:31,961 INFO util.GSet: 1.0% max memory 1.7 GB = 17.0 MB
2022-05-27 23:29:31,961 INFO util.GSet: capacity      = 2^21 = 2097152 entries
2022-05-27 23:29:31,968 INFO namenode.FSDirectory: ACLs enabled? true
2022-05-27 23:29:31,968 INFO namenode.FSDirectory: POSIX ACL inheritance enabled? true
2022-05-27 23:29:31,968 INFO namenode.FSDirectory: XAttrs enabled? true
2022-05-27 23:29:31,969 INFO namenode.NameNode: Caching file names occurring more than 10 times
2022-05-27 23:29:31,973 INFO snapshot.SnapshotManager: Loaded config captureOpenFiles: false, skipCaptureAccessTimeOnlyChange: false, snapshotDiffAllowSnapRootDescendant: true, maxSnapshotLimit: 65536
2022-05-27 23:29:31,975 INFO snapshot.SnapshotManager: SkipList is disabled
2022-05-27 23:29:31,979 INFO util.GSet: Computing capacity for map cachedBlocks
2022-05-27 23:29:31,979 INFO util.GSet: VM type       = 64-bit
2022-05-27 23:29:31,979 INFO util.GSet: 0.25% max memory 1.7 GB = 4.3 MB
2022-05-27 23:29:31,979 INFO util.GSet: capacity      = 2^19 = 524288 entries
2022-05-27 23:29:31,986 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.window.num.buckets = 10
2022-05-27 23:29:31,986 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.num.users = 10
2022-05-27 23:29:31,986 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.windows.minutes = 1,5,25
2022-05-27 23:29:31,991 INFO namenode.FSNamesystem: Retry cache on namenode is enabled
2022-05-27 23:29:31,991 INFO namenode.FSNamesystem: Retry cache will use 0.03 of total heap and retry cache entry expiry time is 600000 millis
2022-05-27 23:29:31,992 INFO util.GSet: Computing capacity for map NameNodeRetryCache
2022-05-27 23:29:31,992 INFO util.GSet: VM type       = 64-bit
2022-05-27 23:29:31,992 INFO util.GSet: 0.029999999329447746% max memory 1.7 GB = 522.7 KB
2022-05-27 23:29:31,993 INFO util.GSet: capacity      = 2^16 = 65536 entries
2022-05-27 23:29:32,017 INFO namenode.FSImage: Allocated new BlockPoolId: BP-2020940483-127.0.1.1-1653694172008
2022-05-27 23:29:32,255 INFO common.Storage: Storage directory /data/dfs/name has been successfully formatted.
2022-05-27 23:29:32,285 INFO namenode.FSImageFormatProtobuf: Saving image file /data/dfs/name/current/fsimage.ckpt_0000000000000000000 using no compression
2022-05-27 23:29:32,368 INFO namenode.FSImageFormatProtobuf: Image file /data/dfs/name/current/fsimage.ckpt_0000000000000000000 of size 401 bytes saved in 0 seconds .
2022-05-27 23:29:32,431 INFO namenode.NNStorageRetentionManager: Going to retain 1 images with txid >= 0
2022-05-27 23:29:32,472 INFO namenode.FSNamesystem: Stopping services started for active state
2022-05-27 23:29:32,473 INFO namenode.FSNamesystem: Stopping services started for standby state
2022-05-27 23:29:32,477 INFO namenode.FSImage: FSImageSaver clean checkpoint: txid=0 when meet shutdown.
2022-05-27 23:29:32,477 INFO namenode.NameNode: SHUTDOWN_MSG: 
/************************************************************
SHUTDOWN_MSG: Shutting down NameNode at sua/127.0.1.1
************************************************************/
hadoop@sua:~$ 
```
> NOTE. 재실행시 Re-format filesystem in Storage Directory root= /tmp/hadoop-hadoop/dfs/name; location= null ? (Y or N) 물어볼 수 있음.

> NOTE. 재실행전 /data에 구성된 hadoop 관련 디렉토리는 삭제하고 실행할 것을 권장.


* 확인
```
$ ls -al /data/dfs/name
```


## ssh 설정
```
$ ssh-keygen -t rsa -P ''
```
```
Generating public/private rsa key pair.
Enter file in which to save the key (/home/hadoop/.ssh/id_rsa): 
Your identification has been saved in /home/hadoop/.ssh/id_rsa
Your public key has been saved in /home/hadoop/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:/6jqPBqEG22Fr4mP8JVr+GeVg5LWS5E6FouRYMmNDIM hadoop@hadoop
The key's randomart image is:
+---[RSA 3072]----+
|B +              |
|EB . .           |
|.. .. ..         |
|  oo.oo          |
|  oo+*.oS.       |
|  .*O++ +.       |
|. o+*+ o ..      |
| o.+.+=    o     |
|  oo==+o... .    |
+----[SHA256]-----+
```

```
$ cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
$ ssh localhost
```
> 암호없이 로그인 성공하면 OK.


## 데몬 시작과 중지

* 시작
```
$ start-dfs.sh
```
```
Starting namenodes on [localhost]
Starting datanodes
Starting secondary namenodes [hadoop]
```

```
$ jps
```
11284 DataNode
11524 SecondaryNameNode
11111 NameNode
11626 Jps
```
```


```
$ start-yarn.sh
```
```
Starting resourcemanager
Starting nodemanagers
```

```
$ jps
```
```
11905 NodeManager
11729 ResourceManager
12211 Jps
11284 DataNode
11524 SecondaryNameNode
11111 NameNode
```

```
$ mr-jobhistory-daemon.sh start historyserver
```

* 중지
```
$ mr-jobhistory-daemon.sh stop historyserver
```

```
$ stop-yarn.sh
```
```
Stopping nodemanagers
Stopping resourcemanager
```

```
$ stop-dfs.sh
```
```
Stopping namenodes on [localhost]
Stopping datanodes
Stopping secondary namenodes [hadoop]
```

* all start
```
$ start-all.sh
```
```
WARNING: Attempting to start all Apache Hadoop daemons as hadoop in 10 seconds.
WARNING: This is not a recommended production deployment configuration.
WARNING: Use CTRL-C to abort.
Starting namenodes on [localhost]
Starting datanodes
Starting secondary namenodes [hadoop]
Starting resourcemanager
Starting nodemanagers
```

* all stop
```
$ stop-all.sh
```
```
WARNING: Stopping all Apache Hadoop daemons as hadoop in 10 seconds.
WARNING: Use CTRL-C to abort.
Stopping namenodes on [localhost]
Stopping datanodes
Stopping secondary namenodes [hadoop]
Stopping nodemanagers
Stopping resourcemanager
```

## 사용자 디렉토리 생성

* 자신의 홈디렉토리 생성
```
$ hadoop fs -mkdir /user
$ hadoop fs -mkdir /user/$USER
```

* 확인
```
$ hadoop fs -ls /user
```

## WEB UI
* namenode
```
http://localhost:9870/dfshealth.html#tab-overview

or

http://192.168.126.81:9870/dfshealth.html#tab-overview
```

* yarn resource manager
```
http://localhost:8088/cluster

or

http://192.168.126.81:8088/cluster
```

* jobhistory
```
http://localhost:19888/jobhistory

or

http://192.168.126.81:19888/jobhistory
```
