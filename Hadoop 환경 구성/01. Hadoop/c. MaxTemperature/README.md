# 개요

O'Reilly's "Hadoop: The Definitive Guide" by Tom White 4판의 2장에 실린예제 수행

## 실행
```
$ hadoop fs -mkdir input
$ hadoop fs -mkdir input/ncdc
$ hadoop fs -put sample.txt input/ncdc/
```

```
$ cd ~/work/hadoop-book
$ hadoop jar hadoop-examples.jar MaxTemperature input/ncdc/sample.txt output
```

```
2022-05-18 10:01:10,461 INFO impl.MetricsConfig: Loaded properties from hadoop-metrics2.properties
2022-05-18 10:01:10,512 INFO impl.MetricsSystemImpl: Scheduled Metric snapshot period at 10 second(s).
2022-05-18 10:01:10,512 INFO impl.MetricsSystemImpl: JobTracker metrics system started
2022-05-18 10:01:10,641 WARN mapreduce.JobResourceUploader: Hadoop command-line option parsing not performed. Implement the Tool interface and execute your application with ToolRunner to remedy this.
2022-05-18 10:01:10,706 INFO input.FileInputFormat: Total input files to process : 1
2022-05-18 10:01:10,762 INFO mapreduce.JobSubmitter: number of splits:1
2022-05-18 10:01:10,838 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_local1183919573_0001
2022-05-18 10:01:10,838 INFO mapreduce.JobSubmitter: Executing with tokens: []
2022-05-18 10:01:10,944 INFO mapreduce.Job: The url to track the job: http://localhost:8080/
2022-05-18 10:01:10,945 INFO mapreduce.Job: Running job: job_local1183919573_0001
2022-05-18 10:01:10,948 INFO mapred.LocalJobRunner: OutputCommitter set in config null
2022-05-18 10:01:10,954 INFO output.FileOutputCommitter: File Output Committer Algorithm version is 2
2022-05-18 10:01:10,954 INFO output.FileOutputCommitter: FileOutputCommitter skip cleanup _temporary folders under output directory:false, ignore cleanup failures: false
2022-05-18 10:01:10,955 INFO mapred.LocalJobRunner: OutputCommitter is org.apache.hadoop.mapreduce.lib.output.FileOutputCommitter
2022-05-18 10:01:11,137 INFO mapred.LocalJobRunner: Waiting for map tasks
2022-05-18 10:01:11,137 INFO mapred.LocalJobRunner: Starting task: attempt_local1183919573_0001_m_000000_0
2022-05-18 10:01:11,160 INFO output.FileOutputCommitter: File Output Committer Algorithm version is 2
2022-05-18 10:01:11,160 INFO output.FileOutputCommitter: FileOutputCommitter skip cleanup _temporary folders under output directory:false, ignore cleanup failures: false
2022-05-18 10:01:11,178 INFO mapred.Task:  Using ResourceCalculatorProcessTree : [ ]
2022-05-18 10:01:11,182 INFO mapred.MapTask: Processing split: hdfs://sccomz-cluster/user/hadoop/input/ncdc/sample.txt:0+529
2022-05-18 10:01:11,232 INFO mapred.MapTask: (EQUATOR) 0 kvi 26214396(104857584)
2022-05-18 10:01:11,232 INFO mapred.MapTask: mapreduce.task.io.sort.mb: 100
2022-05-18 10:01:11,232 INFO mapred.MapTask: soft limit at 83886080
2022-05-18 10:01:11,232 INFO mapred.MapTask: bufstart = 0; bufvoid = 104857600
2022-05-18 10:01:11,232 INFO mapred.MapTask: kvstart = 26214396; length = 6553600
2022-05-18 10:01:11,235 INFO mapred.MapTask: Map output collector class = org.apache.hadoop.mapred.MapTask$MapOutputBuffer
2022-05-18 10:01:11,301 INFO mapred.LocalJobRunner: 
2022-05-18 10:01:11,303 INFO mapred.MapTask: Starting flush of map output
2022-05-18 10:01:11,303 INFO mapred.MapTask: Spilling map output
2022-05-18 10:01:11,303 INFO mapred.MapTask: bufstart = 0; bufend = 45; bufvoid = 104857600
2022-05-18 10:01:11,303 INFO mapred.MapTask: kvstart = 26214396(104857584); kvend = 26214380(104857520); length = 17/6553600
2022-05-18 10:01:11,308 INFO mapred.MapTask: Finished spill 0
2022-05-18 10:01:11,314 INFO mapred.Task: Task:attempt_local1183919573_0001_m_000000_0 is done. And is in the process of committing
2022-05-18 10:01:11,317 INFO mapred.LocalJobRunner: map
2022-05-18 10:01:11,317 INFO mapred.Task: Task 'attempt_local1183919573_0001_m_000000_0' done.
2022-05-18 10:01:11,322 INFO mapred.Task: Final Counters for attempt_local1183919573_0001_m_000000_0: Counters: 23
        File System Counters
                FILE: Number of bytes read=186605
                FILE: Number of bytes written=842546
                FILE: Number of read operations=0
                FILE: Number of large read operations=0
                FILE: Number of write operations=0
                HDFS: Number of bytes read=529
                HDFS: Number of bytes written=0
                HDFS: Number of read operations=5
                HDFS: Number of large read operations=0
                HDFS: Number of write operations=1
                HDFS: Number of bytes read erasure-coded=0
        Map-Reduce Framework
                Map input records=5
                Map output records=5
                Map output bytes=45
                Map output materialized bytes=61
                Input split bytes=120
                Combine input records=0
                Spilled Records=5
                Failed Shuffles=0
                Merged Map outputs=0
                GC time elapsed (ms)=0
                Total committed heap usage (bytes)=370671616
        File Input Format Counters 
                Bytes Read=529
2022-05-18 10:01:11,322 INFO mapred.LocalJobRunner: Finishing task: attempt_local1183919573_0001_m_000000_0
2022-05-18 10:01:11,323 INFO mapred.LocalJobRunner: map task executor complete.
2022-05-18 10:01:11,325 INFO mapred.LocalJobRunner: Waiting for reduce tasks
2022-05-18 10:01:11,325 INFO mapred.LocalJobRunner: Starting task: attempt_local1183919573_0001_r_000000_0
2022-05-18 10:01:11,335 INFO output.FileOutputCommitter: File Output Committer Algorithm version is 2
2022-05-18 10:01:11,335 INFO output.FileOutputCommitter: FileOutputCommitter skip cleanup _temporary folders under output directory:false, ignore cleanup failures: false
2022-05-18 10:01:11,335 INFO mapred.Task:  Using ResourceCalculatorProcessTree : [ ]
2022-05-18 10:01:11,337 INFO mapred.ReduceTask: Using ShuffleConsumerPlugin: org.apache.hadoop.mapreduce.task.reduce.Shuffle@21782428
2022-05-18 10:01:11,338 WARN impl.MetricsSystemImpl: JobTracker metrics system already initialized!
2022-05-18 10:01:11,349 INFO reduce.MergeManagerImpl: MergerManager: memoryLimit=5220964864, maxSingleShuffleLimit=1305241216, mergeThreshold=3445837056, ioSortFactor=10, memToMemMergeOutputsThreshold=10
2022-05-18 10:01:11,352 INFO reduce.EventFetcher: attempt_local1183919573_0001_r_000000_0 Thread started: EventFetcher for fetching Map Completion Events
2022-05-18 10:01:11,371 INFO reduce.LocalFetcher: localfetcher#1 about to shuffle output of map attempt_local1183919573_0001_m_000000_0 decomp: 57 len: 61 to MEMORY
2022-05-18 10:01:11,373 INFO reduce.InMemoryMapOutput: Read 57 bytes from map-output for attempt_local1183919573_0001_m_000000_0
2022-05-18 10:01:11,374 INFO reduce.MergeManagerImpl: closeInMemoryFile -> map-output of size: 57, inMemoryMapOutputs.size() -> 1, commitMemory -> 0, usedMemory ->57
2022-05-18 10:01:11,374 INFO reduce.EventFetcher: EventFetcher is interrupted.. Returning
2022-05-18 10:01:11,375 INFO mapred.LocalJobRunner: 1 / 1 copied.
2022-05-18 10:01:11,375 INFO reduce.MergeManagerImpl: finalMerge called with 1 in-memory map-outputs and 0 on-disk map-outputs
2022-05-18 10:01:11,381 INFO mapred.Merger: Merging 1 sorted segments
2022-05-18 10:01:11,381 INFO mapred.Merger: Down to the last merge-pass, with 1 segments left of total size: 50 bytes
2022-05-18 10:01:11,381 INFO reduce.MergeManagerImpl: Merged 1 segments, 57 bytes to disk to satisfy reduce memory limit
2022-05-18 10:01:11,382 INFO reduce.MergeManagerImpl: Merging 1 files, 61 bytes from disk
2022-05-18 10:01:11,382 INFO reduce.MergeManagerImpl: Merging 0 segments, 0 bytes from memory into reduce
2022-05-18 10:01:11,382 INFO mapred.Merger: Merging 1 sorted segments
2022-05-18 10:01:11,383 INFO mapred.Merger: Down to the last merge-pass, with 1 segments left of total size: 50 bytes
2022-05-18 10:01:11,383 INFO mapred.LocalJobRunner: 1 / 1 copied.
2022-05-18 10:01:11,462 INFO Configuration.deprecation: mapred.skip.on is deprecated. Instead, use mapreduce.job.skiprecords
2022-05-18 10:01:11,948 INFO mapreduce.Job: Job job_local1183919573_0001 running in uber mode : false
2022-05-18 10:01:11,950 INFO mapreduce.Job:  map 100% reduce 0%
2022-05-18 10:01:12,145 INFO mapred.Task: Task:attempt_local1183919573_0001_r_000000_0 is done. And is in the process of committing
2022-05-18 10:01:12,150 INFO mapred.LocalJobRunner: 1 / 1 copied.
2022-05-18 10:01:12,151 INFO mapred.Task: Task attempt_local1183919573_0001_r_000000_0 is allowed to commit now
2022-05-18 10:01:12,195 INFO output.FileOutputCommitter: Saved output of task 'attempt_local1183919573_0001_r_000000_0' to hdfs://sccomz-cluster/user/hadoop/output
2022-05-18 10:01:12,196 INFO mapred.LocalJobRunner: reduce > reduce
2022-05-18 10:01:12,197 INFO mapred.Task: Task 'attempt_local1183919573_0001_r_000000_0' done.
2022-05-18 10:01:12,198 INFO mapred.Task: Final Counters for attempt_local1183919573_0001_r_000000_0: Counters: 30
        File System Counters
                FILE: Number of bytes read=186759
                FILE: Number of bytes written=842607
                FILE: Number of read operations=0
                FILE: Number of large read operations=0
                FILE: Number of write operations=0
                HDFS: Number of bytes read=529
                HDFS: Number of bytes written=17
                HDFS: Number of read operations=10
                HDFS: Number of large read operations=0
                HDFS: Number of write operations=3
                HDFS: Number of bytes read erasure-coded=0
        Map-Reduce Framework
                Combine input records=0
                Combine output records=0
                Reduce input groups=2
                Reduce shuffle bytes=61
                Reduce input records=5
                Reduce output records=2
                Spilled Records=5
                Shuffled Maps =1
                Failed Shuffles=0
                Merged Map outputs=1
                GC time elapsed (ms)=4
                Total committed heap usage (bytes)=370671616
        Shuffle Errors
                BAD_ID=0
                CONNECTION=0
                IO_ERROR=0
                WRONG_LENGTH=0
                WRONG_MAP=0
                WRONG_REDUCE=0
        File Output Format Counters 
                Bytes Written=17
2022-05-18 10:01:12,198 INFO mapred.LocalJobRunner: Finishing task: attempt_local1183919573_0001_r_000000_0
2022-05-18 10:01:12,198 INFO mapred.LocalJobRunner: reduce task executor complete.
2022-05-18 10:01:12,953 INFO mapreduce.Job:  map 100% reduce 100%
2022-05-18 10:01:12,954 INFO mapreduce.Job: Job job_local1183919573_0001 completed successfully
2022-05-18 10:01:12,962 INFO mapreduce.Job: Counters: 36
        File System Counters
                FILE: Number of bytes read=373364
                FILE: Number of bytes written=1685153
                FILE: Number of read operations=0
                FILE: Number of large read operations=0
                FILE: Number of write operations=0
                HDFS: Number of bytes read=1058
                HDFS: Number of bytes written=17
                HDFS: Number of read operations=15
                HDFS: Number of large read operations=0
                HDFS: Number of write operations=4
                HDFS: Number of bytes read erasure-coded=0
        Map-Reduce Framework
                Map input records=5
                Map output records=5
                Map output bytes=45
                Map output materialized bytes=61
                Input split bytes=120
                Combine input records=0
                Combine output records=0
                Reduce input groups=2
                Reduce shuffle bytes=61
                Reduce input records=5
                Reduce output records=2
                Spilled Records=10
                Shuffled Maps =1
                Failed Shuffles=0
                Merged Map outputs=1
                GC time elapsed (ms)=4
                Total committed heap usage (bytes)=741343232
        Shuffle Errors
                BAD_ID=0
                CONNECTION=0
                IO_ERROR=0
                WRONG_LENGTH=0
                WRONG_MAP=0
                WRONG_REDUCE=0
        File Input Format Counters 
                Bytes Read=529
        File Output Format Counters 
                Bytes Written=17
```

## 결과

* 확인
```                
$ hadoop fs -cat output/part-r-00000
```
```
1949    111
1950    22
```