# 개요

hadoop에 포함된 mapred 예저를 통하여 wordcount 실행이 제대로 되는지 확인

## 실행
```
$ hadoop fs -mkdir -p wc-in
```
> NOTE. hsdfs에 기본적으로 /user/$USER/ 하위에 wc-in 디렉토리가 생성됨.

```
$ echo "bla bla" > a.txt
$ echo "bla wa wa " > b.txt
$ hadoop fs -put a.txt wc-in/.
$ hadoop fs -put b.txt wc-in/.

$ cd $HADOOP_HOME/share/hadoop/mapreduce/
$ hadoop jar hadoop-mapreduce-examples-3.3.1.jar wordcount wc-in wc-out
```

```
2021-12-30 04:51:56,409 INFO client.DefaultNoHARMFailoverProxyProvider: Connecting to ResourceManager at localhost/127.0.0.1:8032
2021-12-30 04:51:57,121 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1640770462584_0026
2021-12-30 04:51:57,887 INFO input.FileInputFormat: Total input files to process : 2
2021-12-30 04:51:57,982 INFO mapreduce.JobSubmitter: number of splits:2
2021-12-30 04:51:58,167 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1640770462584_0026
2021-12-30 04:51:58,169 INFO mapreduce.JobSubmitter: Executing with tokens: []
2021-12-30 04:51:58,536 INFO conf.Configuration: resource-types.xml not found
2021-12-30 04:51:58,537 INFO resource.ResourceUtils: Unable to find 'resource-types.xml'.
2021-12-30 04:51:58,897 INFO impl.YarnClientImpl: Submitted application application_1640770462584_0026
2021-12-30 04:51:59,037 INFO mapreduce.Job: The url to track the job: http://hadoop:8088/proxy/application_1640770462584_0026/
2021-12-30 04:51:59,039 INFO mapreduce.Job: Running job: job_1640770462584_0026
2021-12-30 04:52:09,534 INFO mapreduce.Job: Job job_1640770462584_0026 running in uber mode : false
2021-12-30 04:52:09,538 INFO mapreduce.Job:  map 0% reduce 0%
2021-12-30 04:52:20,838 INFO mapreduce.Job:  map 100% reduce 0%
2021-12-30 04:52:28,915 INFO mapreduce.Job:  map 100% reduce 100%
2021-12-30 04:52:29,941 INFO mapreduce.Job: Job job_1640770462584_0026 completed successfully
2021-12-30 04:52:30,086 INFO mapreduce.Job: Counters: 54
        File System Counters
                FILE: Number of bytes read=35
                FILE: Number of bytes written=819405
                FILE: Number of read operations=0
                FILE: Number of large read operations=0
                FILE: Number of write operations=0
                HDFS: Number of bytes read=229
                HDFS: Number of bytes written=11
                HDFS: Number of read operations=11
                HDFS: Number of large read operations=0
                HDFS: Number of write operations=2
                HDFS: Number of bytes read erasure-coded=0
        Job Counters 
                Launched map tasks=2
                Launched reduce tasks=1
                Data-local map tasks=2
                Total time spent by all maps in occupied slots (ms)=17782
                Total time spent by all reduces in occupied slots (ms)=4823
                Total time spent by all map tasks (ms)=17782
                Total time spent by all reduce tasks (ms)=4823
                Total vcore-milliseconds taken by all map tasks=17782
                Total vcore-milliseconds taken by all reduce tasks=4823
                Total megabyte-milliseconds taken by all map tasks=18208768
                Total megabyte-milliseconds taken by all reduce tasks=4938752
        Map-Reduce Framework
                Map input records=2
                Map output records=5
                Map output bytes=38
                Map output materialized bytes=41
                Input split bytes=210
                Combine input records=5
                Combine output records=3
                Reduce input groups=2
                Reduce shuffle bytes=41
                Reduce input records=3
                Reduce output records=2
                Spilled Records=6
                Shuffled Maps =2
                Failed Shuffles=0
                Merged Map outputs=2
                GC time elapsed (ms)=224
                CPU time spent (ms)=2760
                Physical memory (bytes) snapshot=638648320
                Virtual memory (bytes) snapshot=7670960128
                Total committed heap usage (bytes)=453509120
                Peak Map Physical memory (bytes)=248598528
                Peak Map Virtual memory (bytes)=2555703296
                Peak Reduce Physical memory (bytes)=159027200
                Peak Reduce Virtual memory (bytes)=2559905792
        Shuffle Errors
                BAD_ID=0
                CONNECTION=0
                IO_ERROR=0
                WRONG_LENGTH=0
                WRONG_MAP=0
                WRONG_REDUCE=0
        File Input Format Counters 
                Bytes Read=19
        File Output Format Counters 
                Bytes Written=11
```


## 결과

* 확인
```                
$ hadoop fs -cat wc-out/*
```
```
bla     3
wa      2
```