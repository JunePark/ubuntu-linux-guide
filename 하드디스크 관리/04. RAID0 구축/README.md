# RAID0 구축

### RAID0 구성
* 구성
![RAID](./images/RAID0-0000.png)

### RAID0 구축

* 디스크 확인
```
# ls -l /dev/sd*
```

* 파티션 생성
```
# vi aaa.sh
```
```
#!/bin/sh
hdd="/dev/sdd /dev/sde"
for i in $hdd;do
echo "n
p
1


t
fd
w
"|fdisk $i;done
```
```
# sh aaa.sh
```
> NOTE. filesystem은 생성할 필요 없음.

* 확인
```
# fdisk -l /dev/sdd /dev/sde
```
```
Disk /dev/sdd: 1 GiB, 1073741824 bytes, 2097152 sectors
Disk model: VMware Virtual S
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xfbca62c2

Device     Boot Start     End Sectors  Size Id Type
/dev/sdd1        2048 2097151 2095104 1023M fd Linux raid autodetect


Disk /dev/sde: 1 GiB, 1073741824 bytes, 2097152 sectors
Disk model: VMware Virtual S
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x103a17bd

Device     Boot Start     End Sectors  Size Id Type
/dev/sde1        2048 2097151 2095104 1023M fd Linux raid autodetect
```

* RAID 구성
```
# mdadm --create /dev/md0 --level=0 --raid-devices=2 /dev/sdd1 /dev/sde1
```
> NOTE. md0 은 임의로 지은 이름.
```
dadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
```
> NOTE. filesystem을 만들 필요가 없는데 만들어진 경우에는 다른 메시지가 나옴.

```
# mdadm --detail --scan
```
```
ARRAY /dev/md/server:9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
ARRAY /dev/md0 metadata=1.2 name=server:0 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
```

* 파일시스템 생성
```
# mkfs.ext4 /dev/md0
```
```
mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 522752 4k blocks and 130816 inodes
Filesystem UUID: d7938f92-8605-4886-a528-d7b8302c57c1
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done 

```

* RAID 구성 확인
```
# mdadm --detail /dev/md0
```
```
/dev/md0:
           Version : 1.2
     Creation Time : Sun Dec 26 20:20:18 2021
        Raid Level : raid0
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Sun Dec 26 20:20:18 2021
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

            Layout : -unknown-
        Chunk Size : 512K

Consistency Policy : none

              Name : server:0  (local to host server)
              UUID : 8dc1ea35:6e4e0551:aa332f52:2b3584d5
            Events : 0

    Number   Major   Minor   RaidDevice State
       0       8       49        0      active sync   /dev/sdd1
       1       8       65        1      active sync   /dev/sde1
```

* 마운트
```
# mkdir /raid0
```

```
# mount /dev/md0 /raid0
# df -h
```


* 시스템 부팅시에 자동 마운트
```
# vi /etc/fstab

...
/dev/md0                                 /raid0          ext4    defaults        0       0
```

or
```
# lkid | grep /dev/md0
```
```
#  vi /etc/fstab

...
UUID=d7938f92-8605-4886-a528-d7b8302c57c1 /raid0          ext4    defaults        0       0
```
> NOTE. UUID를 사용하면 disk 삭제/추가 등으로 인해 장치명(/dev/xxx)이 변경되는 것과는 상관없이 사용할 수 있는 장점이 있음.

* 리부팅 후 확인
```
# mdadm --detail /raid0
```
