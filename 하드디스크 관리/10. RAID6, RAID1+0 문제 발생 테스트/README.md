# RAID6, RAID1+0 문제 발생 테스트

## RAID 문제 발생

* 구성
![RAID](./images/RAID6Fix-0000.png)

* HDD 제거

* 부팅
> NOTE. 부팅시간이 평상시 보다는 오래걸림.

## RAID 문제 해결

* 확인
```
# ls -l /dev/sd*
```
> NOTE. sdb ~ sde (4개) 까지만 있음.

* 마운트 확인
```
# df -h
```
NOTE. 마운트가 된 RAID가 없음.

* RAID 상태 체크
```
# mdadm --detail --scan
```
```
INACTIVE-ARRAY /dev/md2 metadata=1.2 name=server:2 UUID=03986a2f:e7cbbcbb:063b4e68:6fb5deda
INACTIVE-ARRAY /dev/md3 metadata=1.2 name=server:3 UUID=ba545063:ba8e8e1c:47124b30:644c7959
INACTIVE-ARRAY /dev/md6 metadata=1.2 name=server:6 UUID=16eae479:5fdf200f:7d74e3a6:023aaab0
```
> NOTE. 모두 INACTIVE 상태임.


## RAID6 복구
* 시작
```
# mdadm --run /dev/md6
```
```
mdadm: started array /dev/md6
```

* 확인
```
# mdadm --detail --scan
```
```
ARRAY /dev/md2 metadata=1.2 name=server:2 UUID=03986a2f:e7cbbcbb:063b4e68:6fb5deda
ARRAY /dev/md3 metadata=1.2 name=server:3 UUID=ba545063:ba8e8e1c:47124b30:644c7959
ARRAY /dev/md6 metadata=1.2 name=server:6 UUID=16eae479:5fdf200f:7d74e3a6:023aaab0
ARRAY /dev/md10 metadata=1.2 name=server:10 UUID=ba418892:a4fc52ec:dc8669bf:afd46230
```
> NOTE. /dev/md6이 이제 작동되고 있음을 확인. (이외의 것이 살아 있는 것은 /dev/md2, /dev/md3로 run을 미리 수행했음.)

* 마운트
```
# mount /dev/md6 /raid6
```

* 확인
```
# df -h
```
> NOTE. raidㅑ 방식으로 구성되어 있으므로 남아있는 Disk로 정상적으로 작동되고 있음을 확인.

```
# mdadm --detail /dev/md6
```
```
/dev/md6:
           Version : 1.2
     Creation Time : Mon Dec 27 17:02:36 2021
        Raid Level : raid6
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 4
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:43:51 2021
             State : clean, degraded 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : server:6  (local to host server)
              UUID : 16eae479:5fdf200f:7d74e3a6:023aaab0
            Events : 19

    Number   Major   Minor   RaidDevice State
       0       8       17        0      active sync   /dev/sdb1
       -       0        0        1      removed
       2       8       33        2      active sync   /dev/sdc1
       -       0        0        3      removed
```
> NOTE. 2/4가 작동되고 있음을 확인.


## RADI1+0 복구

* 시작
```
# mdadm --run /dev/md2
# mdadm --run /dev/md3
```
```
mdadm: started array /dev/md2
mdadm: started array /dev/md3
```

* 확인
```
# mdadm --detail --scan
```
```
ARRAY /dev/md2 metadata=1.2 name=server:2 UUID=03986a2f:e7cbbcbb:063b4e68:6fb5deda
ARRAY /dev/md3 metadata=1.2 name=server:3 UUID=ba545063:ba8e8e1c:47124b30:644c7959
ARRAY /dev/md6 metadata=1.2 name=server:6 UUID=16eae479:5fdf200f:7d74e3a6:023aaab0
ARRAY /dev/md10 metadata=1.2 name=server:10 UUID=ba418892:a4fc52ec:dc8669bf:afd46230
```
> NOTE. /dev/md2, /dev/md6, /dev/md10이 이제 작동되고 있음을 확인.(/dev/md10은 기동을 안했지만 살아남.)

* 마운트
```
# mount /dev/md10 /raid10
```

* 확인
```
# df -h
```
> NOTE. raid1+0 방식으로 구성되어 있으므로 남아있는 Disk로 정상적으로 작동되고 있음을 확인.

```
# mdadm --detail /dev/md10
```
```
/dev/md10:
           Version : 1.2
     Creation Time : Mon Dec 27 17:14:26 2021
        Raid Level : raid0
        Array Size : 2088960 (2040.00 MiB 2139.10 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:14:26 2021
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

            Layout : -unknown-
        Chunk Size : 512K

Consistency Policy : none

              Name : server:10  (local to host server)
              UUID : ba418892:a4fc52ec:dc8669bf:afd46230
            Events : 0

    Number   Major   Minor   RaidDevice State
       0       9        2        0      active sync   /dev/md2
       1       9        3        1      active sync   /dev/md3
```
> NOTE. 2/2가 작동되고 있음을 확인.

```
# mdadm --detail /dev/md2
```
```
/dev/md2:
           Version : 1.2
     Creation Time : Mon Dec 27 17:09:53 2021
        Raid Level : raid1
        Array Size : 1046528 (1022.00 MiB 1071.64 MB)
     Used Dev Size : 1046528 (1022.00 MiB 1071.64 MB)
      Raid Devices : 2
     Total Devices : 1
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:45:55 2021
             State : clean, degraded 
    Active Devices : 1
   Working Devices : 1
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : server:2  (local to host server)
              UUID : 03986a2f:e7cbbcbb:063b4e68:6fb5deda
            Events : 21

    Number   Major   Minor   RaidDevice State
       0       8       49        0      active sync   /dev/sdd1
       -       0        0        1      removed
```
> NOTE. 1/2가 작동되고 있음을 확인.

```
# mdadm --detail /dev/md3
```
```
/dev/md3:
           Version : 1.2
     Creation Time : Mon Dec 27 17:11:09 2021
        Raid Level : raid1
        Array Size : 1046528 (1022.00 MiB 1071.64 MB)
     Used Dev Size : 1046528 (1022.00 MiB 1071.64 MB)
      Raid Devices : 2
     Total Devices : 1
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:19:57 2021
             State : clean, degraded 
    Active Devices : 1
   Working Devices : 1
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : server:3  (local to host server)
              UUID : ba545063:ba8e8e1c:47124b30:644c7959
            Events : 17

    Number   Major   Minor   RaidDevice State
       -       0        0        0      removed
       1       8       65        1      active sync   /dev/sde1
```
> NOTE. 1/2가 작동되고 있음을 확인.


## 모두 stop

```
# umount /dev/md6 /dev/md10
```

```
# mdadm --stop /dev/md6
```

```
# mdadm --stop /dev/md10
# mdadm --stop /dev/md2
# mdadm --stop /dev/md3
```
NOTE. 순서 중요

* fstab 수정
```
#UUID=843b67c2-8d0b-4726-926f-eb5dad367be2 /raid6          ext4    defaults        0       0
#UUID=3785eee2-e746-4a4e-99b9-bd5646753271 /raid10         ext4    defaults        0       0
```
> NOTE. 주석 처리