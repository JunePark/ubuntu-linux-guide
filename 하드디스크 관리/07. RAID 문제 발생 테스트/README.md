# RAID 문제 발생 테스트

## RAID 문제 발생

* 구성
![RAID](./images/RAIDProblem-0000.png)

* HDD 제거
![RAID](./images/RAIDProblem-0001.png)

* 부팅
> NOTE. 부팅시간이 평상시 보다는 오래걸림.

## RAID 문제 해결

* 확인
```
# ls -l /dev/sd*
```
> NOTE. sdb ~ sdf (5개) 까지만 있음.

* 마운트 확인
```
# df -h
```
NOTE. 마운트가 된 RAID가 없음.

* RAID 상태 체크
```
# mdadm --detail --scan
```
```
INACTIVE-ARRAY /dev/md0 metadata=1.2 name=server:0 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
INACTIVE-ARRAY /dev/md5 metadata=1.2 name=server:5 UUID=66d3bd12:0af4ad44:0eef22a5:92a1e085
INACTIVE-ARRAY /dev/md1 metadata=1.2 name=server:1 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
INACTIVE-ARRAY /dev/md9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
```
> NOTE. 모두 INACTIVE 상태임.


## RAID1 복구
* 시작
```
# mdadm --run /dev/md1
```
```
mdadm: started array /dev/md1
```

* 확인
```
# mdadm --detail --scan
```
```
INACTIVE-ARRAY /dev/md0 metadata=1.2 name=server:0 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
INACTIVE-ARRAY /dev/md5 metadata=1.2 name=server:5 UUID=66d3bd12:0af4ad44:0eef22a5:92a1e085
ARRAY /dev/md1 metadata=1.2 name=server:1 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
INACTIVE-ARRAY /dev/md9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
```
> NOTE. /dev/md1이 이제 작동되고 있음을 확인.

* 마운트
```
# mount /dev/md1 /raid1
```

* 확인
```
# df -h
```
> NOTE. raid1 방식으로 구성되어 있으므로 남아있는 Disk로 정상적으로 작동되고 있음을 확인.

```
# mdadm --detail /dev/md1
```
```
/dev/md1:
           Version : 1.2
     Creation Time : Sun Dec 26 20:31:21 2021
        Raid Level : raid1
        Array Size : 1046528 (1022.00 MiB 1071.64 MB)
     Used Dev Size : 1046528 (1022.00 MiB 1071.64 MB)
      Raid Devices : 2
     Total Devices : 1
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 15:44:24 2021
             State : clean, degraded 
    Active Devices : 1
   Working Devices : 1
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : server:1  (local to host server)
              UUID : 5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
            Events : 19

    Number   Major   Minor   RaidDevice State
       -       0        0        0      removed
       1       8       49        1      active sync   /dev/sdd1
```
> NOTE. 1/2가 작동되고 있음을 확인.


## RADI5 복구

* 시작
```
# mdadm --run /dev/md5
```
```
mdadm: started array /dev/md5
```

* 확인
```
# mdadm --detail --scan
```
```
INACTIVE-ARRAY /dev/md0 metadata=1.2 name=server:0 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
ARRAY /dev/md5 metadata=1.2 name=server:5 UUID=66d3bd12:0af4ad44:0eef22a5:92a1e085
ARRAY /dev/md1 metadata=1.2 name=server:1 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
INACTIVE-ARRAY /dev/md9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
```
> NOTE. /dev/md5가 이제 작동되고 있음을 확인.

* 마운트
```
# mount /dev/md5 /raid5
```

* 확인
```
# df -h
```
> NOTE. raid5 방식으로 구성되어 있으므로 남아있는 Disk로 정상적으로 작동되고 있음을 확인.

```
# mdadm --detail /dev/md5
```
```
/dev/md5:
           Version : 1.2
     Creation Time : Mon Dec 27 14:48:15 2021
        Raid Level : raid5
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 3
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 15:49:27 2021
             State : clean, degraded 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : server:5  (local to host server)
              UUID : 66d3bd12:0af4ad44:0eef22a5:92a1e085
            Events : 22

    Number   Major   Minor   RaidDevice State
       0       8       65        0      active sync   /dev/sde1
       -       0        0        1      removed
       3       8       81        2      active sync   /dev/sdf1
```
> NOTE. 2/3가 작동되고 있음을 확인.

* 재부팅시에도 정상적으로 작동하도록 설정
```
# vi /etc/fstab
```
```
...
#UUID=3897fecb-5b50-4ea1-8f7e-8d860eb187f5 /raidLinear     ext4    defaults        0       0
#UUID=d7938f92-8605-4886-a528-d7b8302c57c1 /raid0          ext4    defaults        0       0
UUID=dea616e0-3f51-4af9-bc0d-fd054ac87bdd /raid1          ext4    defaults        0       0
UUID=45b62433-76a9-43b7-babc-b6daa2c2587d /raid5          ext4    defaults        0       0
```
> NOTE. RAID0과 Linear RAID는 복구가 불가능 함으로 제외.


```
# reboot
```

* 마운트 확인
```
# df -h
```
