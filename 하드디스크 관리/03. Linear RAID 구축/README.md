# Linear RAID 구축

## Linear RAID 구성

* 구성
![RAID](./images/LinearRAID-0000.png)
![RAID](./images/LinearRAID-0001.png)

## Linear RAID 구축

* 디스크 확인
```
# ls -l /dev/sd*
```

* 파티션 생성
```
# vi aaa.sh
```
```
#!/bin/sh
hdd="/dev/sdb /dev/sdc"
for i in $hdd;do
echo "n
p
1


t
fd
w
"|fdisk $i;done
```
```
# sh aaa.sh
```
> NOTE. filesystem은 생성할 필요 없음.

* 확인
```
# fdisk -l /dev/sdb /dev/sdc
```
```
Disk /dev/sdb: 2 GiB, 2147483648 bytes, 4194304 sectors
Disk model: VMware Virtual S
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xa1f3073e

Device     Boot Start     End Sectors Size Id Type
/dev/sdb1        2048 4194303 4192256   2G fd Linux raid autodetect


Disk /dev/sdc: 1 GiB, 1073741824 bytes, 2097152 sectors
Disk model: VMware Virtual S
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xa4ebf408

Device     Boot Start     End Sectors  Size Id Type
/dev/sdc1        2048 2097151 2095104 1023M fd Linux raid autodetect
```

* mdadm 설치
```
# apt install mdadm
```

* RAID 구성
```
# mdadm --create /dev/md9 --level=linear --raid-devices=2 /dev/sdb1 /dev/sdc1
```
> NOTE. md9 은 임의로 지은 이름.
```
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md9 started.
```
> NOTE. filesystem을 만들 필요가 없는데 만들어진 경우에는 다른 메시지가 나옴.

```
# mdadm --detail --scan
```
```
ARRAY /dev/md9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
```


* 파일시스템 생성
```
# mkfs.ext4 /dev/md9
```
```
mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 784896 4k blocks and 196224 inodes
Filesystem UUID: 3897fecb-5b50-4ea1-8f7e-8d860eb187f5
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done 

```

* RAID 구성 확인
```
# mdadm --detail /dev/md9
```
```
/dev/md9:
           Version : 1.2
     Creation Time : Sun Dec 26 20:04:00 2021
        Raid Level : linear
        Array Size : 3139584 (2.99 GiB 3.21 GB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Sun Dec 26 20:04:00 2021
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

          Rounding : 0K

Consistency Policy : none

              Name : server:9  (local to host server)
              UUID : f99e6c39:99284225:4de3ac86:94416c81
            Events : 0

    Number   Major   Minor   RaidDevice State
       0       8       17        0      active sync   /dev/sdb1
       1       8       33        1      active sync   /dev/sdc1
```

* 마운트
```
# mkdir /raidLinear
```

```
# mount /dev/md9 /raidLinear
# df -h
```


* 시스템 부팅시에 자동 마운트
```
# vi /etc/fstab

...
/dev/md9                                  /raidLinear     ext4    defaults        0       0
```

or
```
# blkid | grep /dev/md9
```
```
#  vi /etc/fstab

...
UUID=3897fecb-5b50-4ea1-8f7e-8d860eb187f5 /raidLinear     ext4    defaults        0       0
```
> NOTE. UUID를 사용하면 disk 삭제/추가 등으로 인해 장치명(/dev/xxx)이 변경되는 것과는 상관없이 사용할 수 있는 장점이 있음.


* 리부팅 후 확인
```
# mdadm --detail /raidLinear
```


