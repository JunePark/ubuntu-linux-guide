# 사용자별 공간 할당(Quota)

## 개념 및 구성

* 개념
![LVM](./images/QUOTA-0000.png)
![LVM](./images/QUOTA-0001.png)


* 구성
![LVM](./images/QUOTA-0002.png)
> NOTE. VMWare를 초기화한 후 Disk 1개(10GB) 추가
 
> NOTE. 사용자 홈디렉토리로 사용 예정

## 파티셔닝 및 파일시스템 생성
 
* 체크
```
# ls -al /dev/sd*
# fdisk -l /dev/sd*
```
> NOTE. sdb(1개).

* fdisk
```
# fdisk /dev/sdb
```
> NOTE. Linux Type으로 생성

* 파일시스템 생성
```
# mkfs.ext4 /dev/sdb1
```
```
mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 2621184 4k blocks and 655360 inodes
Filesystem UUID: cd8f82e0-042f-44f3-8831-1ad9cc3bc40a
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done 
```

* 디렉토리 생성
```
# mkdir /userHome
```

* 마운트
```
# mount /dev/sdb1 /userHome
```

* 재기동시에도 마운트 되도록 설정
```
# blkid |grep sdb1
```
```
/dev/sdb1: UUID="cd8f82e0-042f-44f3-8831-1ad9cc3bc40a" TYPE="ext4" PARTUUID="fc3ca18e-01"
```

```
# vi /etc/fstab
```
```
...
UUID=cd8f82e0-042f-44f3-8831-1ad9cc3bc40a /userHome       ext4    defaults        0       0
```


## 사용자 추가
```
# adduser --home /userHome/john john
```
```
'john' 사용자를 추가 중...
새 그룹 'john' (1001) 추가 ...
새 사용자 'john' (1001) 을(를) 그룹 'john' (으)로 추가 ...
'/userHome/john' 홈 디렉터리를 생성하는 중...
'/etc/skel'에서 파일들을 복사하는 중...
새  암호: 
새  암호 재입력: 
passwd: 암호를 성공적으로 업데이트했습니다
john의 사용자의 정보를 바꿉니다
새로운 값을 넣거나, 기본값을 원하시면 엔터를 치세요
	이름 []: 
	방 번호 []: 
	직장 전화번호 []: 
	집 전화번호 []: 
	기타 []: 
정보가 올바릅니까? [Y/n] 
```

```
# adduser --home /userHome/daniel daniel
```
```
'daniel' 사용자를 추가 중...
새 그룹 'daniel' (1002) 추가 ...
새 사용자 'daniel' (1002) 을(를) 그룹 'daniel' (으)로 추가 ...
'/userHome/daniel' 홈 디렉터리를 생성하는 중...
'/etc/skel'에서 파일들을 복사하는 중...
새  암호: 
새  암호 재입력: 
passwd: 암호를 성공적으로 업데이트했습니다
daniel의 사용자의 정보를 바꿉니다
새로운 값을 넣거나, 기본값을 원하시면 엔터를 치세요
	이름 []: 
	방 번호 []: 
	직장 전화번호 []: 
	집 전화번호 []: 
	기타 []: 
정보가 올바릅니까? [Y/n] 
```

## 디스크를 Quota 형으로 변경

* fstab 수정
```
# vi /etc/fstab
```
```
...
UUID=cd8f82e0-042f-44f3-8831-1ad9cc3bc40a /userHome       ext4    defaults,usrjquota=aquota.user,jqfmt=vfsv0        0       0
```

* 다시 마운트
```
# mount --options remount /userHome/
```
> NOTE. 재부팅하지 않고 위의 명령어를 이용하여 변경 내용을 적용할 수 있음.

* 확인
```
# mount | grep userHome
```
```
/dev/sdb1 on /userHome type ext4 (rw,relatime,jqfmt=vfsv0,usrjquota=aquota.user)
```


## Quata 설정

* 패키지 설정
```
# apt install quata
```

* 설정
```
# cd /userHome/
```

```
# quotaoff -avug
```
```
/dev/sdb1 [/userHome]: user quotas turned off
```

```
# quotacheck -augmn
# rm -f aquota.*
# quotacheck -augmn
# touch aquota.user aquota.group
# chmod 600 aquota.*
# quotacheck -augmn
```

```
# quotaon -avug
```
```
/dev/sdb1 [/userHome]: user quotas turned on
```


## 사용자별 Quata 설정

* john 사용자에게 설정
```
# edquota -u john
```

soft => 30720

hard => 40690

> NOTE. 약 30MB 제한(단위는 kb단위)
 
* 확인
```
# su - john
$ whoami
$ pwd
```

```
$ cp /boot/vmlinuz-* test1
$ cp /boot/vmlinuz-* test2
$ cp /boot/vmlinuz-* test3
```
> NOTE. 약 11mb

```
$ cp /boot/vmlinuz-* test4
``` 
```
cp: 'test4'에 쓰는 도중 오류 발생: 디스크 할당량이 초과됨
```

```
$ ls -l
```
```
합계 40672
-rw-r--r-- 1 john john 11657976 12월 27 19:19 test1
-rw-r--r-- 1 john john 11657976 12월 27 19:19 test2
-rw-r--r-- 1 john john 11657976 12월 27 19:19 test3
-rw-r--r-- 1 john john  6664192 12월 27 19:19 test4
```

* quota 확인
```
$ quota
```
```
Disk quotas for user john (uid 1001): 
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
      /dev/sdb1   40688*  30720   40690   6days       8       0       0     
```

## Quata 확인

* 전체 Quota 확인
```
# repquota /userHome/
```
```

*** Report for user quotas on device /dev/sdb1
Block grace time: 7days; Inode grace time: 7days
                        Block limits                File limits
User            used    soft    hard  grace    used  soft  hard  grace
----------------------------------------------------------------------
root      --      20       0       0              3     0     0       
john      +-   40688   30720   40690  6days       9     0     0       
daniel    --      16       0       0              4     0     0       


```

* Quota 복사
```
# edquota -p john daniel
```
> NOTE. john의 설정을 daniel 에 복사.

* 확인
```
# repquota /userHome/
```
```
*** Report for user quotas on device /dev/sdb1
Block grace time: 7days; Inode grace time: 7days
                        Block limits                File limits
User            used    soft    hard  grace    used  soft  hard  grace
----------------------------------------------------------------------
root      --      20       0       0              3     0     0       
john      +-   40688   30720   40690  6days       9     0     0       
daniel    --      16   30720   40690              4     0     0       


```

 