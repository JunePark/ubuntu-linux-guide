# LVM

## 개념 및 구성

* 개념
![LVM](./images/LVM-0000.png)
![LVM](./images/LVM-0001.png)

* 구성
![LVM](./images/LVM-0002.png)
![LVM](./images/LVM-0003.png)
![LVM](./images/LVM-0004.png)
> NOTE. VMWare를 초기화한 후 Disk 2개 추가 

## 파티셔닝
* 체크
```
# ls -al /dev/sd*
# fdisk -l /dev/sd*
```
> NOTE. sdb, sdc (2개).

* fdisk shell script
```
# vi aaa.sh
```
```
#!/bin/sh
hdd="/dev/sdb /dev/sdc"
for i in $hdd;do
echo "n
p
1


t
8e
w
"|fdisk $i;done
```
```
# sh aaa.sh
```

* 체크
```
# fdisk -l /dev/sd*
```


## 물리 볼륨 생성

* 패키지 설치
```
# apt install lvm2
```

```
# pvcreate /dev/sdb1
# pvcreate /dev/sdc1
```
```
  Physical volume "/dev/sdb1" successfully created.
  Physical volume "/dev/sdc1" successfully created.
```


## 볼륨 그룹 생성

* 생성
```
# vgcreate myVG /dev/sdb1 /dev/sdc1
```
```
  Volume group "myVG" successfully created
```

* 확인
```
# vgdisplay
```
```
  --- Volume group ---
  VG Name               myVG
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               4.99 GiB
  PE Size               4.00 MiB
  Total PE              1278
  Alloc PE / Size       0 / 0   
  Free  PE / Size       1278 / 4.99 GiB
  VG UUID               jDGhNT-4LTm-0iiA-G747-PJod-C1HG-uprBrK
   
```
> NOTE. 2G + 3G => 5GB로 구성된 것을 알 수 있음.

 
## 논리 그룹 생성

* 생성
```
# lvcreate --size 1G --name myLG1 myVG
# lvcreate --size 3G --name myLG2 myVG
# lvcreate --extents 100%FREE --name myLG3 myVG
```
> NOTE. 마지막 논리그룹 생성은 위와 같은 설정으로...
```
  Logical volume "myLG1" created.
  Logical volume "myLG2" created.
  Logical volume "myLG3" created.
```

* 체크
```
# ls -al /dev/myVG/
```
```
lrwxrwxrwx  1 root root    7 12월 27 18:29 myLG1 -> ../dm-0
lrwxrwxrwx  1 root root    7 12월 27 18:29 myLG2 -> ../dm-1
lrwxrwxrwx  1 root root    7 12월 27 18:29 myLG3 -> ../dm-2
```

* 파일 시스템 생성
```
# mkfs.ext4 /dev/myVG/myLG1
# mkfs.ext4 /dev/myVG/myLG2
# mkfs.ext4 /dev/myVG/myLG3
```

```
mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: e8299736-6345-48c3-b59b-eb095bb39b47
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done



mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 786432 4k blocks and 196608 inodes
Filesystem UUID: 4ce51d31-9282-40d3-88f6-eecc659192c7
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done 



mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 260096 4k blocks and 65024 inodes
Filesystem UUID: a2f3080a-5835-49a6-a498-4c251debc1b8
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (4096 blocks): done
Writing superblocks and filesystem accounting information: done
```

* 마운트
```
# mkdir /lvm1 /lvm2 /lvm3
```

```
# mount /dev/myVG/myLG1 /lvm1
# mount /dev/myVG/myLG2 /lvm2
# mount /dev/myVG/myLG3 /lvm3
```

* 확인
```
# df -h
```


## 재부팅시에도 되게끔 설정

* fstab 설정
```
# blkid |grep myLG
```
```
/dev/mapper/myVG-myLG1: UUID="e8299736-6345-48c3-b59b-eb095bb39b47" TYPE="ext4"
/dev/mapper/myVG-myLG2: UUID="4ce51d31-9282-40d3-88f6-eecc659192c7" TYPE="ext4"
/dev/mapper/myVG-myLG3: UUID="a2f3080a-5835-49a6-a498-4c251debc1b8" TYPE="ext4"
```

```
# vi /etc/fstab
```
```
...
UUID=e8299736-6345-48c3-b59b-eb095bb39b47 /lvm1           ext4    defaults        0       0
UUID=4ce51d31-9282-40d3-88f6-eecc659192c7 /lvm2           ext4    defaults        0       0
UUID=a2f3080a-5835-49a6-a498-4c251debc1b8 /lvm3           ext4    defaults        0       0
```

* 리부팅
```
3 reboot
```

* 확인
```
# df -h
```