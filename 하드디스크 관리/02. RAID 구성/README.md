# RAID

## RAID 비교

* RAID 방식 비교
![RAID](./images/RAID-0000.png)


* Linear RAID, RAID0
![RAID](./images/RAID-0001.png)


* RAID1
![RAID](./images/RAID-0002.png)


* RAID5
![RAID](./images/RAID-0003.png)
![RAID](./images/RAID-0004.png)


* RAID6, RAID1+0
![RAID](./images/RAID-0005.png)


## RAID 구현

* 실습 구성도
![RAID](./images/RAID-0006.png)

* 실습 목표
![RAID](./images/RAID-0007.png)