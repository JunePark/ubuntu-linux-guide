# RAID5 구축

## RAID5 구성

* 구성
![RAID](./images/RAID5-0000.png)

## RAID5 구축
* 디스크 확인
```
# ls -l /dev/sd*
```

* 파티션 생성
```
# fdisk /dev/sdh
# fdisk /dev/sdi
# fdisk /dev/sdj
```
> NOTE. filesystem은 생성할 필요 없음.

* 확인
```
# fdisk -l /dev/sdh /dev/sdi /dev/sdj
```
* 
* RAID 구성
```
# mdadm --create /dev/md5 --level=5 --raid-devices=3 /dev/sdh1 /dev/sdi1 /dev/sdj1
```
> NOTE. md5 은 임의로 지은 이름.
```
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md5 started.
```

```
# mdadm --detail --scan
```
```
ARRAY /dev/md/server:1 metadata=1.2 name=server:1 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
ARRAY /dev/md/server:0 metadata=1.2 name=server:0 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
ARRAY /dev/md/server:9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
ARRAY /dev/md5 metadata=1.2 name=server:5 UUID=66d3bd12:0af4ad44:0eef22a5:92a1e085
```

* 파일시스템 생성
```
# mkfs.ext4 /dev/md5
```
```
mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 522752 4k blocks and 130816 inodes
Filesystem UUID: 45b62433-76a9-43b7-babc-b6daa2c2587d
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done 

```

* RAID 구성 확인
```
# mdadm --detail /dev/md5
```
```
/dev/md5:
           Version : 1.2
     Creation Time : Mon Dec 27 14:48:15 2021
        Raid Level : raid5
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 3
     Total Devices : 3
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 14:48:53 2021
             State : clean 
    Active Devices : 3
   Working Devices : 3
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : server:5  (local to host server)
              UUID : 66d3bd12:0af4ad44:0eef22a5:92a1e085
            Events : 18

    Number   Major   Minor   RaidDevice State
       0       8      113        0      active sync   /dev/sdh1
       1       8      129        1      active sync   /dev/sdi1
       3       8      145        2      active sync   /dev/sdj1
```

* 마운트
```
# mkdir /raid5
```

```
# mount /dev/md5 /raid5
# df -h
```


* 시스템 부팅시에 자동 마운트
```
# vi /etc/fstab

...
/dev/md5                                 /raid5          ext4    defaults        0       0
```

or
```
# blkid | grep /dev/md5
```
```
#  vi /etc/fstab

...
UUID=45b62433-76a9-43b7-babc-b6daa2c2587d /raid5          ext4    defaults        0       0
```
> NOTE. UUID를 사용하면 disk 삭제/추가 등으로 인해 장치명(/dev/xxx)이 변경되는 것과는 상관없이 사용할 수 있는 장점이 있음.

```
# mdadm --detail -scan
```
```
ARRAY /dev/md/server:1 metadata=1.2 name=server:1 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
ARRAY /dev/md/server:0 metadata=1.2 name=server:0 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
ARRAY /dev/md/server:9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
ARRAY /dev/md5 metadata=1.2 name=server:5 UUID=66d3bd12:0af4ad44:0eef22a5:92a1e085
```

* mdadm.conf 설정
```
# vi /etc/mdadm/mdadm.conf
```

```
...
ARRAY /dev/md1 metadata=1.2 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
ARRAY /dev/md0 metadata=1.2 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
ARRAY /dev/md9 metadata=1.2 UUID=f99e6c39:99284225:4de3ac86:94416c81
ARRAY /dev/md5 metadata=1.2 UUID=66d3bd12:0af4ad44:0eef22a5:92a1e085
```

```
# update-initramfs -u
```
```
update-initramfs: Generating /boot/initrd.img-5.4.0-26-generic
I: The initramfs will attempt to resume from /dev/sda1
I: (UUID=48562437-9ad1-433e-8217-2926d6202a7b)
I: Set the RESUME variable to override this.
```

* 리부팅 후 확인
```
# mdadm --detail /raid5
```

```
# ls -al /dev/md*
```

```
# df -h
```