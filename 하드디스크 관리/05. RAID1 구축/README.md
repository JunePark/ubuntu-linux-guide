# RAID1 구축

## RAID1 구성

* 구성
![RAID](./images/RAID1-0000.png)

## RAID1 구축
* 디스크 확인
```
# ls -l /dev/sd*
```

* 파티션 생성
```
# fdisk /dev/sdf
# fdisk /dev/sdg
```
> NOTE. filesystem은 생성할 필요 없음.

* 확인
```
# fdisk -l /dev/sdf /dev/sdg
```
* 
* RAID 구성
```
# mdadm --create /dev/md1 --level=1 --raid-devices=2 /dev/sdf1 /dev/sdg1
```
> NOTE. md1 은 임의로 지은 이름.
```
# mdadm --create /dev/md1 --level=1 --raid-devices=2 /dev/sdf1 /dev/sdg1
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
Continue creating array? 
Continue creating array? (y/n) y
```

```
# mdadm --detail --scan
```
```
ARRAY /dev/md/server:9 metadata=1.2 name=server:9 UUID=f99e6c39:99284225:4de3ac86:94416c81
ARRAY /dev/md/server:0 metadata=1.2 name=server:0 UUID=8dc1ea35:6e4e0551:aa332f52:2b3584d5
ARRAY /dev/md1 metadata=1.2 name=server:1 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
```

* 파일시스템 생성
```
# mkfs.ext4 /dev/md1
```

* RAID 구성 확인
```
# mdadm --detail /dev/md1
```

* 마운트
```
# mkdir /raid1
```

```
# mount /dev/md1 /raid1
# df -h
```


* 시스템 부팅시에 자동 마운트
```
# vi /etc/fstab

...
/dev/md1                                 /raid1          ext4    defaults        0       0
```

or
```
# blkid | grep /dev/md1
```
```
#  vi /etc/fstab

...
UUID=dea616e0-3f51-4af9-bc0d-fd054ac87bdd /raid1          ext4    defaults        0       0
```
> NOTE. UUID를 사용하면 disk 삭제/추가 등으로 인해 장치명(/dev/xxx)이 변경되는 것과는 상관없이 사용할 수 있는 장점이 있음.


* 리부팅 후 확인
```
# mdadm --detail /raid1
```