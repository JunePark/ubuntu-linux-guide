# RAID6, RAID1+0 구축

## RAID 구성

* 구성
![RAID](./images/RAIDComplex-0000.png)

* HDD 추가
![RAID](./images/RAIDComplex-0001.png)
> NOTE. 초기화한 다음에 수행.

* 부팅

## 파티셔닝
* 체크
```
# ls -al /dev/sd*
# fdisk -l /dev/sd*
```

* fdisk shell script
```
# vi aaa.sh
```
```
#!/bin/sh
hdd="/dev/sdb /dev/sdc /dev/sdd /dev/sde /dev/sdf /dev/sdg /dev/sdh /dev/sdi"
for i in $hdd;do
echo "n
p
1


t
fd
w
"|fdisk $i;done
```

```
# sh aaa.sh
```

* 결과 확인
```
# fdisk -l /dev/sd*
# ls -al /dev/sd*
```
> NOTE. /dev/sdb1 ~ /dev/sdi1 까지 생성되었는지 확인.

* mdadm 설치
```
# apt install mdadm
```


## RAID6 구축

* 생성
```
# mdadm --create /dev/md6 --level=6 --raid-devices=4 /dev/sdb1 /dev/sdc1 /dev/sdd1 /dev/sde1
```
```
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md6 started.
```

* 체크
```
# mdadm --detail /dev/md6
```
```
/dev/md6:
           Version : 1.2
     Creation Time : Mon Dec 27 17:02:36 2021
        Raid Level : raid6
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 4
     Total Devices : 4
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:02:42 2021
             State : clean 
    Active Devices : 4
   Working Devices : 4
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : server:6  (local to host server)
              UUID : 16eae479:5fdf200f:7d74e3a6:023aaab0
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       8       17        0      active sync   /dev/sdb1
       1       8       33        1      active sync   /dev/sdc1
       2       8       49        2      active sync   /dev/sdd1
       3       8       65        3      active sync   /dev/sde1
```
> NOTE. 4/4로 작동중임.
 
* filesystem 생성
```
# mkfs.ext4 /dev/md6
```
```
ke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 522752 4k blocks and 130816 inodes
Filesystem UUID: 843b67c2-8d0b-4726-926f-eb5dad367be2
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done 

```

* 마운트
```
# mkdir /raid6
# mount /dev/md6 /raid6/
```

* 체크
```
# df -h
```



## RAID1+0 구축

* 순서

먼저 RAID1 을 먼저 생성한 다음 이것을 다시 RAID0으로 구성하는 순서로 진행.

* RAID1 생성 (/dev/md2)
```
# mdadm --create /dev/md2 --level=1 --raid-devices=2 /dev/sdf1 /dev/sdg1
```
> NOTE md2는 임의로 작성한 명칭임.
```
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md2 started.
```
> NOTE. Continue creating array? 에서 y 엔터

* RAID1 생성 (/dev/md3)
```
# mdadm --create /dev/md3 --level=1 --raid-devices=2 /dev/sdh1 /dev/sdi1
```
> NOTE md3는 임의로 작성한 명칭임.
```
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md3 started.
```

* RAID0 생성 (/dev/md10)
```
# mdadm --create /dev/md10 --level=0 --raid-devices=2 /dev/md2 /dev/md3
```
> NOTE md10는 임의로 작성한 명칭임.
```
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md10 started.
```

* 체크
```
# mdadm --detail /dev/md2
```
```
/dev/md2:
           Version : 1.2
     Creation Time : Mon Dec 27 17:09:53 2021
        Raid Level : raid1
        Array Size : 1046528 (1022.00 MiB 1071.64 MB)
     Used Dev Size : 1046528 (1022.00 MiB 1071.64 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:14:26 2021
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : server:2  (local to host server)
              UUID : 03986a2f:e7cbbcbb:063b4e68:6fb5deda
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       8       81        0      active sync   /dev/sdf1
       1       8       97        1      active sync   /dev/sdg1
```

```
# mdadm --detail /dev/md3
```
```
/dev/md3:
           Version : 1.2
     Creation Time : Mon Dec 27 17:11:09 2021
        Raid Level : raid1
        Array Size : 1046528 (1022.00 MiB 1071.64 MB)
     Used Dev Size : 1046528 (1022.00 MiB 1071.64 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:14:26 2021
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : server:3  (local to host server)
              UUID : ba545063:ba8e8e1c:47124b30:644c7959
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       8      113        0      active sync   /dev/sdh1
       1       8      129        1      active sync   /dev/sdi1
```

```
# mdadm --detail /dev/md10
```
```
/dev/md10:
           Version : 1.2
     Creation Time : Mon Dec 27 17:14:26 2021
        Raid Level : raid0
        Array Size : 2088960 (2040.00 MiB 2139.10 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 17:14:26 2021
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

            Layout : -unknown-
        Chunk Size : 512K

Consistency Policy : none

              Name : server:10  (local to host server)
              UUID : ba418892:a4fc52ec:dc8669bf:afd46230
            Events : 0

    Number   Major   Minor   RaidDevice State
       0       9        2        0      active sync   /dev/md2
       1       9        3        1      active sync   /dev/md3
```

* filesystem 생성
```
# mkfs.ext4 /dev/md10
```
```
mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 522240 4k blocks and 130560 inodes
Filesystem UUID: 3785eee2-e746-4a4e-99b9-bd5646753271
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done 

```

* 마운트
```
# mkdir /raid10
# mount /dev/md10 /raid10/
```

* 체크
```
# df -h
```


## 재부팅시에도 정상적으로 작동하도록 설정

* fstab 설정
```
# blkid |grep md
```
```
/dev/md3: UUID="ba418892-a4fc-52ec-dc86-69bfafd46230" UUID_SUB="fa2b4877-236a-4148-b871-6655baad9d9b" LABEL="server:10" TYPE="linux_raid_member"
/dev/md6: UUID="843b67c2-8d0b-4726-926f-eb5dad367be2" TYPE="ext4"
/dev/md2: UUID="ba418892-a4fc-52ec-dc86-69bfafd46230" UUID_SUB="9b34ca7d-793d-a380-70d5-b937b6d5ff0e" LABEL="server:10" TYPE="linux_raid_member"
/dev/md10: UUID="3785eee2-e746-4a4e-99b9-bd5646753271" TYPE="ext4"
```

```
# vi /etc/fstab
```
```
...
UUID=843b67c2-8d0b-4726-926f-eb5dad367be2 /raid6          ext4    defaults        0       0
UUID=3785eee2-e746-4a4e-99b9-bd5646753271 /raid10         ext4    defaults        0       0
```

* mdadm.conf 설정

```
# mdadm --detail --scan
```
```
ARRAY /dev/md6 metadata=1.2 name=server:6 UUID=16eae479:5fdf200f:7d74e3a6:023aaab0
ARRAY /dev/md2 metadata=1.2 name=server:2 UUID=03986a2f:e7cbbcbb:063b4e68:6fb5deda
ARRAY /dev/md3 metadata=1.2 name=server:3 UUID=ba545063:ba8e8e1c:47124b30:644c7959
ARRAY /dev/md10 metadata=1.2 name=server:10 UUID=ba418892:a4fc52ec:dc8669bf:afd46230
```
> NOTE. 간혹 이름이 바뀌는 경우가 있음.(그럴때는 바뀐 이름으로 /etc/fstab에 설정)

```
# vi /etc/mdadm/mdadm.conf
```
> NOTE. mdadm --detail --scan 결과에서 name부분만 삭제하고 붙여넣기

```
# update-initramfs -u
```
```
update-initramfs: Generating /boot/initrd.img-5.4.0-26-generic
I: The initramfs will attempt to resume from /dev/sda1
I: (UUID=48562437-9ad1-433e-8217-2926d6202a7b)
I: Set the RESUME variable to override this.
```

* 재부팅
```
# reboot
```

* 마운트 확인
```
# df -h
```
