
# HDD 추가

## 구성 개요

* VMWare에서 지원하는 HDD 구성
![hdd](./images/hdd_1.png)

* 하드디스크 1개 추가하기
![hdd](./images/hdd_2.png)

* 구성 흐름도
![hdd](./images/hdd_3.png)

* 추가한 결과
![hdd](./images/hdd_4.png)


## 구성 흐름

1. 파티션 생성
```
# fdisk /dev/sdb <<EOF
n
p
1


w
EOF
```

2. 파티션 확인
```
# fdisk /dev/sdb
```
```
Disk /dev/sdb: 1 GiB, 1073741824 bytes, 2097152 sectors
Disk model: VMware Virtual S
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x6503ec39

Device     Boot Start     End Sectors  Size Id Type
/dev/sdb1        2048 2097151 2095104 1023M 83 Linux
```

3. 파일시스템 생성
```
# mkfs.ext4 /dev/sdb1
```
```
mke2fs 1.45.5 (07-Jan-2020)
Creating filesystem with 261888 4k blocks and 65536 inodes
Filesystem UUID: 0d56ecd3-2b50-4051-8e69-8f4a39c3f835
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (4096 blocks): done
Writing superblocks and filesystem accounting information: done
```

4. 마운트 전
```
# mkdir /mydata
# cp /boot/vmlinuz-5.4.0-26-generic /mydata/test1
```

5. 마운트
```
# mount /dev/sdb1 /mydata/
# ls -l /mydata/
# cp /boot/vmlinuz-5.4.0-26-generic /mydata/test2
# ls -l /mydata/
```
> NOTE. test1 파일은 보이지 않고 test2 파일만 보임.

6. 마운트 해제
```
# cd
# umount /dev/sdb1
# ls -l /mydata/
```
> NOTE. test2 파일은 보이지 않고 test1 파일만 보임.


7. 시스템 부팅시에 자동 마운트
```
# vi /etc/fstab

...
/dev/sdb1           /mydata      ext4    defaults    0       0
```

or

```
# blkid | grep sdb1
```
```
/dev/sdb1: UUID="0d56ecd3-2b50-4051-8e69-8f4a39c3f835" TYPE="ext4" PARTUUID="6503ec39-01"
```

```
#  vi /etc/fstab

...
UUID=0d56ecd3-2b50-4051-8e69-8f4a39c3f835 /mydata         ext4    defaults        0       0
```
> NOTE. UUID를 사용하면 disk 삭제/추가 등으로 인해 장치명(/dev/xxx)이 변경되는 것과는 상관없이 사용할 수 있는 장점이 있음.
