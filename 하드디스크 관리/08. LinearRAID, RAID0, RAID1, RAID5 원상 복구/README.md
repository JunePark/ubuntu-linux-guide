# LinearRAID, RAID0, RAID1, RAID5 원상 복구

## RAID 복구

* 구성
![RAID](./images/RAIDFix-0000.png)

* 추가
* ![RAID](./images/RAIDFix-0001.png)
> NOTE. Disk 추가시 비어있는 SCSI 순서대로 채워짐.

* 부팅


## RAID 복구 절차

* 확인
```
# mdadm --detail /dev/md9
# mdadm --detail /dev/md0
# mdadm --detail /dev/md1
# mdadm --detail /dev/md5
```
> NOTE. 자동으로 복구는 되지는 않음.

* 디스크 파티셔닝
```
# fdisk /dev/sdc
# fdisk /dev/sde
# fdisk /dev/sdg
# fdisk /dev/sdi
```

```
# ls -l /dev/sd*
```

### Linear RAID 복구

* cation
 
아래의 작업시에는 데이터가 모두 삭제됨.(결함허용을 하지 않는 Linear RAID 한계)

* 중지
```
# mdadm --stop /dev/md9
```
```
mdadm: stopped /dev/md9
```

* 생성
```
# mdadm --create /dev/md9 --level=linear --raid-devices=2 /dev/sdb1 /dev/sdc1
```
```
/dev/sdb1 /dev/sdc1
mdadm: /dev/sdb1 appears to be part of a raid array:
       level=linear devices=2 ctime=Sun Dec 26 20:04:00 2021
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md9 started.
```
> NOTE. Continue creating array? 에서 y 엔터

### RAID0 복구

* cation
 
아래의 작업시에는 데이터가 모두 삭제됨.(결함허용을 하지 않는 RAID0 한계)

* 중지
```
# mdadm --stop /dev/md0
```
```
mdadm: stopped /dev/md0
```

* 생성
```
# mdadm --create /dev/md0 --level=0 --raid-devices=2 /dev/sdd1 /dev/sde1
```
```
mdadm: /dev/sdd1 appears to be part of a raid array:
       level=raid0 devices=2 ctime=Sun Dec 26 20:20:18 2021
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
```
> NOTE. Continue creating array? 에서 y 엔터

### RAID1 복구

* caution

결함을 허용함으로 작동중인 상태에서 디스크만 추가하면 됨.

* 체크
```
# mdadm --detail /dev/md1
```

* 추가
```
# mdadm /dev/md1 --add /dev/sdg1
```
> NOTE. 디스크가 사용 용량이 크다면 시간이 오래 걸릴수 있음.
```
mdadm: added /dev/sdg1
```

* 체크
```
# mdadm --detail /dev/md1
```
```
/dev/md1:
           Version : 1.2
     Creation Time : Sun Dec 26 20:31:21 2021
        Raid Level : raid1
        Array Size : 1046528 (1022.00 MiB 1071.64 MB)
     Used Dev Size : 1046528 (1022.00 MiB 1071.64 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 16:24:13 2021
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : server:1  (local to host server)
              UUID : 5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
            Events : 46

    Number   Major   Minor   RaidDevice State
       2       8       97        0      active sync   /dev/sdg1
       1       8       81        1      active sync   /dev/sdf1
```
> NOTE. 2/2로 정상적인 상태로 돌아옴.


### RAID5 복구

* caution

결함을 허용함으로 작동중인 상태에서 디스크만 추가하면 됨.

* 체크
```
# mdadm --detail /dev/md5
```

* 추가
```
# mdadm /dev/md5 --add /dev/sdi1
```
> NOTE. 디스크가 사용 용량이 크다면 시간이 오래 걸릴수 있음.
```
mdadm: added /dev/sdi1
```

* 체크
```
# mdadm --detail /dev/md5
```
```
/dev/md5:
           Version : 1.2
     Creation Time : Mon Dec 27 14:48:15 2021
        Raid Level : raid5
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 3
     Total Devices : 3
       Persistence : Superblock is persistent

       Update Time : Mon Dec 27 16:27:13 2021
             State : clean 
    Active Devices : 3
   Working Devices : 3
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : server:5  (local to host server)
              UUID : 66d3bd12:0af4ad44:0eef22a5:92a1e085
            Events : 49

    Number   Major   Minor   RaidDevice State
       0       8      113        0      active sync   /dev/sdh1
       4       8      129        1      active sync   /dev/sdi1
       3       8      145        2      active sync   /dev/sdj1
```
> NOTE. 3/3으로 정상적인 상태로 돌아옴.



### 재부팅시에도 정상적으로 작동하도록 설정

* fstab 설정
```
# vi /etc/fstab
```
```
...
UUID=3897fecb-5b50-4ea1-8f7e-8d860eb187f5 /raidLinear     ext4    defaults        0       0
UUID=d7938f92-8605-4886-a528-d7b8302c57c1 /raid0          ext4    defaults        0       0
UUID=dea616e0-3f51-4af9-bc0d-fd054ac87bdd /raid1          ext4    defaults        0       0
UUID=45b62433-76a9-43b7-babc-b6daa2c2587d /raid5          ext4    defaults        0       0
```

* mdadm.conf 설정

```
# mdadm --detail --scan
```
```
ARRAY /dev/md1 metadata=1.2 name=server:1 UUID=5211a0fb:a3636ac2:c42ed3a8:c8acbfcd
ARRAY /dev/md5 metadata=1.2 name=server:5 UUID=66d3bd12:0af4ad44:0eef22a5:92a1e085
ARRAY /dev/md9 metadata=1.2 name=server:9 UUID=cb1f7d0e:6f658bfd:526822ea:b05adc77
ARRAY /dev/md0 metadata=1.2 name=server:0 UUID=247e0319:bd42ef88:38856517:3d9f18cc
```
> NOTE. 간혹 이름이 바뀌는 경우가 있음.(그럴때는 바뀐 이름으로 /etc/fstab에 설정)

```
# vi /etc/mdadm/mdadm.conf
```
> NOTE. /dev/md9 /dev/md0은 재구성한 관계로 이전에 설정한 mdadm.conf에서 UUID가 변경되어짐.

```
# update-initramfs -u
```
```
update-initramfs: Generating /boot/initrd.img-5.4.0-26-generic
I: The initramfs will attempt to resume from /dev/sda1
I: (UUID=48562437-9ad1-433e-8217-2926d6202a7b)
I: Set the RESUME variable to override this.
```

* 재부팅
```
# reboot
```

* 마운트 확인
```
# df -h
```
